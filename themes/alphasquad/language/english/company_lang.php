<?php
/******************************************
US English
Company Section Language
******************************************/

$lang['company_title'] = 'Company Information';
$lang['company_glancetitle'] = 'Seino Indomobil Logistics at a glance';
$lang['company_glancetxt'] = '<strong>PT Seino Indomobil Logistics</strong>, is a joint venture between "<strong>Seino Holdings</strong>" the biggest commercial logistics in Japan and "<strong>Indorent</strong>" a member of Indomobil Group.';
$lang['company_glancecaption'] = 'We can provide<br> "the highest standards in logistics services" <br> that are suitable for your needs.';
$lang['company_integrationtitle'] = 'Integration of two professionals';
$lang['company_integrationindorent'] = '
<li>Indorent is a subsidiary of Indomobil Group which is the second largest automotive groups in Indonesia.</li>
<li>Indorent has been active for 28 years in car rental business to provide solutions corporate transportation needs.</li>
<li>Variety of vehicle portfolio mix to cater customer needs.</li>
<li>Experience in handling reputable corporate customers with a long-term contracts.</li>
';
$lang['company_integrationseino'] = '
<li>SEINO holdings co., ltd (established in 1930) is one of the largest logistics provider in Japan, 4th position in sales and no.1 in handling weights.</li>
<li>SEINO is being a pioneer in Japan logistics industry, the first introducer of “consolidated transport service”, “computer system”, “safety vehicle”, and “smart phone app”.</li>
<li>SEINO aims to “develop the company and keep employees happy”. Deep mutual understanding between management and drivers provide stable & trustworthy transport quality.</li>
<li>To serve the society, SEINO began its transport earlier than any others to the affected areas after the Japan earthquake in 2011.</li>
';
$lang['company_historytitle'] = 'Company history – SEINO TRANSPORTATION JAPAN';
$lang['company_historytop'] = '
<p>Seino Transportation was founded in 1930. After the world war II, 38 trucks and 78 employees restarted the company in 1946.</p>
<p>The situation in Japan at the time was completely different from now. There was no good road (unpaved), no new truck (industry is weak), and no fuel (there was no gasoline, running with firewood).</p>
<p>Under such circumstances, Rihachi Taguchi, the founder, considered that we could contribute to the development of Japan if we start long-distance transportation using trucks. Therefore he started a new business.</p>
';
$lang['company_historymid'] = 'Fast & Trust';
$lang['company_historybot'] = '
<p>What we offered to the world at that time are:</p>
<ul class="visimisipoint">
	<li>Speed (faster than rail cargo),</li>
	<li>Trust (transport information, clear accounting, courtesy - evolution to service industry).</li>
</ul>
<p>Those are now in common but were not the case for 70 years ago. It was our company that produced them first in Japan. It was a sailing under the harsh conditions. The company expanded while employees and officials worked hard together, and the company was listed in 1971.</p>
<p>We also actively engaged in technological innovation, introducing computers early in the Japanese truck industry, and in1983, all the stores were connected online and our system electronically manages all cargos.</p>
<p>Our company started from a region, and now supports the economy of Japan on the scale of 25000 trucks and 50000 people in all our groups.</p>
';
$lang['company_missiontitle'] = 'Mission';
$lang['company_missioncontent'] = '
<p>To contribute to society through logistics</p>
';
$lang['company_kangorootitle'] = 'Sun star kangaroo spirit';
$lang['company_kangoroocontent'] = '
<p>Trademark of Seino reflects the spirit of service in which seino transportation has acted since its establishment. It includes the sense of transporting the customer’s freight day or night, with strong legs and warm devotion.</p>
<ul class="visimisipoint">
	<li>Fostering affectionate warmth with a belly pouch for carrying its baby</li>
	<li>The kangaroo actively hopping:  Image of deep trust and affection</li>
	<li>Approximately 16 strips which run along its sides:  Impression of dynamic speed</li>
	<li>The “ sunstar ” design in the “ O “ of seino:  Denoting day and night : seino’s 24-hour availability</li>
</ul>
';
$lang['company_principletitle'] = 'Seino Principle in Customers';
$lang['company_principle1'] = '
<h4 class="txt-grey3">The Best Service</h4>
<p>Provide quick and accurate services as foundation of carrier.</p>
';
$lang['company_principle2'] = '
<h4 class="txt-grey3">Civility</h4>
<p>Keep the civility that is always loved and trusted by our customers.</p>
';
$lang['company_principle3'] = '
<h4 class="txt-grey3">Contribution to the nation and society</h4>
<p>Contribute to the nation by playing a role of industry aorta, and to the society for safety and environment issue as a corporate citizen.</p>
';
$lang['company_strengthtitle'] = 'Our Strength';
$lang['company_strengthcontent'] = '
<ul class="visimisipoint">
	<li>
		<p><strong>Always Update</strong>. Customers can monitor their cargo position in real time.</p>
	</li>
	<li>
		<p><strong>Secure Space</strong>. Our 24 hours operations and dedicated truck will secure your space when others can not.</p>
	</li>
	<li>
		<p><strong>No “One-Fits-All” approach</strong>. We tailor our services to your specific needs.</p>
	</li>
	<li>
		<p><strong>Keep your costs low with competitive rates</strong>.</p>
	</li>
	<li>
		<p><strong>Ship “Fast”, “Reliably”, and “Clean”</strong>. Your cargo will be shipped with our average 4 years old trucks.</p>
	</li>
	<li>
		<p><strong>Well-trained Drivers</strong>.</p>
	</li>
</ul>
';