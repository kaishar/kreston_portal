<?php
/******************************************
US English
Contact Us Language
******************************************/

//menu
$lang['customers_title'] = 'Our Customers';
$lang['finddealer_title'] = 'Find your nearest Seino branch';
$lang['contact_title'] = 'Contact Us';
$lang['contact_head'] = 'Head Office';
$lang['contact_customer'] = 'Customer Service';
$lang['company_txt56'] = 'Send Us Message';
$lang['company_txt57'] = "If you have any question or concerning about Seino Indomobil logistics, please fill out the form below, then click '<strong>Submit</strong>'.";
$lang['company_txt58'] = 'Full Name';
$lang['company_txt59'] = 'Email address';
$lang['company_txt60'] = 'Phone';
$lang['company_txt61'] = 'Your questions will be distributed to our proprietary Customer Service will reply to your email within 2 working days time.';
$lang['company_txt62'] = 'We hope to find a solution to any of your questions. Thus help us provide data completeness and correctness of the information so that we can respond immediately.';