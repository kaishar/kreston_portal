<?php
/******************************************
US English
Menu Language
******************************************/

//menu
$lang['menu_service'] = 'Our Service';
$lang['menu_company_information']= 'Company Information';
$lang['menu_careers'] = 'Careers';
$lang['menu_news']= 'News';
$lang['menu_member']= 'Membership';
$lang['menu_contact'] = 'Contact Us';