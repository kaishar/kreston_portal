<?php
/******************************************
US English
Service Section Language
******************************************/

$lang['service_miantitle'] = 'Our Service';
$lang['service_longdistancetitle'] = 'Long-Distance Trucking';
$lang['service_longdistancecontent'] = '
<p>Our network covers the entire island of Java, one of the largest islands in Indonesia. Java Island is big from east to west, 450 km from Jakarta to Semarang, 800 km to Surabaya.</p>
<p>30 or more units of large size trucks run everyday to the towns and cities from West to East, day and night. We provide effective services not only in the city but also outside the city.</p>
<p>In 2017, we will start our operation to Sumatra, then will expand our network to all Indonesia.</p>
';
$lang['service_milkruntitle'] = 'Milk Run Delivery';
$lang['service_milkruncontent'] = "
<p>The milk run is required to be at dairy farmers at fixed time because they squeeze milk at a fixed time and hand out those at a fixed time.</p>
<p>It will deliver every day to the time required by the customer from the customer’s site.</p>
";
$lang['service_trailertransportationtitle'] = 'Trailer Transportation';
$lang['service_trailertransportationcontent'] = '
<p>Ship and truck, rail and truck, it is convenient to use container when combining multiple transportation.</p>
<p>In our company we are preparing a trailer to fit your needs.</p>
';
$lang['service_carcarriertitle'] = 'Car Carrier';
$lang['service_carcarriercontent'] = '<p>We will carry your car carefully.</p>';
$lang['service_internationaltransportationtitle'] = 'International Transportation';
$lang['service_internationaltransportationcontent'] = '
<p>Seino Transportation has partnered with Schenker (SCHENKER: Head Office Germany, one of the typical company that provides worldwide international comprehensive logistics services).</p>
<p>We will arrange for international transport in the one-stop.</p>
';
$lang['service_truedoortitle'] = 'True Door-to-Door Service';
$lang['service_truedoorcontent'] = '
<p>The courier service will start from 2017.</p>
<p>We will provide at an affordable price to high-quality services of world standard.</p>
<p>Please look forward to our new service.</p>
';