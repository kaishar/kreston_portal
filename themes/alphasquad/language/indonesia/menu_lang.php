<?php
/******************************************
ID Indonesia
Menu Language
******************************************/

//menu
$lang['menu_service'] = 'Layanan Kami';
$lang['menu_company_information']= 'Profile Perusahaan';
$lang['menu_careers'] = 'Karir';
$lang['menu_news']= 'Berita';
$lang['menu_member']= 'Keanggotaan';
$lang['menu_contact'] = 'Hubungi Kami';
