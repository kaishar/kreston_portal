<?php
/******************************************
ID Indonesia
Service Section Language
******************************************/

$lang['service_miantitle'] = 'Layanan Kami';
$lang['service_longdistancetitle'] = 'Long-Distance Trucking';
$lang['service_longdistancecontent'] = '
<p>Jaringan kami meliputi seluruh Pulau Jawa, salah satu pulau terbesar di Indonesia. Pulau Jawa adalah salah satu  pulau terbesar di Indonesia yang terbentang di selatan nusantara. Jarak dari Jakarta ke Semarang 450km, sedangkan jarak dari Jakarta ke Semarang 800km.</p>
<p>30 truk unit besar atau lebih berjalan setiap hari menuju ke kota-kota di Indonesia dari barat ke timur. Selain beroprasi di siang dan malam hari, kami menyediakan pelayanan yang efektif tidak hanya di dalam kota tapi juga di luar kota.</p>
<p>Tahun 2017 kami akan memulai oprasi ke Sumatera lalu akan mengembangkan jaringan ke seluruh Indonesia.</p>
';
$lang['service_milkruntitle'] = 'Pengiriman Milk Run';
$lang['service_milkruncontent'] = '
<p>Milk Run lahir dari sejarah, peternak sapi perah memerah susu sesuai waktu yang tetap, dan pada waktu itu juga akan didistribusikan.</p>
<p>Itu akan didistribusikan setiap hari sesuai waktu yang dibutuhkan customer dari lokasi customer.</p>
';
$lang['service_trailertransportationtitle'] = 'Trailer Transportation';
$lang['service_trailertransportationcontent'] = '
<p>Ketika menggunakan beberapa transportasi seperti Kapal dan truk atau kereta api dan truk untuk pengiriman, menggunakan container adalah cara yang paling tepat.</p>
<p>Perusahaan kami mempersiapkan sebuah Trailer yang dapat disesuaikan dengan kebutuhan anda.</p>
';
$lang['service_carcarriertitle'] = 'Car Carrier';
$lang['service_carcarriercontent'] = '<p>Kami akan membawa mobil anda dengan penuh  kehati-hatian.</p>';
$lang['service_internationaltransportationtitle'] = 'Transportasi Internasional';
$lang['service_internationaltransportationcontent'] = '
<p>Seino Transportasi telah bermitra dengan Schenker (SCHENKER: Perusahaan Jerman. Salah satu Perusahaan yang khusus menyediakan jasa Logistik yang komperensif di seluruh dunia).</p>
<p>Kami akan bekerjasama dalam dunia Transportasi Internasional dengan one-stop service.</p>
';
$lang['service_truedoortitle'] = 'Layanan Door-to-Door';
$lang['service_truedoorcontent'] = '
<p>Jasa Kurir door-to-door akan dimulai pada tahun 2017</p>
<p>Kami akan memberikan harga yang terjangkau untuk layanan kualitas tinggi sesuai dengan standard dunia.</p>
<p>Nantikan jasa kami!</p>
';