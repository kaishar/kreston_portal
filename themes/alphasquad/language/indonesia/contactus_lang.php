<?php
/******************************************
IN Indonesia
Contact Us Language
******************************************/

//menu
$lang['customers_title'] = 'Pelanggan Kami';
$lang['finddealer_title'] = 'Temukan cabang Seino terdekat anda';
$lang['contact_title'] = 'Hubungi Kami';
$lang['contact_head'] = 'Kantor Pusat';
$lang['contact_customer'] = 'Layanan Pelanggan';
$lang['company_txt56'] = 'Kirim Pesan';
$lang['company_txt57'] = "Apabila Anda memiliki pertanyaan mengenai Seino Indomobil Logistic, silahkan mengisi formulir di bawah ini, kemudian klik '<strong>Submit</strong>'.";
$lang['company_txt58'] = 'Nama Lengkap';
$lang['company_txt59'] = 'Alamat Email';
$lang['company_txt60'] = 'Nomor Telefon';
$lang['company_txt61'] = 'Pertanyaan anda akan disalurkan kepada Layanan Pelanggan milik kami yang akan membalas email Anda dalam kurun waktu 2 kali hari kerja.';
$lang['company_txt62'] = 'Kami berharap dapat menemukan solusi atas setiap pertanyaan Anda. Dengan demikian bantu kami untuk menyediakan kelengkapan data dan kebenaran informasi agar kami dapat merespon dengan segera.';