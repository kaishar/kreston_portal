<?php
/******************************************
IN Indonesia
Company Section Language
******************************************/

$lang['company_title'] = 'Tentang Kami';
$lang['company_glancetitle'] = 'Sekilas tentang Seino Indomobil Logistics';
$lang['company_glancetxt'] = '<strong>PT Seino Indomobil Logistics</strong>, adalah Perusahaan gabungan antara "<strong>Seino Holdings</strong>" perusahaan Logistik Komersial terbesar di Jepang dengan "<strong>Indorent</strong>" salah satu member dari Indomobil Group.';
$lang['company_glancecaption'] = 'Kami dapat menyediakan<br> "Jasa Logistik dengan Standar Tertinggi"<br> sesuai kebutuhan Anda.';
$lang['company_integrationtitle'] = 'Integrasi dua Profesional';
$lang['company_integrationindorent'] = '
<li>Indorent merupakan anak perusahaan dari Indomobil Group yang merupakan kelompok otomotif terbesar kedua di Indonesia.</li>
<li>Indorent telah aktif selama 28 tahun. Dalam bisnis sewa mobil tujuannya untuk memberikan solusi kebutuhan transportasi perusahaan.</li>
<li>Untuk kebutuhan Pelanggan Berbagai jenis Investasi kendaraan.</li>
<li>Pengalaman dalam menangani  Perusahaan customer terkemuka adalah dengan kontrak jangka panjang.</li>
';
$lang['company_integrationseino'] = '
<li>SEINO Holdings co., ltd (didirikan pada 1930) adalah salah satu penyedia logistik terbesar di Jepang, posisi ke-4 dalam penjualan dan no.1 di dalam penanganan pengiriman barang berat.</li>
<li>SEINO adalah pelopor dalam industri logistik Jepang. SEINO menjadi yang pertama dengan "konsolidasi layanan transportasi", "sistem komputer", "keamanan kendaraan", dan “smart phone app".</li>
<li>SEINO bertujuan untuk "mengembangkan perusahaan dan menjaga karyawan tetap bahagia". Dengan saling memahami kebutuhan dan keadaan manajemen serta driver dilapangan, kami dapat memberikan kualitas transportasi stabil & dapat dipercaya.</li>
<li>Untuk melayani masyarakat di Jepang, SEINO memulai transportasi lebih awal dari yang lainnya ke daerah-daerah setelah gempa Jepang pada tahun 2011.</li>
';
$lang['company_historytitle'] = 'Riwayat Perusahan – SEINO TRANSPORTATION JAPAN';
$lang['company_historytop'] = '
<p>Perusahaan berdiri pada tahun 1930. Karena gejolak perang dunia dan pada tahun 1946 dengan jumlah truck 38 unit dan  manpower 78 orang perusahaan re-start kembali.</p>
<p>Kondisi Jepang saat itu dengan kondisi saat ini jelas sekali berbeda, Infrastruktur jalan tidak bagus ( Belum aspal ), Sulit mendapatkan truck baru ( Industri lemah ), Tidak ada bahan bakar minyak ( Tidak ada bensin, Tenaga penggerak menggunakan kayu bakar).</p>
<p>Didalam kondisi yang seperti ini Bapak pendiri perusahaan RIHACHI TAGUCHI Mr.menurut beliau jika kita bisa memulai delivery dengan jarak jauh maka kita dapat berkontribusi pada pengembangan jepang, oleh karena itu dibentuklah bisnis baru.</p>
';
$lang['company_historymid'] = 'Cepat & Terpercaya';
$lang['company_historybot'] = '
<p>Saat ini yang kami persembahkan untuk dunia adalah :</p>
<ul class="visimisipoint">
	<li>Speed (Dari transportasi Kereta api lebih cepat dan handal),</li>
	<li>Kepercayaan (Informasi delivery, harga jelas dan pasti, sopan santun - Evolisi ke bidang service),</li>
</ul>
<p>Segala sesuatu di saat sekarang ini adalah merupakan hal yang lumrah. Tetapi hal yang lumrah tersebut pada era 70 an tidak ada. Di Jepang pertama kali yang menciptakan hal tersebut adalah Perusahaan kami. Didalam kondisi yang begitu berat perusahaan tidak berjalan dengan lancar, akan tetapi seluruh karyawan bersatu padu dan ingin membesarkan Perusahaan, dan pada tahun 1971 perusahaan berhasil meningkat.</p>
<p>Selalu berusaha Terhadap  inovasi Teknologi, dalam dunia bisnis truck di jepang  pelaksanaan pengenalan komputer satu langkah lebih awal.Pada tahun 1983 semua tempat collection telah terhubung dengan online dan semua paket/ parcel sudah dapat dikontrol dengan sistem kontrol elektronik.</p>
<p>Perusahaan kami yang dimulai dari satu wilayah, dan saat ini dengan seluruh Group dengan skala 25,000Truck, 50,000 Manpower mendukung perekonomian Jepang.</p>
';
$lang['company_missiontitle'] = 'Misi';
$lang['company_missioncontent'] = '
<p>Berkontribusi kepada masyarakat melalui logistik</p>
';
$lang['company_kangorootitle'] = 'Semangat Kanguru Sun Star';
$lang['company_kangoroocontent'] = '
<p>Lambang dari Seino mencerminkan semangat prinsip pelayanan yang sudah dilaksanakan sejak Seino didirikan. Lambang ini juga menggambarkan oprasi pada siang dan malam, dengan kaki yang kuat dan penuh kehati-hatian.</p>
<ul class="visimisipoint">
	<li>Kangguru yang aktif melompat membina kehangatan kasih sayang dengan kantong perut untuk membawa bayinya</li>
	<li>Simbol ini melambangkan kepercayaan yang mendalam dan kasih sayang</li>
	<li>16 strip melambangkan kecepatan dan kedinamis-an</li>
	<li>Simbol “ sunstar”  yang ada didalam “ O “ SEINO menunjukkan oprasi yang terus bergerak siang dan malam selama 24 jam</li>
</ul>
';
$lang['company_principletitle'] = 'Prinsip Seino terhadap Pelanggan';
$lang['company_principle1'] = '
<h4 class="txt-grey3">Layanan Terbaik</h4>
<p>Menyediakan layanan cepat dan akurat sebagai dasar dari carrier.</p>
';
$lang['company_principle2'] = '
<h4 class="txt-grey3">Kesopanan</h4>
<p>Menjaga kesopanan yang selalu dicintai dan dipercaya oleh pelanggan kami.</p>
';
$lang['company_principle3'] = '
<h4 class="txt-grey3">Kontribusi terhadap bangsa dan masyarakat</h4>
<p>Berkontribusi kepada bangsa dengan memainkan peran aorta industri, dan untuk keselamatan Masyarakat dan masalah  lingkungan adalah korporasi warga.</p>
';
$lang['company_strengthtitle'] = 'Keunggulan Kami';
$lang['company_strengthcontent'] = '
<ul class="visimisipoint">
	<li>
		<p><strong>Selalu Terbaru</strong>. Pelanggan bisa memonitor posisi barang pelanggan dengan real time.</p>
	</li>
	<li>
		<p><strong>Space Aman</strong>. Selain beroprasi 24 jam, truk kami akan melindungi space anda.</p>
	</li>
	<li>
		<p><strong>Bukan layanan “One-Fits-All"</strong>. Kami menyesuaiakan layanan kami dengan kebutuhan spesifik Anda.</p>
	</li>
	<li>
		<p><strong>Menjaga cost rendah Anda dengan tarif yang kompetitif</strong>.</p>
	</li>
	<li>
		<p><strong>Kapal “Fast”, “Aktual”, and “Bersih”</strong>. Barang Anda akan dikirim dengan truk kami yang rata-rata berumur 4 tahun.</p>
	</li>
	<li>
		<p><strong>Drivers Terlatih</strong>.</p>
	</li>
</ul>
';