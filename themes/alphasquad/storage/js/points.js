

var P1 = [

    {

        lat: 45.468945,

        lon: 45.73684365,

        title: 'Title',

        html: 'Content',

        zoom: 10,

        animation: google.maps.Animation.DROP

    }

];



var krestonlocations = [

	{

        lat: -6.2295696,

        lon: 106.7594785,

        title: 'Jakarta',

		zoom: 15,

		html: [

			'<h5 class="text-uppercase txt-white bg-blue1">Jakarta</h5>',

			'<address>Address: <strong>somewhere on this city</strong></address>',

        ].join(''),

		icon: 'storage/img/logoicon/map-branch.png',

		animation: google.maps.Animation.DROP,

    },

	{

        lat: -6.903363,

        lon: 107.6081382,

        title: 'Bandung',

		zoom: 15,

		html: [

			'<h5 class="text-uppercase txt-white bg-blue1">Bandung</h5>',

			'<address>Address: <strong>somewhere on this city</strong></address>',

        ].join(''),

		icon: 'storage/img/logoicon/map-branch.png',

		animation: google.maps.Animation.DROP,

    },

	{

        lat: -7.024689,

        lon: 110.3838289,

        title: 'Semarang',

		zoom: 15,

		html: [

			'<h5 class="text-uppercase txt-white bg-blue1">Semarang</h5>',

			'<address>Address: <strong>somewhere on this city</strong></address>',

        ].join(''),

		icon: 'storage/img/logoicon/map-branch.png',

		animation: google.maps.Animation.DROP,

    },

	{

        lat: -7.2754665,

        lon: 112.6416449,

        title: 'Surabaya',

		zoom: 15,

		html: [

			'<h5 class="text-uppercase txt-white bg-blue1">Surabaya</h5>',

			'<address>Address: <strong>somewhere on this city</strong></address>',

        ].join(''),

		icon: 'storage/img/logoicon/map-branch.png',

		animation: google.maps.Animation.DROP,

    },

	{

        lat: -5.122674,

        lon: 119.4262507,

        title: 'Makassar',

		zoom: 15,

		html: [

			'<h5 class="text-uppercase txt-white bg-blue1">Makassar</h5>',

			'<address>Address: <strong>somewhere on this city</strong></address>',

        ].join(''),

		icon: 'storage/img/logoicon/map-branch.png',

		animation: google.maps.Animation.DROP,

    },

	{

        lat: 1.3150701,

        lon: 103.7069367,

        title: 'Singapore',

		zoom: 15,

		html: [

			'<h5 class="text-uppercase txt-white bg-blue1">Singapore</h5>',

			'<address>Address: <strong>somewhere on this city</strong></address>',

        ].join(''),

		icon: 'storage/img/logoicon/map-branch.png',

		animation: google.maps.Animation.DROP,

    },

	{

        lat: 4.0891032,

        lon: 100.5538699,

        title: 'Malaysia',

		zoom: 15,

		html: [

			'<h5 class="text-uppercase txt-white bg-blue1">Malaysia</h5>',

			'<address>Address: <strong>somewhere on this city</strong></address>',

        ].join(''),

		icon: 'storage/img/logoicon/map-branch.png',

		animation: google.maps.Animation.DROP,

    },

];