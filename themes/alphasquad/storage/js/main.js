jQuery(function(t){"use strict";function o(o){var e=t(this);o=t.extend({},o||{},e.data("countToOptions")||{}),e.countTo(o)}t("li.dropdown").find(".fa-angle-down").each(function(){t(this).on("click",function(){return t(window).width()<768&&t(this).parent().next().slideToggle(),!1})}),(new WOW).init(),t(".timer").each(o),t("img").bind("contextmenu",function(t){return!1}),t("img").bind("mouseover",function(){var o=t(this);o.title||o.attr("title",o.attr("alt"))}),t("body").scrollspy({target:"#navalphaoo"});var e=t("#toTop");t("#toTop").hide(),t(window).scroll(function(){t(this).scrollTop()>100?e.fadeIn("slow"):e.is(":visible")&&e.fadeOut("slow")}),t("#toTop").click(function(){return t("html,body").animate({scrollTop:0},1200),!1}),t(".gowes").click(function(){return t("html, body").animate({scrollTop:t(t(this).attr("href")).offset().top},600),!1}),t("#header").affix({offset:{top:70}})}),$("document").ready(function(){$("#focussectorlist").indolist({itemsBox:".list",itemPath:".list-item",panelPath:".indolist-panel"})}),$(function(){$("#slidealpha").responsiveSlides({auto:!1,pager:!0,nav:!0,timeout:5500,speed:900,namespace:"callbacks"})}),$(".searchclk").click(function(){$(".searchhere").toggleClass("expanded")});

$('.dropdown').hover(function(){ 
  $('.dropdown-toggle', this).trigger('click'); 
});
/*
$('a#ask').click(function(){
    $('#modal-contact').modal({onShow: function (dialog) {
        // handle the close yourself.
        // Don't use the closeClass on this element
        // call $.modal.close(); when you are done
        // call a function to open a new modal
    });
    return false;
});

$('a#ask').click(function(){
    $.modal.close();
    $('#modal-contact').modal();
});

*/