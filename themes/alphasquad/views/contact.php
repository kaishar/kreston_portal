	<!-- image header -->
    <section class="sectionslide framesection">
    	<div class="imageheader head_about">
        	<div class="container">
            	<h1 class="txt-yellow1">Contact Us</h1>
                <span class="bordertext15 border-yellow wow  fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
        </div>
    </section>
    <!-- end image header -->
    <!-- Visi Misi -->
    <section class="framesection">
    	<div class="container">
            <div class="openingsubpage bg-yellow1">
            	<h4 class="txt-white wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Visi Misi</h4>
            </div>
        	<div class="row">
            	<div class="col-smx-6">
                	<div class="visimisibase bg-grey1">
                		<ul class="listpoint">
                        	<li>Mempersatukan, membina dan memberikan pelayanan kepada anggota untuk memajukan/mengembangkan usaha jasa pembiayaan sesuai dengan wewenang yang dimiliki dan ketentuan-ketentuan yang berlaku di Indonesia.</li>
                            <li>Meningkatkan kerjasama, pertukaran informasi dan menumbuhkan sikap kebersamaan diantara para anggota Asosiasi maupun dengan pemerintah atau pihak ketiga lainnya, sehingga tercipta hubungan yang baik dan harmonis.</li>
                            <li>Memajukan/meningkatkan peranan Lembaga Pembiayaan sebagai salah satu alternatif pembiayaan di Indonesia serta memberikan sumbangsih bagi kemajuan perekonomian nasional.</li>
                            <li>Memberikan pendapat maupun saran kepada Pemerintah dalam rangka menciptakan iklim usaha yang sehat dan kompetitif bagi industri usaha jasa pembiayaan di Indonesia dan memperjuangkan kepentingan bersama para anggotanya.</li>
                            <li>Mewakili perusahaan-perusahaan pembiayaan di Indonesia dalam kepentingan pembahasan perkembangan industri pembiayaan baik di dalam maupun di luar negeri.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-smx-6">
                	<div class="visimisibase bg-grey1">
                		<ul class="listpoint">
                        	<li>Menjadikan ASOSIASI PERUSAHAAN PEMBIAYAAN INDONESIA sebagai wadah utama untuk bertukar pikiran dan informasi, serta mengumpulkan, mengadakan penelitian dan mengolah bahan-bahan keterangan yang berhubungan dengan masalah-masalah mengenai Lembaga Pembiayaan dalam arti seluas-luasnya.</li>
                            <li>Menampung dan membahas masalah-masalah yang dihadapi para anggota dalam bidang pembiayaan dan bilamana perlu menyampaikan pendapatnya kepada Instansi Pemerintah baik di tingkat pusat maupun daerah dan/atau lembaga-lembaga lain yang berwenang.</li>
                            <li>Memberikan penerangan, saran, pendidikan, latihan, dan bimbingan serta pelayanan kepada para anggota, guna meningkatkan kemampuan dan ketrampilan sumber daya manusia para anggota untuk memenuhi tenaga profesional yang dibutuhkan.</li>
                            <li>Membentuk komite-komite yang dianggap perlu baik ditingkat pusat maupun daerah dalam rangka melancarkan kegiatan/aktivitas ASOSIASI PERUSAHAAN PEMBIAYAAN INDONESIA.</li>
                            <li>Menggalang kerjasama dan hubungan baik dengan Instansi/Badan/Lembaga Pemerintah dan Swasta, baik di dalam maupun di luar negeri sepanjang tidak bertentangan dengan azas dan tujuan ASOSIASI PERUSAHAAN PEMBIAYAAN INDONESIA, serta dengan cara yang tidak bertentangan dengan perundang-undangan yang berlaku.</li>
                            <li>Melakukan usaha-usaha lainnya sepanjang tidak bertentangan dengan azas dan tujuan ASOSIASI PERUSAHAAN PEMBIAYAAN INDONESIA.</li>
                        </ul>
                    </div>
                </div>
            </div>
        	<hr class="divider_boder-white">
        </div>
    </section>
    <!-- end Visi Misi -->
    <!-- latar belakang -->
    <section class="sectionalpha bg-grey1">
    	<div class="container">
          <div class="">
            <h4 class="txt-blue3 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Latar Belakang</h4>
            <span class="bordertext15 border-blue wow  fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
          </div>
          <ul class="timeline">
            <li>
              <div class="timeline-badge bg-blue1">
              	<i class="fa fa-calendar-check-o fa-lg"></i>
              </div>
              <div class="timeline-panel bg-white">
                <div class="timeline-heading">
                  <h3 class="txt-blue1">1975</h3>
                  <h5>Pendirian</h5>
                </div>
                <div class="timeline-body">
                  <p>PT Pembangunan Armada Niaga Nasional. Kelak perusahaan tersebut mengganti namanya menjadi PT. PANN Multifinance.</p>
                  <p>Kemudian melalui keppres No.61/1988, yang ditindaklanjuti dengan SK Mentri Keuangan No. 1251/KMK.013/1988, Pemerintah membuka lebih luas lagi bagi bisnis pembiayaan, dengan cakupan kegiatan meliputi leasing, factoring, consumer finance, modal ventura, dan kartu kredit.</p>
                </div>
              </div>
            </li>
            <li class="timeline-inverted">
              <div class="timeline-badge bg-blue1"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
              <div class="timeline-panel bg-white">
                <div class="timeline-heading">
                  <h3 class="txt-blue1">1982</h3>
                  <h5>Asosiasi Leasing Indonesia (ALI)</h5>
                </div>
                <div class="timeline-body">
                  <p>Sebuah organisasi profesi Asosiasi Leasing Indonesia (ALI) berdiri pada tanggal 2 Juli 1982.</p>
                  <p>ALI didirikan sebagai wadah komunikasi bagi perusahaan-perusahaan pembiayaan. Disini mereka secara bersama-sama membicarakan dan memecahkan berbagai masalah yang dihadapi. ALI juga hadir untuk memperjuangkan kepentingan anggotanya kepada pemerintah. Disisi lain, organisasi ini juga bermaksud menjadi jembatan untuk meneruskan keinginan dan bimbingan pemerintah kepada anggota.</p>
                  <p>Tujuan utama organisasi ini diantaranya memajukan dan mengembagkan  peranan lembaga pembiayaan di Indonesia serta memberikan sumbangsih bagi kemajuan perekonomian nasional.</p>
                </div>
              </div>
            </li>
            <li>
              <div class="timeline-badge bg-blue1"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
              <div class="timeline-panel bg-white">
                <div class="timeline-heading">
                  <h3 class="txt-blue1">2000</h3>
                  <h5>Dari ALI ke APPI</h5>
                </div>
                <div class="timeline-body">
                  <p>Seiring dengan pertumbuhan sector usaha jasa pembiayaan dan guna menampung aspirasi seluruh anggota maka pada tanggal 20 Juli 2000 telah diambil keputusan untuk mengubah ALI menjadi ASOSIASI PERUSAHAAN PEMBIAYAAN INDONESIA (APPI).</p>
                  <p>Dalam Perkembangannya pada tanggal 21 Desember 2000 Asosiasi Factoring Indonesia (AFI) juga telah bergabung kedalam APPI.</p>
                  <p>Sesuai dengan tujuan didirikannya, APPI bersama pemerintah terus berupaya memberikan andil dan peran lebih berarti dalam peningkatan perekonomian nasional khususnya pada sector usaha jasa pembiayaan.</p>
                </div>
              </div>
            </li>
            <!--
            <li class="timeline-inverted">
              <div class="timeline-badge bg-blue1"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
              <div class="timeline-panel bg-white">
                <div class="timeline-heading">
                  <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                </div>
                <div class="timeline-body">
                  <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo.
                    Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                </div>
              </div>
            </li>
            -->
          </ul>
        </div>
    </section>
    <!-- end latar belakang -->