	<!-- backtotop -->
    	<div class="backtotop" id="toTop"><p><b class="bttcaret"></b></p><a href="javascript:goToTop()">back to top</a></div>
    <!-- end backtotop -->
	<!-- footer -->
    <footer id="footer" class="bg-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="copyright-text text-center txt-white">
                        <p>&copy; 2016-<?php echo date("Y"); ?> | Kreston HHES. <br>A member of <a href="http://kreston.com/" target="_blank" class="txt-white">Kreston International</a> | A global network of independent accounting firms.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- end footer -->
	<?php require_once('detail_service.php'); ?>    
	<!-- modal Viewer -->
	<div class="modal fade bs-example-modal-lg" id="detail_events" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modalpage">
			<div class="modal-content"></div>
		</div>
	</div>
	<div class="modal fade bs-example-modal-lg" id="detail_people" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
	</div>
    <div class="modal fade bs-example-modal-lg" id="detail_careers" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
	</div>
	<div class="modal fade bs-example-modal-lg" id="detail_affiliation" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
	</div>
	<!-- End modalviewer -->
	<script src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.22&key=AIzaSyBICgJXm8vb-oBJhkRjERYQarM4oNKxF8o"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <!-- <script type="text/javascript" src="<?=theme_js('jquery.min.js')?>"></script> -->
    <script type="text/javascript" src="<?=theme_js('bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?=theme_js('wow.min.js')?>"></script>
	<script type="text/javascript" src="<?=theme_js('indolist.min.js')?>"></script>
    <script type="text/javascript" src="<?=theme_js('jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=theme_js('modernizr.custom.js')?>"></script>
    <script type="text/javascript" src="<?=theme_js('responsiveslides.min.js')?>"></script>
    
    <script type="text/javascript" src="<?=theme_js('main.js')?>"></script>
    <script type="text/javascript" src="<?=theme_js('analytic.js')?>"></script>
	<?php if(\CI::uri()->segment(2) == 'home' || \CI::uri()->segment(2) == ''): ?>
	<!-- maps -->
	<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBICgJXm8vb-oBJhkRjERYQarM4oNKxF8o&callback=initMap"></script> -->
	<script src="<?php echo theme_js('maplace.min.js'); ?>"></script>
    <script type="text/javascript">
        var branch;
        branch = <?php echo $locations; ?>;
        new Maplace({
            locations: branch,
            map_div: '#gmap-findbranch',
            controls_div: '#controls-findbranch',
            controls_type: 'list',
            controls_title: 'KAP HHES:',
            controls_on_map: false
        }).Load();
    </script>
	<script type="text/javascript">
		$("#contact").click(function(){
			var a=$("#c_fullname").val(),
				b=$("#c_email").val(),
				c=$("#c_phone").val(),
				d=$("#c_branch").val(),
				e=$("#c_sender").val(),
				f=$("#c_messagetype").val(),
				g=$("#c_messagecategory").val(),
				h=$("#c_message").val(),
				i=grecaptcha.getResponse();

			//$('#c_form')[0].reset(); // reset form
		    $('.form-group').removeClass('has-error'); // clear error class
		    $('.help-block').empty(); // clear error string
							
			$.ajax({
				url:"<?=site_url('page/contactus')?>",
				type:"post",
				data:"fullname="+a+"&email="+b+"&phone="+c+"&branch="+d+"&sender="+e+"&messagetype="+f+"&messagecategory="+g+"&message="+h+"&g-recaptcha-response="+i,
				dataType: "JSON",
				success:function(resp) {
					if(resp.status)
		            {
			            $("#message_contactus").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+resp.message+"</div>");
			          	$('#c_form')[0].reset(); // reset form
					    $('.form-group').removeClass('has-error'); // clear error class
					    $('.help-block').empty(); // clear error string		                
		            }else {
		            	for (var i = 0; i < resp.inputerror.length; i++) 
		                {
		                    $('[name="'+resp.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
		                    $('[name="'+resp.inputerror[i]+'"]').next().text(resp.error_string[i]); //select span help-block class set text error string
		                }
		            }			
				}
			})
		});
	</script>
	<?php endif; ?>
	<script type="text/javascript">	
	    $(document).ready(function() {
	    	$('#detail_events').on('hidden.bs.modal', function (e) { 
				$(e.target).removeData("bs.modal").find(".modal-content").empty(); 
				$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
			});
			$('#detail_people').on('hidden.bs.modal', function (e) { 
				$(e.target).removeData("bs.modal").find(".modal-content").empty(); 
				$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
			});
			$('#detail_affiliation').on('hidden.bs.modal', function (e) { 
				$(e.target).removeData("bs.modal").find(".modal-content").empty(); 
				$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
			});
			$('#detail_careers').on('hidden.bs.modal', function (e) { 
				$(e.target).removeData("bs.modal").find(".modal-content").empty(); 
				$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
			});
			$(".bs-example-modal-lg").on('show.bs.modal', function () {
				setTimeout( function() {
					$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
				}, 0);
			});
	    });
    </script>
</body>
</html>