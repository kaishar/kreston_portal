	<!-- image header -->
    <section class="sectionimageheader">
    	<div class="imageheader head_career txt-white">
        	<div class="container">
            	<h1 class="txt-white">Career</h1>
                <h3 class="txt-white">Kreston HHES celebrate the talents and passion of our people</h3>
                <span class="bordertext15 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
                <p>Kreston HHES seek talented and energetic accounting professionals to join our young and fast - growing firm in various positions. Kreston HHES believe that professionals with the right mindset, right attitude, right philosophy, and are technically competent, can be developed to their full potential.</p>
                <p>Besides a career and development, Kreston HHES has a strong commitment to healthy life - style and corporate social responsibility.</p>
            </div>
        </div>
    </section>
    <!-- end image header -->
    <section class="sectionalpha">
    	<div class="container">
        	<div class="openingtitle">
                <h2 id="careersss" class="text-uppercase txt-blue1 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">Career</h2>
                <h4 class="txt-grey3">Kreston HHES are always looking for talented individuals. </h4>
                <span class="bordertext5 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
            <!--  Career -->
            <div class="row row-centered">
				<?php
					foreach($vacancy as $rows):
				?>
            	<div class="col-lg-2 col-md-3 col-smx-4 col-xs-6 col-centered">
                    <div class="keypeople_thumb">
						<div class="keypeople-txt">
                            <h5 class="txt-blue1"><?=$rows->division?></h5>
                            <h5 class="txt-grey3"><?=$rows->position?></h5>
                        </div>
                        <div class="btn-group">
                            <a class="btn btn-xs btn-alpha_whiteline" data-toggle="modal" data-target="#detail_careers" href="<?php echo site_url('frame/career/'.$rows->id); ?>">detail</a>
                        </div>
                    </div>
                </div>
				<?php endforeach; ?>
            </div>            
            <!-- loop  here -->
            
            <hr class="divider_boder-white">
            
            <!--  End Career -->
			<p class="careerlink">If you are interested in joining us, please email us at <a class="btn btn-link">HRD Kreston HHES</a> with your resume and a covering letter stating which position you would like to apply and reasons supporting why you believe you are a suitable candidate.</p>
            <hr class="divider_boder-white">
        </div>
    </section>
    
    