<!-- image header -->
    <section class="sectionimageheader">
    	<div class="imageheader head_newsroom txt-white">
        	<div class="container">
            	<h1 class="txt-white">Newsroom</h1>
                <h3 class="txt-white">Kreston HHES Latest News</h3>
                <span class="bordertext15 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
        </div>
    </section>
    <!-- end image header -->
    <!-- media -->
    <section id="media" class="sectionalpha">
    	<div class="container">
            <div class="newsroompage">
                <div class="row">
                	<div class="col-sm-8 col-md-9">
                    	<div class="row">
							<?php foreach($news as $rows): ?>
                            <div class="col-xs-12 col-smx-6 col-md-4">						
                                <div class="news_headline">
                                    <a class="imgheadline" data-toggle="modal" data-target="#detail_events" href="<?php echo site_url('frame/news/'.$rows->id); ?>">
                                        <img src="<?=theme_upload('news/'.$rows->image)?>" alt="img">
                                    </a>
                                    <!-- data -->
                                    <h4 class="title"><a data-toggle="modal" data-target="#detail_events" href="<?php echo site_url('frame/news/'.$rows->id); ?>"><?=$rows->title?></a></h4>
                                    <div class="small">
                                        <p class="date inline"><?php echo (\CI::lang()->lang() == 'id' ? date_bahasa($rows->news_date) : format_date($rows->news_date)); ?></p>
                                        <p class="author inline"><?=$rows->news_by?></p>							
                                    </div>
                                    <div class="newshighlights"><p><?=char_limit($rows->news_desc, 250)?></p></div>
                                    <a class="btn btn-alpha_whiteline" data-toggle="modal" data-target="#detail_events" href="<?php echo site_url('frame/news/'.$rows->id); ?>">read more</a>
                                </div>											                   
                            </div>
                            <?php endforeach; ?>                     
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="text-center">
                                <?php
                                    $page_links = CI::pagination()->create_links();
                                
                                    if($page_links != ''):
                                        echo $page_links;
                                    endif;
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3">
                    	<div class="twitterpage">
                        	<div id="twitt">
                                <div class="socialcontent text-center">
                                	<!--<a class="twitter-timeline" href="https://twitter.com/KrestonIntLtd">Tweets by KrestonIntLtd</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>-->                     
                                    <a class="twitter-timeline" href="https://twitter.com/kreston_hhes" target="_blank">Tweets by kreston_hhes</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                </div>
                                <div class="gotosocial text-right hidden">
                                    <a href="https://twitter.com/kreston_hhes" class="btn btn-sm btn-alpha_blueline" target="_blank">Twitter</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="pagingproductdivider">
                
            </div>
        </div>
    </section>
    <!-- End media -->