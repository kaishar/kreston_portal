<!-- slider -->
    <section id="mainslide" class="bg-mainimage">
    <div class="container">
    	<div id="wrapper">
            <div class="callbacks_container">
                <ul class="rslides" id="slidealpha">
                    <li>
                        <img src="<?=theme_img('slider/trans.png')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                        <div class="caption">
                            <div class="slideleft sliderhome_caption">
                                <h1 class="uppercase">KRESTON HHES</h1>
                                <h2 class="uppercase">Knowing Indonesia</h2>
                                <p>Kreston HHES is a national network of independent accounting firms. <span class="hidden-smx hidden-xs">As trusted, long-term counsellors, we combine expertise with empathy to help you achieve your professional and personal goals.</span></p>
                                <a class="btn btn-sm btn-alpha_white"  href="<?=site_url('about-us')?>">More Detail</a>
                            </div>
                            <div class="slideright sliderhome_logo hidden">
                                <img src="<?=theme_img('slider/slide1.jpg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="<?=theme_img('slider/trans.png')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                        <div class="caption">
                            <div class="slideleft sliderhome_caption">
                                <h1 class="uppercase">KRESTON HHES</h1>
                                <h2 class="uppercase">OUR COMMITMENT TO QUALITY</h2>
                                <p>Kreston member firms comply with the professional standards appropriate to their respective countries.</p>
                                <a class="btn btn-sm btn-alpha_white"  href="<?=base_url()?>#focussectors">More Detail</a>
                            </div>
                            <div class="slideright sliderhome_logo hidden">
                                <img src="<?=theme_img('slider/slide1.jpg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="<?=theme_img('slider/trans.png')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                        <div class="caption">
                            <div class="slideleft sliderhome_caption">
                                <h1 class="uppercase">KRESTON HHES</h1>
                                <h2 class="uppercase">THE WAY WE WORK</h2>
                                <p>With a Kreston member at your side, you reduce the learning curve and lower the risk of entering new markets. <span class="hidden-smx hidden-xs">We guarantee one-to-one contact with our people, which means you stay in control of every decision.</span></p>
                                <a class="btn btn-sm btn-alpha_white"  href="<?=base_url()?>#contactus">More Detail</a>
                            </div>
                            <div class="slideright sliderhome_logo hidden">
                                <img src="<?=theme_img('slider/slide2.jpg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        </div>
    </section>
    <!-- end slider -->
    <!-- Services -->
	<section id="services" class="sectionalpha">
    	<div class="container">
        	<div class="openingpage">
                <h1 class="text-uppercase txt-blue1 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Our Services</h1>
                <span class="bordertext5 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
        	<div class="row stepbystepalpha">
            	<a data-toggle="modal" data-target="#auditing" class="servicespoin">
                    <div class="col-smx-6 col-sm-7 col-md-6 col-lg-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                      <h2 class="txt-blue1">Auditing and Assurance</h2>
                      <p>Kreston members offer partner led audit and assurance services</p>
                    </div>
                    <div class="col-smx-6 col-sm-5 col-md-6 col-lg-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                        <div class="img-display">
                            <img class="img-service" src="<?=theme_img('services/services_auditing.svg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                        </div>
                    </div>
                </a>
            </div>
            
            <hr class="divider_shadow">
            
            <div class="row stepbystepalpha">
            	<a data-toggle="modal" data-target="#taxation" class="servicespoin">
                <div class="col-smx-6 col-sm-5 col-md-6 col-lg-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="img-display">
                        <img class="img-service" src="<?=theme_img('services/services_corporate.svg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                    </div>
                </div>
                <div class="col-smx-6 col-sm-7 col-md-6 col-lg-7 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                  <h2 class="txt-blue1">Corporate and Personal Taxation</h2>
                  <p>Kreston member firms, you will have the benefit of access to tax expertise from around the world.</p>
                </div>
                </a>
            </div>
            
            <hr class="divider_shadow">
            
            <div class="row stepbystepalpha">
            	<a data-toggle="modal" data-target="#financialadvisory" class="servicespoin">
                <div class="col-smx-6 col-sm-7 col-md-6 col-lg-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                  <h2 class="txt-blue1">Financial Advisory Services</h2>
                  <p>The complexities of international acquisitions.</p>
                </div>
                <div class="col-smx-6 col-sm-5 col-md-6 col-lg-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="img-display">
                        <img class="img-service" src="<?=theme_img('services/services_corporatefinance.svg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                    </div>
                </div>
                </a>
            </div>
            
            <hr class="divider_shadow">
            
            <div class="row stepbystepalpha">
            	<a data-toggle="modal" data-target="#riskmanagement" class="servicespoin">
                <div class="col-smx-6 col-sm-5 col-md-6 col-lg-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="img-display">
                        <img class="img-service" src="<?=theme_img('services/services_riskmanagement.svg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                    </div>
                </div>
                <div class="col-smx-6 col-sm-7 col-md-6 col-lg-7 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                  <h2 class="txt-blue1">Risk Management</h2>
                  <p>We provide ongoing assurance that potential risks to your business strategy have been appropriately assessed and managed.</p>
                </div>
                </a>
            </div>
            
            <hr class="divider_shadow">
            
            <div class="row stepbystepalpha">
            	<a data-toggle="modal" data-target="#consultingservices" class="servicespoin">
                <div class="col-smx-6 col-sm-7 col-md-6 col-lg-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h2 class="txt-blue1">Consulting Services</h2>
                    <p>Corporate Finance, IT Advisory and Solutions.</p>
                </div>
                <div class="col-smx-6 col-sm-5 col-md-6 col-lg-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="img-display">
                        <img class="img-service" src="<?=theme_img('services/services_restructuring.svg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                    </div>
                </div>
                </a>
            </div>
            
        </div>
    </section>
    <!-- end services -->
    <!-- Sectors -->
    <section id="focussectors" class="sectionalpha bg-blue1">
    	<div class="container">
            <div class="openingpage">
                <h1 class="text-uppercase txt-white wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Our Focus Sectors</h1>
                <span class="bordertext5 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
                <p class="txt-white">We have successfully worked with different people, skills, alliances and technology from owner managed entreprenerial to publicly listed companies and multinational companies some of industries that we have effectively managed with more than 300 private and public companies.</p>
            </div>
            <!-- Focus Sectors -->
            <div id="focussectorlist" class="box indolist">
                <div class="row hidden">
                    <div class="col-xs-12">
                        <div class="indolist-acttop">
                            <!-- panel top -->
                            <div class="indolist-panel box panel-top">
                                <div 
                                    class="indolist-drop-down hidden" 
                                    data-control-type="drop-down" 
                                    data-control-name="paging" 
                                    data-control-action="paging">
                                    <ul>
                                        <li><span data-number="4" data-default="true"> 4 per page </span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <!-- data -->   
                            <div class="list box text-shadow">
                            
                                <!-- sector 1 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                    	<div class="thumbproduct">
                                            <div class="row">
                                                <div class="col-smx-6 col-md-12">
                                                    <!-- img -->
                                                    <div class="img theme img-sectors">
                                                    	<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>travel</title><path class="cls-1" d="M883.44,667.37a26.26,26.26,0,1,1,26.26-26.26,26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09A25.12,25.12,0,0,0,883.44,616h0Z" transform="translate(-857.18 -614.85)"/><path class="cls-1" d="M893.27,630.89a1.28,1.28,0,0,1,.94.35c1.17,1.17-.72,4.43-4.32,8l1,2.2a1.18,1.18,0,0,1,.77-0.39,0.5,0.5,0,0,1,.36.14c0.34,0.34.06,1-.61,1.43l1.05,2.39a1.17,1.17,0,0,1,.75-0.38,0.5,0.5,0,0,1,.36.14c0.34,0.34.06,0.95-.59,1.41l1.32,3,0.18,2.91-7.88-9.95-0.22.18c-2.76,2.17-5.32,4.08-7.27,5.48l-0.26,6-2.46-4.19a3,3,0,0,1-.93.46h0a1.92,1.92,0,0,1,.44-1l-4.19-2.46,6-.26c1.39-2,3.3-4.5,5.47-7.27l0.18-.22L873.39,631l2.91,0.19,3,1.32a1.49,1.49,0,0,1,1.05-.73,0.49,0.49,0,0,1,.36.15,0.8,0.8,0,0,1-.23,1.11l2.39,1.05a1.51,1.51,0,0,1,1.07-.75,0.49,0.49,0,0,1,.36.15,0.81,0.81,0,0,1-.25,1.12l2.2,1c2.82-2.88,5.47-4.66,7-4.66m0-1.17c-2.31,0-5.36,2.56-7.28,4.44l-0.38-.17a1.69,1.69,0,0,0-.48-1.36,1.66,1.66,0,0,0-1.18-.49,2.17,2.17,0,0,0-1.35.52l-0.57-.25a1.69,1.69,0,0,0-.48-1.33,1.65,1.65,0,0,0-1.18-.49,2.17,2.17,0,0,0-1.33.5l-2.26-1a1.17,1.17,0,0,0-.39-0.1l-2.91-.19h-0.08a1.17,1.17,0,0,0-.72,2.08l9,7.16c-1.74,2.23-3.35,4.37-4.63,6.15l-5.46.24a1.17,1.17,0,0,0-.54,2.17l3.24,1.9a1.23,1.23,0,0,0,.25,1.34,1.28,1.28,0,0,0,.88.37,1.48,1.48,0,0,0,.48-0.08l1.9,3.24a1.17,1.17,0,0,0,1,.58,1.2,1.2,0,0,0,.28,0,1.17,1.17,0,0,0,.88-1.08l0.24-5.46c1.81-1.3,4-2.92,6.15-4.63l7.16,9a1.17,1.17,0,0,0,2.08-.8l-0.18-2.91a1.18,1.18,0,0,0-.1-0.39l-1-2.26a2.23,2.23,0,0,0,.5-1.18,1.65,1.65,0,0,0-.48-1.33,1.67,1.67,0,0,0-1.18-.49H893l-0.25-.58a2.26,2.26,0,0,0,.51-1.19,1.66,1.66,0,0,0-.48-1.34,1.68,1.68,0,0,0-1.19-.49h-0.18l-0.17-.39c1.87-1.91,4.43-4.95,4.44-7.26a2.35,2.35,0,0,0-2.45-2.47h0Z" transform="translate(-857.18 -614.85)"/></svg>
                                                    </div>
                                                </div>
                                                <div class="col-smx-6 col-md-12">
                                                    <!-- data -->
                                                    <div class="block right newslist-body">
                                                        <h5 class="text-uppercase text-center">TRAVEL, HOSPITALITY AND LEISURE</h5>
                                                    </div>
                                                </div>
                                            </div>
                                    	</div>
                                    </a>
                                </div>
                                <!-- sector 2 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                    	<div class="thumbproduct">
                                            <div class="row">
                                                <div class="col-smx-6 col-md-12">
                                                    <!-- img -->
                                                    <div class="img theme img-sectors">
                                                    	<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Charity</title><path class="cls-1" d="M1047.61,667.37a26.26,26.26,0,1,1,26.26-26.26,26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09A25.12,25.12,0,0,0,1047.61,616h0Z" transform="translate(-1021.35 -614.85)"></path><path class="cls-1" d="M1047.82,654.53a0.58,0.58,0,0,1-.28-0.07l-0.09-.06-0.43-.3c-0.91-.63-3-2.12-4.45-3.2-5.27-4.11-8.08-7.15-8.57-9.3a8.53,8.53,0,0,1-.4-2.52,8.79,8.79,0,0,1,2.44-6.26,7.4,7.4,0,0,1,5.27-2.27h0a7.75,7.75,0,0,1,6.22,3.06,7.68,7.68,0,0,1,6.15-3.21h0.15c4.23,0,7.72,3.73,7.78,8.32a8.72,8.72,0,0,1-.35,2.58c-0.42,2.12-3.22,5.32-8.3,9.48a52.76,52.76,0,0,1-4.84,3.65,0.59,0.59,0,0,1-.31.09h0Zm-6.41-22.8a6.29,6.29,0,0,0-4.53,1.92,7.61,7.61,0,0,0-2.11,5.42,7.51,7.51,0,0,0,.36,2.22c0.3,1.32,2,3.86,8.16,8.69,1.37,1.06,3.49,2.54,4.4,3.17l0.14,0.1c1.13-.75,2.64-1.9,4.4-3.35,6.06-5,7.65-7.56,7.91-8.86a7.7,7.7,0,0,0,.32-2.29c-0.05-4-3-7.17-6.61-7.17h-0.13a6.57,6.57,0,0,0-5.65,3.39,0.58,0.58,0,0,1-.5.3h0a0.59,0.59,0,0,1-.5-0.28,6.59,6.59,0,0,0-5.65-3.25h0Z" transform="translate(-1021.35 -614.85)"></path><path class="cls-1" d="M1057.06,641h-0.12a0.58,0.58,0,0,1-.45-0.69l0.09-.35a3.85,3.85,0,0,0,.16-1.14,3.26,3.26,0,0,0-3-3.49,0.58,0.58,0,0,1,0-1.17,4.41,4.41,0,0,1,4.14,4.64,5,5,0,0,1-.2,1.48l-0.07.27a0.58,0.58,0,0,1-.57.46h0Z" transform="translate(-1021.35 -614.85)"></path></svg>
                                                    </div>
                                                </div>
                                                <div class="col-smx-6 col-md-12">
                                                    <!-- data -->
                                                    <div class="block right newslist-body">
                                                        <h5 class="text-uppercase text-center">Charity, not for profit and Education</h5>
                                                    </div>
                                                </div>
                                            </div>
                                    	</div>
                                    </a>
                                </div>
                                <!-- sector 3 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                        <div class="thumbproduct">			
                                            <div class="row">
                                                <div class="col-smx-6 col-md-12">
                                                    <!-- img -->
                                                    <div class="img theme img-sectors">
                                                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Real-Estate</title><path class="cls-1" d="M1208.76,667.37A26.26,26.26,0,1,1,1235,641.11a26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09A25.12,25.12,0,0,0,1208.76,616h0Z" transform="translate(-1182.5 -614.85)"></path><path class="cls-1" d="M1219.89,652.83h-0.67V631.27a1.35,1.35,0,0,0-1.35-1.35h-4v-1.35a1.35,1.35,0,0,0-1.35-1.35h-6.74a1.35,1.35,0,0,0-1.35,1.35v1.35h-4a1.35,1.35,0,0,0-1.35,1.35v21.56h-0.67a0.67,0.67,0,1,0,0,1.35h21.56A0.67,0.67,0,1,0,1219.89,652.83Zm-14.15-24.25h6.74v6.74h-6.74v-6.74Zm-5.39,2.69h4v4h-4v-4Zm0,5.39h4v4h-4v-4Zm0,5.39h4v4h-4v-4Zm0,10.78v-5.39h4v5.39h-4Zm8.08,0v-2.25h1.35v2.25h-1.35Zm2.69,0v-4h-4v4h-1.35V636.66h6.74v16.17h-1.35Zm6.74,0h-4v-5.39h4v5.39Zm0-6.74h-4v-4h4v4Zm0-5.39h-4v-4h4v4Zm0-5.39h-4v-4h4v4Z" transform="translate(-1182.5 -614.85)"></path><path class="cls-1" d="M1207.08,642h4v-4h-4v4Zm1.35-2.69h1.35v1.35h-1.35v-1.35Z" transform="translate(-1182.5 -614.85)"></path><path class="cls-1" d="M1207.08,634h4v-4h-4v4Zm1.35-2.69h1.35v1.35h-1.35v-1.35Z" transform="translate(-1182.5 -614.85)"></path><path class="cls-1" d="M1207.08,647.44h4v-4h-4v4Zm1.35-2.69h1.35v1.35h-1.35v-1.35Z" transform="translate(-1182.5 -614.85)"></path></svg>
                                                    </div>
                                                </div>
                                                <div class="col-smx-6 col-md-12">
                                                    <!-- data -->
                                                    <div class="block right newslist-body">
                                                        <h5 class="text-uppercase text-center">Real Estate & Construction</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!-- sector 4 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                        <div class="thumbproduct">			
                                            <div class="row">
                                                <div class="col-smx-6 col-md-12">
                                                    <!-- img -->
                                                    <div class="img theme img-sectors">
                                                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Professional-Services</title><path class="cls-1" d="M1372.29,667.37a26.26,26.26,0,1,1,26.26-26.26,26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09A25.12,25.12,0,0,0,1372.29,616h0Z" transform="translate(-1346.03 -614.85)"></path><path class="cls-1" d="M1383.7,653.1h-22.81a1.88,1.88,0,0,1-1.88-1.87V635.3a1.88,1.88,0,0,1,1.88-1.87h22.81a1.88,1.88,0,0,1,1.87,1.87v15.92a1.88,1.88,0,0,1-1.87,1.87h0Zm-22.81-18.5a0.71,0.71,0,0,0-.71.71v15.92a0.71,0.71,0,0,0,.71.71h22.81a0.71,0.71,0,0,0,.7-0.71V635.3a0.71,0.71,0,0,0-.7-0.71h-22.81Z" transform="translate(-1346.03 -614.85)"></path><path class="cls-1" d="M1377,634.48a0.58,0.58,0,0,1-.58-0.58V631a0.71,0.71,0,0,0-.71-0.71h-6.77a0.71,0.71,0,0,0-.71.71v2.91a0.58,0.58,0,0,1-1.17,0V631a1.88,1.88,0,0,1,1.88-1.87h6.77a1.88,1.88,0,0,1,1.88,1.87v2.91a0.58,0.58,0,0,1-.58.58h0Z" transform="translate(-1346.03 -614.85)"></path><path class="cls-1" d="M1369.76,642.23h-9.68a0.58,0.58,0,1,1,0-1.17h9.68a0.58,0.58,0,1,1,0,1.17h0Z" transform="translate(-1346.03 -614.85)"></path><path class="cls-1" d="M1384.1,642.23h-9.68a0.58,0.58,0,0,1,0-1.17h9.68a0.58,0.58,0,1,1,0,1.17h0Z" transform="translate(-1346.03 -614.85)"></path><path class="cls-1" d="M1373.91,645.46h-3.23a0.58,0.58,0,0,1-.58-0.58V640a0.58,0.58,0,0,1,.58-0.58h3.23a0.58,0.58,0,0,1,.58.58v4.84a0.58,0.58,0,0,1-.58.58h0Zm-2.64-1.17h2.06v-3.67h-2.06v3.67h0Z" transform="translate(-1346.03 -614.85)"></path></svg>
                                                    </div>
                                                </div>
                                                <div class="col-smx-6 col-md-12">
                                                    <!-- data -->
                                                    <div class="block right newslist-body">
                                                        <h5 class="text-uppercase text-center">Professional Services Firms</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!-- sector 5 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                    <div class="thumbproduct">			
                                        <div class="row">
                                            <div class="col-smx-6 col-md-12">
                                                <!-- img -->
                                                <div class="img theme img-sectors">
                                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Public</title><path class="cls-1" d="M962.76,800.8A26.26,26.26,0,1,1,989,774.54a26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09,25.12,25.12,0,0,0-25.09-25.09h0Z" transform="translate(-936.5 -748.28)"></path><path class="cls-1" d="M962.85,773.15c-2.62,0-4.74-2.41-4.74-5.37s2.13-5.37,4.74-5.37,4.74,2.41,4.74,5.37-2.13,5.37-4.74,5.37h0Zm0-9.51c-1.94,0-3.51,1.86-3.51,4.14s1.57,4.14,3.51,4.14,3.51-1.86,3.51-4.14-1.57-4.14-3.51-4.14h0Z" transform="translate(-936.5 -748.28)"></path><path class="cls-1" d="M970.69,786.68H954.8a0.62,0.62,0,0,1-.62-0.57L953.84,781a4.65,4.65,0,0,1,2.89-4.28,17.91,17.91,0,0,1,12,0,4.63,4.63,0,0,1,2.9,4.28l-0.35,5.12a0.62,0.62,0,0,1-.62.57h0Zm-15.31-1.23h14.74l0.31-4.55a3.4,3.4,0,0,0-2.09-3,16.69,16.69,0,0,0-11.18,0,3.42,3.42,0,0,0-2.08,3l0.31,4.55h0Z" transform="translate(-936.5 -748.28)"></path><path class="cls-1" d="M970.69,786.68H954.8a0.62,0.62,0,0,1-.62-0.57L953.84,781a4.65,4.65,0,0,1,2.89-4.28,17.91,17.91,0,0,1,12,0,4.63,4.63,0,0,1,2.9,4.28l-0.35,5.12a0.62,0.62,0,0,1-.62.57h0Zm-15.31-1.23h14.74l0.31-4.55a3.4,3.4,0,0,0-2.09-3,16.69,16.69,0,0,0-11.18,0,3.42,3.42,0,0,0-2.08,3l0.31,4.55h0Z" transform="translate(-936.5 -748.28)"></path><path class="cls-1" d="M973.67,773.37a4.68,4.68,0,1,1,4.11-4.64,4.41,4.41,0,0,1-4.11,4.64h0Zm0-8.05a3.46,3.46,0,1,0,2.87,3.41,3.18,3.18,0,0,0-2.87-3.41h0Z" transform="translate(-936.5 -748.28)"></path><path class="cls-1" d="M980.31,784.21H971a0.62,0.62,0,0,1,0-1.23h8.78l0.26-3.76a2.81,2.81,0,0,0-1.71-2.48,14,14,0,0,0-9-.14,0.62,0.62,0,0,1-.38-1.17,15.29,15.29,0,0,1,9.79.16,4,4,0,0,1,2.52,3.72l-0.3,4.34a0.62,0.62,0,0,1-.62.57h0Z" transform="translate(-936.5 -748.28)"></path><path class="cls-1" d="M951.85,773.37a4.68,4.68,0,1,1,4.11-4.64,4.41,4.41,0,0,1-4.11,4.64h0Zm0-8.05a3.46,3.46,0,1,0,2.88,3.41,3.18,3.18,0,0,0-2.88-3.41h0Z" transform="translate(-936.5 -748.28)"></path><path class="cls-1" d="M945.21,784.21h9.36a0.62,0.62,0,1,0,0-1.23h-8.78l-0.26-3.76a2.81,2.81,0,0,1,1.71-2.48,14,14,0,0,1,9-.14,0.62,0.62,0,0,0,.38-1.17,15.29,15.29,0,0,0-9.79.16,4,4,0,0,0-2.52,3.72l0.3,4.34a0.62,0.62,0,0,0,.62.57h0Z" transform="translate(-936.5 -748.28)"></path></svg>
                                                </div>
                                            </div>
                                            <div class="col-smx-6 col-md-12">
                                                <!-- data -->
                                                <div class="block right newslist-body">
                                                   <h5 class="text-uppercase text-center">Public Sector</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                                <!-- sector 6 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                    <div class="thumbproduct">			
                                        <div class="row">
                                            <div class="col-smx-6 col-md-12">
                                                <!-- img -->
                                                <div class="img theme img-sectors">
                                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Medical</title><path class="cls-1" d="M1122.78,765a8.74,8.74,0,0,1,15.31,2.24h1.54a10.21,10.21,0,0,0-18-3.1A0.73,0.73,0,0,0,1122.78,765Z" transform="translate(-1103.59 -748.28)"></path><path class="cls-1" d="M1143.33,769.67a1.46,1.46,0,0,0-1.37-1h-24.22a1.46,1.46,0,0,0-.93,2.58,20.43,20.43,0,0,0,12.32,4.68s0,0,0,0v2.92h-0.73a3.65,3.65,0,1,0,0,7.29h0.73v1.46h-3.65a0.73,0.73,0,1,0,0,1.46h8.75a0.73,0.73,0,1,0,0-1.46h-3.65v-1.46h0.73a0.73,0.73,0,1,0,0-1.46h-0.73v-4.42a10.2,10.2,0,0,0,7.85-4.64,22.07,22.07,0,0,1-2.68.93,8.71,8.71,0,0,1-5.16,2.23V776s0,0,0,0a20.43,20.43,0,0,0,12.32-4.68A1.46,1.46,0,0,0,1143.33,769.67Zm-14.94,15.08a2.19,2.19,0,1,1,0-4.38h0.73v4.38h-0.73Zm1.46-10.21a18.88,18.88,0,0,1-12.11-4.38H1142A18.88,18.88,0,0,1,1129.85,774.54Z" transform="translate(-1103.59 -748.28)"></path><path class="cls-1" d="M1129.85,800.8a26.26,26.26,0,1,1,26.26-26.26,26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09,25.12,25.12,0,0,0-25.09-25.09h0Z" transform="translate(-1103.59 -748.28)"></path></svg>
                                                </div>
                                            </div>
                                            <div class="col-smx-6 col-md-12">
                                                <!-- data -->
                                                <div class="block right newslist-body">
                                                    <h5 class="text-uppercase text-center">Medical & Healthcare</h5>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                                <!-- sector 7 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                    <div class="thumbproduct">			
                                        <div class="row">
                                            <div class="col-smx-6 col-md-12">
                                                <!-- img -->
                                                <div class="img theme img-sectors">
                                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Agriculturel</title><path class="cls-1" d="M1295.77,800.8A26.26,26.26,0,1,1,1322,774.54a26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09,25.12,25.12,0,0,0-25.09-25.09h0Z" transform="translate(-1269.51 -748.28)"></path><path class="cls-1" d="M1289.66,784.39a3.31,3.31,0,1,1,3.31-3.31,3.31,3.31,0,0,1-3.31,3.31h0Zm0-5.32a2,2,0,1,0,2,2,2,2,0,0,0-2-2h0Z" transform="translate(-1269.51 -748.28)"></path><path class="cls-1" d="M1299.58,781.73a0.65,0.65,0,0,1-.65-0.65,9.27,9.27,0,0,0-18.53,0,0.65,0.65,0,1,1-1.3,0,10.57,10.57,0,0,1,21.13,0,0.65,0.65,0,0,1-.65.65h0Z" transform="translate(-1269.51 -748.28)"></path><path class="cls-1" d="M1289.17,789a1.08,1.08,0,0,1-.93-0.53,7.51,7.51,0,0,1-1.44-.43l-1.23-.24a1.08,1.08,0,0,1-.86-1,7.75,7.75,0,0,1-1-1.08l-0.95-.83a1.08,1.08,0,0,1-.23-1.34,7.54,7.54,0,0,1-.35-1.45l-0.41-1.19a1.08,1.08,0,0,1,.47-1.27,7.42,7.42,0,0,1,.43-1.44l0.24-1.22a1.08,1.08,0,0,1,1-.87,7.61,7.61,0,0,1,1.08-1l0.83-1a1.1,1.1,0,0,1,1.33-.23,7.69,7.69,0,0,1,1.45-.35l1.19-.41a1.09,1.09,0,0,1,1.27.47,7.54,7.54,0,0,1,1.44.42l1.23,0.24a1.08,1.08,0,0,1,.86,1,7.64,7.64,0,0,1,1,1.08l1,0.83a1.08,1.08,0,0,1,.23,1.34,7.5,7.5,0,0,1,.35,1.45l0.41,1.19a1.08,1.08,0,0,1-.47,1.27,7.65,7.65,0,0,1-.42,1.44l-0.24,1.23a1.09,1.09,0,0,1-1,.86,7.76,7.76,0,0,1-1.07,1l-0.83,1a1.11,1.11,0,0,1-1.33.23,7.49,7.49,0,0,1-1.45.35l-1.19.41a1.08,1.08,0,0,1-.34.06h0Zm-3.12-2.42,1.13,0.23a7,7,0,0,0,1.61.44,0.65,0.65,0,0,1,.51.4l1.1-.37a6.81,6.81,0,0,0,1.61-.42,0.66,0.66,0,0,1,.64.09l0.77-.87a7.13,7.13,0,0,0,1.18-1.17,0.66,0.66,0,0,1,.6-0.24l0.23-1.13a7,7,0,0,0,.44-1.61,0.65,0.65,0,0,1,.4-0.51l-0.37-1.1a7,7,0,0,0-.42-1.61,0.65,0.65,0,0,1,.09-0.64l-0.87-.77a7.06,7.06,0,0,0-1.17-1.18,0.65,0.65,0,0,1-.24-0.6l-1.12-.23a7.07,7.07,0,0,0-1.62-.44,0.65,0.65,0,0,1-.51-0.41l-1.09.38a7.13,7.13,0,0,0-1.61.43,0.65,0.65,0,0,1-.64-0.09l-0.77.87a7.06,7.06,0,0,0-1.18,1.17,0.65,0.65,0,0,1-.6.24l-0.23,1.13a6.9,6.9,0,0,0-.44,1.62,0.65,0.65,0,0,1-.4.51l0.37,1.1a7,7,0,0,0,.43,1.61,0.65,0.65,0,0,1-.09.64l0.87,0.77a7.28,7.28,0,0,0,1.18,1.18,0.65,0.65,0,0,1,.24.6h0Z" transform="translate(-1269.51 -748.28)"></path><path class="cls-1" d="M1306.53,788.65a4.21,4.21,0,1,1,4.21-4.21,4.22,4.22,0,0,1-4.21,4.21h0Zm0-7.12a2.91,2.91,0,1,0,2.91,2.91,2.91,2.91,0,0,0-2.91-2.91h0Zm0,4.93a2,2,0,1,1,2-2,2,2,0,0,1-2,2h0Zm0-2.73a0.71,0.71,0,1,0,.71.71,0.72,0.72,0,0,0-.71-0.71h0Z" transform="translate(-1269.51 -748.28)"></path><path class="cls-1" d="M1310.72,779.65h-10.94a0.65,0.65,0,0,1,0-1.3h10.29v-1.62l1-6.55-11-.82a0.65,0.65,0,0,1-.6-0.57l-0.9-7.44h-12.44c-0.54,2.37-2.72,12-2.81,12.39a0.66,0.66,0,0,1-.79.47,0.65,0.65,0,0,1-.47-0.79c0.08-.34,1.95-8.57,2.92-12.87a0.65,0.65,0,0,1,.63-0.51h13.54a0.65,0.65,0,0,1,.65.57l0.91,7.48,11.16,0.83a0.65,0.65,0,0,1,.59.75l-1.08,7.15V779a0.65,0.65,0,0,1-.65.65h0Z" transform="translate(-1269.51 -748.28)"></path><path class="cls-1" d="M1298.91,763.72h-13.54a0.65,0.65,0,1,1,0-1.3h13.54a0.65,0.65,0,0,1,0,1.3h0Z" transform="translate(-1269.51 -748.28)"></path><path class="cls-1" d="M1294.69,770.66a0.65,0.65,0,0,1-.45-1.12l2.71-2.6a0.65,0.65,0,1,1,.9.94l-2.71,2.6a0.64,0.64,0,0,1-.45.18h0Z" transform="translate(-1269.51 -748.28)"></path><path class="cls-1" d="M1298,775.2a0.65,0.65,0,0,1-.61-0.88l2.17-5.63a0.65,0.65,0,1,1,1.21.47l-2.17,5.63a0.65,0.65,0,0,1-.61.42h0Z" transform="translate(-1269.51 -748.28)"></path><path class="cls-1" d="M1299.13,772.17a0.64,0.64,0,0,1-.43-0.16l-2.87-2.54a0.65,0.65,0,1,1,.86-1l2.87,2.54a0.65,0.65,0,0,1-.43,1.14h0Z" transform="translate(-1269.51 -748.28)"></path></svg>
                                                </div>
                                            </div>
                                            <div class="col-smx-6 col-md-12">
                                                <!-- data -->
                                                <div class="block right newslist-body">
                                                    <h5 class="text-uppercase text-center">Agriculture</h5>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                                <!-- sector 8 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                    <div class="thumbproduct">			
                                        <div class="row">
                                            <div class="col-smx-6 col-md-12">
                                                <!-- img -->
                                                <div class="img theme img-sectors">
                                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Tech</title><path class="cls-1" d="M883.44,924.73a26.26,26.26,0,1,1,26.26-26.26,26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09,25.12,25.12,0,0,0-25.09-25.09h0Z" transform="translate(-857.18 -872.21)"></path><path class="cls-1" d="M887.89,901l-1.1-.38c0.29-.84.54-1.67,0.75-2.47a29.58,29.58,0,0,0,1.12-9.59c-0.19-2.42-.89-4-1.87-4.28-1.32-.36-3.3,1.57-5,4.9l-1-.54c2.09-4,4.41-6,6.37-5.49,1.52,0.41,2.49,2.29,2.73,5.31a30.77,30.77,0,0,1-1.16,10c-0.22.82-.48,1.68-0.77,2.54h0Z" transform="translate(-857.18 -872.21)"></path><path class="cls-1" d="M888.3,895.83c-0.56-.64-1.15-1.27-1.76-1.89a29.6,29.6,0,0,0-7.74-5.76c-2.19-1-3.92-1.23-4.64-.52-1,1-.29,3.64,1.73,6.81l-1,.63c-2.43-3.82-3-6.83-1.57-8.26,1.11-1.11,3.23-1,6,.29a30.75,30.75,0,0,1,8.06,6c0.63,0.63,1.24,1.28,1.82,1.94l-0.88.77h0Z" transform="translate(-857.18 -872.21)"></path><path class="cls-1" d="M876.48,903.37c-3.9,0-6.38-1-6.86-2.8a2.89,2.89,0,0,1,.4-2.23c1.47-2.55,6.2-5.21,12.06-6.78,0.84-.23,1.71-0.43,2.59-0.6l0.23,1.14c-0.85.17-1.7,0.36-2.51,0.58-5.48,1.47-10,4-11.35,6.23a1.78,1.78,0,0,0-.28,1.35c0.35,1.32,3,2.07,6.76,1.91l0.05,1.17-1.08,0h0Z" transform="translate(-857.18 -872.21)"></path><path class="cls-1" d="M880.28,910.74a2.39,2.39,0,0,1-.63-0.08c-1.52-.41-2.49-2.29-2.73-5.31a30.76,30.76,0,0,1,1.16-10c0.24-.88.5-1.73,0.78-2.55l1.1,0.38c-0.27.79-.52,1.62-0.75,2.47a29.57,29.57,0,0,0-1.12,9.58c0.19,2.42.89,4,1.87,4.28,1.32,0.35,3.29-1.57,5-4.9l1,0.54c-1.87,3.59-3.93,5.57-5.74,5.57h0Z" transform="translate(-857.18 -872.21)"></path><path class="cls-1" d="M891.27,907.79c-2.95,0-7.62-2.77-11.9-7.05-0.62-.62-1.23-1.28-1.82-1.94l0.88-.77c0.56,0.65,1.16,1.28,1.76,1.89,4,4,8.46,6.71,11.07,6.71a1.79,1.79,0,0,0,1.31-.43c1-1,.29-3.64-1.73-6.81l1-.63c2.43,3.82,3,6.83,1.57,8.26a2.9,2.9,0,0,1-2.13.77h0Z" transform="translate(-857.18 -872.21)"></path><path class="cls-1" d="M882.08,902.89l-0.23-1.14c0.82-.16,1.67-0.36,2.52-0.58a29.57,29.57,0,0,0,8.86-3.82c2-1.37,3-2.78,2.77-3.76-0.35-1.32-3-2.08-6.76-1.9l-0.05-1.17c4.52-.19,7.42.81,7.94,2.77,0.41,1.52-.74,3.3-3.23,5a30.79,30.79,0,0,1-9.22,4c-0.87.23-1.74,0.43-2.59,0.6h0Z" transform="translate(-857.18 -872.21)"></path><path class="cls-1" d="M883.37,896.63a0.3,0.3,0,1,1-.3.3,0.3,0.3,0,0,1,.3-0.3m0-1.17a1.47,1.47,0,1,0,1.47,1.47,1.47,1.47,0,0,0-1.47-1.47h0Z" transform="translate(-857.18 -872.21)"></path></svg>
                                                </div>
                                            </div>
                                            <div class="col-smx-6 col-md-12">
                                                <!-- data -->
                                                <div class="block right newslist-body">
                                                    <h5 class="text-uppercase text-center">Tech & Life Science</h5>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                                <!-- sector 9 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                    <div class="thumbproduct">			
                                        <div class="row">
                                            <div class="col-smx-6 col-md-12">
                                                <!-- img -->
                                                <div class="img theme img-sectors">
                                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Sports</title><path class="cls-1" d="M1047.61,924.73a26.26,26.26,0,1,1,26.26-26.26,26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09,25.12,25.12,0,0,0-25.09-25.09h0Z" transform="translate(-1021.35 -872.21)"></path><path class="cls-1" d="M1048,910.86a13.16,13.16,0,1,1,13.16-13.16A13.18,13.18,0,0,1,1048,910.86h0Zm0-25.15a12,12,0,1,0,12,12,12,12,0,0,0-12-12h0Z" transform="translate(-1021.35 -872.21)"></path><path class="cls-1" d="M1059.32,899.58a13.18,13.18,0,0,1-13.16-13.16,12.85,12.85,0,0,1,.07-1.3,0.58,0.58,0,0,1,.53-0.52c0.43,0,.86-0.06,1.29-0.06a13.18,13.18,0,0,1,13.16,13.16,12.81,12.81,0,0,1-.07,1.3,0.58,0.58,0,0,1-.52.52c-0.42,0-.85.06-1.29,0.06h0Zm-12-13.85c0,0.23,0,.46,0,0.69a12,12,0,0,0,12,12l0.69,0c0-.23,0-0.46,0-0.7a12,12,0,0,0-12-12l-0.7,0h0Z" transform="translate(-1021.35 -872.21)"></path><path class="cls-1" d="M1048,910.86a13.18,13.18,0,0,1-13.16-13.16c0-.44,0-0.87.06-1.29a0.59,0.59,0,0,1,.53-0.53c0.43,0,.86-0.06,1.29-0.06A13.18,13.18,0,0,1,1049.93,909c0,0.44,0,.87-0.06,1.29a0.59,0.59,0,0,1-.53.53c-0.43,0-.85.06-1.29,0.06h0Zm-12-13.85c0,0.23,0,.46,0,0.69a12,12,0,0,0,12,12l0.69,0c0-.23,0-0.46,0-0.7a12,12,0,0,0-12-12l-0.7,0h0Z" transform="translate(-1021.35 -872.21)"></path><polygon class="cls-1" points="18.3 34.7 17.47 33.88 35.08 16.27 35.91 17.1 18.3 34.7 18.3 34.7"></polygon></svg>
                                                </div>
                                            </div>
                                            <div class="col-smx-6 col-md-12">
                                                <!-- data -->
                                                <div class="block right newslist-body">
                                                    <h5 class="text-uppercase text-center">Sports</h5>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                                <!-- sector 10 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                    <div class="thumbproduct">
                                        <div class="row">
                                            <div class="col-smx-6 col-md-12">
                                                <!-- img -->
                                                <div class="img theme img-sectors">
                                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Retaill</title><path class="cls-1" d="M1208.76,924.73A26.26,26.26,0,1,1,1235,898.47a26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09,25.12,25.12,0,0,0-25.09-25.09h0Z" transform="translate(-1182.5 -872.21)"></path><path class="cls-1" d="M1209.48,912.24L1195,897.75l12.81-12.8h14.49v14.49l-12.8,12.81h0Zm-12.84-14.49,12.84,12.84,11.64-11.64V886.11h-12.84l-11.64,11.64h0Z" transform="translate(-1182.5 -872.21)"></path><path class="cls-1" d="M1204.71,900.09a0.58,0.58,0,0,1-.41-1l6.72-6.71a0.58,0.58,0,1,1,.83.83l-6.72,6.71a0.58,0.58,0,0,1-.41.17h0Z" transform="translate(-1182.5 -872.21)"></path><path class="cls-1" d="M1207.72,903.1a0.58,0.58,0,0,1-.41-1l6.72-6.72a0.58,0.58,0,0,1,.83.83l-6.72,6.72a0.58,0.58,0,0,1-.41.17h0Z" transform="translate(-1182.5 -872.21)"></path><path class="cls-1" d="M1216.5,892.91a2.17,2.17,0,0,1-1.54-.64,2.19,2.19,0,0,1,0-3.09,2.24,2.24,0,0,1,3.09,0,2.19,2.19,0,0,1-1.55,3.73h0Zm0-3.2a1,1,0,0,0-.72.3,1,1,0,0,0,0,1.44,1,1,0,0,0,1.44,0,1,1,0,0,0,0-1.44,1,1,0,0,0-.72-0.3h0Z" transform="translate(-1182.5 -872.21)"></path></svg>
                                                </div>
                                            </div>
                                            <div class="col-smx-6 col-md-12">
                                                <!-- data -->
                                                <div class="block right newslist-body">
                                                    <h5 class="text-uppercase text-center">Retail</h5>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                                <!-- sector 11 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                    <div class="thumbproduct">
                                        <div class="row">
                                            <div class="col-smx-6 col-md-12">
                                                <!-- img -->
                                                <div class="img theme img-sectors">
                                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Manufacturing</title><path class="cls-1" d="M1372.29,924.73a26.26,26.26,0,1,1,26.26-26.26,26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09,25.12,25.12,0,0,0-25.09-25.09h0Z" transform="translate(-1346.03 -872.21)"></path><path class="cls-1" d="M1385.53,913.26h-7.82a0.53,0.53,0,0,1-.53-0.53l2.13-22.93a0.53,0.53,0,0,1,.53-0.53h3.55a0.53,0.53,0,0,1,.53.53l2.13,22.93a0.53,0.53,0,0,1-.53.53h0Zm-7.29-1.07H1385l-2.13-21.86h-2.49l-2.13,21.86h0Z" transform="translate(-1346.03 -872.21)"></path><path class="cls-1" d="M1377.27,913.26h-7.82a0.53,0.53,0,0,1-.53-0.53l1.07-9.78a0.53,0.53,0,0,1,.53-0.53h5.69a0.53,0.53,0,0,1,.53.53l1.07,9.78a0.53,0.53,0,0,1-.53.53h0Zm-7.29-1.07h6.75l-1.07-8.71H1371l-1.07,8.71h0Z" transform="translate(-1346.03 -872.21)"></path><path class="cls-1" d="M1369,913.26h-10a0.53,0.53,0,0,1-.53-0.53l2.13-14.57a0.53,0.53,0,0,1,.53-0.53h5.69a0.53,0.53,0,0,1,.53.53l2.13,14.57c0,0.29-1.31.53-1.6,0.53H1369Zm-9.42-1.07h8.89l-2.13-13.51h-4.62l-2.13,13.51h0Z" transform="translate(-1346.03 -872.21)"></path><path class="cls-1" d="M1365,889.56a8.31,8.31,0,0,0-3.29,4.1,0.53,0.53,0,0,0,1,.39c1.78-4.55,6.13-5.12,10.73-5.73s9.57-1.26,11.59-6.39a0.53,0.53,0,1,0-1-.39c-1.78,4.55-6.13,5.12-10.73,5.73-3,.39-6,0.78-8.3,2.29h0Z" transform="translate(-1346.03 -872.21)"></path><path class="cls-1" d="M1365.53,893a8.29,8.29,0,0,0-1.35,1.08,0.53,0.53,0,0,0,0,.75,0.54,0.54,0,0,0,.75,0,10,10,0,0,1,5.12-2.45,0.53,0.53,0,0,0-.23-1,12.88,12.88,0,0,0-4.3,1.66h0Z" transform="translate(-1346.03 -872.21)"></path><path class="cls-1" d="M1366.87,901.79h-5.35a0.53,0.53,0,0,1,0-1.07h5.35a0.53,0.53,0,0,1,0,1.07h0Z" transform="translate(-1346.03 -872.21)"></path><path class="cls-1" d="M1376,906.5h-5.35a0.53,0.53,0,0,1,0-1.07H1376a0.53,0.53,0,0,1,0,1.07h0Z" transform="translate(-1346.03 -872.21)"></path><path class="cls-1" d="M1383.32,893.48H1380a0.59,0.59,0,0,1,0-1.07h3.34a0.59,0.59,0,0,1,0,1.07h0Z" transform="translate(-1346.03 -872.21)"></path></svg>
                                                </div>
                                            </div>
                                            <div class="col-smx-6 col-md-12">
                                                <!-- data -->
                                                <div class="block right newslist-body">
                                                    <h5 class="text-uppercase text-center">Manufacturing</h5>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                                <!-- sector 11 -->
                                <div class="col-xs-6 col-md-3 list-item">
                                    <a data-toggle="modal" data-target="#detail_sector" class="focussectorbtn">
                                    <div class="thumbproduct">
                                        <div class="row">
                                            <div class="col-smx-6 col-md-12">
                                                <!-- img -->
                                                <div class="img theme img-sectors">
                                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52.52 52.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Banking-Finance</title><path class="cls-1" d="M1372.29,667.37a26.26,26.26,0,1,1,26.26-26.26,26.29,26.29,0,0,1-26.26,26.26h0Zm0-51.35a25.09,25.09,0,1,0,25.09,25.09A25.12,25.12,0,0,0,1372.29,616h0Z" transform="translate(-1346.03 -614.85)"></path><path class="cls-1" d="M1383.7,653.1h-22.81a1.88,1.88,0,0,1-1.88-1.87V635.3a1.88,1.88,0,0,1,1.88-1.87h22.81a1.88,1.88,0,0,1,1.87,1.87v15.92a1.88,1.88,0,0,1-1.87,1.87h0Zm-22.81-18.5a0.71,0.71,0,0,0-.71.71v15.92a0.71,0.71,0,0,0,.71.71h22.81a0.71,0.71,0,0,0,.7-0.71V635.3a0.71,0.71,0,0,0-.7-0.71h-22.81Z" transform="translate(-1346.03 -614.85)"></path><path class="cls-1" d="M1377,634.48a0.58,0.58,0,0,1-.58-0.58V631a0.71,0.71,0,0,0-.71-0.71h-6.77a0.71,0.71,0,0,0-.71.71v2.91a0.58,0.58,0,0,1-1.17,0V631a1.88,1.88,0,0,1,1.88-1.87h6.77a1.88,1.88,0,0,1,1.88,1.87v2.91a0.58,0.58,0,0,1-.58.58h0Z" transform="translate(-1346.03 -614.85)"></path><path class="cls-1" d="M1369.76,642.23h-9.68a0.58,0.58,0,1,1,0-1.17h9.68a0.58,0.58,0,1,1,0,1.17h0Z" transform="translate(-1346.03 -614.85)"></path><path class="cls-1" d="M1384.1,642.23h-9.68a0.58,0.58,0,0,1,0-1.17h9.68a0.58,0.58,0,1,1,0,1.17h0Z" transform="translate(-1346.03 -614.85)"></path><path class="cls-1" d="M1373.91,645.46h-3.23a0.58,0.58,0,0,1-.58-0.58V640a0.58,0.58,0,0,1,.58-0.58h3.23a0.58,0.58,0,0,1,.58.58v4.84a0.58,0.58,0,0,1-.58.58h0Zm-2.64-1.17h2.06v-3.67h-2.06v3.67h0Z" transform="translate(-1346.03 -614.85)"></path></svg>
                                                </div>
                                            </div>
                                            <div class="col-smx-6 col-md-12">
                                                <!-- data -->
                                                <div class="block right newslist-body">
                                                    <h5 class="text-uppercase text-center">Banking & Finance</h5>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                                
                            </div>
                            <div class="box indolist-no-results">
                                <p>No results found</p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="indolist-actbottom pagingproductdivider">
                            <!-- ios button: show/hide panel -->
                            <div class="indolist-ios-button hidden">
                                <i class="fa fa-sort"></i>Navigation
                            </div>
                            <!-- panel bottom -->
                            <div class="indolist-panel box panel-bottom indolist-ios-show">
                                <!-- pagination -->
                                <div 
                                    class="indolist-pagination pagingproductnav" 
                                    data-control-type="pagination" 
                                    data-control-name="paging" 
                                    data-control-action="paging">
                                </div>
                                <!-- pagination results -->
                                <div 
                                    class="indolist-label hidden" 
                                    data-type="Page {current} of {pages}" 
                                    data-control-type="pagination-info" 
                                    data-control-name="paging" 
                                    data-control-action="paging">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end Focus Sectors -->
        </div>
    </section>
    <!-- End Sectors -->
        
    <!-- Our Locations -->
    <section id="membership" class="sectionalpha bg-white framesection">
    	<div class="container contentfull">
        	<div class="openingpage">
                <h1 class="text-uppercase txt-blue1 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Our Locations</h1>
                <span class="bordertext5 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
                <p>Wherever in the world you meet us, Kreston HHES guarantee the same exceptional level of service.</p>
            </div>
            <div class="row">
                <div class="col-smx-9">
                    <div id="gmap-findbranch" class="gmap"></div>
                    <div id="info"></div>
                </div>
                <div class="col-smx-3">
                    <div class="gmap_controls" id="controls-findbranch">
                    </div>
                    <a class="btn btn-sm btn-alpha_whiteline"  href="http://kreston.com/find-a-member/" target="_blank">Find a Member</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Locations -->
    <!-- contact us -->
	<section id="contactus" class="sectionalpha bg-white">
    	<div class="container">
        	<div class="openingpage">
                <h1 class="text-uppercase txt-blue1 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Contact Us</h1>
                <span class="bordertext5 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
            <div class="row">
                <div class="col-smx-4 col-sm-5 col-lg-4">
                    <h3 class="text-uppercase">Head Office</h3>
                    <span class="bordertext15"><span></span></span>
                    <address>
                        <strong>Kreston HHES</strong><br />
						<strong>KAP Hendrawinata Hanny Erwin & Sumargo</strong><br /><br />
                        Intiland Tower 18th Floor<br />
                        Jl. Jend. Sudirman Kav. 32<br />
                        Jakarta 10220, Indonesia<br />
                        <a href="tel:+62215712000" class="btn btn-link btn-xs">T: +62-21 571 2000</a><br/>
                        F: +62-21 570 6118 | +62-21 571 1818
                    </address>
                    <address>
                        <strong>Email</strong><br>
                        <a href="mailto:hhes.jakarta@kreston.co.id">hhes.jakarta@kreston.co.id</a>
                    </address>
                    <hr class="pagingproductdivider visible-xs">
                </div>
                <div class="col-smx-8 col-sm-7 col-lg-8">
                	<div id="message_contactus"></div>
                    <div class="enquiries">
                        <h3 class="text-uppercase">Send us Message</h3>
                        <span class="bordertext15"><span></span></span>
                        <p>Leave your contact details and we will contact you shortly.</p>
                        <form id="c_form">
                        <div class="formframe">
                            <div class="form-group">
                                <input type="text" class="form-control" id="c_fullname" name="c_fullname" placeholder="Enter full name">
                            	<span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="c_email" name="c_email" placeholder="Enter email">
                            	<span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="c_phone" name="c_phone" placeholder="Phone number">
                            	<span class="help-block"></span>
                            </div>
                            <div class="form-group">
                         		<label for="city">KAP HHES</label>
                                <?php
									$attributes = array('id' => 'c_branch',
										'class' => 'form-control');
									echo form_dropdown('c_branch', $list_branch, '', $attributes);
								?>
                            </div>
                            <div class="row">
                            	<div class="col-md-4">
                                	<div class="form-group">
                                        <label for="c_sender">Sender</label>
                                        <select id="c_sender" name="c_sender" class="form-control">
                                            <option value="">Choose</option>
                                            <option value="CUSTOMER">Customer</option>
                                            <option value="BRANCH">KAP HHES</option>
                                            <option value="THIRD_PARTY">Third Party</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                	<div class="form-group">
                                        <label for="c_messagetype">Message Type</label>
                                        <select id="c_messagetype" name="c_messagetype" class="form-control">
                                            <option value="">Choose</option>
                                            <option value="QUESTION">Question</option>
                                            <option value="REQUEST">Request</option>
                                            <option value="COMPLAIN">Complain</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                	<div class="form-group">
                                        <label for="c_messagecategory">Message Category</label>
                                        <select id="c_messagecategory" name="c_messagecategory" class="form-control">
                                            <option value="">Choose</option>
                                            <option value="PRODUCT">Product</option>
                                            <option value="SERVICES">Services</option>
                                            <option value="GENERAL">General</option>
                                            <option value="CAREER">Career</option>
                                            <option value="EVENT">Event</option>
                                            <option value="OTHER">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="c_message" name="c_message" rows="3" placeholder="Message"></textarea>
                            	<span class="help-block"></span>
                            </div>
							<div class="form-group">
                            	<div class="g-recaptcha" name="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
					            <script type="text/javascript"
					                    src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>">
					            </script>
					            <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="contactsubmit">
                        	<button type="button" id="contact" class="btn btn-alpha_whiteline">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <hr class="pagingproductdivider">
        </div>
    </section>
    <!-- End contact us -->