    <!-- ===== Modal services ===== -->
    <div class="modal fade bs-example-modal-lg" id="auditing" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modalpage">
            <div class="modal-content bg-modalservice">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title modal-sharing text-center hidden">Description</h4>
                </div>
                <div class="modal-body">
                    <h3 class="txt-blue1 text-center">Auditing and Assurance</h3>
                    <div class="img-responsive text-center">
                        <img class="img-display img-service" src="<?=theme_img('services/services_auditing.svg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                    </div>
                    <div class="txt-spec">
                    	<p>Kreston offers partner - led audit and assurance services. During an audit, we aim to gain a real understanding of your business and the quality and effectiveness of your accounting and control systems. This means we can focus on the risks of material misstatements in the financial statements.</p>
                        <p>In addition to an audit, a range of other assurance services can be tailored to your needs.</p>
                    	<p>These include :</p>
                    	<ul style="list-style-type:disc">
                    		<li>Limited reviews</li>
                    		<li>Agreed-upon procedures</li>
                    		<li>Assurance on non - financial information, including sustainability reports</li>
                    		<li>Initial Public Offering (IPO) bonds issuance, asset securitization merger transaction, acquisitions, spin - off  and conversion to International Finance Reporting Standard (IFRS).</li>
                    	</ul>
                    	<p>Kreston International is a member of the Forum of Firms, a group of the largest accounting networks in the world, including the "Big Four". We carry out transnational audits in accordance with International Auditing Standards and in compliance with the code of ethics issued by IESBA.</p>
                    </div>
                </div>
                <div class="modal-footer centerbtn">
                	<div class="btn-group">
                    	<button type="button" class="btn btn-alpha_whiteline" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-alpha_blue" onclick="window.location.href='<?=base_url()?>#contactus'" data-dismiss="modal">Contact Us</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade bs-example-modal-lg" id="taxation" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modalpage">
            <div class="modal-content bg-modalservice">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center hidden">Description</h4>
                </div>
                <div class="modal-body">
                    <h3 class="txt-blue1 text-center">Corporate and Personal Taxation</h3>
                    <div class="img-responsive text-center">
                        <img class="img-display img-service" src="<?=theme_img('services/services_corporate.svg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                    </div>
                    <div class="txt-spec">
                    	<p>Kreston can give you access to tax expertise from around the world. We specialize in the full range of cross - border issues, including :</p>
                    	<ul style="list-style-type:disc">
                    		<li>International tax structures for subsidiary operations, funding and dividend repatriation</li>
                    		<li>Indirect taxes and customs duties</li>
                    		<li>Transfer pricing</li>
                    		<li>Expatriate employee advice</li>
                    	</ul>
                    </div>
                </div>
                <div class="modal-footer centerbtn">
                	<div class="btn-group">
                    	<button type="button" class="btn btn-alpha_whiteline" data-dismiss="modal">Close</button>
                		<button type="button" class="btn btn-alpha_blue" onclick="window.location.href='<?=base_url()?>#contactus'" data-dismiss="modal">Contact Us</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade bs-example-modal-lg" id="financialadvisory" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modalpage">
            <div class="modal-content bg-modalservice">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center hidden">Description</h4>
                </div>
                <div class="modal-body">
                    <h3 class="txt-blue1 text-center">Financial Advisory Services</h3>
                    <div class="img-responsive text-center">
                        <img class="img-display img-service" src="<?=theme_img('services/services_corporatefinance.svg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                    </div>
                    <div class="txt-spec">
                        <h5>Accounting & Transactional Services</h5>
                        <p>Kreston provides a full range of accounting and outsourcing services to help your business develop and grow both locally and internationally.</p>
                        <p>The trusted relationship between member firms means we can pull together to overcome barriers to international growth.</p>
                        <p>Our accounting services include:</p>
                        <ul style="list-style-type:disc">
                            <li>Preparation of financial statements in compliance with International Financial Reporting Standards (IFRS), US GAAP or local GAAP</li>
                            <li>Budgeting and business planning</li>
                            <li>Management accounting</li>
                            <li>Outsourced bookkeeping and payroll</li>
                        </ul>
                        <hr class="divider_shadow">
                        <h5>Financial Due Diligence</h5>
                        <p>For company mergers or acquisitions, we provide financial due diligence services that provide both the buyers and sellers the financial reports they need to make informed decisions regarding the selling prices and fair values of the businesses in focus.</p>
                        <p>We help present and discuss, on updated and user friendly reports the company’s historical results, financial risks, and current operating conditions which will fairly reflect the current values of the target companies.</p>
                        <hr class="divider_shadow">
                        <h5>Management Advisory Services</h5>
                        <p>We provide advisory services to businesses which are undergoing management or administration turnovers, liquidation or company restructuring, among others.</p>
                        <p>Solutions generally need to encompass multiple business entities, subject to widely differing formal procedures across several jurisdictions.</p>
                        <p>Kreston offers specialist support from experts across the globe in conducting independent business reviews and advising on business structures and performance improvement, optimising returns for stakeholders and investigating business failures and challenging antecedent transactions.</p>
                	</div>
                </div>
                <div class="modal-footer centerbtn">
                	<div class="btn-group">
                    	<button type="button" class="btn btn-alpha_whiteline" data-dismiss="modal">Close</button>
                		<button type="button" class="btn btn-alpha_blue" onclick="window.location.href='<?=base_url()?>#contactus'" data-dismiss="modal">Contact Us</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade bs-example-modal-lg" id="riskmanagement" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modalpage">
            <div class="modal-content bg-modalservice">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center hidden">Description</h4>
                </div>
                <div class="modal-body">
                    <h3 class="txt-blue1 text-center">Risk Management</h3>
                    <div class="img-responsive text-center">
                        <img class="img-display img-service" src="<?=theme_img('services/services_riskmanagement.svg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                    </div>
                    <div class="txt-spec">
                    	<p>We provide ongoing assurance that potential risks to your business strategy have been appropriately assessed and managed.</p>
                        <p>This includes:</p>
						<ul style="list-style-type:disc">
                    		<li>Systems and internal control reviews</li>
                            <li>Internal audit and other compliance services</li>
                            <li>Fraud prevention review and investigations</li>
                            <li>Forensic accounting</li>
                    	</ul>
                        <hr class="divider_shadow">
                        <h5>System & Internal Control Reviews</h5>
                        <p>We provide management on the status of their companies’ current systems by surveying the company’s environment, studying current business processes - conducting interviews with key personnel, checking documents; analyzing the quality of financial reports given to the management and discussing issues and providing solutions.</p>
                        <hr class="divider_shadow">
                        <h5>Internal Audit and other Compliance Services</h5>
                        <p>We provide independent assurance that the companies’ risk management, governance and internal control processes are operating effectively.</p>
                        <p>We will discuss solutions to gaps in internal control, providing companies with the most cost effective and efficient solutions.</p>
                        <hr class="divider_shadow">
                        <h5>Fraud Prevention Review & Investigation</h5>
                        <p>Company management seeking fraud review related services originates from the need for prevention or investigation of suspected fraudulent activities.</p>
                        <p>For fraud prevention, we provide a comprehensive review of the company’s susceptibility to fraud which may come in the forms of either one or all of the following: asset misappropriation, corruption and/or financial statement fraud. We will also discuss options for the establishment of antifraud policies and procedures.</p>
                        <p>We also provide an investigation for suspected fraud inspecting and providing a limited review of the suspected accounts and transactions.</p>
                        <hr class="divider_shadow">
                        <h5>Forensic Accounting</h5>
                        <p>We provide forensic accounting services which include, but are the not limited to, the following: review of money laundering, embezzlement, violations of generally accepted accounting principles or auditing standards, financial statement fraud and supplier and various third party contract fraud.</p>
                    </div>
                </div>
                <div class="modal-footer centerbtn">
                	<div class="btn-group">
                    	<button type="button" class="btn btn-alpha_whiteline" data-dismiss="modal">Close</button>
                		<button type="button" class="btn btn-alpha_blue" onclick="window.location.href='<?=base_url()?>#contactus'" data-dismiss="modal">Contact Us</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade bs-example-modal-lg" id="consultingservices" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modalpage">
            <div class="modal-content bg-modalservice">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center hidden">Description</h4>
                </div>
                <div class="modal-body">
                    <h3 class="txt-blue1 text-center">Consulting Services</h3>
                    <div class="img-responsive text-center">
                        <img class="img-display img-service" src="<?=theme_img('services/services_restructuring.svg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                    </div>
                    <div class="txt-spec">
                        <h5>Corporate Finance</h5>
                        <p>We provide corporate finance services to multinational companies that have businesses internationally, that are involved in acquisitions and sales of business segments locally, which requires the guidance from professional staff with experience with local regulations. </p>
                        <p>Being connected throughout our global member firms, we can help you manage and expand your international operations through the following services:</p>
                        <ul style="list-style-type:disc">
                            <li>Fundraising support – both equity and debt</li>
                            <li>Corporate valuation</li>
                            <li>Initial public offerings</li>
                            <li>Lead advisory services for mergers and disposals</li>
                            <li>Structuring for international partnerships and joint ventures</li>
                            <li>Due diligence services – acquisition or vendor</li>
                        </ul>
                        <hr class="divider_shadow">
                        <h5>IT Advisory and Solutions</h5>
                        <p>Other consultancy services provided includes IT advisory and solution where services range from first time IT implementation, to system maintenance or check-ups, to transition from a current system to a new one with minimal disruptions.</p>
                    </div>
                </div>
                <div class="modal-footer centerbtn">
                	<div class="btn-group">
                    	<button type="button" class="btn btn-alpha_whiteline" data-dismiss="modal">Close</button>
                		<button type="button" class="btn btn-alpha_blue" onclick="window.location.href='<?=base_url()?>#contactus'" data-dismiss="modal">Contact Us</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- ===== End Modal services ===== -->
    <!-- ===== modal focussector ===== -->
    <div class="modal fade bs-example-modal-lg" id="detail_sector" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modalpage">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title modal-sharing text-center hidden">Description</h4>
                </div>
                <div class="modal-body">
                	<h1 class="txt-blue1 text-center">Focus Sector Detail Page</h1>
                    <div class="img-display img-modal">
                        <img class="img-responsive" src="<?=theme_img('space/space_4.jpg')?>" alt="kreston indonesia - kreston hhes" title="kreston indonesia - kreston hhes">
                    </div>
                    <div class="txt-spec">
                    	<p>We have successfully worked with different people, skills, alliances and technology form owner managed entreprenerial to publicly listed companies and multinational companies some of industries that we have effectively managed with more than 300 private and public companies.</p>
                    </div>
                </div>
                <div class="modal-footer">
                	<div class="btn-group">
                	<button type="button" class="btn btn-alpha_blue" onclick="window.location.href='<?=base_url()?>#contactus'" data-dismiss="modal">Contact Us</button>
                    <button type="button" class="btn btn-alpha_whiteline" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===== end modal focussector ===== -->