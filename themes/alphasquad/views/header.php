<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="MobileOptimized" content="360">
    <meta name="description" content="Kreston HHES is a national network of independent accounting firms.">
    <meta name="designer" content="alphasquad inc.">
    <meta name="author" content="Alphasquad Inc.">
    <meta name="copyright" content="krestonindonesia">
    <meta name="keywords" content="krestonindonesia, kreston indonesia, keston hhes, indonesia accounting firms">
    <title>Kreston HHES | KAP Hendrawinata Hanny Erwin & Sumargo | Kreston Indonesia </title>
    <link href="<?=theme_css('bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=theme_css('font-awesome.min.css')?>" rel="stylesheet">
    <link href="<?=theme_css('animate.min.css')?>" rel="stylesheet">
    <link href="<?=theme_css('alphalist.css')?>" rel="stylesheet">
    <link href="<?=theme_css('das.css')?>" rel="stylesheet">
    
    <link rel="canonical" href="http://www.kreston.co.id/" />
	<link href="https://plus.google.com/115097356609424070163" rel="publisher" />
	
	<meta property="og:url"				content="http://kreston.co.id/" />
	<meta property="og:type"			content="business.business" />
	<meta property="og:title"			content="Kreston HHES | KAP Hendrawinata Hanny Erwin & Sumargo" />
	<meta property="og:description"		content="Kreston Indonesia HHES is a national network of independent accounting firms." />
	<meta property="og:image"			content="<?=theme_img('logoicon/square.png')?>" />
	<meta property="business:contact_data:street_address" content="Intiland Tower 18th Floor, Jl. Jend. Sudirman Kav. 32" />
	<meta property="business:contact_data:locality"       content="Jakarta" />
	<meta property="business:contact_data:postal_code"    content="10220" />
	<meta property="business:contact_data:country_name"   content="Indonesia" />
	<meta property="place:location:latitude"              content="-6.2140275" />
	<meta property="place:location:longitude"             content="106.8173226" />
	<meta name="twitter:card"			content="summary" />
	<meta name="twitter:url"			content="http://kreston.co.id/" />
	<meta name="twitter:title"			content="Kreston HHES | KAP Hendrawinata Hanny Erwin & Sumargo" />
	<meta name="twitter:description"	content="Kreston Indonesia HHES is a national network of independent accounting firms." />
	<meta name="twitter:image"			content="<?=theme_img('logoicon/square.png')?>" />
	<meta name="twitter:site"			content="@kreston_hhes" />
	<meta name="twitter:creator"		content="@kreston_hhes" />
    
    
    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?=theme_img('logoicon/favicon.ico')?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=theme_img('apple-touch-icon-144-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=theme_img('apple-touch-icon-114-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=theme_img('apple-touch-icon-72-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" href="<?=theme_img('apple-touch-icon-57-precomposed.png')?>">
    <link href="https://fonts.googleapis.com/css?family=Raleway:200,400,700" rel="stylesheet">
</head><!--/head-->
<body data-spy="scroll" data-target="#navalphaoo">
	<!--   header   -->
	<header id="header" class="bgheader-grad">
        <div class="container">
        	<div class="navbar navbar-inverse" role="banner">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logoalpha" href="<?=base_url()?>"></a>
                </div>
                <div class="collapse navbar-collapse" id="navalphaoo">
                	<div class="socialiconframe">
                    	<a href="https://www.instagram.com/krestonhhes/" target="_blank" class="txt-grey2"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        
                        
                        <a href="https://www.facebook.com/pg/krestonhhes/posts/" target="_blank" class="txt-grey2"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                        <a href="https://plus.google.com/115097356609424070163" target="_blank" class="txt-grey2"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/kreston_hhes" target="_blank" class="txt-grey2"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                        <a href="https://www.linkedin.com/company/kreston-hhes" target="_blank" class="txt-grey2"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                    </div>
                    <div class="langsearch">
                        <div class="lang hidden">
                            <a class="btn-lang" href="<?=site_url(\CI::lang()->switch_uri('id'))?>">Indonesia</a> | <a class="btn-lang" href="<?=site_url()?>">English</a>
                        </div>
                        <form action="<?=site_url('search')?>" method="post">
	                        <div class="searchs">
	                            <input type="text" name="term" class="searchhere" placeholder="Enter your search ...">
	                            <div class="searchclk"><i class="fa fa-search txt-white" aria-hidden="true"></i></div>
	                        </div>
                        </form>
                    </div>
                    <ul class="nav navbar-nav navbar-right navalpha">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle uppercase" data-toggle="dropdown">About Us<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?=site_url('about-us')?>#whoweare" class="gowes menudrop">Who We Are</a></li>
                                <li><a href="<?=site_url('about-us')?>#ourvalue" class="gowes menudrop">Our Value</a></li>
                                <li><a href="<?=site_url('about-us')?>#ourvisimisi" class="gowes menudrop">Vision & Mission</a></li>
                                <li><a href="<?=site_url('about-us')?>#ouraffiliation" class="gowes menudrop">Our Affiliation</a></li>
                                <li><a href="<?=site_url('about-us')?>#keypeople" class="gowes menudrop">Key People</a></li>
                                <li><a href="<?=site_url('about-us')?>#krestinal" class="gowes menudrop">About Kreston International</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle uppercase" data-toggle="dropdown">Services<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a data-toggle="modal" data-target="#auditing" class="gowes menudrop">Auditing and Assurance</a></li>
                                <li><a data-toggle="modal" data-target="#taxation" class="gowes menudrop">Corporate and Personal Taxation</a></li>
                                <li><a data-toggle="modal" data-target="#financialadvisory" class="gowes menudrop">Financial Advisory Services</a></li>
                                <li><a data-toggle="modal" data-target="#riskmanagement" class="gowes menudrop">Risk Management</a></li>
                                <li><a data-toggle="modal" data-target="#consultingservices" class="gowes menudrop">Consulting Services</a></li>
                            </ul>
                        </li>
                        <li><a href="<?=base_url()?>#focussectors" class="gowes uppercase">Focus Sectors</a></li>
                        <li><a href="<?=site_url('newsroom')?>" class="gowes uppercase">Newsroom</a></li>
                        <li><a href="<?=base_url()?>#membership" class="gowes uppercase">Our Locations</a></li>
						<li><a href="<?=site_url('career')?>" class="gowes uppercase">Careers</a></li>
                        <li><a href="<?=base_url()?>#contactus" class="gowes uppercase">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!--   End header   -->