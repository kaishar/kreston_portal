	<!-- image header -->
    <section class="sectionimageheader">
    	<div class="imageheader head_about txt-white">
        	<div class="container">
            	<h1 class="txt-white">About us</h1>
                <h3 class="txt-white">Kreston HHES is a member of Kreston International Limited</h3>
                <span class="bordertext15 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
                <p>one of the worlds leading accounting and consulting organizations providing specialist advice to assist business owner to achieve success and reach their goals.</p>
            </div>
        </div>
    </section>
    <!-- end image header -->
    <!--  About Us -->
    <section id="aboutus" class="sectionalpha">
    	<div class="container">
        	<div class="openingtitle">
                <h2 id="whoweare" class="text-uppercase txt-blue1 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">Who We Are</h2>
                <span class="bordertext5  wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
            <div class="row stepbystepalpha">
                <h4 class="txt-blue2">Hendrawinata Hanny Erwin & Sumargo (HHES) is a member of Kreston International Limited, one of the world’s leading accounting and consulting organizations providing specialist advice to assist business owners to achieve success and reach their goals.</h4>
                <span class="bordertext15 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
                <p>Our industry experience is highly regarded – we provide broad global resources and proven experience in consulting and outsourcing. We combine our expertise and experience of more than 16 skilled professionals with more than 25 years of experience in the industry and supported by more than 200 members of our professional staff, to be able to provide a wide range and value-added services to clients.</p>
            </div>
            
            <hr class="divider_boder-white">
            <!--  Our Value -->
            <div class="openingtitle">
                <h2 id="ourvalue" class="text-uppercase txt-blue1 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">Our Values</h2>
                <span class="bordertext5 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
            <div class="row stepbystepalpha">
                <div class="col-smx-6 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h3 class="txt-blue2">Trust</h3>
                	<p class="">Our clients know we’ve got their backs, no matter what – that’s why they trust us to help them achieve their business and life goals. And within the network, we trust one another to treat any referred matter as a priority.</p>
                </div>
                <div class="col-smx-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h3 class="txt-blue2">Responsiveness</h3>
                	<p class="">Business never stands still – so we don’t either. Our creative and dynamic approach means we’re always on hand with the fresh thinking and personalised advice our clients need – often before they even know it.</p>
                </div>
                <div class="col-smx-6 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h3 class="txt-blue2">Quality</h3>
                	<p class="">We offer the very best advice because we recruit the most talented, innovative professionals – and share their knowledge throughout our network. Our monitoring and inspection system ensures that we hold ourselves to the highest international standards of quality, ethics and working practices.</p>
                </div>
                <div class="col-smx-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h3 class="txt-blue2">Integrity</h3>
                	<p class="">We’re proud that the work we do helps secure long-term business sustainability and the comfort and security of future generations, and we approach every engagement with that in mind.</p>
                </div>
                <div class="col-smx-6 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h3 class="txt-blue2">Collaboration</h3>
                	<p class="">As individuals, we work side by side with clients to understand their ambitions and tackle their challenges. As a network, we’ve got the connections to bring the right knowledge to the table, whatever the challenge.</p>
                </div>
            </div>
            <!--  End Our Value -->
            <hr class="divider_boder-white">
            <!--  Our Visimisi -->
            <div class="openingtitle">
                <h2 id="ourvisimisi" class="text-uppercase txt-blue1 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">OUR VISION & MISSION</h2>
                <span class="bordertext5 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
            <div class="row stepbystepalpha">
            	<h3 class="txt-blue2">Our Vision</h3>
                <p>To become one of the leading and most trusted business partner in the region.</p>
                <h3 class="txt-blue2">Our Mission</h3>
                <ul style="list-style-type:disc">
                    <li>To continuously improve the quality of our human capital to produce the best and trusted professionals that would make us one of the best accounting and consultancy firms in Indonesia.</li>
                    <li>To provide quality services and business solutions that can add value to clients.</li>
                    <li>To create a harmonious and family - like work environment that we can be proud of.</li>
                </ul>
            </div>
            <!--  End Our Visimisi -->
            <hr class="divider_boder-white">
        </div>
    </section>
    <section class="sectionalpha bg-grey1">
    	<div class="container">
            <!--  Our Affiliation -->
            <div class="openingtitle">
                <h2 id="ouraffiliation" class="text-uppercase txt-blue1 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">Our Affiliation</h2>
                <h4 class="txt-grey3">Kreston HHES commit to comply with the professional standards appropriate to the respective jurisdictions and adhere to the following international standards.</h4>
                <span class="bordertext5 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
                        
            <hr class="pagingproductdivider">
            
            <div class="row row-centered">
			<?php
				foreach($affiliation as $rows):
			?>
            	<div class="col-md-4 col-smx-6 col-centered">
                    <div class="keypeople_thumb bg-white">
                        <div class="img-display">
                            <img class="img-responsive" src="<?=theme_upload('affiliation/'.$rows->image)?>" alt="<?=$rows->image?>">
                        </div>
                        <div class="keypeople-txt">
                            <h4 class="txt-blue1"><?=$rows->company_name?></h4>
                        </div>
                        <div class="btn-group">
							<a class="btn btn-xs btn-alpha_whiteline" data-toggle="modal" data-target="#detail_affiliation" href="<?php echo site_url('frame/affiliation/'.$rows->id); ?>">detail</a>
                        </div>
                    </div>
                </div>
			<?php
				endforeach;
			?>
                <!-- loop  here -->
                
            </div>           
            <!--  End Our Affiliation -->
            <hr class="divider_boder-white">
        </div>
    </section>
    <section class="sectionalpha">
    	<div class="container">
            <!--  Key People -->
            <div class="openingtitle">
                <h2 id="keypeople" class="text-uppercase txt-blue1 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">Key People</h2>
                <h4 class="txt-grey3">Whatever your business, accounting and consulting requirements, a Kreston HHES member will be there to provide expert support.</h4>
                <span class="bordertext5 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
            
            <hr class="pagingproductdivider">
            <?php
				foreach($category as $rows):
			?>
            <div class="row row-centered">
            	<div class="col-xs-12">
                	<div class="openingtitle">
                    	<h4 class="txt-blue1 text-left"><?=$rows->description?></h4>
                        <hr class="divider_boder-blue">
                    </div>
                </div>
				<?php
					foreach(${$rows->value} as $people):
				?>
            	<div class="col-lg-2 col-md-3 col-smx-4 col-xs-6 col-centered">
                    <div class="keypeople_thumb">
                        <div class="img-display">
                            <img class="img-responsive" src="<?=theme_upload('keypeople/'.$people->photo)?>" alt="<?=$people->fullname?>">
                        </div>
                        <div class="keypeople-txt">
                            <h5 class="txt-blue1"><?=$people->fullname?></h5>
                            <h5 class="txt-grey3"><small><?=$people->position?></small></h5>
                        </div>
                        <div class="btn-group">
                        	<a class="btn btn-xs btn-alpha_whiteline" data-toggle="modal" data-target="#detail_people" href="<?php echo site_url('frame/people/'.$people->id); ?>">detail</a>
                        </div>
                    </div>
                </div>
				<?php endforeach; ?>
            </div>
                
            <hr class="divider_boder-white">
            <?php
				endforeach;
			?>
            <!-- loop  here -->           
            <!--  End Key People -->
        </div>
    </section>
    <section class="sectionalpha">
    	<div class="container">
            <!--  About Kreston International -->
            <div class="openingtitle">
                <h2 id="krestinal" class="text-uppercase txt-blue1 wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">About Kreston International Limited</h2>
                <span class="bordertext5 wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms"><span></span></span>
            </div>
            <div class="row stepbystepalpha">
                <p>Kreston International Limited is a global network of independent accounting firms. </p>
                <p>A cohesive network of 180 firms in 113 countries that is home to more than 23,000 dedicated professionals, Kreston gives you access to top-quality advice and exceptional service wherever in the world you happen to do business.</p>
                <p>Founded in 1971, Kreston is currently the 12th largest accounting network in the world. As new markets develop and technology evolves, your business operates on an increasingly global scale. And when you’re branching out into the unknown, you can’t beat a bit of local knowledge. Our members leverage their network of local contacts to shape international solutions that are right for you and your business.</p>
                <h5>Forum of Firms</h5>
                <p>Kreston International is a member of the Forum of Firms. The forum is an association of international networks of accounting firms. Its goal is to promote consistent and high-quality standards of financial reporting and auditing practices worldwide.</p>
                <p>For additional details please go to<a class="btn btn-link btn-xs" href="http://kreston.com/" target="_blank">www.kreston.com</a></p>
            </div>
            <!--  About Kreston International -->
        </div>
    </section>
    <!-- End About Us -->
    
    
    