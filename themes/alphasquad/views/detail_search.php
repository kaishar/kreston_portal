<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="MobileOptimized" content="360">
    <meta name="description" content="Kreston Indonesia - website preview">
    <meta name="designer" content="alphasquad inc.">
    <meta name="author" content="Alphasquad Inc.">
    <meta name="copyright" content="krestonindonesia">
    <meta name="keywords" content="krestonindonesia">
    <title>Kreston Indonesia | Search</title>
    <link href="<?=theme_css('bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=theme_css('font-awesome.min.css')?>" rel="stylesheet">
    <link href="<?=theme_css('animate.min.css')?>" rel="stylesheet">
    <link href="<?=theme_css('alphalist.css')?>" rel="stylesheet">
    <link href="<?=theme_css('das.css')?>" rel="stylesheet">
    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?=theme_img('logoicon/favicon.ico')?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=theme_img('apple-touch-icon-144-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=theme_img('apple-touch-icon-114-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=theme_img('apple-touch-icon-72-precomposed.png')?>">
    <link rel="apple-touch-icon-precomposed" href="<?=theme_img('apple-touch-icon-57-precomposed.png')?>">
    <link href="https://fonts.googleapis.com/css?family=Raleway:200,400,700" rel="stylesheet">
</head><!--/head-->

<body data-spy="scroll" data-target="#navalphaoo">
	<!--   header   -->
	<header id="header" class="bgheader-grad">
        <div class="container">
        	<div class="navbar navbar-inverse" role="banner">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logoalpha" href="<?=base_url()?>"></a>
                </div>
                
                <div class="collapse navbar-collapse" id="navalphaoo">
                	<div class="socialiconframe">
                    	<a href="" class="txt-grey2"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                        <a href="" class="txt-grey2"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                        <a href="" class="txt-grey2"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                        <a href="" class="txt-grey2"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                    </div>
                    <div class="langsearch">
                        <div class="lang">
                            <a class="btn-lang" href="<?=site_url(\CI::lang()->switch_uri('id'))?>">Indonesia</a> | <a class="btn-lang active" href="<?=site_url()?>">English</a>
                        </div>
                        <form action="<?=site_url('search')?>">
	                        <div class="searchs">
	                            <input type="search" class="searchhere" placeholder="Enter your search ...">
	                            <div class="searchclk"><i class="fa fa-search txt-white" aria-hidden="true"></i></div>
	                        </div>
                        </form>
                    </div>
                    
                    <ul class="nav navbar-nav navbar-right navalpha">
                        <li><a href="<?=site_url('home#services')?>" class="gowes uppercase">Services</a></li>
                        <li><a href="<?=site_url('home#focussectors')?>" class="gowes uppercase">Focus Sectors</a></li>
                        <li><a href="<?=site_url('home#media')?>" class="gowes uppercase">Newsroom</a></li>
                        <li><a href="<?=site_url('home#aboutus')?>" class="gowes uppercase">About Us</a></li>
                        <li><a href="<?=site_url('home#membership')?>" class="gowes uppercase">Membership</a></li>
                        <li><a href="<?=site_url('home#contactus')?>" class="gowes uppercase">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!--   End header   -->
    	
	<section class="generalsection">
        <div class="container bg-white">
        	<div class="row">
				<div class="wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">
					<div class="header-edukasi">
						<div class="txt-header">
							<h1 class="txt-blue5">Search Result</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="margin: 20px;">
				<div class="col-md-5">
					<div class="pull-left">
					<?php
						$page_links = CI::pagination()->create_links();
					
						if($page_links != ''):
							echo $page_links;
						endif;
					?>
					</div>
				</div>
				<div class="col-md-7">
					<div class="pull-right">
						<?php echo form_open('search', 'class="form-inline"');?>
							<div class="form-group">
								<input type="text" size="50" class="form-control" name="term" placeholder="kata kunci" />
							</div>
							<input type="submit" value="<?php echo ('Search'); ?>" class="btn btn-alpha_blue" />				
						</form>
					</div>
				</div>                        
			</div>
			<hr class="divider_boder-blue">
			<?php
				foreach($news as $rows):
			?>
					<div class="row">
						<div class="col-sm-3">
							<img src='<?php echo theme_upload('news/'.$rows->image); ?>' class='img-responsive'>
						</div>
						<div class="col-sm-9">
							<dl class="dl-horizontal">
								<dt>Title</dt>
								<dd><?php echo $rows->title; ?></dd>
								<dt>Events By</dt>
								<dd><?php echo $rows->news_by; ?></dd>
								<dt>Descriptions</dt>
								<dd><?=char_limit($rows->news_desc, 250)?></dd>						
							</dl>	
							<dl class="dl-horizontal">
								<dt></dt>
								<dd>
									<a data-toggle="modal" data-target="#detail_events" class="btn btn-alpha_blue" href="<?php echo site_url('frame/news/'.$rows->id); ?>"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;&nbsp;VIEW&nbsp;</a>									
								</dd>
							</dl>			        			        
						</div>            	                
					</div>
					<hr>
			<?php
				endforeach;
			?>
        </div>
    </section>
					
	<!-- backtotop -->
		<div class="backtotop" id="toTop"><p><b class="bttcaret"></b></p><a href="javascript:goToTop()">back to top</a></div>
    <!-- end backtotop -->
	<!-- footer -->
    <footer id="footer" class="bg-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="copyright-text text-center txt-white">
                        <p>&copy; 2016-<?php echo date("Y"); ?> | Kreston Indonesia. <br>A member of <a href="http://kreston.com/" target="_blank" class="txt-white">Kreston International</a> | A global network of independent accounting firms.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- end footer -->
	<?php require_once('detail_service.php'); ?>    
	<!-- modal Viewer -->
	<div class="modal fade bs-example-modal-lg" id="detail_events" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modalpage">
			<div class="modal-content"></div>
		</div>
	</div>
	<!-- End modalviewer -->
	
    <script type="text/javascript" src="<?=theme_js('jquery.min.js')?>"></script>
    <script type="text/javascript" src="<?=theme_js('bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?=theme_js('wow.min.js')?>"></script>
	<script type="text/javascript" src="<?=theme_js('indolist.min.js')?>"></script>
    <script type="text/javascript" src="<?=theme_js('jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=theme_js('modernizr.custom.js')?>"></script>
	<script type="text/javascript" src="<?=theme_js('responsiveslides.min.js')?>"></script>
	<script type="text/javascript">	
	    $(document).ready(function() {
	    	$('#detail_events').on('hidden.bs.modal', function (e) { 
				$(e.target).removeData("bs.modal").find(".modal-content").empty(); 
				$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
			})
			$(".bs-example-modal-lg").on('show.bs.modal', function () {
				setTimeout( function() {
					$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
				}, 0);
			});
	    });
    </script>
    <script type="text/javascript" src="<?=theme_js('main.js')?>"></script>
    <script type="text/javascript" src="<?=theme_js('analytic.js')?>"></script>
</body>
</html>