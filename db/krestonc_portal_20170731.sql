SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DROP TABLE IF EXISTS affiliation;
CREATE TABLE IF NOT EXISTS affiliation (
  id int(11) NOT NULL AUTO_INCREMENT,
  company_name varchar(60) NOT NULL,
  image varchar(60) NOT NULL,
  descriptions text NOT NULL,
  address1 varchar(200) NOT NULL,
  address2 varchar(200) DEFAULT NULL,
  address3 varchar(200) DEFAULT NULL,
  email varchar(60) NOT NULL,
  phone varchar(20) NOT NULL,
  website varchar(100) DEFAULT NULL,
  createdby varchar(20) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  updatedby varchar(20) DEFAULT NULL,
  updatedat datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO affiliation (id, company_name, image, descriptions, address1, address2, address3, email, phone, website, createdby, createdat, updatedby, updatedat) VALUES
(6, 'PT. Kreston Advisory Indonesia', '74bc1a89f595d42ae1e529f5a92b416d.jpg', '<p>We provide Financial Advisory Services, Risk Management and Strategic Management, catering to companies in various industries, which ranges fSMEs&rsquo; to medium sized and large companies.</p>\r\n\r\n<p><strong>We know our clients. </strong></p>\r\n\r\n<p>We customize our procedures and detail reports which are planned and executed according to each entity&rsquo;s specific needs.</p>\r\n\r\n<p><strong><em>Our Services :</em></strong></p>\r\n\r\n<p><strong>A. Financial Advisory Services</strong></p>\r\n\r\n<ul>\r\n	<li>Accounting &amp; Transactional Services.</li>\r\n	<li>Financial Due Diligence.</li>\r\n	<li>Management Advisory Services.</li>\r\n</ul>\r\n\r\n<p><strong>B. Risk Management</strong></p>\r\n\r\n<ul>\r\n	<li>Systems and internal control reviews.</li>\r\n	<li>Internal audit and other compliance services.</li>\r\n	<li>Fraud prevention review and investigations.</li>\r\n	<li>Forensic accounting.</li>\r\n</ul>\r\n\r\n<p><strong>C. Consulting Services</strong></p>\r\n\r\n<ul>\r\n	<li>Corporate Finance.</li>\r\n	<li>IT advisory and Solutions.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', 'Intiland Tower 18th Floor', ' Jl. Jend. Sudirman Kav. 32, Jakarta 13220, Indonesia.', '', 'info.kai@kreston.co.id', '+62 21 5712000', 'http://www.kreston.co.id', 'adminkreston', '2017-07-25 16:27:49', 'adminkreston', '2017-07-28 11:38:47'),
(4, 'PT. Pratama Indomitra', '3672f2fc89e2473ba3c1faf04ad57539.jpg', '<p>Pratama Indomitra is a tax consultant who serves both corporate and private clients who need help in good tax management. Consisting of consultants who have expertise as well as extensive experience in the field of taxation, have high integrity, and uphold the values of business ethics.</p>', 'Antam Office Park Tower B Lantai 8', 'Jl. T.B. Simatupang No.1,Jakarta 12530, Indonesia.', '', 'info@pratamaindomitra.co.id', '+62 21 2963 4945', 'http://www.pratamaindomitra.co.id', 'adminkreston', '2017-07-25 14:38:34', 'adminkreston', '2017-07-28 11:38:15');

DROP TABLE IF EXISTS branch;
CREATE TABLE IF NOT EXISTS branch (
  id int(11) NOT NULL AUTO_INCREMENT,
  branch_name varchar(255) DEFAULT NULL,
  address1 varchar(255) DEFAULT NULL,
  address2 varchar(255) DEFAULT NULL,
  city varchar(60) DEFAULT NULL,
  phone varchar(50) DEFAULT NULL,
  fax varchar(50) DEFAULT NULL,
  photo varchar(100) DEFAULT NULL,
  latitude double DEFAULT NULL,
  longitude double DEFAULT NULL,
  pic_name varchar(60) DEFAULT NULL,
  pic_email varchar(60) DEFAULT NULL,
  createdby varchar(20) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  changedby varchar(20) DEFAULT NULL,
  changedat datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

INSERT INTO branch (id, branch_name, address1, address2, city, phone, fax, photo, latitude, longitude, pic_name, pic_email, createdby, createdat, changedby, changedat) VALUES
(2, 'HHES Jakarta', 'Intiland Tower 18th Floor Jl.Jend. Sudirman Kav.32', 'Jakarta 10220, Indonesia', 'Jakarta', '+62 21 571 2000', '+62-21 570 6118 | +62-21 571 1818', '86139ee77fda3f76d56dc758b9abb225.jpg', -6.2297264, 106.6894317, 'hhes.jakarta@kreston.co.id', 'Ferry', 'admin', '2017-05-29 13:52:25', 'adminkreston', '2017-07-26 13:10:17'),
(3, 'HHES Medan', 'Kreston Building Jl.Palang Merah No.40', 'Medan 20111, Indonesia', 'Medan', '+62-61 455 7925 | +62-61 415 7295', '+62-61 451 3159', '50752f7a095bfe6294f02ca77431bd6d.jpg', 3.6422756, 98.5294071, 'hhes.medan@kreston.co.id', 'medan', 'admin', '2017-05-29 13:54:27', 'adminkreston', '2017-07-26 13:10:08'),
(4, 'HHES Surabaya', 'Jl. Raya Gubeng 56', 'Surabaya 60225, Indonesia', 'Surabaya', '+62-31 5035046 | +62-31 5032289 | +62-31 5016879', '+62-31 5035689', '1ea073b7c6d6156e8c77f0953426a9b3.png', -7.2756368, 112.6416439, 'hhes.surabaya@kreston.co.id', 'Chris', 'admin', '2017-05-29 13:56:00', 'adminkreston', '2017-07-26 13:09:59'),
(9, 'PT. Kreston Advisory Indonesia', 'Intiland Tower 18 Floor Jl. Jend. Sudirman Kav.32', 'Jakarta 10220, Indonesia', 'Jakarta', '+62-21 571 2000', '+62-21 570 6118 | +62-21 571 1818', '0bc5a376d9ed5a6498dfd71090876a8b.jpg', -6.2297264, 106.6894317, 'info.kai@kreston.co.id', 'info KAI', 'admin', '2017-06-01 22:45:35', 'adminkreston', '2017-07-25 13:28:15'),
(13, 'PT. Pratama Indomitra', 'Antam Office Park Tower B Lantai 8, Jl. T.B. Simatupang No.1', 'Jakarta', 'Jakarta', '+62-21 2963 4945', '+62-21 2963 4946', '0100bcb0dbb04af39a623429000c3833.jpg', -6.3021856, 106.8409883, ' info@pratamaindomitra.co.id', 'Pratama', 'adminkreston', '2017-07-25 13:39:37', NULL, NULL);

DROP TABLE IF EXISTS canned_messages;
CREATE TABLE IF NOT EXISTS canned_messages (
  id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  deletable tinyint(1) NOT NULL DEFAULT '1',
  type enum('internal','order') NOT NULL,
  name varchar(50) DEFAULT NULL,
  subject varchar(100) DEFAULT NULL,
  content text NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO canned_messages (id, deletable, `type`, `name`, `subject`, content) VALUES
(1, 1, 'internal', 'contaus', 'Contact Us Information', '<html><head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=us-ascii\"> <meta name=\"designer\" content=\"alphasquad inc.\"> <style>body{font-family: Arial, Helvetica, sans-serif;}table{width:768px;margin:0 auto;}</style></head><body style=\"margin:10px auto;\"><table style=\"margin-bottom: 25px;\"> <thead> <tr> <td width=\"48%\"><a href=\"http://kreston.co.id/\" style=\"color: #00a0d6; font-size: 18px; text-decoration: none;\">kreston.co.id</a> </td><td width=\"4%\">&nbsp;</td><td width=\"48%\" align=\"right\" style=\"text-align:right\"> <img align=\"right\" style=\"width: 70px; margin-right: 30px;margin-left: 50%;\" src=\"http://kreston.co.id/beta/themes/alphasquad/storage/img/logoicon/krestonlogo.png\"> </td></tr></thead> </table> <table id=\"alphasquad\" style=\"background-color: #b3bdbe; padding: 20px;\"> <thead> <tr> <th colspan=\"3\" style=\"padding-bottom: 30px;\">Contact Us Information :</th> </tr></thead> <tbody> <tr> <td width=\"40%\" style=\"padding-bottom: 15px;\"><small>Full Name</small></td><td width=\"1%\" style=\"padding-bottom: 15px;\">:</td><td width=\"59%\" style=\"padding-bottom: 15px;\"><strong>{{fullname}}</strong></td></tr><tr> <td width=\"40%\"><small>Email Address</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{email}}</strong></td></tr><tr> <td width=\"40%\"><small>Phone Number</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{phone}}</strong></td></tr><tr> <td width=\"40%\"><small>Branch</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{branch}}</strong></td></tr><tr> <td width=\"40%\"><small>Sender</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{sender}}</strong></td></tr><tr> <td width=\"40%\"><small>Message Type</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{message_type}}</strong></td></tr><tr> <td width=\"40%\"><small>Message Category</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{message_category}}</strong></td></tr><tr> <td width=\"40%\"><small>Message</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{message}}</strong></td></tr></tbody></table> <table style=\"background-color: #ffffff; padding: 20px;\"> <thead> <tr> <td align=\"center\" style=\"text-align:center\"> &copy;2017. Kreston Indonesia</td></tr></thead> </table></body></html>');

DROP TABLE IF EXISTS contact;
CREATE TABLE IF NOT EXISTS contact (
  id int(11) NOT NULL AUTO_INCREMENT,
  fullname varchar(60) DEFAULT NULL,
  email varchar(60) DEFAULT NULL,
  phone varchar(20) DEFAULT NULL,
  branch varchar(20) DEFAULT NULL,
  sender varchar(60) DEFAULT NULL,
  message_type varchar(20) DEFAULT NULL,
  message_category varchar(20) DEFAULT NULL,
  message text,
  createdby varchar(20) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  changedby varchar(20) DEFAULT NULL,
  changedat datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS customers;
CREATE TABLE IF NOT EXISTS customers (
  id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  firstname varchar(32) NOT NULL,
  lastname varchar(32) NOT NULL,
  email varchar(128) NOT NULL,
  email_subscribe tinyint(1) NOT NULL DEFAULT '0',
  phone varchar(32) NOT NULL,
  company varchar(128) NOT NULL,
  password varchar(40) DEFAULT NULL,
  active tinyint(1) NOT NULL,
  group_id int(11) NOT NULL DEFAULT '1',
  confirmed tinyint(1) NOT NULL DEFAULT '0',
  is_guest tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8;

INSERT INTO customers (id, firstname, lastname, email, email_subscribe, phone, company, `password`, active, group_id, confirmed, is_guest) VALUES
(1, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(2, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(3, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(4, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(5, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(6, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(7, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(8, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(9, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(10, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(11, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(12, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(13, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(14, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(15, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(16, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(17, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(18, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(19, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(20, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(21, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(22, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(23, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(24, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(25, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(26, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(27, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(28, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(29, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(30, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(31, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(32, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(33, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(34, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(35, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(36, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(37, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(38, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(39, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(40, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(41, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(42, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(43, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(44, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(45, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(46, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(47, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(48, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(49, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(50, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(51, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(52, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(53, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(54, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(55, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(56, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(57, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(58, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(59, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(60, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(61, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(62, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(63, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(64, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(65, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(66, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(67, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(68, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(69, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(70, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(71, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(72, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(73, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(74, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(75, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(76, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(77, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(78, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(79, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(80, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(81, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(82, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(83, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(84, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(85, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(86, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(87, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(88, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(89, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(90, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(91, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(92, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(93, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(94, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(95, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(96, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(97, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(98, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(99, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(100, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(101, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(102, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(103, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(104, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(105, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(106, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(107, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(108, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(109, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(110, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(111, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(112, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(113, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(114, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(115, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(116, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(117, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(118, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(119, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(120, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(121, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(122, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(123, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(124, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(125, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(126, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(127, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(128, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(129, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(130, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(131, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(132, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(133, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(134, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(135, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(136, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(137, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(138, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(139, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(140, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(141, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(142, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(143, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(144, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(145, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(146, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(147, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(148, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(149, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(150, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(151, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(152, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(153, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(154, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(155, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(156, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(157, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(158, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(159, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(160, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(161, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(162, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(163, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(164, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(165, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(166, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(167, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(168, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(169, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(170, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(171, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(172, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(173, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(174, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(175, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(176, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(177, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(178, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(179, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(180, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(181, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(182, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(183, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(184, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(185, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(186, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(187, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(188, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(189, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(190, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(191, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(192, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(193, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(194, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(195, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(196, '', '', '', 1, '', '', '', 1, 1, 0, 1),
(197, '', '', '', 1, '', '', '', 1, 1, 0, 1);

DROP TABLE IF EXISTS customer_groups;
CREATE TABLE IF NOT EXISTS customer_groups (
  id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO customer_groups (id, `name`) VALUES
(1, 'Retail');

DROP TABLE IF EXISTS group_access;
CREATE TABLE IF NOT EXISTS group_access (
  group_id int(11) NOT NULL,
  module_id int(11) NOT NULL,
  r int(1) NOT NULL DEFAULT '0',
  w int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (group_id,module_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO group_access (group_id, module_id, r, w) VALUES
(1, 6, 1, 1),
(1, 4, 1, 1),
(1, 5, 1, 1),
(1, 7, 1, 1);

DROP TABLE IF EXISTS key_people;
CREATE TABLE IF NOT EXISTS key_people (
  id int(11) NOT NULL AUTO_INCREMENT,
  fullname varchar(100) NOT NULL,
  category varchar(30) NOT NULL,
  photo varchar(60) NOT NULL,
  position varchar(60) NOT NULL,
  profile text NOT NULL,
  email varchar(60) DEFAULT NULL,
  phone varchar(20) DEFAULT NULL,
  createdby varchar(20) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  changedby varchar(20) DEFAULT NULL,
  changedat datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

INSERT INTO key_people (id, fullname, category, photo, `position`, `profile`, email, phone, createdby, createdat, changedby, changedat) VALUES
(98, 'Hendra Winata', '18', '8c61553f98ba35f1c1527b03a0b3e9ce.JPG', 'Managing Partner', '<p>Managing Partner</p>', 'hendra.winata@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 11:03:37', NULL, NULL),
(77, 'Lianty Widjaja', '19', '3979d9bdb2e83aa99982e5d8e42df975.JPG', 'Partner', '<p>Partner</p>', 'lianty.widjaja@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 09:42:16', NULL, NULL),
(81, 'Welly Adrianto', '19', '00f0d510b42449ea12514d71b4ea0799.JPG', 'Partner', '<p>Partner</p>', 'welly.adrianto@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 09:44:43', NULL, NULL),
(83, 'Iskariman Supardjo', '19', 'a2f00e21a26857e991323cf0d7b84495.JPG', 'Partner', '<p>Partner</p>', 'iskariman.supardjo@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 10:48:30', NULL, NULL),
(82, 'Razmal Muin', '19', 'a1bb8acf89bf8c30b7c7c547b45b83c7.JPG', 'Partner', '<p>Partner</p>', 'razmal.muin@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 10:47:53', NULL, NULL),
(99, 'Hanny Wurangian', '18', '5f3c2029d0848917b2c81527806af223.JPG', 'Deputy Managing Partner', '<p>Deputy Managing Partner</p>', 'hanny.wurangian@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 11:05:17', NULL, NULL),
(92, 'Lisa Novianty', '21', 'f33d11c7c57549ce5bf36cd38571dcdc.JPG', 'Partner', '<p>Partner</p>', 'lisa.novianty@kreston.co.id', '+62 61 455 7925', 'adminkreston', '2017-07-31 10:55:09', NULL, NULL),
(84, 'Fritz William W', '20', 'd0b4078f761f4f9b94a4884dcdbf0463.JPG', 'Partner', '<p>Partner</p>', 'fritz.williamw@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:49:37', NULL, NULL),
(85, 'Bpk. Chris Edward W', '20', 'a54b92133df3019f101ea3ee71786a96.JPG', 'Partner', '<p>Partner</p>', 'chris.edwardw@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:50:31', NULL, NULL),
(86, 'Go Bosse Gozali', '20', 'a8d1ef49783f826014042d431307849e.JPG', 'Partner', '<p>Partner</p>', 'bosse.gozali@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:51:03', NULL, NULL),
(87, 'Swandajani Sutjiadi', '20', '8e421cc6460263696c26f7673a70c3bd.JPG', 'Partner', '<p>Partner</p>', 'swandajani.s@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:51:46', NULL, NULL),
(90, 'Lilik Hartatik', '20', 'e06d08cb2512e9cc30ef2b935883a0b9.JPG', 'Partner', '<p>Partner</p>', 'lilik.h@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:53:45', NULL, NULL),
(88, 'Denny Megaliong', '20', 'edddc8fa5042dadddf36e5463ff4c988.JPG', 'Partner', '<p>Partner</p>', 'denny.megaliong@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:52:19', NULL, NULL),
(91, 'Razuar Yahya', '21', 'd7051ac77a2ce59d26852e7a0f6d56c8.JPG', 'Principle', '<p>Principle</p>', 'razuar.yahya@kreston.co.id', '+62 61 455 7925', 'adminkreston', '2017-07-31 10:54:19', NULL, NULL),
(89, 'Prianto Budi S.', '23', 'b341bffb70c6d2d141c035a6480b391b.JPG', 'Director', '<p>Director</p>', '-', '+62 21 2963 4945', 'adminkreston', '2017-07-31 10:53:07', 'adminkreston', '2017-07-31 13:58:07'),
(76, 'Arief Budiman', '22', 'f3a79a5fd33ff118866fce3353fbbbc1.JPG', 'Director', '<p>Director</p>', 'arief.budiman@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 09:41:37', NULL, NULL),
(100, 'Erwin Winata', '18', '3d8246cc76d59a2e7a71a82e5d48cb19.JPG', 'Internasional Liaison Partner', '<p>Internasional Liaison Partner</p>', 'erwin.winata@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 11:05:56', NULL, NULL),
(97, 'Robby Sumargo', '18', '3127f2e1433f11354d2632869d03b516.JPG', 'Deputy Managing Partner', '<p>Deputy Managing Partner</p>', 'robby.sumargo@kreston.co.id', '+62 61 455 7925', 'adminkreston', '2017-07-31 11:02:58', NULL, NULL);

DROP TABLE IF EXISTS mst_module;
CREATE TABLE IF NOT EXISTS mst_module (
  module_id int(3) NOT NULL AUTO_INCREMENT,
  module_name varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  module_path varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  module_desc varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (module_id),
  UNIQUE KEY module_name (module_name,module_path)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

INSERT INTO mst_module (module_id, module_name, module_path, module_desc) VALUES
(4, 'master', 'master', 'Manage your referential data that will be used by other modules'),
(6, 'system', 'system', 'Configure system behaviour, user and backups'),
(5, 'katalog', 'katalog', 'Transaksi katalog'),
(7, 'web', 'web', 'Backend for web portal');

DROP TABLE IF EXISTS news;
CREATE TABLE IF NOT EXISTS news (
  id int(11) NOT NULL,
  lang char(2) NOT NULL,
  image varchar(255) DEFAULT NULL,
  photo_by varchar(90) DEFAULT NULL,
  news_by varchar(20) DEFAULT NULL,
  place varchar(90) DEFAULT NULL,
  news_date date DEFAULT NULL,
  title varchar(150) NOT NULL,
  slugs varchar(255) NOT NULL,
  news_desc text,
  created_at varchar(10) DEFAULT NULL,
  changed_by varchar(10) DEFAULT NULL,
  changed_at datetime DEFAULT NULL,
  created_by datetime DEFAULT NULL,
  PRIMARY KEY (id,lang),
  UNIQUE KEY slugs (slugs)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO news (id, lang, image, photo_by, news_by, place, news_date, title, slugs, news_desc, created_at, changed_by, changed_at, created_by) VALUES
(3, 'id', 'cf0c6d127e204a50560fac1ff29a4d4a.jpg', 'Admin', 'Admin', 'Marrakech', '2017-05-25', 'Bergerak maju di Marrakech', 'bergerak-maju-di-marrakech', '<p>The 2017 Kreston EMEA Conference was held in Marrakech, Morocco where 105 delegates drawn from 27 countries were in attendance.</p>\r\n\r\n<p>Host firm, Kreston Exco Maroc presented an overview of their firm and the economy and investment opportunities in Morocco.</p>\r\n\r\n<p>The business programme included updates on the global network development and systems for the application of data analytics in audit and consultancy services. Lively debate took place in the international tax sessions around Brexit; the impact of BEPS in members&rsquo; respective countries and the implications of Trump&rsquo;s proposed tax changes.</p>\r\n\r\n<p>Kreston CEO, Jon Lisby said:<br />\r\n&ldquo;Another lively programme in a great location which provided exceptional opportunities for our members to network, discuss international referrals and opportunities &ndash; these conferences deliver significant value for all&rdquo;</p>\r\n\r\n<p>The 2018 EMEA Conference will be held in Zurich 10th &ndash; 13 May 2018</p>', '2017-06-01', NULL, NULL, '0000-00-00 00:00:00'),
(3, 'en', 'cf0c6d127e204a50560fac1ff29a4d4a.jpg', 'Admin', 'Admin', 'Marrakech', '2017-05-25', 'Moving forward in Marrakech', 'moving-forward-in-marrakech', '<p>The 2017 Kreston EMEA Conference was held in Marrakech, Morocco where 105 delegates drawn from 27 countries were in attendance.</p>\r\n\r\n<p>Host firm, Kreston Exco Maroc presented an overview of their firm and the economy and investment opportunities in Morocco.</p>\r\n\r\n<p>The business programme included updates on the global network development and systems for the application of data analytics in audit and consultancy services. Lively debate took place in the international tax sessions around Brexit; the impact of BEPS in members&rsquo; respective countries and the implications of Trump&rsquo;s proposed tax changes.</p>\r\n\r\n<p>Kreston CEO, Jon Lisby said:<br />\r\n&ldquo;Another lively programme in a great location which provided exceptional opportunities for our members to network, discuss international referrals and opportunities &ndash; these conferences deliver significant value for all&rdquo;</p>\r\n\r\n<p>The 2018 EMEA Conference will be held in Zurich 10th &ndash; 13 May 2018</p>', '2017-06-01', NULL, NULL, '0000-00-00 00:00:00'),
(5, 'id', '7b319e8c2ab254a65b9ff9c8a0fb4023.jpg', 'kreston.com', 'kreston.com', 'Glasgow', '2017-02-28', 'xxx', 'xxx', '<p>xxx</p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(6, 'en', 'e11a0885670d547ee04cc12e2396e66c.jpg', 'Kreston Reeves', 'Kreston Reeves', 'UK', '2016-12-23', 'MANUFACTURING AND BREXIT: CAUTIOUS OPTIMISM BUT….', 'manufacturing-and-brexit-cautious-optimism-but…', '<p>Rodney Sutton, head of manufacturing at Kreston Reeves, recently shared his thoughts on the UK manufacturing industry following the referendum vote.</p>\r\n\r\n<p>In his article, Sutton traces an overview of the market reaction to the decision by the British electorate on the 23rd June to exit the EU. He lists several effects following the referendum as Sterling devalues to its lowest level in 30 years against the dollar and the euro, UK manufactured goods become instantly more competitive, manufacturing outputs rose to their highest level in 25 years in August and raw material imports were predicted to become more expensive.</p>\r\n\r\n<p>Despite the initial tumultuousness in the market after Brexit, economic numbers showed that the British manufacturing sector is able to compete with German manufacturers as the sector &ldquo;has outperformed the productivity growth of the service sector in the 20 years prior to 2014&rdquo;, says Sutton.</p>\r\n\r\n<p>But trade with the EU is still very important. &ldquo;There is an air of cautious optimism across the sector. Most manufacturers echo the sentiment that EU customers and suppliers who trade with the UK will want this to continue as the trade is very significant, 54% of UK imports come from the EU and 47% of UK exports go to the EU. Would it be foolhardy to jeopardise this lucrative trading arrangement?&rdquo;, questions Sutton in his article.</p>\r\n\r\n<p>Come rain or shine, there is an air of uncertainty. Sutton lists three important issues that need to be clarified.</p>\r\n\r\n<p>&ndash; The Brexit government&rsquo;s ability to negotiate a free trade agreement with the EU and the looming spectre of immigration which could derail this process.<br />\r\n&ndash; Government consideration as to how to fund EU manufacturing- will this be replaced once this source of funding has been switched off?<br />\r\n&ndash; Will the UK government support and drive the manufacturing sector?</p>\r\n\r\n<p>According to Sutton, it is very important to keep a free trade environment between the UK and the EU, but things need time to be settle. &ldquo;UK manufacturing needs to recognise that any deal to negotiate free trade status in the EU will be lengthy, face many hiccups along the way and ultimately will need the agreement of all 28 EU members&rdquo;.</p>\r\n\r\n<p>Clearly there are big challenges ahead.</p>\r\n\r\n<p>Source:&nbsp;<a href=\"http://www.krestonreeves.com/news-and-events/15/11/2016/manufacturing-and-brexit-cautious-optimism-but\" target=\"_blank\">Kreston Reeves</a></p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(4, 'en', 'eb185081d8fa3e06776cebbadad4c131.png', 'Kreston.com', 'Kreston.com', 'Madrid', '2017-03-21', 'Kreston welcomes new member MG Madrid & Company, Philippines.', 'kreston-welcomes-new-member-mg-madrid--company-philippines', '<p>Kreston International is pleased to announce that MG Madrid &amp; Co, in the Philippines, has joined the global membership, adding to its offering in the Asia Pacific region. Established in 2003, this two Partner firm offers audit, tax and consulting services from its head office in Makati City, Manila and has over 70 professional and support staff. A second branch office is in Muntinlupa City.</p>\r\n\r\n<p>Jon Lisby, Kreston CEO, said:<br />\r\n&ldquo;We have been looking for a quality firm to represent Kreston in the Philippines and we are delighted that MG Madrid &amp; Company have agreed to join the global network, adding to our coverage in Asia and to support our members in the region&rdquo;</p>\r\n\r\n<p>Partner Mario G. Madrid commented:<br />\r\n&ldquo;It is an honour to join Kreston International. Our values, integrity, professionalism and commitment, are very much aligned with those of the network and we look forward to working with colleagues both across Kreston Asia and the global membership to further promote the Kreston brand in the region.&rdquo;</p>\r\n\r\n<p><em>Picture features left to right, Partners, Mr. Benedict A. Ang Ban Giok and Mr. Mario G. Madrid Jr.</em></p>', '2017-06-08', 'admin', '2017-06-08 19:46:48', '0000-00-00 00:00:00'),
(4, 'id', 'eb185081d8fa3e06776cebbadad4c131.png', 'Kreston.com', 'Kreston.com', 'Madrid', '2017-03-21', 'xxxx', 'xxxx', '<p>xxxx</p>', '2017-06-08', 'admin', '2017-06-08 19:46:48', '0000-00-00 00:00:00'),
(5, 'en', '7b319e8c2ab254a65b9ff9c8a0fb4023.jpg', 'kreston.com', 'kreston.com', 'Glasgow', '2017-02-28', 'KRESTON UK COLLABORATE IN GLASGOW.', 'kreston-uk-collaborate-in-glasgow', '<p>100 delegates from Kreston firms across the UK and Ireland attended the 2017 Conference at the Hilton Hotel in Glasgow to share best practice and to discuss developments across several specialist sectors including Audit, Corporate Tax, IT, Marketing, Probate and Private Client; Managing Partners, HR and Advisory and Outsourcing.</p>\r\n\r\n<p>Kreston CEO Jon Lisby, who provided an update on global developments and brand strategy, commented:<br />\r\n&ldquo;Thank you to local member&nbsp;<a href=\"http://consiliumca.com/\" target=\"_blank\">Consilium</a>&nbsp;for hosting the event this year, the organisation was flawless. The feedback from each of the group sessions again testifies to the value of the event and the outcome sees a further strengthening of future collaboration and client service. With the organic growth and mergers achieved over the past year, Kreston UK, as a combined organisation would now rank as the 10th largest UK accounting firm with over 2,400 professional and support staff generating fee revenues of US$200m&rdquo;.</p>\r\n\r\n<p>John Warner, Chairman of Kreston UK and Managing Partner of BHP (North of England) concluded the business sessions and the conference ended with dinner at famous House for an Art Lover, designed by Mackintosh, Glasgow&rsquo;s most famous architect.</p>\r\n\r\n<p>The 2018 conference will take place in Jersey.</p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(7, 'en', '1a8a859e6e65b5719c464d5a1930b4e8.jpg', 'kreston.com', 'kreston.com', 'UK', '2017-06-07', 'PROGRESSION IN SETTING GLOBAL STANDARDS FOR NOT-FOR-PROFIT ORGANISATIONS', 'progression-in-setting-global-standards-for-notforprofit-organisations', '<p>The adoption of international accounting standards has brought many benefits to the private and public sectors. This being said, there is still progress to be made in the not-for-profit (NFP) sector.</p>\r\n\r\n<p>In an excellent article by,&nbsp;<a href=\"http://www.publicfinance.co.uk/opinion/2016/11/not-profit-standards-go-global\" target=\"_blank\">Easton Bilsborough</a>, technical manager for charities and NFP sectors at the Chartered Institute of Public Finance &amp; Accounting (CIPFA), Bilsborough talks about issues facing the NFP sector and points out the benefits of adopting accounting standards.</p>\r\n\r\n<p>According to Bilsborough, a recent study by the Consultative Committee of Accountancy Bodies (CCAB) on financial reporting by NFP, showed organisations in this sector, in at least 179 countries, having problems related to accounting for non-exchange transactions; fund accounting; income recognition; gifts in kind; mergers; treatment of branches; and reporting on reserves. In addition, the absence of global standards prevented the attainment of high-quality standards within individual jurisdictions.</p>\r\n\r\n<p>Accounting in the NFP sector has made slow progress and according to Bilsborough, the International Forum of Accounting Standard Setters has formed a working group to build a database of practice to help move forward NFP reporting across the globe.</p>\r\n\r\n<p>Achieving international accounting standards would bring several benefits to the NFP sector. It would improve the quality of reporting and make charities more transparent, making it clear for support from donors and funders. Also, with consistent reporting, the not-for-profit sector would be better understood.</p>\r\n\r\n<p>Many NFP entities work across a number of jurisdictions and the development of standards that are accepted internationally will be of significant benefit to them. There is however, a long way to go until these standards are ready to be implemented.</p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(7, 'id', '1a8a859e6e65b5719c464d5a1930b4e8.jpg', 'kreston.com', 'kreston.com', 'UK', '2017-06-07', 'eee', 'eee', '<p>eee</p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(8, 'en', '2c5f1de57566d376ceafe685a499b22d.jpg', 'IFRS.org', 'IFRS.org', 'Saudi', '2016-12-07', 'SAUDI ARABIA MOVES TOWARDS IFRS STANDARDS', 'saudi-arabia-moves-towards-ifrs-standards', '<p>A common global language for business affairs, IFRS Standards are required by more than 120 jurisdictions in the world.</p>\r\n\r\n<p>Recently, Saudi Arabia signed a license agreement with the International Financial Reporting Standards (IFRS) Foundation to stamp its intention to incorporate IFRS Standards into its legal framework. This means that the Saudi organisation for Certified Public Accountants will have the right to publish the official Arabic translation of IFRS Standards and the IFRS for SMEs Standard for the purpose of adoption into law.</p>\r\n\r\n<p>In 2017, banks and insurance companies will be required to use IFRS Standards, with listed companies. From 2018, other companies will be required to use the IFRS for SMEs Standard.</p>\r\n\r\n<p>The wider adoption of IFRS in Saudi Arabia will help drive the quality and comparability of financial information. These rules bring transparency, enabling investors and other market participants to make informed economic decisions. It is important that entities impacted by the change start to plan in good time and talk to their professional adviser early in the process.</p>\r\n\r\n<p><em>Source:&nbsp;<a href=\"http://www.ifrs.org/Alerts/PressRelease/Pages/Saudi-Arabia-to-publish-ifrs-Standards-for-adoption-purposes.aspx\">IFRS.org</a></em></p>\r\n\r\n<p>A common global language for business affairs, IFRS Standards are required by more than 120 jurisdictions in the world.</p>\r\n\r\n<p>Recently, Saudi Arabia signed a license agreement with the International Financial Reporting Standards (IFRS) Foundation to stamp its intention to incorporate IFRS Standards into its legal framework. This means that the Saudi organisation for Certified Public Accountants will have the right to publish the official Arabic translation of IFRS Standards and the IFRS for SMEs Standard for the purpose of adoption into law.</p>\r\n\r\n<p>In 2017, banks and insurance companies will be required to use IFRS Standards, with listed companies. From 2018, other companies will be required to use the IFRS for SMEs Standard.</p>\r\n\r\n<p>The wider adoption of IFRS in Saudi Arabia will help drive the quality and comparability of financial information. These rules bring transparency, enabling investors and other market participants to make informed economic decisions. It is important that entities impacted by the change start to plan in good time and talk to their professional adviser early in the process.</p>\r\n\r\n<p><em>Source:&nbsp;<a href=\"http://www.ifrs.org/Alerts/PressRelease/Pages/Saudi-Arabia-to-publish-ifrs-Standards-for-adoption-purposes.aspx\">IFRS.org</a></em></p>\r\n\r\n<p>&nbsp;</p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(9, 'en', '9a03efbc88b730405918fbc47b329d7c.jpg', 'kreston.com', 'kreston.com', 'England', '2017-07-20', 'Kreston admits Kreston HHES, Indonesia', 'kreston-admits-kreston-hhes-indonesia', '<p>Kreston International has announced that Kreston HHES (legal name KAP Hendrawinata Hanny Erwin &amp; Sumargo) has been admitted as a member. The firm was created following a restructuring of Kreston&rsquo;s former member which provides the added benefit that the partners and staff are well known within the global network having moved to Kreston from Grant Thornton in 2011.</p>\r\n\r\n<p>Kreston HHES now has 16 partners and 250 professional and support staff operating from excellent offices in Jakarta Surabaya and Medan. The firm is a full service practice providing audit and taxation services with advisory and consultancy services handed by PT Kreston Advisory Indonesia and Tax and Transfer Pricing by PT Pratama Indomitra Konsultan.</p>\r\n\r\n<p>Kreston CEO, Jon Lisby said &ldquo;We are delighted to re-establish our representation in Indonesia with Kreston HHES, a firm that has an exceptional reputation for the quality of services to international clients. The future growth of Indonesia on the global stage makes it essential for us to have secure the strongest representation available. Kreston members from throughout the Asia Pacific Region look forward to meeting up with Kreston HHES at the Kreston Regional Conference in Kuala Lumpur early next month.</p>\r\n\r\n<p>Erwin Winata, International Liaison Partner for Kreston HHES said &ldquo; We are proud to represent Kreston in Indonesia and we look forward to building the strength of the brand in the region together with our relationships with fellow Kreston Members and their clients from throughout the world.</p>\r\n\r\n<p><em>Image left to right:&nbsp;Erwin Winata of Kreston HHES, Nigel Banks Kreston International Director, Chris Edward of Kreston HHES, Jon Lisby Kreston CEO, Edomd Chan Kreston International Director, Sunil Goyal Kreston International Director.</em></p>', '2017-07-21', NULL, NULL, '0000-00-00 00:00:00');

DROP TABLE IF EXISTS pages;
CREATE TABLE IF NOT EXISTS pages (
  id int(9) NOT NULL AUTO_INCREMENT,
  parent_id int(9) NOT NULL,
  title varchar(128) NOT NULL,
  menu_title varchar(128) NOT NULL,
  slug varchar(255) NOT NULL,
  content longtext NOT NULL,
  sequence int(11) NOT NULL DEFAULT '0',
  seo_title text NOT NULL,
  meta text NOT NULL,
  url varchar(255) NOT NULL,
  new_window tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO pages (id, parent_id, title, menu_title, slug, content, sequence, seo_title, meta, url, new_window) VALUES
(1, 0, 'Hubungi Kami', 'Hubungi Kami', 'contact-us', 'Lorem ipsum dolor sit amet, ignota latine salutatus mel no, labitur senserit at nec. Per ad altera aperiri pertinax, mel omnium democritum ut, ocurreret consequat adolescens vix in. Usu iudico maiorum eu, eum ei eius iracundia sadipscing, reque assueverit in sed. Nisl aeque virtute in sit, sed no nibh phaedrum, ea vis elitr saperet singulis.<br><br>Sit id aeque deterruisset. Eruditi disputationi vix no. At fugit decore invenire mei, ei eam inciderint scribentur, per epicurei urbanitas eu. In vis numquam splendide. Sed at viderer epicuri laboramus, mel eros perfecto legendos ea.<br><br>Ignota animal quo id, elit iisque sea ad. Est hinc vero conceptam an, ex ius amet audiam impetus. Ea elitr expetendis suscipiantur vim, aliquam consectetuer vim cu. Nonumy repudiare maiestatis ea per. Te duo exerci impetus, vim odio referrentur an. Eum et exerci voluptatum definitiones.<br><br>Ex zril vidisse eos, vis at viderer persius intellegam. Ius id mentitum repudiare vulputate, est ne delenit ceteros, ius eu labores praesent. Vim vocent gloriatur ne, antiopam sapientem mel at, at utinam minimum dissentiet nec. No cum recusabo eleifend persequeris. Ne mei sumo partem appareat.<br><br>His ex utinam cotidieque, in eos sale iisque. His ex senserit temporibus, id usu habeo altera pericula. Pro assum officiis eu. Et vero vivendo deleniti cum, vis novum nullam maiestatis ut, duo graeco omnium labores ne. Et vero scribentur vim, et mutat graece fastidii pri, cu zril ceteros perfecto sea.', 3, 'contact-us', '', '', 0),
(3, -1, 'Beranda', 'Beranda', 'beranda', '{{ banner(\'1\', \'5\', \'default\') }}\r\n<br><br>\r\nSelamat Datang di  Griyagaya..<br><br>Kami menyediakan berbagai macam jenis fashion berkualitas tinggi untuk segala kebutuhan penampilan anda. Mulai dari baju, celana, <br>jaket dan lainnya.<br><br>Berkonsep butik online, kami berusaha memanjakan sekaligus memudahkan para customer untuk berbelanja di toko online kami. <br>Kenyamanan dan keamanan pun kami jaga sehingga tercipta hubungan baik penjual dan pembeli.<br><br><br>Selamat berbelanja. Dan terima kasih atas kunjungan anda…<br><br>', 1, 'beranda', '', '', 0),
(4, 0, 'Cara Pembelian', 'Cara Pembelian', 'cara-pembelian', '<ul><li>Klik pada tombol Beli pada produk yang ingin Anda pesan.</li></ul><ul><li>Produk yang Anda pesan akan masuk ke dalam Keranjang Belanja. Anda dapat melakukan perubahan jumlah produk yang diinginkan dengan mengganti angka di kolom Jumlah, kemudian klik tombol Update. Sedangkan untuk menghapus sebuah produk dari Keranjang Belanja, klik tombol Kali yang berada di kolom paling kanan.</li></ul><ul><li>Jika sudah selesai, klik tombol Selesai Belanja, maka akan tampil form untuk pengisian data kustomer/pembeli.</li></ul><ul><li>Setelah data pembeli selesai diisikan, klik tombol Proses, maka akan tampil data pembeli beserta produk yang dipesannya (jika diperlukan catat nomor ordernya). Dan juga ada total pembayaran serta nomor rekening pembayaran.</li></ul><ul><li>Apabila telah melakukan pembayaran, maka produk/barang akan segera kami kirimkan.</li></ul>', 2, 'cara-pembelian', '', '', 0),
(5, 0, 'Profil', 'Profil', 'profil', 'ArtFashion  adalah toko fashion online, yang menyediakan segala kebutuhan fashion anda. ArtFashion ingin memberikan kemudahan kepada para calon pembeli, cara santai, mudah dan hemat dalam berbelanja fashion berkualias dengan harga yang terjangkau.<br><br>Karena itulah website ini dibuat sedemikian sederhananya sehingga diharapkan dapat membantu para pengunjung untuk dapat menelusuri produk-produk yang ditawarkan secara lebih mudah.<br><br>Selain melayani pesanan produk-produk yang ada di toko online, kami menerima pembuatan/pemesanan fashion sesuai design/pola  yang anda inginkan.<br><br>Akhirnya, kami mengucapkan terima kasih atas kunjungan anda di ArtFashion.', 1, 'profil', '', '', 0);

DROP TABLE IF EXISTS search;
CREATE TABLE IF NOT EXISTS search (
  code varchar(40) NOT NULL,
  term varchar(255) NOT NULL,
  PRIMARY KEY (code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO search (`code`, term) VALUES
('0a19a86f8796ede1c213d0395a99c2db', '{\"term\":\"Kreston\"}'),
('0cfceb4d515f07ead2af171430dddb16', '{\"variable\":\"7\"}'),
('167d0702bcaaf66acaeadf71985e0871', '{\"variable\":\"MESSAGE_CATEGORY\"}'),
('20f9a662afd9ff58131fa507bac787ea', '{\"term\":\"saa\"}'),
('3d1ce1034bc2826ca7209dafa483e447', '{\"term\":\"danang\"}'),
('519ab11f3d236175bbdba22ae8d6b79e', '{\"term\":\"MARR\"}'),
('525cceb6438bcae61bc58f5d34556ccf', '{\"term\":\"test\"}'),
('53f10223d8f20fec9ed7f9d7a1a8620b', '{\"variable\":\"MESSAGE_TYPE\"}'),
('599f84a63b53153da13c0d654a9db66a', '{\"variable\":\"1\"}'),
('5ce126e4e69a4d1dc7c58d791617312a', '{\"variable\":\"\"}'),
('5cf1793d6602fe097468678cd71a866e', '{\"variable\":\"4\"}'),
('5db4e0e4c4687bbe9e83efdcbc5241ec', '{\"term\":\"marr\"}'),
('68dcd64065d817c113562effdfeeecbc', '{\"variable\":\"PEOPLE\"}'),
('7a91ed7b4d3a1818a63fe4465a2a0674', '{\"term\":\"Phili\"}'),
('8061e2abab24fe377fd6c71d231ff8a0', '{\"variable\":\"MESSAGE_SENDER\"}'),
('8bd31fc762e7fef2abee89619d18bff8', '{\"term\":\"career\"}'),
('967510225f65978c5e77760a5b7a173a', '{\"term\":\"tes\"}'),
('d56b8adf596f3b95fb670b429d0e8b3d', '{\"term\":\"phliphines\"}'),
('e59e2e8956462e6150ab7f6b0958b5db', '{\"term\":\"\"}');

DROP TABLE IF EXISTS settings;
CREATE TABLE IF NOT EXISTS settings (
  id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  setting_key varchar(255) DEFAULT NULL,
  setting longtext,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

INSERT INTO settings (id, `code`, setting_key, setting) VALUES
(1, 'cms', 'theme', 'alphasquad'),
(2, 'cms', 'locale', 'id_ID'),
(3, 'cms', 'currency_iso', 'IDR'),
(4, 'cms', 'new_customer_status', NULL),
(7, 'cms', 'default_meta_description', 'Kreston Indonesia'),
(8, 'cms', 'default_meta_keywords', 'Kreston Indonesia'),
(9, 'cms', 'timezone', 'Asia/Jakarta'),
(10, 'cms', 'company_name', ''),
(11, 'cms', 'homepage', NULL),
(12, 'cms', 'email_to', 'danang.alvaris@kreston.co.id'),
(13, 'cms', 'email_from', 'no-reply@kreston.co.id'),
(14, 'cms', 'email_method', 'smtp'),
(15, 'cms', 'smtp_server', 'mail.kreston.co.id'),
(16, 'cms', 'smtp_port', '25'),
(17, 'cms', 'smtp_username', 'danang.alvaris@kreston.co.id'),
(18, 'cms', 'smtp_password', '%^oax8Bahrb1'),
(21, 'cms', 'address1', ''),
(22, 'cms', 'address2', ''),
(23, 'cms', 'city', ''),
(25, 'cms', 'zip', ''),
(26, 'cms', 'stage_username', ''),
(27, 'cms', 'stage_password', ''),
(31, 'cms', 'ssl_support', '0'),
(32, 'cms', 'require_login', '0'),
(36, 'cms', 'enabled', '0'),
(40, 'cms', 'school_code', 'K');

DROP TABLE IF EXISTS slider;
CREATE TABLE IF NOT EXISTS slider (
  id int(11) NOT NULL AUTO_INCREMENT,
  image varchar(60) DEFAULT NULL,
  createdat varchar(20) DEFAULT NULL,
  createdby datetime DEFAULT NULL,
  changedat varchar(20) DEFAULT NULL,
  changedby datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS user;
CREATE TABLE IF NOT EXISTS `user` (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  realname varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  password varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  user_type smallint(2) DEFAULT NULL,
  user_image varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  social_media text COLLATE utf8_unicode_ci,
  nip varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  last_login datetime DEFAULT NULL,
  last_login_ip char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  groups varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  input_date date DEFAULT '0000-00-00',
  last_update date DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY username (username),
  KEY realname (realname)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (id, username, realname, `password`, email, user_type, user_image, social_media, nip, last_login, last_login_ip, groups, input_date, last_update) VALUES
(1, 'admin', 'Admin', '04275db24ad3070dc85c2d959640958d5d5a2901', 'webmaster@gmail.com', NULL, 'login.png', NULL, '197034', '2017-07-25 16:04:17', '118.97.70.76', 'a:1:{i:0;s:1:\"1\";}', '2015-11-11', '2017-07-25'),
(2, 'adminkreston', 'Administrator', '879711da7e576e934f4a7f11f6fc96a63dc720cd', 'webmaster@kreston.co.id', 1, 'login1.png', NULL, '99999', '2017-07-31 13:26:26', '118.97.70.76', 'a:1:{i:0;s:1:\"1\";}', '2017-07-25', NULL);

DROP TABLE IF EXISTS user_group;
CREATE TABLE IF NOT EXISTS user_group (
  group_id int(11) NOT NULL,
  group_name varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  input_date date DEFAULT NULL,
  last_update date DEFAULT NULL,
  PRIMARY KEY (group_id),
  UNIQUE KEY group_name (group_name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO user_group (group_id, group_name, input_date, last_update) VALUES
(1, 'Administrator', '2015-11-11', '2017-05-02'),
(2, 'Operator', '2015-11-18', '2015-11-18');

DROP TABLE IF EXISTS vacancy;
CREATE TABLE IF NOT EXISTS vacancy (
  id int(11) NOT NULL AUTO_INCREMENT,
  division varchar(20) NOT NULL,
  position varchar(150) DEFAULT NULL,
  heading text,
  requirements text,
  responsibilities text,
  description text,
  createdby varchar(20) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  updatedby varchar(20) DEFAULT NULL,
  updatedat datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO vacancy (id, division, `position`, heading, requirements, responsibilities, description, createdby, createdat, updatedby, updatedat) VALUES
(3, 'Audit', 'Senior Auditor', '<p>A challenging yet rewarding career with work life balance await you. Cheerfulness, a sense of togetherness, loyalty and sharing a sense of belongingness in our company and become an asset to us as we carry out our mission to ultimately achieve our vision. So&nbsp;for those of you who feel challenged and interested please join us and have a fulfilling career.</p>\r\n', '<ol>\r\n	<li>Bachelor or Master Degree in accounting with a minimum GPA of 3.00 (out of 4.00) from a reputable university, Registered Accountant would be preferable.</li>\r\n	<li>Has a relevant working experience as an external auditor in Public Accounting Firms with min. 3 years for Senior.</li>\r\n	<li>Strong knowledge and good understanding in Indo GAAP (PSAK), SPAP, IFRS, ISA.</li>\r\n	<li>Professional integrity, good character, pleasant personality, energetic, proactive.</li>\r\n	<li>Good leadership and communication skill, English fluently is highly desirable.</li>\r\n	<li>Strong analytical skill, meticulous, easy and good adaptable.</li>\r\n	<li>Familiar with computer is a must and others accounting software will be an advantage. &nbsp;</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Prepare related audit examination</li>\r\n	<li>Develop the concept of audit examination</li>\r\n	<li>Able to follow the examination assigment</li>\r\n</ol>\r\n', '<p>Please mark the applied position on the top left corner of the envelope, and send your Comprehensive CV completed with relevant documents and recent photograph to :</p>\r\n\r\n<p><strong>HRD Manager</strong></p>\r\n\r\n<p><strong>KAP Hendrawinata Hanny Erwin &amp; Sumargo - Kreston</strong></p>\r\n\r\n<p><strong>Intiland Tower 18th Floor, Jl. Jend. Sudirman Kav. 32, Jakarta 10220</strong></p>\r\n\r\n<p><strong>or</strong>&nbsp;<strong>Email to : </strong><a href=\"mailto:hrd.jakarta@kreston.co.id\">hrd.jakarta@kreston.co.id</a></p>\r\n', 'adminkreston', '2017-07-27 13:38:29', 'adminkreston', '2017-07-27 16:01:35');

DROP TABLE IF EXISTS variable;
CREATE TABLE IF NOT EXISTS variable (
  id smallint(6) NOT NULL AUTO_INCREMENT,
  variable varchar(30) DEFAULT NULL,
  value varchar(100) DEFAULT NULL,
  description varchar(50) DEFAULT NULL,
  createdby varchar(12) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  changedby varchar(12) DEFAULT NULL,
  changedat datetime DEFAULT NULL,
  orderby tinyint(4) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

INSERT INTO variable (id, variable, `value`, description, createdby, createdat, changedby, changedat, orderby) VALUES
(1, 'MESSAGE_SENDER', 'CUSTOMER', 'CUSTOMER', NULL, NULL, NULL, NULL, NULL),
(2, 'MESSAGE_SENDER', 'BRANCH', 'BRANCH', NULL, NULL, NULL, NULL, NULL),
(3, 'MESSAGE_SENDER', 'THIRD_PARTY', 'THIRD PARTY', NULL, NULL, NULL, NULL, NULL),
(4, 'MESSAGE_TYPE', 'QUESTION', 'QUESTION', NULL, NULL, NULL, NULL, NULL),
(5, 'MESSAGE_TYPE', 'REQUEST', 'REQUEST', NULL, NULL, NULL, NULL, NULL),
(6, 'MESSAGE_TYPE', 'COMPLAIN', 'COMPLAIN', NULL, NULL, NULL, NULL, NULL),
(7, 'MESSAGE_CATEGORY', 'PRODUCT', 'PRODUCT', NULL, NULL, NULL, NULL, NULL),
(8, 'MESSAGE_CATEGORY', 'SERVICES', 'SERVICES', NULL, NULL, NULL, NULL, NULL),
(9, 'MESSAGE_CATEGORY', 'GENERAL', 'GENERAL', NULL, NULL, NULL, NULL, NULL),
(10, 'MESSAGE_CATEGORY', 'CAREER', 'CAREER', NULL, NULL, NULL, NULL, NULL),
(11, 'MESSAGE_CATEGORY', 'EVENT', 'EVENT', NULL, NULL, NULL, NULL, NULL),
(12, 'MESSAGE_CATEGORY', 'OTHER', 'OTHER', NULL, NULL, NULL, NULL, NULL),
(26, 'PEOPLE', 'QUALITY', 'QUALITY CONTROL', 'adminkreston', '2017-07-31 13:31:48', NULL, NULL, 5),
(18, 'PEOPLE', 'PARTNER', 'Board of Partner', 'adminkreston', '2017-07-27 09:15:18', 'adminkreston', '2017-07-31 13:26:57', 1),
(19, 'PEOPLE', 'JAKARTA', 'JAKARTA', 'adminkreston', '2017-07-27 09:17:35', NULL, NULL, 2),
(20, 'PEOPLE', 'SURABAYA', 'SURABAYA', 'adminkreston', '2017-07-27 09:18:02', NULL, NULL, 3),
(21, 'PEOPLE', 'MEDAN', 'MEDAN', 'adminkreston', '2017-07-27 09:18:16', NULL, NULL, 4),
(22, 'PEOPLE', 'KAI', 'PT. Kreston Advisory Indonesia', 'adminkreston', '2017-07-27 09:19:39', NULL, NULL, 6),
(23, 'PEOPLE', 'PRATAMA', 'PT. Pratama Indomitra', 'adminkreston', '2017-07-27 09:20:01', NULL, NULL, 7);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
