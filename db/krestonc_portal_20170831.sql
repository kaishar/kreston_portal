SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DROP TABLE IF EXISTS affiliation;
CREATE TABLE IF NOT EXISTS affiliation (
  id int(11) NOT NULL AUTO_INCREMENT,
  company_name varchar(60) NOT NULL,
  image varchar(60) NOT NULL,
  descriptions text NOT NULL,
  address1 varchar(200) NOT NULL,
  address2 varchar(200) DEFAULT NULL,
  address3 varchar(200) DEFAULT NULL,
  email varchar(60) NOT NULL,
  phone varchar(20) NOT NULL,
  website varchar(100) DEFAULT NULL,
  createdby varchar(20) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  updatedby varchar(20) DEFAULT NULL,
  updatedat datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO affiliation (id, company_name, image, descriptions, address1, address2, address3, email, phone, website, createdby, createdat, updatedby, updatedat) VALUES
(6, 'PT. Kreston Advisory Indonesia', '74bc1a89f595d42ae1e529f5a92b416d.jpg', '<p>We provide Financial Advisory Services, Risk Management and Strategic Management, catering to companies in various industries, which ranges for SMEs to medium sized and large companies.</p>\r\n\r\n<p><strong>We know our clients. </strong></p>\r\n\r\n<p>We customize our procedures and detail reports which are planned and executed according to each entity&rsquo;s specific needs.</p>\r\n\r\n<p><strong><em>Our Services :</em></strong></p>\r\n\r\n<p><strong>A. Financial Advisory Services</strong></p>\r\n\r\n<ul>\r\n	<li>Accounting &amp; Transactional Services.</li>\r\n	<li>Financial Due Diligence.</li>\r\n	<li>Management Advisory Services.</li>\r\n</ul>\r\n\r\n<p><strong>B. Risk Management</strong></p>\r\n\r\n<ul>\r\n	<li>Systems and internal control reviews.</li>\r\n	<li>Internal audit and other compliance services.</li>\r\n	<li>Fraud prevention review and investigations.</li>\r\n	<li>Forensic accounting.</li>\r\n</ul>\r\n\r\n<p><strong>C. Consulting Services</strong></p>\r\n\r\n<ul>\r\n	<li>Corporate Finance.</li>\r\n	<li>IT advisory and Solutions.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', 'Intiland Tower 18th Floor', ' Jl. Jend. Sudirman Kav. 32, Jakarta 13220, Indonesia.', '', 'advisory@kreston.co.id', '+62 21 5712000', 'http://www.kreston.co.id', 'adminkreston', '2017-07-25 16:27:49', 'adminkreston', '2017-08-22 09:10:59'),
(4, 'PT. Pratama Indomitra', '3672f2fc89e2473ba3c1faf04ad57539.jpg', '<p>Pratama Indomitra is a tax consultant who serves both corporate and private clients who need help in good tax management. Consisting of consultants who have expertise as well as extensive experience in the field of taxation, have high integrity, and uphold the values of business ethics.</p>', 'Antam Office Park Tower B Lantai 8', 'Jl. T.B. Simatupang No.1,Jakarta 12530, Indonesia.', '', 'info.pratama@kreston.co.id', '+62 21 2963 4945', 'http://www.pratamaindomitra.co.id', 'adminkreston', '2017-07-25 14:38:34', 'adminkreston', '2017-08-15 10:45:16');

DROP TABLE IF EXISTS branch;
CREATE TABLE IF NOT EXISTS branch (
  id int(11) NOT NULL AUTO_INCREMENT,
  branch_name varchar(255) DEFAULT NULL,
  address1 varchar(255) DEFAULT NULL,
  address2 varchar(255) DEFAULT NULL,
  city varchar(60) DEFAULT NULL,
  phone varchar(50) DEFAULT NULL,
  fax varchar(50) DEFAULT NULL,
  photo varchar(100) DEFAULT NULL,
  latitude double DEFAULT NULL,
  longitude double DEFAULT NULL,
  pic_email varchar(60) DEFAULT NULL,
  pic_name varchar(60) DEFAULT NULL,
  createdby varchar(20) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  changedby varchar(20) DEFAULT NULL,
  changedat datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

INSERT INTO branch (id, branch_name, address1, address2, city, phone, fax, photo, latitude, longitude, pic_email, pic_name, createdby, createdat, changedby, changedat) VALUES
(2, 'HHES Jakarta', 'Intiland Tower 18th Floor Jl.Jend. Sudirman Kav.32', 'Jakarta 10220, Indonesia', 'Jakarta', '+62 21 571 2000', '+62-21 570 6118 | +62-21 571 1818', '86139ee77fda3f76d56dc758b9abb225.jpg', -6.2143954, 106.8173792, 'hhes.jakarta@kreston.co.id', 'HHES Jakarta', 'admin', '2017-05-29 13:52:25', 'adminkreston', '2017-08-04 16:02:30'),
(3, 'HHES Medan', 'Kreston Building Jl.Palang Merah No. 40', 'Medan 20111, Indonesia', 'Medan', '+62-61 455 7925 | +62-61 415 7295', '+62-61 451 3159', '09c2134f45c177abb0affe28b1e1d083.jpg', 3.5843478, 98.6795034, 'hhes.medan@kreston.co.id', 'HHES Medan', 'admin', '2017-05-29 13:54:27', 'adminkreston', '2017-08-04 16:07:48'),
(4, 'HHES Surabaya', 'Jl. Raya Gubeng No. 56', ' Gubeng 60281', 'Surabaya', '+62-31 5035046 | +62-31 5032289 | +62-31 5016879', '+62-31 5035689', '1ea073b7c6d6156e8c77f0953426a9b3.png', -7.2724893, 112.7471915, 'hhes.surabaya@kreston.co.id', 'HHES Surabaya', 'admin', '2017-05-29 13:56:00', 'adminkreston', '2017-08-04 16:00:34'),
(9, 'PT. Kreston Advisory Indonesia', 'Intiland Tower 18 Floor Jl. Jend. Sudirman Kav.32', 'Jakarta 10220, Indonesia', 'Jakarta', '+62-21 571 2000', '+62-21 570 6118 | +62-21 571 1818', '0bc5a376d9ed5a6498dfd71090876a8b.jpg', -6.2143954, 106.8173792, 'advisory@kreston.co.id', 'PT. Kreston Advisory Indonesia', 'admin', '2017-06-01 22:45:35', 'adminkreston', '2017-08-22 09:12:43'),
(13, 'PT. Pratama Indomitra', 'Antam Office Park Tower B Lantai 8, Jl. T.B. Simatupang No.1', 'Jakarta', 'Jakarta', '+62-21 2963 4945', '+62-21 2963 4946', '0100bcb0dbb04af39a623429000c3833.jpg', -6.3019476, 106.8426189, 'info.pratama@kreston.co.id', 'PT. Pratama Indomitra', 'adminkreston', '2017-07-25 13:39:37', 'adminkreston', '2017-08-22 09:12:29');

DROP TABLE IF EXISTS canned_messages;
CREATE TABLE IF NOT EXISTS canned_messages (
  id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  deletable tinyint(1) NOT NULL DEFAULT '1',
  type enum('internal','order') NOT NULL,
  name varchar(50) DEFAULT NULL,
  subject varchar(100) DEFAULT NULL,
  content text NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO canned_messages (id, deletable, `type`, `name`, `subject`, content) VALUES
(1, 1, 'internal', 'contaus', 'Contact Us Information', '<html><head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=us-ascii\"> <meta name=\"designer\" content=\"alphasquad inc.\"> <style>body{font-family: Arial, Helvetica, sans-serif;}table{width:768px;margin:0 auto;}</style></head><body style=\"margin:10px auto;\"><table style=\"margin-bottom: 25px;\"> <thead> <tr> <td width=\"48%\"><a href=\"http://kreston.co.id/\" style=\"color: #00a0d6; font-size: 18px; text-decoration: none;\">kreston.co.id</a> </td><td width=\"4%\">&nbsp;</td><td width=\"48%\" align=\"right\" style=\"text-align:right\"> <img align=\"right\" style=\"width: 70px; margin-right: 30px;margin-left: 50%;\" src=\"http://kreston.co.id/beta/themes/alphasquad/storage/img/logoicon/krestonlogo.png\"> </td></tr></thead> </table> <table id=\"alphasquad\" style=\"background-color: #b3bdbe; padding: 20px;\"> <thead> <tr> <th colspan=\"3\" style=\"padding-bottom: 30px;\">Contact Us Information :</th> </tr></thead> <tbody> <tr> <td width=\"40%\" style=\"padding-bottom: 15px;\"><small>Full Name</small></td><td width=\"1%\" style=\"padding-bottom: 15px;\">:</td><td width=\"59%\" style=\"padding-bottom: 15px;\"><strong>{{fullname}}</strong></td></tr><tr> <td width=\"40%\"><small>Email Address</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{email}}</strong></td></tr><tr> <td width=\"40%\"><small>Phone Number</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{phone}}</strong></td></tr><tr> <td width=\"40%\"><small>Branch</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{branch}}</strong></td></tr><tr> <td width=\"40%\"><small>Sender</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{sender}}</strong></td></tr><tr> <td width=\"40%\"><small>Message Type</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{message_type}}</strong></td></tr><tr> <td width=\"40%\"><small>Message Category</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{message_category}}</strong></td></tr><tr> <td width=\"40%\"><small>Message</small></td><td width=\"1%\">:</td><td width=\"59%\"><strong>{{message}}</strong></td></tr></tbody></table> <table style=\"background-color: #ffffff; padding: 20px;\"> <thead> <tr> <td align=\"center\" style=\"text-align:center\"> &copy;2017. Kreston Indonesia</td></tr></thead> </table></body></html>');

DROP TABLE IF EXISTS contact;
CREATE TABLE IF NOT EXISTS contact (
  id int(11) NOT NULL AUTO_INCREMENT,
  fullname varchar(60) DEFAULT NULL,
  email varchar(60) DEFAULT NULL,
  phone varchar(20) DEFAULT NULL,
  branch varchar(150) DEFAULT NULL,
  sender varchar(60) DEFAULT NULL,
  message_type varchar(20) DEFAULT NULL,
  message_category varchar(20) DEFAULT NULL,
  message text,
  createdby varchar(20) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  changedby varchar(20) DEFAULT NULL,
  changedat datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

INSERT INTO contact (id, fullname, email, phone, branch, sender, message_type, message_category, message, createdby, createdat, changedby, changedat) VALUES
(1, 'ferry', 'ferry_hndy@yahoo.com', '082121217782', '2', 'Customer', 'Question', 'Product', 'Test', NULL, '2017-08-07 16:02:00', NULL, NULL),
(2, 'remitta', 'remittachrys2311@gmail.com', '082232856003', '4', 'Customer', 'Question', 'Other', 'lowongan pekerjaan', NULL, '2017-08-28 14:15:50', NULL, NULL),
(3, 'ferry', 'ferry_hndy@yahoo.com', '082121217782', ' info.pratama@kresto', 'Third Party', 'Question', 'Product', 'test', NULL, '2017-08-31 08:53:54', NULL, NULL),
(5, 'ferry', 'ferry_hndy@yahoo.com', '0821324123', ' info.pratama@kreston.co.id', 'Customer', 'Question', 'Services', 'asfFS', NULL, '2017-08-31 09:06:01', NULL, NULL),
(7, 'ferry', 'ferry@dsf.dfs', '32412341234', ' info.pratama@kreston.co.id', 'Customer', 'Question', 'Product', 'test', NULL, '2017-08-31 09:08:52', NULL, NULL),
(29, 'Test PIk', 'PIK@pik.cd', '12344', 'info.pratama@kreston.co.id', 'CUSTOMER', 'REQUEST', 'SERVICES', 'Test PIK', NULL, '2017-08-31 11:23:19', NULL, NULL),
(9, 'ferry', 'werqw@sdf.df', '345134534', ' info.pratama@kreston.co.id', 'Customer', 'Question', 'Product', '34', NULL, '2017-08-31 09:10:41', NULL, NULL),
(28, 'Tes', 'tes@yahoo.com', '087635844555', 'info.pratama@kreston.co.id', 'THIRD_PARTY', 'REQUEST', 'OTHER', 'Webtest', NULL, '2017-08-31 11:22:22', NULL, NULL),
(13, 'ferry', 'ferry_hndy@yahoo.com', '082121217782', ' info.pratama@kreston.co.id', 'Customer', 'Question', 'Product', 'test', NULL, '2017-08-31 09:34:53', NULL, NULL),
(27, 'PIK', 'pik@pik.co', '12345678', ' info.pratama@kreston.co.id', 'THIRD_PARTY', 'QUESTION', 'PRODUCT', 'PIK TEST', NULL, '2017-08-31 11:08:46', NULL, NULL),
(26, 'Test KAI', 'kai@kai.kai', '1234567', 'advisory@kreston.co.id', 'BRANCH', 'REQUEST', 'PRODUCT', 'Test KAI', NULL, '2017-08-31 11:07:04', NULL, NULL),
(25, 'test', 'test@test.test', '3456789', 'hhes.surabaya@kreston.co.id', 'CUSTOMER', 'REQUEST', 'SERVICES', 'testing', NULL, '2017-08-31 11:05:16', NULL, NULL),
(24, 'test', 'tes@test.test', '09123`123`12', 'hhes.medan@kreston.co.id', 'Customer', 'Question', 'Product', 'test', NULL, '2017-08-31 11:03:25', NULL, NULL);

DROP TABLE IF EXISTS customers;
CREATE TABLE IF NOT EXISTS customers (
  id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  firstname varchar(32) NOT NULL,
  lastname varchar(32) NOT NULL,
  email varchar(128) NOT NULL,
  email_subscribe tinyint(1) NOT NULL DEFAULT '0',
  phone varchar(32) NOT NULL,
  company varchar(128) NOT NULL,
  password varchar(40) DEFAULT NULL,
  active tinyint(1) NOT NULL,
  group_id int(11) NOT NULL DEFAULT '1',
  confirmed tinyint(1) NOT NULL DEFAULT '0',
  is_guest tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS customer_groups;
CREATE TABLE IF NOT EXISTS customer_groups (
  id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO customer_groups (id, `name`) VALUES
(1, 'Retail');

DROP TABLE IF EXISTS group_access;
CREATE TABLE IF NOT EXISTS group_access (
  group_id int(11) NOT NULL,
  module_id int(11) NOT NULL,
  r int(1) NOT NULL DEFAULT '0',
  w int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (group_id,module_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO group_access (group_id, module_id, r, w) VALUES
(1, 6, 1, 1),
(1, 4, 1, 1),
(1, 5, 1, 1),
(1, 7, 1, 1);

DROP TABLE IF EXISTS key_people;
CREATE TABLE IF NOT EXISTS key_people (
  id int(11) NOT NULL AUTO_INCREMENT,
  fullname varchar(100) NOT NULL,
  category varchar(30) NOT NULL,
  photo varchar(60) NOT NULL,
  position varchar(60) NOT NULL,
  profile text NOT NULL,
  email varchar(60) DEFAULT NULL,
  phone varchar(20) DEFAULT NULL,
  createdby varchar(20) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  changedby varchar(20) DEFAULT NULL,
  changedat datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

INSERT INTO key_people (id, fullname, category, photo, `position`, `profile`, email, phone, createdby, createdat, changedby, changedat) VALUES
(103, 'Derick Mallari', '26', 'b1b2a13a3e147ce6f741d5c9253c385a.jpg', 'Manager Quality Control', '<p>Derick has been with Kreston since 2012 and has over 9 years of experience in external audit and corporate actions such as Initial and Limited Public Offerings.&nbsp; He has started his career with KPMG Philippines in 2008. He has handled clients from various industries ranging from manufacturing, services, distribution, real estate property, and non-profit entities, among others.</p>\r\n\r\n<p>Derick is a Certified Public Accountant since 2008 and earned his bachelor degree from Holy Angel University in Angeles City, Philippines.</p>', 'derick.mallari@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-08-07 13:37:36', 'adminkreston', '2017-08-28 14:16:28'),
(98, 'Hendra Winata', '18', '8c61553f98ba35f1c1527b03a0b3e9ce.JPG', 'Managing Partner', '<p>Arief Hendra Winata is an Indonesian Certified Public Accountant registered with the Indonesian Financial Services Authority (OJK) and is a Registered Public Accountant from the Indonesian Ministry of Finance (Departemen Keuangan RI).</p>\r\n\r\n<p>He has over 37 years of experience in auditing, assurance and consulting and specializes in providing audit and assurance services to domestic and multinational clients and has been with Kreston since 2011. He was formerly a partner at Darmawan &amp; Co. (Touche Ross), and was also the managing partner of Hendrawinata Gani &amp; Hidayat and currently serves as the managing partner of Kreston Indonesia.</p>\r\n\r\n<p>He is a member of the Indonesian Institute of Accountants (IAI) and the Indonesian Institute of Certified Public Accountants (IAPI). He earned his bachelor&rsquo;s degree in Economics, majoring in Accounting, from the University of Indonesia (Universitas Indonesia).</p>', 'hendra.winata@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 11:03:37', 'adminkreston', '2017-08-25 15:25:54'),
(77, 'Lianty Widjaja', '19', '3979d9bdb2e83aa99982e5d8e42df975.JPG', 'Partner', '<p>Lianty Widjaja is an audit partner with 39 years of experience handling engagement for various client industries ranging from trading, manufacturing and logistics, among others.</p>\r\n\r\n<p>She started her career with KAP Darmawan &amp; Rekan (Touche Ross), where she worked for 14 years. During her time there, she was sent on secondment to Touche Ross &amp; Co., Phoenix, Arizona, USA.</p>\r\n\r\n<p>She subsequently joined KAP Hendrawinata Gani &amp; Hidayat in 1992 as an audit partner. She specializes in providing audit and assurance services to domestic and multinational clients and has been with Kreston since 2011.</p>\r\n\r\n<p>Lianty Widjaja is a member of Indonesian Institute of Certified Public Accountants (IAPI). She earned her bachelor&rsquo;s degree in Economy from Trisakti University (Universitas Trisakti).</p>', 'lianty.widjaja@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 09:42:16', 'adminkreston', '2017-08-25 15:28:35'),
(81, 'Welly Adrianto', '19', '00f0d510b42449ea12514d71b4ea0799.JPG', 'Partner', '<p>Welly Adrianto is an Indonesian Certified Public Accountant registered with the Indonesian Financial Services Authority (OJK) and is a registered Public Accountant with the Indonesian Ministry of Finance (Departemen Keuangan RI).</p>\r\n\r\n<p>He specializes in providing audit and assurance services to domestic and multinational clients and has been with Kreston since 2012.</p>\r\n\r\n<p>Welly Adrianto was formerly a partner at KAP Abdi Ichjar, BAP &amp; Rekan before joining Kreston Indonesia. He has handled clients in various sizes of various industries such as real estate property, hospitality, trading, manufacturing, logistics, professional services, telecommunications, and banking and financial services.</p>\r\n\r\n<p>Welly Adrianto is a member of Indonesian Institute of Accountants (IAI) and Indonesian Institute of Certified Public Accountants (IAPI). He earned his bachelor&rsquo;s degree in Economics, majoring in Accounting, from Padang State University (Universitas Negeri Padang) and earned his master&#39;s degree in the field of science at Padjajaran University.</p>', 'welly.adrianto@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 09:44:43', 'adminkreston', '2017-08-25 20:15:55'),
(83, 'Iskariman Supardjo', '19', 'a2f00e21a26857e991323cf0d7b84495.JPG', 'Partner', '<p>Iskariman Supardjo is an Indonesian Certified Public Accountant registered with the Indonesian Financial Services Authority (OJK). He is an audit partner, with more than 35 years of auditing experience with various companies from different industries.</p>\r\n\r\n<p>He started his career working for the Financial and Development Supervision Agency (Badan Pengawasan Keuangan dan Pembangunan &ndash; BPKP) for 17 years. He then subsequently served on the audit committee of PT Jasa Raharja. For 6 years from 2002, he served as an audit partner at KAP Dra. Suhartati and joined KAP Hendrawinata Gani &amp; Hidayat on 2008.</p>\r\n\r\n<p>Iskariman Supardjo is a member of Indonesian Institute of Accountants (IAI) and Indonesian Institute of Certified Public Accountants (IAPI). He earned his bachelor&rsquo;s degree in Economy from Institut Ilmu Keuangan.</p>', 'iskariman.supardjo@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 10:48:30', 'adminkreston', '2017-08-25 15:29:02'),
(82, 'Razmal Muin', '19', 'a1bb8acf89bf8c30b7c7c547b45b83c7.JPG', 'Partner', '<p>Razmal Muin is an Indonesian Certified Public Accountant registered with the Indonesian Financial Services Authority (OJK). He specializes in providing tax services to domestic and multinational clients and has been with Kreston Indonesia since 2011.</p>\r\n\r\n<p>He has more than 51 years of experience, which started when he served as an examiner in the Directorate General of Taxes, working there for more than 20 years. He then joined KAP Darmawan &amp; Co. as a manager until 1992 before joining KAP Hendrawinata Gani &amp; Hidayat as a tax partner.</p>\r\n\r\n<p>Razmal Muin is a member of Indonesian Institute of Accountants (IAI) and Indonesian Institute of Certified Public Accountants (IAPI). He earned his bachelor&rsquo;s degree in Economy from Institut Ilmu Keuangan.</p>\r\n\r\n<p>He is a registered public accountant with the Indonesian Ministry of Finance (Departemen Keuangan RI), and is a member of the Indonesian Institute of Accountants (IAI) and Indonesian Institute of Certified Public Accountants (IAPI).</p>', 'razmal.muin@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 10:47:53', 'adminkreston', '2017-08-25 15:27:56'),
(99, 'Hanny Wurangian', '18', '5f3c2029d0848917b2c81527806af223.JPG', 'Deputy Managing Partner', '<p>Hanny Wurangian is an Indonesian public accountant. He served as a Partner of Drs. Wolfrey Jademurni &amp; Partners Registered Public Accountants from 1978 to 1985 and then as a Managing Partner from 1985 to 1993. Furthermore, he also served as a Managing Partner of Drs. Hanny, Wolfrey and Partners since 1994 to 2017. He has been a Bank Audit Committee Chairman of PT Bank HSBC Indonesia since 1996 and its Commissioner since 2005. He started his career as a Lecturer in 1979 and served as Chief of Accountancy Department at Universitas Airlangga since 1990.<br />\r\n<br />\r\nMr. Hanny is a member of&nbsp; Indonesian Institute of Certified Public Accountants (IAPI) and Institute of Indonesia Chartered Accountants (IAI). He got his Bachelor of Economic degree in 1975 and Bachelor of Accounting degree in 1978 both from Universitas Airlangga. He also got his Magister of Sains in Accounting at Universitas Airlangga in 2005, Certified Public Accountant from IAPI, and Chartered Accountant from IAI.</p>', 'hanny.wurangian@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 11:05:17', 'adminkreston', '2017-08-23 13:34:06'),
(92, 'Lisa Novianty', '21', 'f33d11c7c57549ce5bf36cd38571dcdc.JPG', 'Partner', '<p>Lisa is an Assurance Partner and a Certified Public Accountant <strong>registered</strong> with the Indonesian Institute of Public Accountants (IAPI).</p>\r\n\r\n<p>She began her auditing career in 2003 with Grant Thornton and has obtained vast experience in auditing different industries such as plantations, manufacturing companies, contractors, and trading companies. She is also experienced in conducting special audit engagements, limited reviews, and other types of attestation services.</p>\r\n\r\n<p>&nbsp;</p>', 'lisa.novianty@kreston.co.id', '+62 61 455 7925', 'adminkreston', '2017-07-31 10:55:09', 'adminkreston', '2017-08-21 17:50:17'),
(84, 'Fritz William W', '20', 'd0b4078f761f4f9b94a4884dcdbf0463.JPG', 'Partner', '<p>Fritz William is an Indonesian public accountant. He is specialized in Audit and Accounting Information Systems since 2008. He started his career as an assistant and now he is one of the Managers in Hendrawinata Hanny Erwin &amp; Sumargo Registered Public Accountants. He has handled clients from various industries such as trading, manufacturing, food &amp; beverage and hospitality. He is also an Authorised Training Employer (ATE) of Institute of Chartered Accountants in England and Wales (ICAEW) since 2016 and he is a member and actively assisting the British Chamber of Commerce&rsquo;s network development in Surabaya.</p>\r\n\r\n<p>He moreover is also a member of Indonesian Institute of Certified Public Accountants (IAPI) and Institute of Indonesia Chartered Accountants (IAI). He awarded his Bachelor of Accounting degree from Universitas Airlangga, Magister of Business in Accounting and Finance degree from University of Technology, Sydney (UTS) and Certified Public Accountant (CPA) from IAPI.</p>', 'fritz.williamw@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:49:37', 'adminkreston', '2017-08-23 13:36:05'),
(85, 'Chris Edward W', '20', 'a54b92133df3019f101ea3ee71786a96.JPG', 'Partner', '<p>Chris Edward is an Indonesian public accountant. He is specialized in Audit and Accounting Information Systems since 2009. Now, he helps in the development of ICAEW (Institute of Chartered Accountants in England &amp; Wales) in order to stretch the wings in Surabaya. Furthermore, he is also the member of British Chamber of Commerce in Indonesia and assisting actively in the network development of British Chamber of Commerce in Surabaya. He started his career as an assistant and with his persistence now he becomes one of the Managers at Hendrawinata Hanny Erwin &amp; Sumargo Registered Public Accountants. He has handled clients from various industries such as manufacturing, services, real estate, trading and shipping.</p>\r\n\r\n<p>Mr. Chris is also a member of Indonesian Institute of Certified Public Accountants (IAPI) and Institute of Indonesia Chartered Accountants (IAI). He got his Bachelor of Science in Business degree from Indiana University - Purdue University Indianapolis (United States of America), Magister of Science in International Accounting and Finance degree from The City Univeristy &ndash; London (England), Certified Public Accountant from IAPI and Chartered Accountant from IAI.</p>', 'chris.edwardw@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:50:31', 'adminkreston', '2017-08-23 13:36:39'),
(86, 'Go Bosse Gozali', '20', 'a8d1ef49783f826014042d431307849e.JPG', 'Partner', '<p>Drs. Bosse Gozali is an Indonesian non-public accountant who graduated with a bachelor&#39;s degree in accounting. He started to join as an assistant until became a Non-Public Accountant Partner in Hanny, Wolfrey &amp; Partners Registered Public Accountants until June 2017 and now he is Partner in with Hendrawinata Hanny Erwin &amp; Sumargo Registered Public Accountants.</p>\r\n\r\n<p>Mr. Bosse has expertise and specializes in :</p>\r\n\r\n<p><strong>Management Services</strong></p>\r\n\r\n<p>They include accounting procedure and systems, integration of information system, Enterprise Resources Planning implementations, business process reengineering, compilation of company financial statements, budgeting, IT audit, organization reviews, company mergers and acquisitions, and family business succession.</p>\r\n\r\n<p><strong>Tax Services</strong></p>\r\n\r\n<p>They include general tax consulting and advisory, tax planning, tax compliance, tax accounting checks, and tax audit consultant.</p>\r\n\r\n<p>He has handled clients from various industries such as manufacturing, trading, building construction, printing, real estate, cottage and apartment, hospitality, restaurant, shopping center, and forwarding.</p>\r\n\r\n<p>&nbsp;</p>', 'bosse.gozali@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:51:03', 'adminkreston', '2017-08-23 13:34:59'),
(87, 'Swandajani Sutjiadi', '20', '8e421cc6460263696c26f7673a70c3bd.JPG', 'Partner', '<p>Swandajani Sutjiadi is an Indonesian public accountant. She has experienced in Audit and Accounting Information Systems since 1979. Swandajani started her career as an Auditor and because of her persistence she has been an Audit Partner position since 1993. She has handled clients from various industries such as trading, manufacturing, construction services and hospitality.<br />\r\n<br />\r\nMs. Swandajani is also a member of Indonesian Institute of Certified Public Accountants (IAPI) and Institute of Indonesia Chartered Accountants (IAI). She got her Bachelor of Accounting degree from Universitas Airlangga and Certified Public Accountant from IAPI.</p>', 'swandajani.s@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:51:46', 'adminkreston', '2017-08-23 13:34:25'),
(90, 'Lilik Hartatik', '20', 'e06d08cb2512e9cc30ef2b935883a0b9.JPG', 'Partner', '<p>Lilik Hartatik is an Indonesian Public Accountant. She is an Audit Partner at Hendrawinata Hanny Erwin &amp; Sumargo Registered Public Accountants. She has been an auditor for more than 28 years and she has handled the company from various industries such as rural bank, commercial bank, manufacturing, property &amp; real estate and hospitality.</p>\r\n\r\n<p>Mrs. Lilik is also a member of Indonesian Institute of Certified Public Accountants (IAPI), Institute of Indonesia Chartered Accountants (IAI) and Indonesian Tax Consultants Association (IKPI). She graduated from Information Management major at STI &amp; K Jakarta and she got her Bachelor of Accounting degree from Universitas Airlangga, Certified Public Accountant from IAPI, Chartered Accountant from IAI and Certified Tax Consultant from IKPI.</p>', 'lilik.h@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:53:45', 'adminkreston', '2017-08-23 13:35:17'),
(88, 'Denny Megaliong', '20', 'edddc8fa5042dadddf36e5463ff4c988.JPG', 'Partner', '<p>Denny C. Megaliong currently is a Quality Review Partner at Hendrawinata Hanny Erwin &amp; Sumargo Registered Public Accountants with his experienced of work at KPMG - Hanadi, Sudjendro &amp; Partners Registered Public Accountants for 7 years and Ernst &amp; Young - Purwantoro, Sungkoro &amp; Surja Registered Public Accountants for 19 years. Denny is not only experienced as an auditor, but more than that, he has experienced in reviews of system and procedure, due diligence reviews, and corporate actions (domestic offering) for companies from various industries such as manufacturing, retail, property and real estate, financial institutions, and etc.</p>\r\n\r\n<p>Mr. Denny is also a member of Indonesian Institute of Certified Public Accountants (IAPI) and Institute of Indonesia Chartered Accountants (IAI). He got his Bachelor of Accounting degree from Universitas Klabat, Manado, Certified Public Accountant from IAPI, and Chartered Accountant from IAI.</p>', 'denny.megaliong@kreston.co.id', '+62 31 503 5046', 'adminkreston', '2017-07-31 10:52:19', 'adminkreston', '2017-08-23 13:35:40'),
(91, 'Razuar Yahya', '21', 'd7051ac77a2ce59d26852e7a0f6d56c8.JPG', 'Principle', '<p>Razuar is an Assurance Principal and is a Chartered Accountant registered with the Indonesian Institute of Accountants (IAI) and a Certified Professional Auditor of Indonesia registered with the Indonesian Institute of Certified Public Accountants (IAPI).</p>\r\n\r\n<p>He has more than 30 years of audit experience and has handle clients from manufacturing, plantations, contractors, and trading. &nbsp;He is also experienced in conducting special audit engagements, limited review, and other types of assurance services. Prior to joining Kreston, he was with Touche Ross for 10 years, with Arthur Andersen for 2 years, with KPMG for 6 years and with Grant Thornton for 13 years.</p>', 'razuar.yahya@kreston.co.id', '+62 61 455 7925', 'adminkreston', '2017-07-31 10:54:19', 'adminkreston', '2017-08-21 17:48:35'),
(118, 'Prianto Budi S.', '23', '0f605a1abf4ca8b92e10dde15dd954f8.JPG', 'CEO', '<p>Prianto Budi is a Registered Tax Consultant and a Licensed Tax Attorney, holder of the C certificate for tax consultant. As a tax consultant, Prianto Budi actively performs as a speaker in various taxation trainings and seminars, including taxation training organized by the Indonesian Tax ConsultantAssociation, the Indonesian Institute of Certified Public Accountants, and the Indonesian Institute of Accountants. Prianto Budi commenced his career in taxation as a tax auditor for several years at the Directorate General of Taxes, Ministry of Finance. He was also a Tax Consulting Manager at Kanaka Puradiredja &amp; Rekan, Registered Public Accountants (recently member of Nexia International), and recently becomes a Company Director of PT Pratama Indomitra Konsultan.</p>\r\n\r\n<p>Prianto Budi is a member of Indonesian Institute of Certified Public Accountants (IAPI) for Tax Committee Division and Central Board of Indonesian Tax Consultans Associations (IKPI) as the Head of Continuing Professional Development (CPD). He earned his Accountant&rsquo;s degree from State College of Accountancy (STAN) Jakarta and his MBA degree from Gajah Mada University (UGM).</p>', 'prianto.budi@kreston.co.id', '+62 21 2963 4945', 'adminkreston', '2017-08-18 18:55:08', 'adminkreston', '2017-08-25 15:30:22'),
(102, 'Jerome De Gala', '26', '8ca8d71b159059793164068a8ab8c861.jpg', 'Manager Quality Control', '<p>Jerome has been with Kreston since 2011 and has more than 9 years of audit and review experience. He has started his career with KPMG Philippines in 2008 before joining Kreston Indonesia. He has handled audit and review of local and multinational clients, including publicly-listed and regulated clients, across various industries such as manufacturing, trading, real estate, gaming, consulting services as well as non-profit organization both in the Philippines and Indonesia.</p>\r\n\r\n<p>Jerome have graduated in Far Eastern University, Manila Philippines in year 2008. In the same year he has been registered as Certified Public Accountant in Philippines. In 2009, he has obtained his Diploma in International Financial Reporting Standards issued by the Institute of Accounting Technicians.</p>', 'jerome.degala@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-08-07 13:36:29', 'adminkreston', '2017-08-28 14:16:36'),
(76, 'Arief Budiman', '22', 'f3a79a5fd33ff118866fce3353fbbbc1.JPG', 'President Director', '<p>Arief Budiman obtained his bachelor&rsquo;s degree as an accountant from Sekolah Tinggi Akuntansi Negara (STAN) and his master&rsquo;s degree in business administration from St. Mary&rsquo;s University Halifax, Nova Scotia, Canada.</p>\r\n\r\n<p>He started his career as a junior auditor in 1982 with Badan Pengawasan Keuangan dan Pembangunan - BPKP (Indonesia&#39;s National Government Internal Auditor), progressing until Echelon IV before joining the Kementrian Badan Usaha Milik Negara - BUMN (Indonesia&rsquo;s Ministry of State Owned Enterprises) as Echelon III.</p>\r\n\r\n<p>And in 2003, he started his career as one of the board of directors for several state-owned companies such as PT Hotel Indonesia Natour and PT Kimia Farma Tbk, (both of which he served as CFO) and more recently with PT Indofarma Tbk as President Director. Arief Budiman joined PT Kreston Advisory Indonesia as President Director in 2017.</p>', 'arief.budiman@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 09:41:37', 'adminkreston', '2017-08-28 15:24:10'),
(100, 'Erwin Winata', '18', '3d8246cc76d59a2e7a71a82e5d48cb19.JPG', 'International Liaison Partner', '<p>Erwin A. Winata is an Indonesian Certified Public Accountant registered with the Indonesian Financial Services Authority (OJK) and is a Registered Public Accountant from the Indonesian Ministry of Finance (Departemen Keuangan RI). He specializes in providing audit and assurance services to domestic and multinational clients and has been with Kreston since 2011.</p>\r\n\r\n<p>Erwin A. Winata started his career as a consultant and has successfully progressed to become a partner at Kreston Indonesia with over 12 years of experience. He has handled clients of different sizes in various industries which includes trading, manufacturing, logistics, not-for-profit organizations, professional services, plantation, coal mining, insurance and financial services.</p>\r\n\r\n<p>Erwin A. Winata is a member of the Indonesian Institute of Accountants (IAI) and Indonesian Institute of Certified Public Accountants (IAPI). He earned his Bachelor&rsquo;s Degree in Accounting and his Master&rsquo;s Degree on Business Administration at California State University, Fullerton, California.</p>', 'erwin.winata@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-07-31 11:05:56', 'adminkreston', '2017-08-25 15:27:27'),
(97, 'Robby Sumargo', '18', '3127f2e1433f11354d2632869d03b516.JPG', 'Deputy Managing Partner', '<p>Robby is a Chartered Accountant, Certified Public Accountant and Certified Tax Consultant registered with the Indonesian Institute of Accountants (IAI), Indonesian Institute of Public Accountants (IAPI) and the Indonesian Tax Consultants Association (IKPI), respectively.</p>\r\n\r\n<p>He graduated from the California State University of Fullerton and obtained his Master&rsquo;s Degree in Corporate Finance. He has 21 years of working experience in assurance, tax compliance, and tax advisory/planning. He has assisted various clients in tax audits and investigation, as well as assisting with tax appeals in tax court, and obtaining tax licenses for foreign companies.</p>\r\n\r\n<p>Prior to joining Kreston, he was with KPMG for 5 years as part of the assurance division. He subsequently joined Grant Thornton for 13 years in assurance as both an Assurance Partner and Tax partner.</p>', 'robby.sumargo@kreston.co.id', '+62 61 455 7925', 'adminkreston', '2017-07-31 11:02:58', 'adminkreston', '2017-08-21 17:47:36'),
(104, 'Kristina Bay Maca', '22', 'fe8645155b4b32fdf09a6bbc0a68ff38.jpg', 'Associate Director', '<p>Kristina has been with Kreston since 2014 and has over 10 years of experience, which includes audit, accounting services and consultancy. She has started her career with Sycip Gorres Velayo &amp; Co. in 2007 and subsequently moved to Deloitte Indonesia. She has handled clients from various industries ranging from manufacturing, professional services, distribution, real estate property, and foundation, among others.</p>\r\n\r\n<p>Kristina is a Certified Public Accountant since 2006 and earned her bachelor degree from the University of Santo Tomas, Manila.&nbsp;</p>', 'kristina.maca@kreston.co.id', '+62 21 571 2000', 'adminkreston', '2017-08-07 14:24:22', 'adminkreston', '2017-08-15 17:23:04'),
(112, 'Ernawati', '23', '710ef4aad28646552b7b9a7906f6650e.JPG', 'Consulting Assistant Manager', '<p>Ernawati is a Registered Tax Consultant and a holder of the C certificate. She specializes in providing tax planning, compliance, and tax consulting. A number of assignments to assist clients in those three subjects have successfully resulted in problem-solving decisions. Ernawati started her career as a Junior Tax Consultant and has accomplished many tax consulting works in various industries, such as: hospital, manufacture, trade, and mining. Many of her clients have gained substansialtaxefficiency,andexemptedfrombeingsubjectto tax penalty risk.</p>\r\n\r\n<p>Ernawati actively involved in the Fiscal Administration Study Group and frequently joined taxation seminars and workshops regarding tax planning, transfer pricing, tax audit technique, and many others during her study at University of Indonesia.</p>', 'ernawati@kreston.co.id', '+62 21 2963 4945', 'adminkreston', '2017-08-18 18:39:19', 'adminkreston', '2017-08-28 10:20:31'),
(113, 'Ade Feri H.', '23', '5521bfdfa7f18d2ea8a700a3486955cc.JPG', 'Consulting Assistant Manager', '<p>Ade Feri Hermansah was an assistant of tax auditor at tax office - Directorate General of Taxes, Ministry of Finance. Currently, he is a Tax Consultant with the A certificate. His expertise in preparing supporting documents and working papers of tax audit is very helpful in assisting clients in dealing with tax auditors. In addition, Ade Feri Hermansah is the person in charge for handling tax audit assistance, including tax objection, tax appeal and judicial review.</p>\r\n\r\n<p>&nbsp;</p>', 'ade.hermansah@kreston.co.id', '+62 21 2963 4945', 'adminkreston', '2017-08-18 18:40:47', 'adminkreston', '2017-08-28 10:20:49'),
(114, 'Maryono', '23', 'eaf6a63a2110bf5bb0bc52f74fe6cf1f.JPG', 'GCG Manager', '<p>Maryono is a Good Corporate Governance consultant with specialization in providing either Manpower Legal or Corporate Legal. He commenced his career in the field of Law by working as a staff for several years at Kantor Pengacara Barry &amp; Partner. In addition, Maryono had been working at PT Multi Utama Indojasa (MUC Consulting Group) for almost 10 (ten) years and was in charge of training in the field of Manpower Law, Corporate Legal, and Human resources. Maryono earned his Bachelor of Law degree from Universitas Islam Nusantara, Bandung, in 1993.</p>', 'maryono@kreston.co.id', '+62 21 2963 4945', 'adminkreston', '2017-08-18 18:42:52', 'adminkreston', '2017-08-25 15:31:04'),
(115, 'Edih Suryadi', '23', '619d3f82dbc6d0cdece3ca633ffaf093.JPG', 'Business Development Manager', '<p>Edih Suryadi is a Business Development and Training Manager with over ten years of experience in the field of marketing and training, either public or in-house training. He specializes in providing seminar and training with various kinds of topics, such as Taxation, Finance &amp; Accounting, Export &amp; Import, Shipping, Insurance, &amp; Trade, Good Corporate Governance (GCG), Human Resource Management (HRM), and Risk Management. Edih Suryadi earned his Bachelor of Management degree from Universitas Kusuma Negara in 2008.</p>', 'edih.suryadi@kreston.co.id', '+62 21 2963 4945', 'adminkreston', '2017-08-18 18:45:24', 'adminkreston', '2017-08-25 15:31:32'),
(116, 'Halim Widi S.', '23', '628f1b9d77a1dce7fc0e8ced0aeb962f.JPG', 'Consulting Manager', '<p>Halim Widi Saptono is an Accounting and Tax Manager who has a lot of experiences in accounting and taxation fields. He specializes in providing transfer pricing documentation services to domestic and multinational clients and has joined PT Pratama Indomitra Konsultan since 2014. Halim Widi Saptono commenced his career as a tax consultant by working at Kanaka Puradiredja &amp; Rekan Registered Public Accountants (recently member of Nexia International) for 4 (four) years. He continued his career in company group of PT Global Mediacom as a Head of Tax Section for 6 (six) years and as a Manager of Tax Consultant at PT Grant Thornton. In addition, Halim Widi Saptono also has the experience in handling several medium to large sized companies in domestic or overseas with various types of business sectors. &nbsp;</p>\r\n\r\n<p>Halim Widi Saptono finished his study in State Finance and Accountant program from Sekolah Tinggi Akuntansi Negara (STAN) on Accountant Program. Therefore, he has an in-depth understanding in Indonesian Financial Accounting Standards (PSAK), Financial Accounting Standard of Entities Without Public Accountability (SAK ETAP) and International Taxation and Transfer Pricing. Further,Halim Widi Saptono also has an excellent understanding and well-experienced in financial and capital market sectors, including the tax planning and structuring in company, financial due diligence, merger and acquisition, and business valuation.</p>', 'halim.saptono@kreston.co.id', '+62 21 2963 4945', 'adminkreston', '2017-08-18 18:48:35', 'adminkreston', '2017-08-25 15:31:49'),
(117, 'Ahmad Fauzi', '23', 'c2262eed3ca2e59a4e55149bc500b850.JPG', 'Consulting Manager', '<p>Ahmad Fauzi is a Tax Consultant having the B Certificate for tax consultant from Indonesian Tax Consultant Association. He has a lot of expertise in providing tax review and consulting services to domestic and multinational clients. Ahmad Fauzi&rsquo;s experience in assisting tax audit has made him fully knowledgeable about effective tax plan, so that taxpayers can be exempted from tax payment without causing any act in disregard of provisions of taxation. Ahmad Fauzi has earned his Bachelor&rsquo;s degree in Fiscal Administration from Univesity of Indonesia.</p>', 'ahmad.fauzi@kreston.co.id', '+62 21 2963 4945', 'adminkreston', '2017-08-18 18:54:08', 'adminkreston', '2017-08-25 15:32:05');

DROP TABLE IF EXISTS mst_module;
CREATE TABLE IF NOT EXISTS mst_module (
  module_id int(3) NOT NULL AUTO_INCREMENT,
  module_name varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  module_path varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  module_desc varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (module_id),
  UNIQUE KEY module_name (module_name,module_path)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

INSERT INTO mst_module (module_id, module_name, module_path, module_desc) VALUES
(4, 'master', 'master', 'Manage your referential data that will be used by other modules'),
(6, 'system', 'system', 'Configure system behaviour, user and backups'),
(5, 'katalog', 'katalog', 'Transaksi katalog'),
(7, 'web', 'web', 'Backend for web portal');

DROP TABLE IF EXISTS news;
CREATE TABLE IF NOT EXISTS news (
  id int(11) NOT NULL,
  lang char(2) NOT NULL,
  image varchar(255) DEFAULT NULL,
  photo_by varchar(90) DEFAULT NULL,
  news_by varchar(20) DEFAULT NULL,
  place varchar(90) DEFAULT NULL,
  news_date date DEFAULT NULL,
  title varchar(150) NOT NULL,
  slugs varchar(255) NOT NULL,
  news_desc text,
  created_at varchar(10) DEFAULT NULL,
  changed_by varchar(10) DEFAULT NULL,
  changed_at datetime DEFAULT NULL,
  created_by datetime DEFAULT NULL,
  PRIMARY KEY (id,lang),
  UNIQUE KEY slugs (slugs)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO news (id, lang, image, photo_by, news_by, place, news_date, title, slugs, news_desc, created_at, changed_by, changed_at, created_by) VALUES
(3, 'id', 'cf0c6d127e204a50560fac1ff29a4d4a.jpg', 'Admin', 'Admin', 'Marrakech', '2017-05-25', 'Bergerak maju di Marrakech', 'bergerak-maju-di-marrakech', '<p>The 2017 Kreston EMEA Conference was held in Marrakech, Morocco where 105 delegates drawn from 27 countries were in attendance.</p>\r\n\r\n<p>Host firm, Kreston Exco Maroc presented an overview of their firm and the economy and investment opportunities in Morocco.</p>\r\n\r\n<p>The business programme included updates on the global network development and systems for the application of data analytics in audit and consultancy services. Lively debate took place in the international tax sessions around Brexit; the impact of BEPS in members&rsquo; respective countries and the implications of Trump&rsquo;s proposed tax changes.</p>\r\n\r\n<p>Kreston CEO, Jon Lisby said:<br />\r\n&ldquo;Another lively programme in a great location which provided exceptional opportunities for our members to network, discuss international referrals and opportunities &ndash; these conferences deliver significant value for all&rdquo;</p>\r\n\r\n<p>The 2018 EMEA Conference will be held in Zurich 10th &ndash; 13 May 2018</p>', '2017-06-01', NULL, NULL, '0000-00-00 00:00:00'),
(3, 'en', 'cf0c6d127e204a50560fac1ff29a4d4a.jpg', 'Admin', 'Admin', 'Marrakech', '2017-05-25', 'Moving forward in Marrakech', 'moving-forward-in-marrakech', '<p>The 2017 Kreston EMEA Conference was held in Marrakech, Morocco where 105 delegates drawn from 27 countries were in attendance.</p>\r\n\r\n<p>Host firm, Kreston Exco Maroc presented an overview of their firm and the economy and investment opportunities in Morocco.</p>\r\n\r\n<p>The business programme included updates on the global network development and systems for the application of data analytics in audit and consultancy services. Lively debate took place in the international tax sessions around Brexit; the impact of BEPS in members&rsquo; respective countries and the implications of Trump&rsquo;s proposed tax changes.</p>\r\n\r\n<p>Kreston CEO, Jon Lisby said:<br />\r\n&ldquo;Another lively programme in a great location which provided exceptional opportunities for our members to network, discuss international referrals and opportunities &ndash; these conferences deliver significant value for all&rdquo;</p>\r\n\r\n<p>The 2018 EMEA Conference will be held in Zurich 10th &ndash; 13 May 2018</p>', '2017-06-01', NULL, NULL, '0000-00-00 00:00:00'),
(5, 'id', '7b319e8c2ab254a65b9ff9c8a0fb4023.jpg', 'kreston.com', 'kreston.com', 'Glasgow', '2017-02-28', 'xxx', 'xxx', '<p>xxx</p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(6, 'en', 'e11a0885670d547ee04cc12e2396e66c.jpg', 'Kreston Reeves', 'Kreston Reeves', 'UK', '2016-12-23', 'MANUFACTURING AND BREXIT: CAUTIOUS OPTIMISM BUT….', 'manufacturing-and-brexit-cautious-optimism-but…', '<p>Rodney Sutton, head of manufacturing at Kreston Reeves, recently shared his thoughts on the UK manufacturing industry following the referendum vote.</p>\r\n\r\n<p>In his article, Sutton traces an overview of the market reaction to the decision by the British electorate on the 23rd June to exit the EU. He lists several effects following the referendum as Sterling devalues to its lowest level in 30 years against the dollar and the euro, UK manufactured goods become instantly more competitive, manufacturing outputs rose to their highest level in 25 years in August and raw material imports were predicted to become more expensive.</p>\r\n\r\n<p>Despite the initial tumultuousness in the market after Brexit, economic numbers showed that the British manufacturing sector is able to compete with German manufacturers as the sector &ldquo;has outperformed the productivity growth of the service sector in the 20 years prior to 2014&rdquo;, says Sutton.</p>\r\n\r\n<p>But trade with the EU is still very important. &ldquo;There is an air of cautious optimism across the sector. Most manufacturers echo the sentiment that EU customers and suppliers who trade with the UK will want this to continue as the trade is very significant, 54% of UK imports come from the EU and 47% of UK exports go to the EU. Would it be foolhardy to jeopardise this lucrative trading arrangement?&rdquo;, questions Sutton in his article.</p>\r\n\r\n<p>Come rain or shine, there is an air of uncertainty. Sutton lists three important issues that need to be clarified.</p>\r\n\r\n<p>&ndash; The Brexit government&rsquo;s ability to negotiate a free trade agreement with the EU and the looming spectre of immigration which could derail this process.<br />\r\n&ndash; Government consideration as to how to fund EU manufacturing- will this be replaced once this source of funding has been switched off?<br />\r\n&ndash; Will the UK government support and drive the manufacturing sector?</p>\r\n\r\n<p>According to Sutton, it is very important to keep a free trade environment between the UK and the EU, but things need time to be settle. &ldquo;UK manufacturing needs to recognise that any deal to negotiate free trade status in the EU will be lengthy, face many hiccups along the way and ultimately will need the agreement of all 28 EU members&rdquo;.</p>\r\n\r\n<p>Clearly there are big challenges ahead.</p>\r\n\r\n<p>Source:&nbsp;<a href=\"http://www.krestonreeves.com/news-and-events/15/11/2016/manufacturing-and-brexit-cautious-optimism-but\" target=\"_blank\">Kreston Reeves</a></p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(4, 'en', 'eb185081d8fa3e06776cebbadad4c131.png', 'Kreston.com', 'Kreston.com', 'Madrid', '2017-03-21', 'Kreston welcomes new member MG Madrid & Company, Philippines.', 'kreston-welcomes-new-member-mg-madrid--company-philippines', '<p>Kreston International is pleased to announce that MG Madrid &amp; Co, in the Philippines, has joined the global membership, adding to its offering in the Asia Pacific region. Established in 2003, this two Partner firm offers audit, tax and consulting services from its head office in Makati City, Manila and has over 70 professional and support staff. A second branch office is in Muntinlupa City.</p>\r\n\r\n<p>Jon Lisby, Kreston CEO, said:<br />\r\n&ldquo;We have been looking for a quality firm to represent Kreston in the Philippines and we are delighted that MG Madrid &amp; Company have agreed to join the global network, adding to our coverage in Asia and to support our members in the region&rdquo;</p>\r\n\r\n<p>Partner Mario G. Madrid commented:<br />\r\n&ldquo;It is an honour to join Kreston International. Our values, integrity, professionalism and commitment, are very much aligned with those of the network and we look forward to working with colleagues both across Kreston Asia and the global membership to further promote the Kreston brand in the region.&rdquo;</p>\r\n\r\n<p><em>Picture features left to right, Partners, Mr. Benedict A. Ang Ban Giok and Mr. Mario G. Madrid Jr.</em></p>', '2017-06-08', 'admin', '2017-06-08 19:46:48', '0000-00-00 00:00:00'),
(4, 'id', 'eb185081d8fa3e06776cebbadad4c131.png', 'Kreston.com', 'Kreston.com', 'Madrid', '2017-03-21', 'xxxx', 'xxxx', '<p>xxxx</p>', '2017-06-08', 'admin', '2017-06-08 19:46:48', '0000-00-00 00:00:00'),
(5, 'en', '7b319e8c2ab254a65b9ff9c8a0fb4023.jpg', 'kreston.com', 'kreston.com', 'Glasgow', '2017-02-28', 'KRESTON UK COLLABORATE IN GLASGOW.', 'kreston-uk-collaborate-in-glasgow', '<p>100 delegates from Kreston firms across the UK and Ireland attended the 2017 Conference at the Hilton Hotel in Glasgow to share best practice and to discuss developments across several specialist sectors including Audit, Corporate Tax, IT, Marketing, Probate and Private Client; Managing Partners, HR and Advisory and Outsourcing.</p>\r\n\r\n<p>Kreston CEO Jon Lisby, who provided an update on global developments and brand strategy, commented:<br />\r\n&ldquo;Thank you to local member&nbsp;<a href=\"http://consiliumca.com/\" target=\"_blank\">Consilium</a>&nbsp;for hosting the event this year, the organisation was flawless. The feedback from each of the group sessions again testifies to the value of the event and the outcome sees a further strengthening of future collaboration and client service. With the organic growth and mergers achieved over the past year, Kreston UK, as a combined organisation would now rank as the 10th largest UK accounting firm with over 2,400 professional and support staff generating fee revenues of US$200m&rdquo;.</p>\r\n\r\n<p>John Warner, Chairman of Kreston UK and Managing Partner of BHP (North of England) concluded the business sessions and the conference ended with dinner at famous House for an Art Lover, designed by Mackintosh, Glasgow&rsquo;s most famous architect.</p>\r\n\r\n<p>The 2018 conference will take place in Jersey.</p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(7, 'en', '1a8a859e6e65b5719c464d5a1930b4e8.jpg', 'kreston.com', 'kreston.com', 'UK', '2017-06-07', 'PROGRESSION IN SETTING GLOBAL STANDARDS FOR NOT-FOR-PROFIT ORGANISATIONS', 'progression-in-setting-global-standards-for-notforprofit-organisations', '<p>The adoption of international accounting standards has brought many benefits to the private and public sectors. This being said, there is still progress to be made in the not-for-profit (NFP) sector.</p>\r\n\r\n<p>In an excellent article by,&nbsp;<a href=\"http://www.publicfinance.co.uk/opinion/2016/11/not-profit-standards-go-global\" target=\"_blank\">Easton Bilsborough</a>, technical manager for charities and NFP sectors at the Chartered Institute of Public Finance &amp; Accounting (CIPFA), Bilsborough talks about issues facing the NFP sector and points out the benefits of adopting accounting standards.</p>\r\n\r\n<p>According to Bilsborough, a recent study by the Consultative Committee of Accountancy Bodies (CCAB) on financial reporting by NFP, showed organisations in this sector, in at least 179 countries, having problems related to accounting for non-exchange transactions; fund accounting; income recognition; gifts in kind; mergers; treatment of branches; and reporting on reserves. In addition, the absence of global standards prevented the attainment of high-quality standards within individual jurisdictions.</p>\r\n\r\n<p>Accounting in the NFP sector has made slow progress and according to Bilsborough, the International Forum of Accounting Standard Setters has formed a working group to build a database of practice to help move forward NFP reporting across the globe.</p>\r\n\r\n<p>Achieving international accounting standards would bring several benefits to the NFP sector. It would improve the quality of reporting and make charities more transparent, making it clear for support from donors and funders. Also, with consistent reporting, the not-for-profit sector would be better understood.</p>\r\n\r\n<p>Many NFP entities work across a number of jurisdictions and the development of standards that are accepted internationally will be of significant benefit to them. There is however, a long way to go until these standards are ready to be implemented.</p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(7, 'id', '1a8a859e6e65b5719c464d5a1930b4e8.jpg', 'kreston.com', 'kreston.com', 'UK', '2017-06-07', 'eee', 'eee', '<p>eee</p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(8, 'en', '2c5f1de57566d376ceafe685a499b22d.jpg', 'IFRS.org', 'IFRS.org', 'Saudi', '2016-12-07', 'SAUDI ARABIA MOVES TOWARDS IFRS STANDARDS', 'saudi-arabia-moves-towards-ifrs-standards', '<p>A common global language for business affairs, IFRS Standards are required by more than 120 jurisdictions in the world.</p>\r\n\r\n<p>Recently, Saudi Arabia signed a license agreement with the International Financial Reporting Standards (IFRS) Foundation to stamp its intention to incorporate IFRS Standards into its legal framework. This means that the Saudi organisation for Certified Public Accountants will have the right to publish the official Arabic translation of IFRS Standards and the IFRS for SMEs Standard for the purpose of adoption into law.</p>\r\n\r\n<p>In 2017, banks and insurance companies will be required to use IFRS Standards, with listed companies. From 2018, other companies will be required to use the IFRS for SMEs Standard.</p>\r\n\r\n<p>The wider adoption of IFRS in Saudi Arabia will help drive the quality and comparability of financial information. These rules bring transparency, enabling investors and other market participants to make informed economic decisions. It is important that entities impacted by the change start to plan in good time and talk to their professional adviser early in the process.</p>\r\n\r\n<p><em>Source:&nbsp;<a href=\"http://www.ifrs.org/Alerts/PressRelease/Pages/Saudi-Arabia-to-publish-ifrs-Standards-for-adoption-purposes.aspx\">IFRS.org</a></em></p>\r\n\r\n<p>A common global language for business affairs, IFRS Standards are required by more than 120 jurisdictions in the world.</p>\r\n\r\n<p>Recently, Saudi Arabia signed a license agreement with the International Financial Reporting Standards (IFRS) Foundation to stamp its intention to incorporate IFRS Standards into its legal framework. This means that the Saudi organisation for Certified Public Accountants will have the right to publish the official Arabic translation of IFRS Standards and the IFRS for SMEs Standard for the purpose of adoption into law.</p>\r\n\r\n<p>In 2017, banks and insurance companies will be required to use IFRS Standards, with listed companies. From 2018, other companies will be required to use the IFRS for SMEs Standard.</p>\r\n\r\n<p>The wider adoption of IFRS in Saudi Arabia will help drive the quality and comparability of financial information. These rules bring transparency, enabling investors and other market participants to make informed economic decisions. It is important that entities impacted by the change start to plan in good time and talk to their professional adviser early in the process.</p>\r\n\r\n<p><em>Source:&nbsp;<a href=\"http://www.ifrs.org/Alerts/PressRelease/Pages/Saudi-Arabia-to-publish-ifrs-Standards-for-adoption-purposes.aspx\">IFRS.org</a></em></p>\r\n\r\n<p>&nbsp;</p>', '2017-06-09', NULL, NULL, '0000-00-00 00:00:00'),
(9, 'en', '9a03efbc88b730405918fbc47b329d7c.jpg', 'kreston.com', 'kreston.com', 'England', '2017-07-20', 'Kreston admits Kreston HHES, Indonesia', 'kreston-admits-kreston-hhes-indonesia', '<p>Kreston International has announced that Kreston HHES (legal name KAP Hendrawinata Hanny Erwin &amp; Sumargo) has been admitted as a member. The firm was created following a restructuring of Kreston&rsquo;s former member which provides the added benefit that the partners and staff are well known within the global network having moved to Kreston from Grant Thornton in 2011.</p>\r\n\r\n<p>Kreston HHES now has 16 partners and 250 professional and support staff operating from excellent offices in Jakarta Surabaya and Medan. The firm is a full service practice providing audit and taxation services with advisory and consultancy services handed by PT Kreston Advisory Indonesia and Tax and Transfer Pricing by PT Pratama Indomitra Konsultan.</p>\r\n\r\n<p>Kreston CEO, Jon Lisby said &ldquo;We are delighted to re-establish our representation in Indonesia with Kreston HHES, a firm that has an exceptional reputation for the quality of services to international clients. The future growth of Indonesia on the global stage makes it essential for us to have secure the strongest representation available. Kreston members from throughout the Asia Pacific Region look forward to meeting up with Kreston HHES at the Kreston Regional Conference in Kuala Lumpur early next month.</p>\r\n\r\n<p>Erwin Winata, International Liaison Partner for Kreston HHES said &ldquo; We are proud to represent Kreston in Indonesia and we look forward to building the strength of the brand in the region together with our relationships with fellow Kreston Members and their clients from throughout the world.</p>\r\n\r\n<p><em>Image left to right:&nbsp;Erwin Winata of Kreston HHES, Nigel Banks Kreston International Director, Chris Edward of Kreston HHES, Jon Lisby Kreston CEO, Edomd Chan Kreston International Director, Sunil Goyal Kreston International Director.</em></p>', '2017-07-21', NULL, NULL, '0000-00-00 00:00:00'),
(10, 'en', 'f43f81b2b6a0b525d60c1427d07cdd04.jpg', 'kreston.com', 'kreston.com', 'UK', '2017-07-27', 'Kreston admits Mohamed Taha Hamood & Co (MTH), Yemen to its global network.', 'kreston-admits-mohamed-taha-hamood--co-mth-yemen-to-its-global-network', '<p>Kreston has announced that Mohamed Taha Hamood &amp; Co based in Sana&rsquo;a, the capital of Yemen has joined its membership expanding its coverage in the Middle East.</p>\r\n\r\n<p>Established in 1990, MTH is a full-service firm headed up by 3 partners and supported by several professional and support staff.</p>\r\n\r\n<p>Jon Lisby, Kreston CEO, said:<br />\r\n&ldquo;We are pleased to welcome Mohamed AlHashimi and his team to Kreston. MTH has excellent experience of working with international networks including the Big 4 and have demonstrated their commitment to international standards of quality and ethics&rdquo;.<br />\r\nManaging Partner Mohamed AlHashimi added:<br />\r\n&ldquo;I am very pleased that we are able to represent Kreston in the Yemen and we look forward to working with fellow members both in the region and across the global network&rdquo;.</p>', '2017-08-07', NULL, NULL, '0000-00-00 00:00:00'),
(11, 'en', 'c3dda88090feb5d9af48367c7f2af836.jpg', 'kreston.com', 'kreston.com', 'Malaysia', '2017-08-08', 'Kreston Asia Pacific Conference focuses on being a leader in a data driven world', 'kreston-asia-pacific-conference-focuses-on-being-a-leader-in-a-data-driven-world', '<p>Kreston&rsquo;s 2017 Asia Pacific Conference was held in the Grand Hyatt Hotel, Kuala Lumpur, Malaysia, from 3rd &ndash; 6th August attracting 53 delegates from 15 countries.</p>\r\n\r\n<p>Hosted by member firm Kreston John &amp; Gan the conference business agenda focused on &ldquo;Being a leader in a Data Driven World&rdquo;.</p>\r\n\r\n<p>Key Note this year was futurist and leading authority on technology trends Patrick Schwerdtfeger from Trend Mastery in California who delivered two sessions looking at the technology trends which will have an impact on the accounting profession and the commercial world &ndash; including artificial intelligence, big data, blockchain and fintech.</p>\r\n\r\n<p>Kreston CEO, Jon Lisby, updated the group on Kreston strategy including international brand development and Andrew Collier, Kreston&rsquo;s Director of Quality and Professional Standards, gave a quality review update and discussed how members can best engage with data analytics.</p>\r\n\r\n<p>Members from Kreston Indonesia, Exco France and Australia updated all on developments in their respective regions.</p>\r\n\r\n<p>Jon commented:<br />\r\n&ldquo;This was another excellent event. The conference gave all who attended a better understanding of how our profession is changing rapidly largely driven by the new technologies. Those leading the growth strategies for Kreston member firms in the &ldquo;Data-Driven World&rdquo; need to gain a thorough understanding of these technologies and how they may be leveraged in our profession for future advantage. Thank you to all at Kreston John and Gan for their excellent organisation. The Asia Pacific Conference continues to go from strength to strength and I look forward to the 2018 conference.&rdquo;</p>', '2017-08-09', NULL, NULL, '0000-00-00 00:00:00');

DROP TABLE IF EXISTS pages;
CREATE TABLE IF NOT EXISTS pages (
  id int(9) NOT NULL AUTO_INCREMENT,
  parent_id int(9) NOT NULL,
  title varchar(128) NOT NULL,
  menu_title varchar(128) NOT NULL,
  slug varchar(255) NOT NULL,
  content longtext NOT NULL,
  sequence int(11) NOT NULL DEFAULT '0',
  seo_title text NOT NULL,
  meta text NOT NULL,
  url varchar(255) NOT NULL,
  new_window tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO pages (id, parent_id, title, menu_title, slug, content, sequence, seo_title, meta, url, new_window) VALUES
(1, 0, 'Hubungi Kami', 'Hubungi Kami', 'contact-us', 'Lorem ipsum dolor sit amet, ignota latine salutatus mel no, labitur senserit at nec. Per ad altera aperiri pertinax, mel omnium democritum ut, ocurreret consequat adolescens vix in. Usu iudico maiorum eu, eum ei eius iracundia sadipscing, reque assueverit in sed. Nisl aeque virtute in sit, sed no nibh phaedrum, ea vis elitr saperet singulis.<br><br>Sit id aeque deterruisset. Eruditi disputationi vix no. At fugit decore invenire mei, ei eam inciderint scribentur, per epicurei urbanitas eu. In vis numquam splendide. Sed at viderer epicuri laboramus, mel eros perfecto legendos ea.<br><br>Ignota animal quo id, elit iisque sea ad. Est hinc vero conceptam an, ex ius amet audiam impetus. Ea elitr expetendis suscipiantur vim, aliquam consectetuer vim cu. Nonumy repudiare maiestatis ea per. Te duo exerci impetus, vim odio referrentur an. Eum et exerci voluptatum definitiones.<br><br>Ex zril vidisse eos, vis at viderer persius intellegam. Ius id mentitum repudiare vulputate, est ne delenit ceteros, ius eu labores praesent. Vim vocent gloriatur ne, antiopam sapientem mel at, at utinam minimum dissentiet nec. No cum recusabo eleifend persequeris. Ne mei sumo partem appareat.<br><br>His ex utinam cotidieque, in eos sale iisque. His ex senserit temporibus, id usu habeo altera pericula. Pro assum officiis eu. Et vero vivendo deleniti cum, vis novum nullam maiestatis ut, duo graeco omnium labores ne. Et vero scribentur vim, et mutat graece fastidii pri, cu zril ceteros perfecto sea.', 3, 'contact-us', '', '', 0),
(3, -1, 'Beranda', 'Beranda', 'beranda', '{{ banner(\'1\', \'5\', \'default\') }}\r\n<br><br>\r\nSelamat Datang di  Griyagaya..<br><br>Kami menyediakan berbagai macam jenis fashion berkualitas tinggi untuk segala kebutuhan penampilan anda. Mulai dari baju, celana, <br>jaket dan lainnya.<br><br>Berkonsep butik online, kami berusaha memanjakan sekaligus memudahkan para customer untuk berbelanja di toko online kami. <br>Kenyamanan dan keamanan pun kami jaga sehingga tercipta hubungan baik penjual dan pembeli.<br><br><br>Selamat berbelanja. Dan terima kasih atas kunjungan anda…<br><br>', 1, 'beranda', '', '', 0),
(4, 0, 'Cara Pembelian', 'Cara Pembelian', 'cara-pembelian', '<ul><li>Klik pada tombol Beli pada produk yang ingin Anda pesan.</li></ul><ul><li>Produk yang Anda pesan akan masuk ke dalam Keranjang Belanja. Anda dapat melakukan perubahan jumlah produk yang diinginkan dengan mengganti angka di kolom Jumlah, kemudian klik tombol Update. Sedangkan untuk menghapus sebuah produk dari Keranjang Belanja, klik tombol Kali yang berada di kolom paling kanan.</li></ul><ul><li>Jika sudah selesai, klik tombol Selesai Belanja, maka akan tampil form untuk pengisian data kustomer/pembeli.</li></ul><ul><li>Setelah data pembeli selesai diisikan, klik tombol Proses, maka akan tampil data pembeli beserta produk yang dipesannya (jika diperlukan catat nomor ordernya). Dan juga ada total pembayaran serta nomor rekening pembayaran.</li></ul><ul><li>Apabila telah melakukan pembayaran, maka produk/barang akan segera kami kirimkan.</li></ul>', 2, 'cara-pembelian', '', '', 0),
(5, 0, 'Profil', 'Profil', 'profil', 'ArtFashion  adalah toko fashion online, yang menyediakan segala kebutuhan fashion anda. ArtFashion ingin memberikan kemudahan kepada para calon pembeli, cara santai, mudah dan hemat dalam berbelanja fashion berkualias dengan harga yang terjangkau.<br><br>Karena itulah website ini dibuat sedemikian sederhananya sehingga diharapkan dapat membantu para pengunjung untuk dapat menelusuri produk-produk yang ditawarkan secara lebih mudah.<br><br>Selain melayani pesanan produk-produk yang ada di toko online, kami menerima pembuatan/pemesanan fashion sesuai design/pola  yang anda inginkan.<br><br>Akhirnya, kami mengucapkan terima kasih atas kunjungan anda di ArtFashion.', 1, 'profil', '', '', 0);

DROP TABLE IF EXISTS search;
CREATE TABLE IF NOT EXISTS search (
  code varchar(40) NOT NULL,
  term varchar(255) NOT NULL,
  PRIMARY KEY (code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO search (`code`, term) VALUES
('0a19a86f8796ede1c213d0395a99c2db', '{\"term\":\"Kreston\"}'),
('0cfceb4d515f07ead2af171430dddb16', '{\"variable\":\"7\"}'),
('167d0702bcaaf66acaeadf71985e0871', '{\"variable\":\"MESSAGE_CATEGORY\"}'),
('20f9a662afd9ff58131fa507bac787ea', '{\"term\":\"saa\"}'),
('3d1ce1034bc2826ca7209dafa483e447', '{\"term\":\"danang\"}'),
('519ab11f3d236175bbdba22ae8d6b79e', '{\"term\":\"MARR\"}'),
('525cceb6438bcae61bc58f5d34556ccf', '{\"term\":\"test\"}'),
('53f10223d8f20fec9ed7f9d7a1a8620b', '{\"variable\":\"MESSAGE_TYPE\"}'),
('599f84a63b53153da13c0d654a9db66a', '{\"variable\":\"1\"}'),
('5ce126e4e69a4d1dc7c58d791617312a', '{\"variable\":\"\"}'),
('5cf1793d6602fe097468678cd71a866e', '{\"variable\":\"4\"}'),
('5db4e0e4c4687bbe9e83efdcbc5241ec', '{\"term\":\"marr\"}'),
('68dcd64065d817c113562effdfeeecbc', '{\"variable\":\"PEOPLE\"}'),
('7a91ed7b4d3a1818a63fe4465a2a0674', '{\"term\":\"Phili\"}'),
('8061e2abab24fe377fd6c71d231ff8a0', '{\"variable\":\"MESSAGE_SENDER\"}'),
('8bd31fc762e7fef2abee89619d18bff8', '{\"term\":\"career\"}'),
('967510225f65978c5e77760a5b7a173a', '{\"term\":\"tes\"}'),
('d56b8adf596f3b95fb670b429d0e8b3d', '{\"term\":\"phliphines\"}'),
('e59e2e8956462e6150ab7f6b0958b5db', '{\"term\":\"\"}');

DROP TABLE IF EXISTS settings;
CREATE TABLE IF NOT EXISTS settings (
  id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  code varchar(255) DEFAULT NULL,
  setting_key varchar(255) DEFAULT NULL,
  setting longtext,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

INSERT INTO settings (id, `code`, setting_key, setting) VALUES
(1, 'cms', 'theme', 'alphasquad'),
(2, 'cms', 'locale', 'id_ID'),
(3, 'cms', 'currency_iso', 'IDR'),
(4, 'cms', 'new_customer_status', NULL),
(7, 'cms', 'default_meta_description', 'Kreston Indonesia'),
(8, 'cms', 'default_meta_keywords', 'Kreston Indonesia'),
(9, 'cms', 'timezone', 'Asia/Jakarta'),
(10, 'cms', 'company_name', ''),
(11, 'cms', 'homepage', NULL),
(12, 'cms', 'email_to', 'danang.alvaris@kreston.co.id'),
(13, 'cms', 'email_from', 'no-reply@kreston.co.id'),
(14, 'cms', 'email_method', 'smtp'),
(15, 'cms', 'smtp_server', 'mail.kreston.co.id'),
(16, 'cms', 'smtp_port', '25'),
(17, 'cms', 'smtp_username', 'danang.alvaris@kreston.co.id'),
(18, 'cms', 'smtp_password', '%^oax8Bahrb1'),
(21, 'cms', 'address1', ''),
(22, 'cms', 'address2', ''),
(23, 'cms', 'city', ''),
(25, 'cms', 'zip', ''),
(26, 'cms', 'stage_username', ''),
(27, 'cms', 'stage_password', ''),
(31, 'cms', 'ssl_support', '1'),
(32, 'cms', 'require_login', '0'),
(36, 'cms', 'enabled', '0'),
(40, 'cms', 'school_code', 'K');

DROP TABLE IF EXISTS slider;
CREATE TABLE IF NOT EXISTS slider (
  id int(11) NOT NULL AUTO_INCREMENT,
  image varchar(60) DEFAULT NULL,
  createdat varchar(20) DEFAULT NULL,
  createdby datetime DEFAULT NULL,
  changedat varchar(20) DEFAULT NULL,
  changedby datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS user;
CREATE TABLE IF NOT EXISTS `user` (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  realname varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  password varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  user_type smallint(2) DEFAULT NULL,
  user_image varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  social_media text COLLATE utf8_unicode_ci,
  nip varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  last_login datetime DEFAULT NULL,
  last_login_ip char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  groups varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  input_date date DEFAULT '0000-00-00',
  last_update date DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY username (username),
  KEY realname (realname)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (id, username, realname, `password`, email, user_type, user_image, social_media, nip, last_login, last_login_ip, groups, input_date, last_update) VALUES
(1, 'admin', 'Admin', '04275db24ad3070dc85c2d959640958d5d5a2901', 'webmaster@gmail.com', NULL, 'login.png', NULL, '197034', '2017-07-25 16:04:17', '118.97.70.76', 'a:1:{i:0;s:1:\"1\";}', '2015-11-11', '2017-07-25'),
(2, 'adminkreston', 'Administrator', '879711da7e576e934f4a7f11f6fc96a63dc720cd', 'webmaster@kreston.co.id', 1, 'login1.png', NULL, '99999', '2017-08-28 15:23:48', '202.158.37.130', 'a:1:{i:0;s:1:\"1\";}', '2017-07-25', NULL);

DROP TABLE IF EXISTS user_group;
CREATE TABLE IF NOT EXISTS user_group (
  group_id int(11) NOT NULL,
  group_name varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  input_date date DEFAULT NULL,
  last_update date DEFAULT NULL,
  PRIMARY KEY (group_id),
  UNIQUE KEY group_name (group_name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO user_group (group_id, group_name, input_date, last_update) VALUES
(1, 'Administrator', '2015-11-11', '2017-05-02'),
(2, 'Operator', '2015-11-18', '2015-11-18');

DROP TABLE IF EXISTS vacancy;
CREATE TABLE IF NOT EXISTS vacancy (
  id int(11) NOT NULL AUTO_INCREMENT,
  division varchar(20) NOT NULL,
  position varchar(150) DEFAULT NULL,
  heading text,
  requirements text,
  responsibilities text,
  description text,
  createdby varchar(20) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  updatedby varchar(20) DEFAULT NULL,
  updatedat datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO vacancy (id, division, `position`, heading, requirements, responsibilities, description, createdby, createdat, updatedby, updatedat) VALUES
(3, 'Audit', 'Senior Auditor', '<p>A challenging yet rewarding career with work life balance await you. Cheerfulness, a sense of togetherness, loyalty and sharing a sense of belongingness in our company and become an asset to us as we carry out our mission to ultimately achieve our vision. So&nbsp;for those of you who feel challenged and interested please join us and have a fulfilling career.</p>\r\n', '<ol>\r\n	<li>Bachelor or Master Degree in accounting with a minimum GPA of 3.00 (out of 4.00) from a reputable university, Registered Accountant would be preferable.</li>\r\n	<li>Has a relevant working experience as an external auditor in Public Accounting Firms with min. 3 years for Senior.</li>\r\n	<li>Strong knowledge and good understanding in Indo GAAP (PSAK), SPAP, IFRS, ISA.</li>\r\n	<li>Professional integrity, good character, pleasant personality, energetic, proactive.</li>\r\n	<li>Good leadership and communication skill, English fluently is highly desirable.</li>\r\n	<li>Strong analytical skill, meticulous, easy and good adaptable.</li>\r\n	<li>Familiar with computer is a must and others accounting software will be an advantage. &nbsp;</li>\r\n</ol>\r\n', '<ol>\r\n	<li>Prepare related audit examination</li>\r\n	<li>Develop the concept of audit examination</li>\r\n	<li>Able to follow the examination assigment</li>\r\n</ol>\r\n', '<p>Please mark the applied position on the top left corner of the envelope, and send your Comprehensive CV completed with relevant documents and recent photograph to :</p>\r\n\r\n<p><strong>HRD Manager</strong></p>\r\n\r\n<p><strong>KAP Hendrawinata Hanny Erwin &amp; Sumargo - Kreston</strong></p>\r\n\r\n<p><strong>Intiland Tower 18th Floor, Jl. Jend. Sudirman Kav. 32, Jakarta 10220, Indonesia.</strong></p>\r\n\r\n<p><strong>or</strong>&nbsp;<strong>Email to: <a href=\"mailto:career@kreston.co.id\">career@kreston.co.id</a></strong></p>\r\n', 'adminkreston', '2017-07-27 13:38:29', 'adminkreston', '2017-08-15 10:39:18');

DROP TABLE IF EXISTS variable;
CREATE TABLE IF NOT EXISTS variable (
  id smallint(6) NOT NULL AUTO_INCREMENT,
  variable varchar(30) DEFAULT NULL,
  value varchar(100) DEFAULT NULL,
  description varchar(50) DEFAULT NULL,
  createdby varchar(12) DEFAULT NULL,
  createdat datetime DEFAULT NULL,
  changedby varchar(12) DEFAULT NULL,
  changedat datetime DEFAULT NULL,
  orderby tinyint(4) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

INSERT INTO variable (id, variable, `value`, description, createdby, createdat, changedby, changedat, orderby) VALUES
(1, 'MESSAGE_SENDER', 'CUSTOMER', 'Customer', NULL, NULL, NULL, NULL, NULL),
(2, 'MESSAGE_SENDER', 'BRANCH', 'Branch', NULL, NULL, NULL, NULL, NULL),
(3, 'MESSAGE_SENDER', 'THIRD_PARTY', 'Third Party', NULL, NULL, NULL, NULL, NULL),
(4, 'MESSAGE_TYPE', 'QUESTION', 'QUESTION', NULL, NULL, NULL, NULL, NULL),
(5, 'MESSAGE_TYPE', 'REQUEST', 'REQUEST', NULL, NULL, NULL, NULL, NULL),
(6, 'MESSAGE_TYPE', 'COMPLAIN', 'COMPLAIN', NULL, NULL, NULL, NULL, NULL),
(7, 'MESSAGE_CATEGORY', 'PRODUCT', 'PRODUCT', NULL, NULL, NULL, NULL, NULL),
(8, 'MESSAGE_CATEGORY', 'SERVICES', 'SERVICES', NULL, NULL, NULL, NULL, NULL),
(9, 'MESSAGE_CATEGORY', 'GENERAL', 'GENERAL', NULL, NULL, NULL, NULL, NULL),
(10, 'MESSAGE_CATEGORY', 'CAREER', 'CAREER', NULL, NULL, NULL, NULL, NULL),
(11, 'MESSAGE_CATEGORY', 'EVENT', 'EVENT', NULL, NULL, NULL, NULL, NULL),
(12, 'MESSAGE_CATEGORY', 'OTHER', 'OTHER', NULL, NULL, NULL, NULL, NULL),
(26, 'PEOPLE', 'QUALITY', 'Quality Control', 'adminkreston', '2017-07-31 13:31:48', 'adminkreston', '2017-08-07 13:25:50', 5),
(18, 'PEOPLE', 'PARTNER', 'Board of Partners', 'adminkreston', '2017-07-27 09:15:18', 'adminkreston', '2017-08-07 13:25:28', 1),
(19, 'PEOPLE', 'JAKARTA', 'Jakarta', 'adminkreston', '2017-07-27 09:17:35', 'adminkreston', '2017-08-16 16:07:54', 2),
(20, 'PEOPLE', 'SURABAYA', 'Surabaya', 'adminkreston', '2017-07-27 09:18:02', 'adminkreston', '2017-08-16 16:07:42', 3),
(21, 'PEOPLE', 'MEDAN', 'Medan', 'adminkreston', '2017-07-27 09:18:16', 'adminkreston', '2017-08-16 16:07:30', 4),
(22, 'PEOPLE', 'KAI', 'PT. Kreston Advisory Indonesia', 'adminkreston', '2017-07-27 09:19:39', NULL, NULL, 6),
(23, 'PEOPLE', 'PRATAMA', 'PT. Pratama Indomitra', 'adminkreston', '2017-07-27 09:20:01', NULL, NULL, 7);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
