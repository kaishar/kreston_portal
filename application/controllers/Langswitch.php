<?php f ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Langswitch extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }
 
    public function switchLanguage($language = "") {
    	$language =  $this->uri->segment(1,'english');
    	$this->session->set_userdata('site_lang', $language);
    	redirect(base_url());
    }
}