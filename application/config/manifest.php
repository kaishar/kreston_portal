<?php defined('BASEPATH') OR exit('No direct script access allowed');
//DO NOT EDIT THIS FILE

//ClassMap for autoloader
$classes = array (
  'GoCart\\Controller\\AdminCustomers' => 'D:\\www\\kreston_portal\\application\\modules\\customers\\controllers\\AdminCustomers.php',
  'Customers' => 'D:\\www\\kreston_portal\\application\\modules\\customers\\models\\Customers.php',
  'GoCart\\Controller\\AdminDashboard' => 'D:\\www\\kreston_portal\\application\\modules\\dashboard\\controllers\\AdminDashboard.php',
  'GoCart\\Controller\\AdminLogin' => 'D:\\www\\kreston_portal\\application\\modules\\login\\controllers\\AdminLogin.php',
  'Login' => 'D:\\www\\kreston_portal\\application\\modules\\login\\models\\Login.php',
  'GoCart\\Controller\\MyAccount' => 'D:\\www\\kreston_portal\\application\\modules\\my_account\\controllers\\MyAccount.php',
  'GoCart\\Controller\\About' => 'D:\\www\\kreston_portal\\application\\modules\\pages\\controllers\\About.php',
  'GoCart\\Controller\\Careers' => 'D:\\www\\kreston_portal\\application\\modules\\pages\\controllers\\Careers.php',
  'GoCart\\Controller\\Contact' => 'D:\\www\\kreston_portal\\application\\modules\\pages\\controllers\\Contact.php',
  'GoCart\\Controller\\Download' => 'D:\\www\\kreston_portal\\application\\modules\\pages\\controllers\\Download.php',
  'GoCart\\Controller\\Frame' => 'D:\\www\\kreston_portal\\application\\modules\\pages\\controllers\\Frame.php',
  'GoCart\\Controller\\Newsroom' => 'D:\\www\\kreston_portal\\application\\modules\\pages\\controllers\\Newsroom.php',
  'GoCart\\Controller\\Page' => 'D:\\www\\kreston_portal\\application\\modules\\pages\\controllers\\Page.php',
  'Pages' => 'D:\\www\\kreston_portal\\application\\modules\\pages\\models\\Pages.php',
  'GoCart\\Controller\\Search' => 'D:\\www\\kreston_portal\\application\\modules\\search\\controllers\\Search.php',
  'Search' => 'D:\\www\\kreston_portal\\application\\modules\\search\\models\\Search.php',
  'GoCart\\Controller\\AdminSettings' => 'D:\\www\\kreston_portal\\application\\modules\\settings\\controllers\\AdminSettings.php',
  'Messages' => 'D:\\www\\kreston_portal\\application\\modules\\settings\\models\\Messages.php',
  'Settings' => 'D:\\www\\kreston_portal\\application\\modules\\settings\\models\\Settings.php',
  'GoCart\\Controller\\AdminSitemap' => 'D:\\www\\kreston_portal\\application\\modules\\sitemap\\controllers\\AdminSitemap.php',
  'GoCart\\Controller\\AdminAppuser' => 'D:\\www\\kreston_portal\\application\\modules\\system\\controllers\\AdminAppuser.php',
  'GoCart\\Controller\\AdminModules' => 'D:\\www\\kreston_portal\\application\\modules\\system\\controllers\\AdminModules.php',
  'GoCart\\Controller\\AdminUsergroup' => 'D:\\www\\kreston_portal\\application\\modules\\system\\controllers\\AdminUsergroup.php',
  'Group_access' => 'D:\\www\\kreston_portal\\application\\modules\\system\\models\\Group_access.php',
  'Mst_module' => 'D:\\www\\kreston_portal\\application\\modules\\system\\models\\Mst_module.php',
  'User' => 'D:\\www\\kreston_portal\\application\\modules\\system\\models\\User.php',
  'User_group' => 'D:\\www\\kreston_portal\\application\\modules\\system\\models\\User_group.php',
  'GoCart\\Controller\\AdminUsers' => 'D:\\www\\kreston_portal\\application\\modules\\users\\controllers\\AdminUsers.php',
  'GoCart\\Controller\\Adminentrinews' => 'D:\\www\\kreston_portal\\application\\modules\\web\\controllers\\adminentrinews.php',
  'GoCart\\Controller\\Adminentrislider' => 'D:\\www\\kreston_portal\\application\\modules\\web\\controllers\\adminentrislider.php',
  'GoCart\\Controller\\Affiliation' => 'D:\\www\\kreston_portal\\application\\modules\\web\\controllers\\affiliation.php',
  'GoCart\\Controller\\Branch' => 'D:\\www\\kreston_portal\\application\\modules\\web\\controllers\\branch.php',
  'GoCart\\Controller\\Keypeople' => 'D:\\www\\kreston_portal\\application\\modules\\web\\controllers\\keypeople.php',
  'GoCart\\Controller\\Parameter' => 'D:\\www\\kreston_portal\\application\\modules\\web\\controllers\\parameter.php',
  'GoCart\\Controller\\Vacancy' => 'D:\\www\\kreston_portal\\application\\modules\\web\\controllers\\vacancy.php',
  'M_affiliation' => 'D:\\www\\kreston_portal\\application\\modules\\web\\models\\M_affiliation.php',
  'M_branch' => 'D:\\www\\kreston_portal\\application\\modules\\web\\models\\M_branch.php',
  'M_contact' => 'D:\\www\\kreston_portal\\application\\modules\\web\\models\\M_contact.php',
  'M_keypeople' => 'D:\\www\\kreston_portal\\application\\modules\\web\\models\\M_keypeople.php',
  'M_news' => 'D:\\www\\kreston_portal\\application\\modules\\web\\models\\M_news.php',
  'M_slider' => 'D:\\www\\kreston_portal\\application\\modules\\web\\models\\M_slider.php',
  'M_vacancy' => 'D:\\www\\kreston_portal\\application\\modules\\web\\models\\M_vacancy.php',
  'GoCart\\Controller\\AdminWysiwyg' => 'D:\\www\\kreston_portal\\application\\modules\\wysiwyg\\controllers\\AdminWysiwyg.php',
  'Auth' => 'D:\\www\\kreston_portal\\application\\libraries\\Auth.php',
  'Breadcrumbs' => 'D:\\www\\kreston_portal\\application\\libraries\\Breadcrumbs.php',
  'GoCart\\Emails' => 'D:\\www\\kreston_portal\\application\\libraries\\Emails.php',
  'GdImage' => 'D:\\www\\kreston_portal\\application\\libraries\\GdImage.php',
  'Menu' => 'D:\\www\\kreston_portal\\application\\libraries\\Menu.php',
  'MY_Form_validation' => 'D:\\www\\kreston_portal\\application\\libraries\\MY_Form_validation.php',
  'MY_Image_lib' => 'D:\\www\\kreston_portal\\application\\libraries\\MY_Image_lib.php',
  'Populate' => 'D:\\www\\kreston_portal\\application\\libraries\\Populate.php',
  'qqUploadedFileXhr' => 'D:\\www\\kreston_portal\\application\\libraries\\QqUploadedFileXhr.php',
  'qqUploadedFileForm' => 'D:\\www\\kreston_portal\\application\\libraries\\QqUploadedFileXhr.php',
  'qqFileUploader' => 'D:\\www\\kreston_portal\\application\\libraries\\QqUploadedFileXhr.php',
  'Session' => 'D:\\www\\kreston_portal\\application\\libraries\\Session.php',
  'simbio_date' => 'D:\\www\\kreston_portal\\application\\libraries\\Simbio_date.php',
  'UploadHandler' => 'D:\\www\\kreston_portal\\application\\libraries\\UploadHandler.php',
  'xmlparser' => 'D:\\www\\kreston_portal\\application\\libraries\\xmlparser.php',
  'GoCart\\Controller\\Admin' => 'D:\\www\\kreston_portal\\application\\core\\AdminController.php',
  'CI' => 'D:\\www\\kreston_portal\\application\\core\\CI.php',
  'GoCart\\Controller' => 'D:\\www\\kreston_portal\\application\\core\\Controller.php',
  'GoCart\\Controller\\Front' => 'D:\\www\\kreston_portal\\application\\core\\FrontController.php',
  'GC' => 'D:\\www\\kreston_portal\\application\\core\\GC.php',
  'GoCart' => 'D:\\www\\kreston_portal\\application\\core\\GoCart.php',
  'MY_Config' => 'D:\\www\\kreston_portal\\application\\core\\MY_Config.php',
  'MY_Lang' => 'D:\\www\\kreston_portal\\application\\core\\MY_Lang.php',
  'MY_Model' => 'D:\\www\\kreston_portal\\application\\core\\MY_Model.php',
  'GoCart\\Libraries\\View' => 'D:\\www\\kreston_portal\\application\\core\\View.php',
);

//Theme Shortcodes
$GLOBALS['themeShortcodes'] = array (
);

//Complete Module List
$GLOBALS['modules'] = array (
  0 => 'D:\\www\\kreston_portal\\application\\modules/customers',
  1 => 'D:\\www\\kreston_portal\\application\\modules/dashboard',
  2 => 'D:\\www\\kreston_portal\\application\\modules/login',
  3 => 'D:\\www\\kreston_portal\\application\\modules/my_account',
  4 => 'D:\\www\\kreston_portal\\application\\modules/pages',
  5 => 'D:\\www\\kreston_portal\\application\\modules/search',
  6 => 'D:\\www\\kreston_portal\\application\\modules/settings',
  7 => 'D:\\www\\kreston_portal\\application\\modules/sitemap',
  8 => 'D:\\www\\kreston_portal\\application\\modules/system',
  9 => 'D:\\www\\kreston_portal\\application\\modules/users',
  10 => 'D:\\www\\kreston_portal\\application\\modules/web',
  11 => 'D:\\www\\kreston_portal\\application\\modules/wysiwyg',
);

//Defined Routes
$routes = array (
  0 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/export',
    2 => 'GoCart\\Controller\\AdminCustomers#export',
  ),
  1 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/get_subscriber_list',
    2 => 'GoCart\\Controller\\AdminCustomers#getSubscriberList',
  ),
  2 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#form',
  ),
  3 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/addresses/[i:id]',
    2 => 'GoCart\\Controller\\AdminCustomers#addresses',
  ),
  4 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/delete/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#delete',
  ),
  5 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/groups',
    2 => 'GoCart\\Controller\\AdminCustomers#groups',
  ),
  6 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/group_form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#groupForm',
  ),
  7 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/delete_group/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#deleteGroup',
  ),
  8 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/address_list/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#addressList',
  ),
  9 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/address_form/[i:customer_id]/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#addressForm',
  ),
  10 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/delete_address/[i:customer_id]/[i:id]',
    2 => 'GoCart\\Controller\\AdminCustomers#deleteAddress',
  ),
  11 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/[:order_by]?/[:direction]?/[i:page]?',
    2 => 'GoCart\\Controller\\AdminCustomers#index',
  ),
  12 => 
  array (
    0 => 'GET',
    1 => '/admin/dashboard',
    2 => 'GoCart\\Controller\\AdminDashboard#index',
  ),
  13 => 
  array (
    0 => 'GET',
    1 => '/admin',
    2 => 'GoCart\\Controller\\AdminDashboard#index',
  ),
  14 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/login',
    2 => 'GoCart\\Controller\\AdminLogin#login',
  ),
  15 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/logout',
    2 => 'GoCart\\Controller\\AdminLogin#logout',
  ),
  16 => 
  array (
    0 => 'GET|POST',
    1 => '/login/[:redirect]?',
    2 => 'GoCart\\Controller\\Login#login',
  ),
  17 => 
  array (
    0 => 'GET|POST',
    1 => '/logout',
    2 => 'GoCart\\Controller\\Login#logout',
  ),
  18 => 
  array (
    0 => 'GET|POST',
    1 => '/forgot-password',
    2 => 'GoCart\\Controller\\Login#forgotPassword',
  ),
  19 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/forgot-password',
    2 => 'GoCart\\Controller\\AdminLogin#forgotPassword',
  ),
  20 => 
  array (
    0 => 'GET|POST',
    1 => '/register',
    2 => 'GoCart\\Controller\\Login#register',
  ),
  21 => 
  array (
    0 => 'GET|POST',
    1 => '/my-account',
    2 => 'GoCart\\Controller\\MyAccount#index',
  ),
  22 => 
  array (
    0 => 'GET|POST',
    1 => '/my-account/downloads',
    2 => 'GoCart\\Controller\\MyAccount#downloads',
  ),
  23 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/frame/news/[i:id]?',
    2 => 'GoCart\\Controller\\Frame#news',
  ),
  24 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/page/contactus',
    2 => 'GoCart\\Controller\\Page#contactus',
  ),
  25 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/search',
    2 => 'GoCart\\Controller\\Page#search',
  ),
  26 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/search/[:orderBy]/[:orderDir]/[:code]/[i:page]?',
    2 => 'GoCart\\Controller\\Page#search',
  ),
  27 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/about-us',
    2 => 'GoCart\\Controller\\About#index',
  ),
  28 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/newsroom',
    2 => 'GoCart\\Controller\\Newsroom#index',
  ),
  29 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/newsroom/[:orderBy]/[:orderDir]/[:code]/[i:page]?',
    2 => 'GoCart\\Controller\\Newsroom#index',
  ),
  30 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/contact-us',
    2 => 'GoCart\\Controller\\Contact#index',
  ),
  31 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/career',
    2 => 'GoCart\\Controller\\Careers#index',
  ),
  32 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/frame/people/[i:id]?',
    2 => 'GoCart\\Controller\\Frame#people',
  ),
  33 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/frame/affiliation/[i:id]?',
    2 => 'GoCart\\Controller\\Frame#affiliation',
  ),
  34 => 
  array (
    0 => 'GET|POST',
    1 => '/[en|id]/frame/career/[i:id]?',
    2 => 'GoCart\\Controller\\Frame#vacancy',
  ),
  35 => 
  array (
    0 => 'GET|POST',
    1 => '/search/[:code]?/[:sort]?/[:dir]?/[:page]?',
    2 => 'GoCart\\Controller\\Search#index',
  ),
  36 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/settings',
    2 => 'GoCart\\Controller\\AdminSettings#index',
  ),
  37 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/settings/canned_messages',
    2 => 'GoCart\\Controller\\AdminSettings#canned_messages',
  ),
  38 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/settings/canned_message_form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminSettings#canned_message_form',
  ),
  39 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/settings/delete_message/[i:id]',
    2 => 'GoCart\\Controller\\AdminSettings#delete_message',
  ),
  40 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap',
    2 => 'GoCart\\Controller\\AdminSitemap#index',
  ),
  41 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap/new-sitemap',
    2 => 'GoCart\\Controller\\AdminSitemap#newSitemap',
  ),
  42 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap/generate-products',
    2 => 'GoCart\\Controller\\AdminSitemap#generateProducts',
  ),
  43 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap/generate-pages',
    2 => 'GoCart\\Controller\\AdminSitemap#generatePages',
  ),
  44 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap/generate-categories',
    2 => 'GoCart\\Controller\\AdminSitemap#generateCategories',
  ),
  45 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap/complete-sitemap',
    2 => 'GoCart\\Controller\\AdminSitemap#completeSitemap',
  ),
  46 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/modules/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\AdminModules#index',
  ),
  47 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/modules/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminModules#form',
  ),
  48 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/modules/delete/[i:itemID]?',
    2 => 'GoCart\\Controller\\AdminModules#delete',
  ),
  49 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/modules/delete_selected/[i:itemID]?',
    2 => 'GoCart\\Controller\\AdminModules#delete_selected',
  ),
  50 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/appuser/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\AdminAppuser#index',
  ),
  51 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/appuser/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminAppuser#form',
  ),
  52 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/appuser/delete/[i:itemID]?',
    2 => 'GoCart\\Controller\\AdminAppuser#delete',
  ),
  53 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/appuser/delete_selected/[i:itemID]?',
    2 => 'GoCart\\Controller\\AdminAppuser#delete_selected',
  ),
  54 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/usergroup/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\AdminUsergroup#index',
  ),
  55 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/usergroup/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminUsergroup#form',
  ),
  56 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/usergroup/delete/[i:itemID]?',
    2 => 'GoCart\\Controller\\AdminUsergroup#delete',
  ),
  57 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/usergroup/delete_selected/[i:itemID]?',
    2 => 'GoCart\\Controller\\AdminUsergroup#delete_selected',
  ),
  58 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/appuser/change-profile/[userName]?',
    2 => 'GoCart\\Controller\\AdminAppuser#change_profile',
  ),
  59 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/users',
    2 => 'GoCart\\Controller\\AdminUsers#index',
  ),
  60 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/users/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminUsers#form',
  ),
  61 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/users/delete/[i:id]',
    2 => 'GoCart\\Controller\\AdminUsers#delete',
  ),
  62 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/branch/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\Branch#index',
  ),
  63 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/branch/form/[i:id]?',
    2 => 'GoCart\\Controller\\Branch#form',
  ),
  64 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/branch/myuploader?',
    2 => 'GoCart\\Controller\\Branch#myuploader',
  ),
  65 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/branch/croping?',
    2 => 'GoCart\\Controller\\Branch#croping',
  ),
  66 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/branch/delete/[i:itemID]?',
    2 => 'GoCart\\Controller\\Branch#delete',
  ),
  67 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/branch/delete_selected/[i:itemID]?',
    2 => 'GoCart\\Controller\\Branch#delete_selected',
  ),
  68 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/news/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\Adminentrinews#index',
  ),
  69 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/news/form/[i:id]?',
    2 => 'GoCart\\Controller\\Adminentrinews#form',
  ),
  70 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/news/myuploader?',
    2 => 'GoCart\\Controller\\Adminentrinews#myuploader',
  ),
  71 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/news/croping?',
    2 => 'GoCart\\Controller\\Adminentrinews#croping',
  ),
  72 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/news/delete/[i:itemID]?',
    2 => 'GoCart\\Controller\\Adminentrinews#delete',
  ),
  73 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/news/delete_selected/[i:itemID]?',
    2 => 'GoCart\\Controller\\Adminentrinews#delete_selected',
  ),
  74 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/parameter/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\Parameter#index',
  ),
  75 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/parameter/form/[i:id]?',
    2 => 'GoCart\\Controller\\Parameter#form',
  ),
  76 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/parameter/delete/[i:itemID]?',
    2 => 'GoCart\\Controller\\Parameter#delete',
  ),
  77 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/parameter/delete_selected/[i:itemID]?',
    2 => 'GoCart\\Controller\\Parameter#delete_selected',
  ),
  78 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/people/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\Keypeople#index',
  ),
  79 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/people/form/[i:id]?',
    2 => 'GoCart\\Controller\\Keypeople#form',
  ),
  80 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/people/myuploader?',
    2 => 'GoCart\\Controller\\Keypeople#myuploader',
  ),
  81 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/people/croping?',
    2 => 'GoCart\\Controller\\Keypeople#croping',
  ),
  82 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/people/delete/[i:itemID]?',
    2 => 'GoCart\\Controller\\Keypeople#delete',
  ),
  83 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/people/delete_selected/[i:itemID]?',
    2 => 'GoCart\\Controller\\Keypeople#delete_selected',
  ),
  84 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/affiliation/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\Affiliation#index',
  ),
  85 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/affiliation/form/[i:id]?',
    2 => 'GoCart\\Controller\\Affiliation#form',
  ),
  86 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/affiliation/myuploader?',
    2 => 'GoCart\\Controller\\Affiliation#myuploader',
  ),
  87 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/affiliation/croping?',
    2 => 'GoCart\\Controller\\Affiliation#croping',
  ),
  88 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/affiliation/delete/[i:itemID]?',
    2 => 'GoCart\\Controller\\Affiliation#delete',
  ),
  89 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/affiliation/delete_selected/[i:itemID]?',
    2 => 'GoCart\\Controller\\Affiliation#delete_selected',
  ),
  90 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/vacancy/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\Vacancy#index',
  ),
  91 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/vacancy/form/[i:id]?',
    2 => 'GoCart\\Controller\\Vacancy#form',
  ),
  92 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/vacancy/delete/[i:itemID]?',
    2 => 'GoCart\\Controller\\Vacancy#delete',
  ),
  93 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/vacancy/delete_selected/[i:itemID]?',
    2 => 'GoCart\\Controller\\Vacancy#delete_selected',
  ),
  94 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/wysiwyg/upload_image',
    2 => 'GoCart\\Controller\\AdminWysiwyg#upload_image',
  ),
  95 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/wysiwyg/get_images',
    2 => 'GoCart\\Controller\\AdminWysiwyg#get_images',
  ),
);