<?php
function format_date($date){	
	if ($date != '' && $date != '0000-00-00')
	{
		$d	= explode('-', $date);
	
		$m	= Array(
		'January'
		,'February'
		,'March'
		,'April'
		,'May'
		,'June'
		,'July'
		,'August'
		,'September'
		,'October'
		,'November'
		,'December'
		);
	
		return $m[$d[1]-1].' '.$d[2].', '.$d[0]; 
	}
	else
	{
		return false;
	}
}

function reverse_format($date)
{
	if(empty($date)) 
	{
		return;
	}
	
	$d = explode('-', $date);
	
	return "{$d[1]}-{$d[2]}-{$d[0]}";
}

function format_ymd($date)
{
	if(empty($date) || $date == '00-00-0000')
	{
		return '';
	}
	else
	{
		$d = explode('-', $date);
		return $d[2].'-'.$d[0].'-'.$d[1];
	}
}

function format_mdy($date)
{
	if(empty($date) || $date == '0000-00-00')
	{
		return '';
	}
	else
	{
		return date('m-d-Y', strtotime($date));
	}
	
}

function date_bahasa($tgl)
{
    $tanggal = substr($tgl,8,2);
    $bulan = get_bulan(substr($tgl,5,2));
    $tahun = substr($tgl,0,4);
    return $tanggal.' '.$bulan.' '.$tahun;		 
}

function get_bulan($bln)
{
    switch ($bln)
    {
        case 1: 
                return "Januari";
                break;
        case 2:
                return "Februari";
                break;
        case 3:
                return "Maret";
                break;
        case 4:
                return "April";
                break;
        case 5:
                return "Mei";
                break;
        case 6:
                return "Juni";
                break;
        case 7:
                return "Juli";
                break;
        case 8:
                return "Agustus";
                break;
        case 9:
                return "September";
                break;
        case 10:
                return "Oktober";
                break;
        case 11:
                return "November";
                break;
        case 12:
                return "Desember";
                break;
    }
}
	
/*Konversi format tanggal dari database ke tampilan di user interface (page)*/
function dateDBToPage($dateDBStr){
    $temp =strtotime($dateDBStr);
    if(trim($temp)==943894800)
        return " ";
    if($temp==0 || $temp==-1)
        return " ";
    else
        return date(DATE_IN_PAGE,strtotime($dateDBStr));
}

/*Konversi format tanggal dari tampilan di user interface ke database*/
function datePageToDB($datePageStr)
{
    if(trim($datePageStr)=="")
        return date(DATE_IN_DB,DEFAULT_DB_DATE);
    else
        return date(DATE_IN_DB,strtotime($datePageStr));
}

/*Cari tanggal sekarang*/
function getCurrentDay(){
    return date("j");
}

/*Cari bulan sekarang*/
function getCurrentMonth(){
    return date("n");
}

/*Cari tahun sekarang*/
function getCurrentYear(){
    return date("Y");
}

/*Cari tanggal sekarang dengan format untuk ditampilkan di page*/
function getCurrentDate(){
    //return "17-Feb-2003";
    return date(DATE_IN_PAGE);
}

/*Cari nama bulan (dalam bahasa indonesia)*/
function getMonthName($monthNo){
    switch($monthNo)
    {
        case 1:
            return "Januari";
            break;
        case 2:
            return "Februari";
            break;
        case 3:
            return "Maret";
            break;
        case 4:
            return "April";
            break;
        case 5:
            return "Mei";
            break;
        case 6:
            return "Juni";
            break;
        case 7:
            return "Juli";
            break;
        case 8:
            return "Agustus";
            break;
        case 9:
            return "September";
            break;
        case 10:
            return "Oktober";
            break;
        case 11:
            return "November";
            break;
        case 12:
            return "Desember";
            break;
    }
}

	/*Cari padanan bilangan Romawi dari bilangan Desimal*/
function getBilRomawi($angka)
{
	switch($angka)
	{
		case 1:
			return "I";
			break;
		case 2:
			return "II";
			break;
		case 3:
			return "III";
			break;
		case 4:
			return "IV";
			break;
		case 5:
			return "V";
			break;
		case 6:
			return "VI";
			break;
		case 7:
			return "VII";
			break;
	}
}

/*Isi option di list nama-nama bulan*/
function drawMonthList($month = false)
{
    echo "<option value=1 ". ($month == 1 ? 'selected' : '') .">Januari</option>\n";
    echo "<option value=2 ". ($month == 2 ? 'selected' : '') .">Februari</option>\n";
    echo "<option value=3 ". ($month == 3 ? 'selected' : '') .">Maret</option>\n";
    echo "<option value=4 ". ($month == 4 ? 'selected' : '') .">April</option>\n";
    echo "<option value=5 ". ($month == 5 ? 'selected' : '') .">Mei</option>\n";
    echo "<option value=6 ". ($month == 6 ? 'selected' : '') .">Juni</option>\n";
    echo "<option value=7 ". ($month == 7 ? 'selected' : '') .">Juli</option>\n";
    echo "<option value=8 ". ($month == 8 ? 'selected' : '') .">Agustus</option>\n";
    echo "<option value=9 ". ($month == 9 ? 'selected' : '') .">September</option>\n";
    echo "<option value=10 ". ($month == 10 ? 'selected' : '') .">Oktober</option>\n";
    echo "<option value=11 ". ($month == 11 ? 'selected' : '') .">November</option>\n";
    echo "<option value=12 ". ($month == 12 ? 'selected' : '') .">Desember</option>\n";
}

/*Isi option di list tahun*/
function drawYearList($year = false)
{
    $thn_awal = date("Y");
    $thn_akhir = $thn_awal+9;

    for($i = $thn_awal; $i <= $thn_akhir; $i++)
    {
        echo "<option value='" . $i . "' " . ($year == $i ? 'selected' : '') . ">" . $i . "</option>\n";
    }
}

/*Isi option di list tahun*/
function drawYearListPast($year = false)
{
    $thn_awal = date("Y");
    $thn_akhir = $thn_awal-10;

    for($i = $thn_akhir; $i <= $thn_awal; $i++)
    {
        echo "<option value='" . $i . "' " . ($year == $i ? 'selected' : '') . ">" . $i . "</option>\n";
    }
}

/*Cari nama hari (dalam bahasan indonesia)*/
function getDayName($dayNo)
{
	switch($dayNo)
	{
		case 1:
			return "Senin";
			break;
		case 2:
			return "Selasa";
			break;
		case 3:
			return "Rabu";
			break;
		case 4:
			return "Kamis";
			break;
		case 5:
			return "Jum'at";
			break;
		case 6:
			return "Sabtu";
			break;
		case 7:
			return "Minggu";
			break;
	}
}

/*
Hitung tanggal untuk x bulan ke depan.
Format tanggal: yyyy-mm-dd
*/
function getNextMonthDate($date,$month)
{
	$time = strtotime($date);
	$y1 = date("Y",$time);
	$m1 = date("n",$time);
	$d1 = date("d",$time);

	$m2 = $m1 + $month;
	if($m2 <= 12){
		$y2 = $y1;			
	}else{
		$sy = $m2 / 12;
		$sy = floor($sy);
		$y2 = $y1 + $sy;
		$m2 = $m2 % 12;
	}

	/*validasi tanggal*/
	if($d1 <= 28)
	{			
		$d2 = $d1;
	} else
	{
		/*Jika februari, cek kabisat */
		if($m2 == 2){
			if($y2 % 4 == 0) /*kabisat*/
				$d2 = 29;
			else /*bukan kabisat*/
				$d2 = 28;
		}else{
			if($d1 <= 30)
				$d2 = $d1;
			else{
				if($m2==1 || $m2==3 || $m2==5 || $m2==7 || $m2==8 || $m2==10 || $m2==12 )
					$d2 = 31;
				else
					$d2 = 30;
			}								
		}		
	}
	$retval = $y2."-".$m2."-".$d2;
	return $retval;	
}

/* End of file welcome.php */
/* Location: ./system/application/helpers/MY_date_helper.php */
