<?php
class variable extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
	
	public function get_parameter($search=false, $sort_by='', $sort_order='desc', $limit=0, $offset=0)
    {
		if ($search)
        {
            if(!empty($search->variable))
            {
                $this->get_parameter_searchlike($search->variable);
            }
        }
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('variable')->result();
    }

    public function get_parameter_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->variable))
            {
                $this->get_parameter_searchlike($search->variable);
            }
        }
        
        return CI::db()->count_all_results('variable');
    }
	
    private function get_parameter_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }
			
            $like = '';
            $like .= "( variable.id ".$not."LIKE '%".$t."%' " ;
            $like .= $operator." variable.variable ".$not."LIKE '%".$t."%' " ;
            $like .= $operator." variable.value ".$not."LIKE '%".$t."%' " ;
            $like .= $operator." variable.description ".$not."LIKE '%".$t."%' )" ;
            
            CI::db()->where($like);
        }
    }
	    
    public function get_variable($str)
    {
        $this->db->select('id,
						variable,
                        value,
                        description');
        $this->db->where('variable',$str);
        return $this->db->get('variable')->result();
    }
	
	public function get_variable_orderby($str, $sort_by, $sort_order)
    {
        $this->db->select('id,
						variable,
                        value,
                        description');
        $this->db->where('variable',$str);
		$this->db->order_by($sort_by, $sort_order);
        return $this->db->get('variable')->result();
    }
	
	public function get_list_parameter()
    {
		$_sql = "SELECT id, variable FROM variable group by variable";
					
        return $this->db->query($_sql)->result();
    }
	
	public function get_parameter_byid($id)
    {
        $this->db->select('id,
        				variable,
                        value,
                        description');
        $this->db->where('id', $id);
		return $this->db->get('variable')->row();
    }
	
	public function save($parameter)
    {
        $admin = $this->session->userdata('admin');
        $record = $this->get_parameter_byid($parameter['id']);
		
		if(count($record) > 0)
        {
        	$parameter['changedby'] = $admin['username'];
            $parameter['changedat'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $parameter['id']);
			CI::db()->update('variable', $parameter);
			
			//echo $this->db->last_query(); exit;
            return $parameter['id'];
        }
        else
        {            
            $parameter['createdby'] = $admin['username'];
            $parameter['createdat'] = date('Y-m-d H:i:s');
                        
            CI::db()->insert('variable', $parameter);
            return $parameter['id'];
        }
    }
    
    public function delete($id)
    {
    	$admin = $this->session->userdata('admin');
    	 
    	CI::db()->where('id', $id);
    	CI::db()->delete('variable');
    }
	
}