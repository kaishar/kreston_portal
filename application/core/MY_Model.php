<?php
class MY_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getNextId($idName, $tableName, $filter="")
    {
        $str = "SELECT MAX($idName) as id FROM $tableName ";
        if($filter != "")
        {
            $str = $str . " WHERE $filter";           
        }
        $query = CI::db()->query($str);
			
        if($query->num_fields() > 0)
        {
            $row = $query->row_array();
            $temp = $row["id"] + 1; //next key*/
        }else { //belum ada recordnya
            $temp = 1;
        }
			
        return $temp;
    }
    
    public function getNextCode($idName, $tableName, $filter="")
    {
        $str = "SELECT MAX(CAST($idName as SIGNED INTEGER)) as id FROM $tableName ";
        if($filter != "")
        {
            $str = $str . " WHERE $filter";           
        }
        
        $query = CI::db()->query($str);
			
        if($query->num_fields() > 0)
        {
            $row = $query->row_array();
            $temp = $row["id"] + 1; //next key*/
        }else { //belum ada recordnya
            $temp = 1;
        }
	
        return $temp;
    }
    
    public function getNextControlnum($idName, $tableName, $filter="") 
    {
        $str = "SELECT MAX(CAST(SUBSTRING_INDEX($idName, '-', -1) as SIGNED INTEGER)) as controlnum from $tableName ";
    
        if($filter != "")
        {
            $str = $str . " WHERE $filter";           
        }
        
        $query = CI::db()->query($str);
			
        if($query->num_fields() > 0)
        {
            $row = $query->row_array();
            $temp = $row["controlnum"] + 1; //next key*/
        }else { //belum ada recordnya
            $temp = 1;
        }
	
        return $temp;
    }
}