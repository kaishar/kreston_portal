<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu
{
    private $modules_dir = 'modules';
    private $module_table = 'mst_module';
    public $module_list = array();
    public $appended_first = '<li><a href="#">Shortcut</a></li>';
        
    // Custom Menu Layout
    public function main_menu()
    {
        $_menu 	        = '';
        $icon           = array(
                            'home'          => 'fa fa-home',
                            'system'        => 'fa fa-keyboard-o',
                            'laporan'       => 'fa fa-file-text-o',
                            'logout'        => 'fa fa-close');
        
        $appended_first  = $this->appended_first;

        $sql = 'SELECT * FROM '.$this->module_table.' ORDER BY module_id ASC';
        $_mods_q = CI::db()->query($sql);
		
        foreach ($_mods_q->result_array() as $_mods_d)
        {
            $module_list[] = array('name' => $_mods_d['module_name'], 'path' => $_mods_d['module_path'], 'desc' => $_mods_d['module_desc']);
        }
        	
        $_menu 	.= '<ul id="menu-top" class="nav navbar-nav navbar-right">';
        //$_menu 	.= $appended_first;
        $_menu 	.= '<li><a href="'.site_url('admin/dashboard').'">Dashboard</a></li>';
        //$_menu 	.= $this->sub_menu('default', $module_list);
        //$_menu 	.= '</li>'."\n";
                
        if ($module_list) {
            foreach ($module_list as $_modul) {
                $_formated_module_name = ucwords(str_replace('_', ' ', $_modul['name']));
                $_mod_dir = $_modul['path'];

                if (isset($_SESSION['priv'][$_modul['path']]['r']) && $_SESSION['priv'][$_modul['path']]['r'] && file_exists(APPPATH.$this->modules_dir.DS.$_mod_dir)) {
                    //$_menu .= '<li>';
                    //print_r($_modul); exit;
                    $_menu .= $this->sub_menu($_mod_dir, $_modul);
                    //$_menu .= '</li>';
                }
            }
        }
        $_menu .= '<li><a href="'. site_url('admin/logout') .'">Logout</a></li>';
        $_menu .= '</ul>';
        return $_menu;
    }

    public function sub_menu($str_module = '', $_module = array())
    {	          
        $_submenu = '<li class="dropdown">';
        $_submenu .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.strtoupper($str_module).'<span class="caret"></span></a>';
        $_submenu .= '<ul class="dropdown-menu">';
                
        if($str_module == 'default') {
            $_submenu_file 	= APPPATH.$this->modules_dir.DS.'dashboard'.DS.'submenu.php';          
        }else {
            $_submenu_file 	= APPPATH.$this->modules_dir.DS.$_module['path'].DS.'submenu.php';
        }               

        if ($str_module == 'default') {
            include $_submenu_file;
            
            $shortcuts = $this->get_shortcuts_menu();
            //print_r($shortcuts); exit;
            if(count($shortcuts) > 0) {
                foreach ($shortcuts as $shortcut) {
                    $path = preg_replace('@^.+?\|/@i', '', $shortcut);
                    $label = preg_replace('@\|.+$@i', '', $shortcut);
                    $menu[] = array(($label), $path, ($label));
                } 
            }
        } else 
        {
            include $_submenu_file;                       
        }
        
        // iterate menu array
        foreach ($menu as $i=>$_list) 
        {
            if ($_list[0] == 'Header') 
            {
                $_submenu .= '<li>'.$menu[$i][1].'</li>'."\n";
            } else 
            {
                $_submenu .= '<li><a href="'.$menu[$i][1].'" title="'.( isset($menu[$i][2])?$menu[$i][2]:$menu[$i][0] ).'"> '.$menu[$i][0].'</a></li>'."\n";
            }
        }
        $_submenu .= '</ul></li>';
        
        return $_submenu;
    }

    public function get_shortcuts_menu()
    {
        $shortcuts = array();

        $sql = 'SELECT setting FROM settings WHERE setting_key LIKE \'shortcuts\'';
        $shortcuts_q = CI::db()->query($sql);
        $shortcuts_d = $shortcuts_q->row_array();
        
        if (count($shortcuts_d) > 0) 
        {
            $shortcuts = unserialize($shortcuts_d['setting']);
        }
        return $shortcuts;
    }
}
