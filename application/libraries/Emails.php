<?php namespace GoCart;
/**
 * Emails Class
 *
 * @package     Emails
 * @subpackage  Library
 * @category    GoCart
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */
class Emails {

    static function sendEmail($email)
    {
        $mailType = config_item('email_method');
        if($mailType == 'smtp')
        {
            $transport = \Swift_SmtpTransport::newInstance(config_item('smtp_server'), config_item('smtp_port'))->setUsername(config_item('smtp_username'))->setPassword(config_item('smtp_password'));
        }
        elseif($mailType == 'sendmail')
        {
            $transport = \Swift_SendmailTransport::newInstance(config_item('sendmail_path'));
        }
        else //Mail
        {
            $transport = \Swift_MailTransport::newInstance();
        }
        //get the mailer
        $mailer = \Swift_Mailer::newInstance($transport);

        //send the message
        $mailer->send($email);
    }
    
    static function Contactus($id) 
    {
    	$email = \Swift_Message::newInstance();
    	
    	$cannedMessage = \CI::db()->where('id', '1')->get('canned_messages')->row_array();
    	
    	$loader = new \Twig_Loader_String();
    	$twig = new \Twig_Environment($loader);
    	
    	$contact_data = \CI::M_contact()->get_contactdetail_byid($id);
    	$branch = \CI::M_branch()->get_branchname_byemail($contact_data['branch']);
    	
    	//fields for the template
    	$fields = ['fullname'		=> $contact_data['fullname'],
    			'email'   		=> $contact_data['email'],
    			'phone'         => $contact_data['phone'],
    			'branch'		=> $branch['branch_name'],
    			'sender'		=> $contact_data['sender'],
				'message_type'			=> $contact_data['message_type'],
    			'message_category'		=> $contact_data['message_category'],
    			'message'       => $contact_data['message']
    	];
    	
    	//render the subject and content to a variable
    	$subject = $twig->render($cannedMessage['subject'], $fields);
    	$content = $twig->render($cannedMessage['content'], $fields);
    	
    	$email->setFrom(config_item('email_from')); //email address the website sends from
    	    	
    	$to = $contact_data['branch']; 
    	$email->setTo($to);
    	
    	$email->setBcc(array(
    			'kaishar.wibisana@gmail.com',
    			'danangalvaris@gmail.com'));
    	
    	$email->setReturnPath(config_item('email_to')); //this is the bounce if they submit a bad email
    	$email->setSubject($subject);
    	$email->setBody($content, 'text/html');
    	$email->setContentType("text/html");
    	
    	self::sendEmail($email);    	
    }

}
