<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Populate
{
    public function select_populate($variable, $choosetxt)
    {
    	\CI::load()->model(array('Variable'));
    	
    	$list = \CI::Variable()->get_variable($variable);
    	$list_select[NULL] = $choosetxt;
    	foreach($list as $rows)
    	{
    		$list_select[$rows->VALUE] = $rows->DESCRIPTION;
    	}
    	
    	return $list_select;
    }
    
}
