<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="designer" content="alphasquad inc.">
    <meta name="programmer" content="materawali">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <title>Kreston Indonesia - Content Management System</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="<?=base_url('assets/css/font-awesome.css')?>" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" />
    <link href="<?=base_url('assets/plugins/fineuploader/fineuploader.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('assets/plugins/jcrop/jquery.Jcrop.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('assets/plugins/colorbox/colorbox.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('assets/plugins/jQuery-Impromptu/jquery-impromptu.css');?>" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo theme_url('storage/img/logoicon/favicon.ico'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_img('logoicon/apple-touch-icon-144-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_img('logoicon/apple-touch-icon-114-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_img('logoicon/apple-touch-icon-72-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo theme_img('logoicon/apple-touch-icon-57-precomposed.png'); ?>">
</head>
<body>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero navbarcms">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('admin'); ?>">
                    <img src="<?=theme_img('logoicon/krestonlogo.png')?>" />
                </a>
            </div>
            <div class="left-div">
                <div class="user-settings-wrapper">
                    <ul class="nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                            </a>
                            <div class="dropdown-menu dropdown-settings">
                                <div class="media">
                                    <a class="media-left" href="#">
                                        <img src="<?=theme_upload('user/thumbnails/'.$profilepict)?>" />
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><?php echo $logininfo; ?></h4>
                                    </div>
                                </div>
                                <hr />
                                &nbsp; <a href="<?=site_url('admin/logout')?>" class="btn btn-danger">Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse">
                        <?php echo $main_menu; ?>
                    </div>
                </div>
            </div>            
        </div>
    </section>
    <!-- MENU SECTION END-->