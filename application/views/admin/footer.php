    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; <?=date('Y')?> KRESTON INDONESIA
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
	<script src="<?=base_url('assets/js/updater.js'); ?>"></script> 
    <script src="<?=base_url('assets/js/gui.js'); ?>"></script> 
    <script src="<?=base_url('assets/js/form.js'); ?>"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="<?=base_url('assets/js/bootstrap.js')?>"></script>
    <script src="<?=base_url('assets/plugins/colorbox/jquery.colorbox-min.js'); ?>"></script>
	<script src="<?=base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
	<script src="<?=base_url('assets/plugins/jQuery-Impromptu/jquery-impromptu.js'); ?>"></script>
	<script src="<?=base_url('assets/plugins/fineuploader/jquery.fineuploader-3.0.min.js'); ?>"></script>
	<script src="<?=base_url('assets/plugins/jcrop/jquery.Jcrop.min.js'); ?>"></script>
	<script src="<?=base_url('assets/js/jquery-uberuploadcropper.js'); ?>"></script>
	<script src="<?=base_url('assets/js/calendar.js')?>"></script>
			
	<?php if(CI::uri()->segment(2) == 'news') { ?>
	<script type="text/javascript">
		$(function() {
			$('#UploadImages').uberuploadcropper({
				//---------------------------------------------------
				// uploadify options..
				//---------------------------------------------------
				fineuploader: {
					//debug : true,
					request	: { 
						// params: {}
						endpoint: "<?php echo site_url('admin/news/myuploader'); ?>" 
					},						
					validation: {
						//sizeLimit	: 0,
						allowedExtensions: ['jpg','png']
					}
				},
				//---------------------------------------------------
				//now the cropper options..
				//---------------------------------------------------
				jcrop: {
					aspectRatio  : 4/3, 
					allowSelect  : false, //can reselect
					allowResize  : true,  //can resize selection
					setSelect    : [ 0, 0, 150, 150 ], //these are the dimensions of the crop box x1,y1,x2,y2
					minSize      : [ 150, 150 ], //if you want to be able to resize, use these
					maxSize      : [ 533, 400 ]
				},
				//---------------------------------------------------
				//now the uber options..
				//---------------------------------------------------
				folder           : '<?php echo base_url('uploads/temp').'/'; ?>', // only used in uber, not passed to server
				cropAction       : '<?php echo site_url('admin/news/croping'); ?>', // server side request to crop image
				onComplete       : function(e,imgs,data){ 
					//var $PhotoPrevs = $('#PhotoPrevs');

					for(var i=0,l=imgs.length; i<l; i++){
						$( "div.first" ).replaceWith('<img src="<?php echo base_url('uploads/temp/'); ?>'+ '/' + imgs[i].filename +'" />');
						$( "div.second" ).replaceWith('<input type="hidden" name="file_name" value="' + imgs[i].filename +'" />');
					}
				}
			});
		});
	</script>	
	<?php }elseif(CI::uri()->segment(2) == 'branch') { ?>
	<script type="text/javascript">
		$(function() {
			$('#UploadImages').uberuploadcropper({
				//---------------------------------------------------
				// uploadify options..
				//---------------------------------------------------
				fineuploader: {
					//debug : true,
					request	: { 
						// params: {}
						endpoint: "<?php echo site_url('admin/branch/myuploader'); ?>" 
					},						
					validation: {
						//sizeLimit	: 0,
						allowedExtensions: ['jpg','png']
					}
				},
				//---------------------------------------------------
				//now the cropper options..
				//---------------------------------------------------
				jcrop: {
					aspectRatio  : 1/1, 
					allowSelect  : false, //can reselect
					allowResize  : true,  //can resize selection
					setSelect    : [ 0, 0, 150, 150 ], //these are the dimensions of the crop box x1,y1,x2,y2
					minSize      : [ 150, 150 ], //if you want to be able to resize, use these
					maxSize      : [ 533, 533 ]
				},
				//---------------------------------------------------
				//now the uber options..
				//---------------------------------------------------
				folder           : '<?php echo base_url('uploads/temp').'/'; ?>', // only used in uber, not passed to server
				cropAction       : '<?php echo site_url('admin/branch/croping'); ?>', // server side request to crop image
				onComplete       : function(e,imgs,data){ 
					//var $PhotoPrevs = $('#PhotoPrevs');

					for(var i=0,l=imgs.length; i<l; i++){
						$( "div.first" ).replaceWith('<img src="<?php echo base_url('uploads/temp/'); ?>'+ '/' + imgs[i].filename +'" />');
						$( "div.second" ).replaceWith('<input type="hidden" name="file_name" value="' + imgs[i].filename +'" />');
					}
				}
			});
		});
	</script>	
	<?php }elseif(CI::uri()->segment(2) == 'people') { ?>
	<script type="text/javascript">
		$(function() {
			$('#UploadImages').uberuploadcropper({
				//---------------------------------------------------
				// uploadify options..
				//---------------------------------------------------
				fineuploader: {
					//debug : true,
					request	: { 
						// params: {}
						endpoint: "<?php echo site_url('admin/people/myuploader'); ?>" 
					},						
					validation: {
						//sizeLimit	: 0,
						allowedExtensions: ['jpg','png']
					}
				},
				//---------------------------------------------------
				//now the cropper options..
				//---------------------------------------------------
				jcrop: {
					aspectRatio  : 3/4, 
					allowSelect  : false, //can reselect
					allowResize  : true,  //can resize selection
					setSelect    : [ 0, 0, 150, 150 ], //these are the dimensions of the crop box x1,y1,x2,y2
					minSize      : [ 150, 150 ], //if you want to be able to resize, use these
					maxSize      : [ 300, 400 ]
				},
				//---------------------------------------------------
				//now the uber options..
				//---------------------------------------------------
				folder           : '<?php echo base_url('uploads/temp').'/'; ?>', // only used in uber, not passed to server
				cropAction       : '<?php echo site_url('admin/people/croping'); ?>', // server side request to crop image
				onComplete       : function(e,imgs,data){ 
					//var $PhotoPrevs = $('#PhotoPrevs');

					for(var i=0,l=imgs.length; i<l; i++){
						$( "div.first" ).replaceWith('<img src="<?php echo base_url('uploads/temp/'); ?>'+ '/' + imgs[i].filename +'" />');
						$( "div.second" ).replaceWith('<input type="hidden" name="file_name" value="' + imgs[i].filename +'" />');
					}
				}
			});
		});
	</script>	
	<?php }elseif(CI::uri()->segment(2) == 'affiliation') { ?>
	<script type="text/javascript">
		$(function() {
			$('#UploadImages').uberuploadcropper({
				//---------------------------------------------------
				// uploadify options..
				//---------------------------------------------------
				fineuploader: {
					//debug : true,
					request	: { 
						// params: {}
						endpoint: "<?php echo site_url('admin/affiliation/myuploader'); ?>" 
					},						
					validation: {
						//sizeLimit	: 0,
						allowedExtensions: ['jpg','png']
					}
				},
				//---------------------------------------------------
				//now the cropper options..
				//---------------------------------------------------
				jcrop: {
					aspectRatio  : 16/9, 
					allowSelect  : false, //can reselect
					allowResize  : true,  //can resize selection
					setSelect    : [ 0, 0, 150, 150 ], //these are the dimensions of the crop box x1,y1,x2,y2
					minSize      : [ 150 , 150 ], //if you want to be able to resize, use these
					maxSize      : [ 800, 450 ]
				},
				//---------------------------------------------------
				//now the uber options..
				//---------------------------------------------------
				folder           : '<?php echo base_url('uploads/temp').'/'; ?>', // only used in uber, not passed to server
				cropAction       : '<?php echo site_url('admin/affiliation/croping'); ?>', // server side request to crop image
				onComplete       : function(e,imgs,data){ 
					//var $PhotoPrevs = $('#PhotoPrevs');

					for(var i=0,l=imgs.length; i<l; i++){
						$( "div.first" ).replaceWith('<img src="<?php echo base_url('uploads/temp/'); ?>'+ '/' + imgs[i].filename +'" />');
						$( "div.second" ).replaceWith('<input type="hidden" name="file_name" value="' + imgs[i].filename +'" />');
					}
				}
			});
		});
	</script>	
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(
			function() 
			{									
				if((document.getElementById('news_desc_id') !== null) || (document.getElementById('news_desc_en') !== null)) 
				{
					CKEDITOR.replace('news_desc_id');
					CKEDITOR.replace('news_desc_en');
					
				}else if(document.getElementById('profile') !== null)
				{
					CKEDITOR.replace('profile');
				}else if(document.getElementById('heading') !== null || document.getElementById('requirements') !== null || document.getElementById('responsibilities') !== null || document.getElementById('description') !== null)
				{
					CKEDITOR.replace('description');
					CKEDITOR.replace('requirements');
					CKEDITOR.replace('responsibilities');
					CKEDITOR.replace('heading');
				}
				CKEDITOR.add 
			}
		);
	</script>
</body>
</html>