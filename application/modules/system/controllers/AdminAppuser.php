<?php namespace GoCart\Controller;

class AdminAppuser extends Admin {
    var $id = false; 
    var $current_admin = false;

    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('User','Variable','User_group'));
        \CI::load()->model('Search');

        \CI::load()->helper(array('formatting'));
        /*
        if (!\CI::auth()->check_access(1, false, false)) {
            \CI::session()->set_flashdata('error', 'You don\'t have enough privileges to view this section');
            redirect(site_url('admin'));
        } 
        */
        
        $this->current_admin = \CI::session()->userdata('admin');
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'System Users';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['user'] = \CI::User()->getUser($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::User()->getUserCount($term); 

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('admin/appuser/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 7;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('appuser', $data);
    } 
    
    public function form($id = false, $profile = false)
    {
        \CI::load()->helper('form');
        \CI::load()->library('form_validation');
                
        $config['upload_path'] = 'uploads/img/user/full';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $config['encrypt_name'] = false;
        \CI::load()->library('upload', $config);
        
        $data['page_title'] = 'Add User';
        
        //default values are empty if the module is new
        $data['id'] = '';
        $data['username'] = '';
        $data['realname'] = '';
        $data['password'] = '';
        $data['email'] = '';
        $data['nip'] = '';
        $data['user_image'] = '';
        $data['groups'] = array();
        $data['message'] = '';
        
        if($profile)
        {
            $data['profile'] = $profile;
        }else {
            $data['profile'] = \CI::input()->post('profile');
        }
                
               
        // get group list
        $data['group_list'] = \CI::User_group()->get_group();
        
        if ($id)
        { 
            $this->id = $id;
            $user = \CI::User()->get_user($id);
            if (!$user)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/appuser/index');
            }
        			
            //set values to db values
            $data['id'] = $user->id;
            $data['username'] = $user->username;
            $data['realname'] = $user->realname;
            $data['password'] = $user->password;
            $data['email'] = $user->email;
            $data['nip'] = $user->nip;
            $data['user_image'] = $user->user_image;
            $data['groups'] = unserialize($user->groups);
            $data['message'] = 'Leave Password field blank if you don\'t want to change the password';
        }
        
        //\CI::form_validation()->set_rules('username', 'Username', 'trim|required|callback_username_check');
        \CI::form_validation()->set_rules('username', 'Username', ['trim', 'required', 'max_length[128]', ['username_callable', function($str){
            //$user = \CI::User()->check_username($str, $this->id);
            $user = \CI::Auth()->check_username($str, $this->id);
            if ($user)
            {
                \CI::form_validation()->set_message('username_callable', 'The {field} field already exist');
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }]]);
        
        \CI::form_validation()->set_rules('realname', 'Realname', ['trim', 'required', 'max_length[128]', ['realname_callable', function($str){
            if ($str == 'administrator')
            {
                \CI::form_validation()->set_message('realname_callable', 'The {field} field can not be the word "administrator"');            
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }]]);
        
        //\CI::form_validation()->set_rules('realname', 'Real Name', 'trim|required|callback_realname_check');                
        //\CI::form_validation()->set_rules('password', 'Password', 'trim|required|min_length[8]|sha1');
        //\CI::form_validation()->set_rules('passconf', 'Password Confirmation', 'trim|required|sha1|matches[password]');
        //if this is a new account require a password, or if they have entered either a password or a password confirmation
        if (\CI::input()->post('password') != '' || \CI::input()->post('passconf') != '' || !$id)
        {
            \CI::form_validation()->set_rules('password', 'Password', 'required|min_length[6]|sha1');
            \CI::form_validation()->set_rules('passconf', 'Password confirmation', 'required|sha1|matches[password]');
        }
        \CI::form_validation()->set_rules('email', 'Email', 'trim|required|valid_email');
        
        if (\CI::form_validation()->run() == FALSE)
        {
            //echo 'print'; exit;
            $this->view('appuser_form', $data);
        }
        else
        {
            $uploaded = \CI::upload()->do_upload('image');
            
            if ($id)
            {
                //delete the original file if another is uploaded
                if($uploaded)
                {
                    
                    if($data['image'] != '')
                    {
                        $file = [];
                        $file[] = 'uploads/img/user/full/'.$data['image'];
                        $file[] = 'uploads/img/user/medium/'.$data['image'];
                        $file[] = 'uploads/img/user/small/'.$data['image'];
                        $file[] = 'uploads/img/user/thumbnails/'.$data['image'];
                        
                        foreach($file as $f)
                        {
                            //delete the existing file if needed
                            if(file_exists($f))
                            {
                                unlink($f);
                            }
                        }
                    }
                }
                
            }
            
            if(!$uploaded)
            {
                $data['error'] = \CI::upload()->display_errors();
                if($_FILES['image']['error'] != 4)
                {
                    $data['error'] .= \CI::upload()->display_errors();
                    $this->view('appuser_form', $data);
                    return; //end script here if there is an error
                }
            }
            else
            {
                $image = \CI::upload()->data();
                $save['user_image'] = $image['file_name'];
                
                \CI::load()->library('image_lib');
                
                //this is the larger image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/img/user/full/'.$save['user_image'];
                $config['new_image'] = 'uploads/img/user/medium/'.$save['user_image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 600;
                $config['height'] = 500;
                \CI::image_lib()->initialize($config);
                \CI::image_lib()->resize();
                \CI::image_lib()->clear();

                //small image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/img/user/medium/'.$save['user_image'];
                $config['new_image'] = 'uploads/img/user/small/'.$save['user_image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 300;
                \CI::image_lib()->initialize($config); 
                \CI::image_lib()->resize();
                \CI::image_lib()->clear();

                //cropped thumbnail
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/img/user/small/'.$save['user_image'];
                $config['new_image'] = 'uploads/img/user/thumbnails/'.$save['user_image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;
                \CI::image_lib()->initialize($config); 
                \CI::image_lib()->resize(); 
                \CI::image_lib()->clear();
            }
            
            // parsing groups data
            $groups = '';
            if (null !== \CI::input()->post('groups_checked')) 
            {
                $groups = serialize(\CI::input()->post('groups_checked'));
            } else {
                $groups = 'literal{NULL}';
            }
            $save['groups'] = trim($groups);
            
            
            $save['id'] = $id;
            $save['username'] = \CI::input()->post('username');
            $save['realname'] = \CI::input()->post('realname');
            if(\CI::input()->post('password')) {
                $save['password'] = \CI::input()->post('password');
            }            
            $save['email'] = \CI::input()->post('email');
            $save['nip'] = \CI::input()->post('nip');
                                                            
            \CI::User()->save($save);
            
            \CI::session()->set_flashdata('message', 'The user has been saved.');
            
            if(!$data['profile']) {
                redirect('admin/appuser/index');  
            }else {
                redirect('admin/appuser/change-profile');  
            }
        }
    }
    
    public function delete()
    {
        $id = \CI::input()->post('itemID'); 
        if ($id)
        { 
            $user = \CI::User()->get_user($id);
            if (!$user)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/appuser/index');
            }
            else
            {
                \CI::User()->delete($id);
                
                \CI::session()->set_flashdata('message', lang('message_customer_deleted'));
                redirect('admin/appuser/index');
            }
        }
        else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
            redirect('admin/appuser/index');
        }
    }
    
    public function delete_selected()
    {
        $item = array();
        $item = \CI::input()->post('itemID');
        if (!is_array($item)) 
        {
            // make an array
            $item = array((integer)$item);
        }
        
        // loop array
        foreach ($item as $id) {
            $id = (integer)$id;
            \CI::User()->delete($id);
        }
        
        \CI::session()->set_flashdata('message', lang('message_customer_deleted'));
        redirect('admin/appuser/index');
    }
    
    public function username_check($str)
    {
        if ($str == 'admin')
        {
            \CI::form_validation()->set_message('username_check', 'The {field} field can not be the word "admin"');            
            return FALSE;
        }else
        {
            if(\CI::User()->check_username($str))
            {
                \CI::form_validation()->set_message('username_check', 'The {field} field already exist');
                return TRUE;
            }else
            {
                return FALSE;
            }
        }
    }
    
    public function realname_check($str)
    {
        if ($str == 'administrator')
        {
            \CI::form_validation()->set_message('realname_check', 'The {realname} field can not be the word "administrator"');            
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
        
    public function change_profile()
    {
        $admin = \CI::session()->userdata('admin');
        
        //find id admin by user name
        $id_admin = \CI::User()->get_id_by_username($admin['username']);
        $this->form($id_admin['id'], true);
    }    
    
}