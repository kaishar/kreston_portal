<?php

// only administrator have privileges for below menus
if (CI::auth()->check_access(1, false, false)) {
    //$menu[] = array(('Modules'), site_url('admin/modules/index'), ('Configure Application Modules'));
    $menu[] = array(('System Users'), site_url('admin/appuser/index'), ('Manage Application User or Library Staff'));
    $menu[] = array(('User Group'), site_url('admin/usergroup/index'), ('Manage Group of Application User'));
}
