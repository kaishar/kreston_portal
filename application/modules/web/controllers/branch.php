<?php namespace GoCart\Controller;

class Branch extends Admin {
    
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('M_branch','Search'));
		\CI::load()->helper(array('formatting'));
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'branch';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['branch'] = \CI::M_branch()->get_branch($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::M_branch()->get_branch_count($term); 

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('admin/branch/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 7;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('branch', $data);
    }
    
    public function form($id = false)
    {
		\CI::load()->helper('form');
        \CI::load()->library('form_validation');
			        
        $data['page_title'] = 'Entri Branch';
        
        $data['id'] = '';
        $data['branch_name'] = '';
        $data['address1'] = '';
        $data['address2'] = '';
        $data['city'] = '';
        $data['phone'] = '';
		$data['fax'] = '';
		$data['photo'] = '';
		$data['latitude'] = '';
        $data['longitude'] = '';
        $data['pic_name'] = '';
        $data['pic_email'] = '';
                
        if ($id)
        { 
            $branch = \CI::M_branch()->get_branch_byid($id);
            
            if (!$branch)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/branch/index');
            }
           			
            //set values to db values
			$data['id'] = $branch->id;
			$data['branch_name'] = $branch->branch_name;
			$data['address1'] = $branch->address1;
			$data['address2'] = $branch->address2;
			$data['city'] = $branch->city;
			$data['phone'] = $branch->phone;
			$data['fax'] = $branch->fax;
			$data['photo'] = $branch->photo;
			$data['latitude'] = $branch->latitude;
			$data['longitude'] = $branch->longitude;
			$data['pic_name'] = $branch->pic_name;
			$data['pic_email'] = $branch->pic_email;
        }
        
        \CI::form_validation()->set_rules('branch_name', 'branch Name', 'trim|required');
		\CI::form_validation()->set_rules('address1', 'Address', 'trim|required');
		\CI::form_validation()->set_rules('phone', 'Phone Number', 'trim|required');
		\CI::form_validation()->set_rules('latitude', 'Latitude', 'trim|required|numeric');
		\CI::form_validation()->set_rules('longitude', 'Longitude', 'trim|required|numeric');
				                                        
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('branch_form', $data);
        }else
        {		
			if(!$id)
			{
				$sImage = \CI::input()->post('file_name');
												
				//jika data baru harus ada upload picture dan pdf file
				if(!$sImage)
				{
					$data['error']  = 'Photo File Required';
					$this->view('branch_form', $data);
				}else
				{
					$save['photo']  = $sImage;					
				}
			}else
			{				
				//delete the picture file if another is uploaded
				$sImage = \CI::input()->post('file_name');
				if($sImage)
				{		
					//hapus gambar lamanya
					if($data['photo'] != '')
					{
						$file = 'uploads/img/branch/'.$data['photo'];
						//delete the existing file if needed
						if(file_exists($file))
						{
							unlink($file);
						}
					}

					$save['photo']  = $sImage;
				}
			}
			
			if ($sImage) {
				//bersih bersih temporary photo
				$gd = \CI::GdImage();
				
				$filePath = UPLOAD_TEMP . $save['photo'];
				$gd->copy($filePath, UPLOAD_BRANCH . $save['photo']);
				unlink($filePath);
			}
			
			$save['id'] = \CI::input()->post('id');
			$save['branch_name'] = \CI::input()->post('branch_name');
			$save['address1'] = \CI::input()->post('address1');
			$save['address2'] = \CI::input()->post('address2');
			$save['city'] = \CI::input()->post('city');
			$save['phone'] = \CI::input()->post('phone');
			$save['fax'] = \CI::input()->post('fax');
			$save['latitude'] = \CI::input()->post('latitude');
			$save['longitude'] = \CI::input()->post('longitude');
			$save['pic_name'] = \CI::input()->post('pic_name');
			$save['pic_email'] = \CI::input()->post('pic_email');
			
            $branch_id = \CI::M_branch()->save($save);
            
            \CI::session()->set_flashdata('message', 'Branch already saved.');
            
            redirect('admin/branch/index');
        }
    }
    
    public function delete()
    {
        $id = \CI::input()->post('itemID'); 
        if ($id)
        { 
            $branch = \CI::M_branch()->get_branch_byid($id);
            if (!$branch)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/branch/index');
            }else
            {            	
                \CI::M_branch()->delete($id);
                \CI::session()->set_flashdata('message', 'Data already deleted');
                redirect('admin/branch/index');
            }
        }else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
            redirect('admin/branch/index');
        }
    }
    
    public function delete_selected()
    {
        $item = array();
        $item = \CI::input()->post('itemID');
        
        if (!is_array($item)) 
        {
            // make an array
            $item = array($item);
        }
        
        // loop array
        foreach ($item as $id) {
        	$branch = \CI::M_branch()->get_branch_byid($id);
        	
        	if ($branch)
        	{        		
        		\CI::M_branch()->delete($id); 
        		\CI::session()->set_flashdata('message', 'Data already deleted');        		
        	}else
        	{
        		\CI::session()->set_flashdata('message', 'No data found');
        	}       	
        }
        redirect('admin/branch/index');
    }
	
	function myuploader()
    {
        // list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $allowedExtensions = array('jpeg','jpg','gif','png');

        // max file size in bytes
        $sizeLimit = 8 * 1024 * 1024; // max file size in bytes
        $uploader = \CI::qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload(UPLOAD_TEMP, false, md5(uniqid()));

        /* 
        For your crop you should:
        1) Make a copy of the full size original
        2) Scale down or up this image so it fits in the browser nicely
        3) When the crop occurs we will manipulate the full size image
        */
        //require "gd_image.php";
        $gd = \CI::GdImage();

        // step 1: make a copy of the original
        $filePath = UPLOAD_TEMP . $result['filename'];
        $copyName = $gd->createName($result['filename'], '_FULLSIZE');
        $gd->copy($filePath, UPLOAD_TEMP . $copyName);

        // step 2: Scale down or up this image so it fits in the browser nicely, lets say 500px is safe
        $oldSize = $gd->getProperties($filePath);
        $Promoize = $gd->getAspectRatio($oldSize['w'], $oldSize['h'], 533, 0);
        $gd->resize($filePath, $Promoize['w'], $Promoize['h']);

        // to pass data through iframe you will need to encode all html tags
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        //echo json_encode($result);
        //return $result['filename'];
    } 
    
    function croping()
    {
        $gd = \CI::GdImage();

        foreach($_POST['imgcrop'] as $k => $v) 
        {
            /*
                1) delete the resized image from upload, we will only be working with the full size
                2) compute new coordinates of full size image
                3) crop full size image
                4) resize the cropped image to what ever size we need
            */

            // 1) delete resized, move to full size
            $filePath = UPLOAD_TEMP . $v['filename'];
            $fullSizeFilePath = UPLOAD_TEMP . $gd->createName($v['filename'], '_FULLSIZE');
            unlink($filePath);
            rename($fullSizeFilePath, $filePath);

            // 2) compute the new coordinates
            $scaledSize = $gd->getProperties($filePath);
            $percentChange = $scaledSize['w'] / 533; // we know we scaled by width of 500 in upload
            $newCoords = array(
                            'x' => $v['x'] * $percentChange,
                            'y' => $v['y'] * $percentChange,
                            'w' => $v['w'] * $percentChange,
                            'h' => $v['h'] * $percentChange
            );

            // 3) crop the full size image
            $gd->crop($filePath, $newCoords['x'], $newCoords['y'], $newCoords['w'], $newCoords['h']);

            // 4) resize the cropped image to whatever size we need (lets go with 533 wide)
            $ar = $gd->getAspectRatio($newCoords['w'], $newCoords['h'], 533, 0);
            $gd->resize($filePath, $ar['w'], $ar['h']);

        }

        echo "1";
    }
}