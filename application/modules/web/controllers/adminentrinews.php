<?php namespace GoCart\Controller;

class Adminentrinews extends Admin {
    
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('M_news'));
		\CI::load()->library(array('qqUploadedFileXhr','GdImage'));
        
        \CI::load()->model('Search');
        \CI::load()->helper(array('formatting','permalink'));
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'News';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['news'] = \CI::M_news()->get_news($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::M_news()->get_news_count($term); 

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('admin/news/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 7;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('news', $data);
    }
    
    public function form($id = false)
    {
		\CI::load()->helper('form');
        \CI::load()->library('form_validation');
			        
        $data['page_title'] = 'Entri News';
        
        $data['id'] = '';
        $data['image'] = '';
        $data['photo_by'] = '';
        $data['news_by'] = '';
        $data['place'] = '';
        $data['news_date'] = '';
        $data['title_id'] = '';
        $data['title_en'] = '';
        $data['news_desc_id'] = '';
        $data['news_desc_en'] = '';
                        
        if ($id)
        { 
            $news = \CI::M_news()->get_news_byid($id);
            
            if (!$news)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/news/index');
            }
			
			foreach($news as $rows):         
				//set values to db values
				$data['id'] = $rows->id;
				$data['image'] = $rows->image;
				$data['photo_by'] = $rows->photo_by;
				$data['news_by'] = $rows->news_by;
				$data['place'] = $rows->place;
				$data['news_date'] = $rows->news_date;
				if($rows->lang == 'en')
				{
					$data['title_en'] = $rows->title;
					$data['news_desc_en'] = $rows->news_desc;
				}else
				{
					$data['title_id'] = $rows->title;
					$data['news_desc_id'] = $rows->news_desc;
				}
			endforeach;
        }
        
        \CI::form_validation()->set_rules('title_id', 'News Title (Indonesia)', 'trim|required');
        \CI::form_validation()->set_rules('title_en', 'News Title (English)', 'trim|required');
        \CI::form_validation()->set_rules('news_desc_id', 'News Descriptions (Indonesia)', 'trim|required');
        \CI::form_validation()->set_rules('news_desc_en', 'News Descriptions (English)', 'trim|required');
                                        
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('news_form', $data);
        }else
        {
			if(!$id)
			{
				$sImage = \CI::input()->post('file_name');
												
				//jika data baru harus ada upload picture dan pdf file
				if(!$sImage)
				{
					$data['error']  = 'Image File Required';
					$this->view('news_form', $data);
				}else
				{
					$save['image']  = $sImage;					
				}
			}else
			{				
				//delete the picture file if another is uploaded
				$sImage = \CI::input()->post('file_name');
				if($sImage)
				{		
					//hapus gambar lamanya
					if($data['image'] != '')
					{
						$file = 'uploads/img/news/'.$data['image'];
						//delete the existing file if needed
						if(file_exists($file))
						{
							unlink($file);
						}
					}

					$save['image']  = $sImage;
				}
			}
			
			if ($sImage) {
				//bersih bersih temporary image
				$gd = \CI::GdImage();
				
				$filePath = UPLOAD_TEMP . $save['image'];
				//$copyName = $gd->createName($save['image']);
				$gd->copy($filePath, UPLOAD_NEWS . $save['image']);
				unlink($filePath);
			}
			
			// jika edit gunakan id lama
            if($id)
            {
            	$save['id'] = $id;
            }else
            {
            	// jika data baru generate id
            	$save['id'] = \CI::M_news()->getNextId('id', 'news');
            }
            	
			$save['lang'] = 'en';            
			$save['photo_by'] = \CI::input()->post('photo_by');
			$save['news_by'] = \CI::input()->post('news_by');
			$save['place'] = \CI::input()->post('place');
			$save['news_date'] = \CI::input()->post('news_date');
			$save['title'] = \CI::input()->post('title_en');
			$save['news_desc'] = \CI::input()->post('news_desc_en');
			$save['slugs'] = set_permalink($save['title']);
							
			\CI::M_news()->save($save);
				
			$save['lang'] = 'id';
			$save['photo_by'] = \CI::input()->post('photo_by');
			$save['news_by'] = \CI::input()->post('news_by');
			$save['place'] = \CI::input()->post('place');
			$save['news_date'] = \CI::input()->post('news_date');
			$save['title'] = \CI::input()->post('title_id');
			$save['news_desc'] = \CI::input()->post('news_desc_id');
			$save['slugs'] = set_permalink($save['title']);
							
			\CI::M_news()->save($save);
			
			\CI::session()->set_flashdata('message', 'News already saved.');
			
			redirect('admin/news/index');
        }
    }
    
    public function delete()
    {
        $id = \CI::input()->post('itemID'); 
        if ($id)
        { 
            $news = \CI::M_news()->get_news_byid($id);
            if (!$news)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/news/index');
            }
            else
            {
            	$file = array();
            	$file[] = 'uploads/img/news/'.$news->image;
            	            	
            	foreach($file as $f)
            	{
            		//delete the existing file if needed
            		if(file_exists($f))
            		{
            			unlink($f);
            		}
            	}
            	
                \CI::M_news()->delete($id);
                \CI::session()->set_flashdata('message', 'Data news already deleted');
                redirect('admin/news/index');
            }
        }
        else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
            redirect('admin/news/index');
        }
    }
    
    public function delete_selected()
    {
        $item = array();
        $item = \CI::input()->post('itemID');
        
        if (!is_array($item)) 
        {
            // make an array
            $item = array($item);
        }
        
        // loop array
        foreach ($item as $id) {
        	$news = \CI::M_news()->get_news_byid($id);
        	
        	if ($news)
        	{
        		if($news->news_pict != '' || $news->brochure != '')
        		{
        			$file = array();
        			$file[] = 'uploads/img/news/'.$news->image;
        			        			        	
        			foreach($file as $f)
        			{
        				//delete the existing file if needed
        				if(file_exists($f))
        				{
        					unlink($f);
        				}
        			}
        		}
        		
        		\CI::M_news()->delete($id);
        		 
        		\CI::session()->set_flashdata('message', 'Data news already deleted');        		
        	}else
        	{
        		\CI::session()->set_flashdata('message', 'No data found');
        	}       	
        }
        redirect('admin/news/index');
    }
	
	function myuploader()
    {
        // list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $allowedExtensions = array('jpeg','jpg','gif','png');

        // max file size in bytes
        $sizeLimit = 8 * 1024 * 1024; // max file size in bytes
        $uploader = \CI::qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload(UPLOAD_TEMP, false, md5(uniqid()));

        /* 
        For your crop you should:
        1) Make a copy of the full size original
        2) Scale down or up this image so it fits in the browser nicely
        3) When the crop occurs we will manipulate the full size image
        */
        //require "gd_image.php";
        $gd = \CI::GdImage();

        // step 1: make a copy of the original
        $filePath = UPLOAD_TEMP . $result['filename'];
        $copyName = $gd->createName($result['filename'], '_FULLSIZE');
        $gd->copy($filePath, UPLOAD_TEMP . $copyName);

        // step 2: Scale down or up this image so it fits in the browser nicely, lets say 500px is safe
        $oldSize = $gd->getProperties($filePath);
        $Promoize = $gd->getAspectRatio($oldSize['w'], $oldSize['h'], 533, 0);
        $gd->resize($filePath, $Promoize['w'], $Promoize['h']);

        // to pass data through iframe you will need to encode all html tags
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        //echo json_encode($result);
        //return $result['filename'];
    } 
    
    function croping()
    {
        $gd = \CI::GdImage();

        foreach($_POST['imgcrop'] as $k => $v) 
        {
            /*
                1) delete the resized image from upload, we will only be working with the full size
                2) compute new coordinates of full size image
                3) crop full size image
                4) resize the cropped image to what ever size we need
            */

            // 1) delete resized, move to full size
            $filePath = UPLOAD_TEMP . $v['filename'];
            $fullSizeFilePath = UPLOAD_TEMP . $gd->createName($v['filename'], '_FULLSIZE');
            unlink($filePath);
            rename($fullSizeFilePath, $filePath);

            // 2) compute the new coordinates
            $scaledSize = $gd->getProperties($filePath);
            $percentChange = $scaledSize['w'] / 533; // we know we scaled by width of 500 in upload
            $newCoords = array(
                            'x' => $v['x'] * $percentChange,
                            'y' => $v['y'] * $percentChange,
                            'w' => $v['w'] * $percentChange,
                            'h' => $v['h'] * $percentChange
            );

            // 3) crop the full size image
            $gd->crop($filePath, $newCoords['x'], $newCoords['y'], $newCoords['w'], $newCoords['h']);

            // 4) resize the cropped image to whatever size we need (lets go with 533 wide)
            $ar = $gd->getAspectRatio($newCoords['w'], $newCoords['h'], 533, 0);
            $gd->resize($filePath, $ar['w'], $ar['h']);

        }

        echo "1";
    }
}