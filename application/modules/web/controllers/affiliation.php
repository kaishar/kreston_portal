<?php namespace GoCart\Controller;

class Affiliation extends Admin {
    
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('M_affiliation'));
		\CI::load()->library(array('qqUploadedFileXhr','GdImage'));
        
        \CI::load()->model('Search');
        \CI::load()->helper(array('formatting','permalink'));
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'News';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['affiliation'] = \CI::M_affiliation()->get_affiliation($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::M_affiliation()->get_affiliation_count($term); 

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('admin/affiliation/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 7;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('affiliation', $data);
    }
    
    public function form($id = false)
    {
		\CI::load()->helper('form');
        \CI::load()->library('form_validation');
			        
        $data['page_title'] = 'Entry Affiliation';
        
        $data['id'] = '';
        $data['company_name'] = '';
        $data['image'] = '';
        $data['descriptions'] = '';
        $data['address1'] = '';
        $data['address2'] = '';
        $data['address3'] = '';
        $data['email'] = '';
        $data['phone'] = '';
        $data['website'] = '';
                        
        if ($id)
        { 
            $affiliation = \CI::M_affiliation()->get_affiliation_byid($id);
            
            if (!$affiliation)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/affiliation/index');
            }
						      
			//set values to db values
			$data['id'] = $affiliation->id;
			$data['company_name'] = $affiliation->company_name;
			$data['image'] = $affiliation->image;
			$data['descriptions'] = $affiliation->descriptions;
			$data['address1'] = $affiliation->address1;
			$data['address2'] = $affiliation->address2;
			$data['address3'] = $affiliation->address3;
			$data['email'] = $affiliation->email;
			$data['phone'] = $affiliation->phone;
			$data['website'] = $affiliation->website;
			
        }
        
        \CI::form_validation()->set_rules('company_name', 'Company Name', 'trim|required');
        \CI::form_validation()->set_rules('descriptions', 'Descriptions', 'trim|required');
        \CI::form_validation()->set_rules('address1', 'Address1', 'trim|required');
        \CI::form_validation()->set_rules('email', 'Email', 'trim|required');
		\CI::form_validation()->set_rules('phone', 'Phone', 'trim|required');
                                        
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('affiliation_form', $data);
        }else
        {
			if(!$id)
			{
				$sImage = \CI::input()->post('file_name');
												
				//jika data baru harus ada upload picture dan pdf file
				if(!$sImage)
				{
					$data['error']  = 'Image File Required';
					$this->view('affiliation_form', $data);
				}else
				{
					$save['image']  = $sImage;					
				}
			}else
			{				
				//delete the picture file if another is uploaded
				$sImage = \CI::input()->post('file_name');
				if($sImage)
				{		
					//hapus gambar lamanya
					if($data['image'] != '')
					{
						$file = 'uploads/img/affiliation/'.$data['image'];
						//delete the existing file if needed
						if(file_exists($file))
						{
							unlink($file);
						}
					}

					$save['image']  = $sImage;
				}
			}
			
			if ($sImage) {
				//bersih bersih temporary image
				$gd = \CI::GdImage();
				
				$filePath = UPLOAD_TEMP . $save['image'];
				//$copyName = $gd->createName($save['image']);
				$gd->copy($filePath, UPLOAD_AFFILIATION . $save['image']);
				unlink($filePath);
			}
			
			$save['id'] = $id;
			$save['company_name'] = \CI::input()->post('company_name');
			$save['descriptions'] = \CI::input()->post('descriptions');
			$save['address1'] = \CI::input()->post('address1');
			$save['address2'] = \CI::input()->post('address2');
			$save['address3'] = \CI::input()->post('address3');
			$save['email'] = \CI::input()->post('email');
			$save['phone'] = \CI::input()->post('phone');
			$save['website'] = \CI::input()->post('website');
										
			\CI::M_affiliation()->save($save);
						
			\CI::session()->set_flashdata('message', 'Affiliation already saved.');
			
			redirect('admin/affiliation/index');
        }
    }
    
    public function delete()
    {
        $id = \CI::input()->post('itemID'); 
        if ($id)
        { 
            $affiliation = \CI::M_affiliation()->get_affiliation_byid($id);
            if (!$affiliation)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/affiliation/index');
            }
            else
            {
            	$file = array();
            	$file[] = 'uploads/img/affiliation/'.$affiliation->image;
            	            	
            	foreach($file as $f)
            	{
            		//delete the existing file if needed
            		if(file_exists($f))
            		{
            			unlink($f);
            		}
            	}
            	
                \CI::M_affiliation()->delete($id);
                \CI::session()->set_flashdata('message', 'Data affiliation already deleted');
                redirect('admin/affiliation/index');
            }
        }
        else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
            redirect('admin/affiliation/index');
        }
    }
    
    public function delete_selected()
    {
        $item = array();
        $item = \CI::input()->post('itemID');
        
        if (!is_array($item)) 
        {
            // make an array
            $item = array($item);
        }
        
        // loop array
        foreach ($item as $id) {
        	$affiliation = \CI::M_affiliation()->get_affiliation_byid($id);
        	
        	if ($affiliation)
        	{
        		if($affiliation->image != '')
        		{
        			$file = array();
        			$file[] = 'uploads/img/affiliation/'.$affiliation->image;
        			        			        	
        			foreach($file as $f)
        			{
        				//delete the existing file if needed
        				if(file_exists($f))
        				{
        					unlink($f);
        				}
        			}
        		}
        		
        		\CI::M_affiliation()->delete($id);
        		 
        		\CI::session()->set_flashdata('message', 'Data affiliation already deleted');        		
        	}else
        	{
        		\CI::session()->set_flashdata('message', 'No data found');
        	}       	
        }
        redirect('admin/affiliation/index');
    }
	
	function myuploader()
    {
        // list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $allowedExtensions = array('jpeg','jpg','gif','png');

        // max file size in bytes
        $sizeLimit = 8 * 1024 * 1024; // max file size in bytes
        $uploader = \CI::qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload(UPLOAD_TEMP, false, md5(uniqid()));

        /* 
        For your crop you should:
        1) Make a copy of the full size original
        2) Scale down or up this image so it fits in the browser nicely
        3) When the crop occurs we will manipulate the full size image
        */
        //require "gd_image.php";
        $gd = \CI::GdImage();

        // step 1: make a copy of the original
        $filePath = UPLOAD_TEMP . $result['filename'];
        $copyName = $gd->createName($result['filename'], '_FULLSIZE');
        $gd->copy($filePath, UPLOAD_TEMP . $copyName);

        // step 2: Scale down or up this image so it fits in the browser nicely, lets say 500px is safe
        $oldSize = $gd->getProperties($filePath);
        $Promoize = $gd->getAspectRatio($oldSize['w'], $oldSize['h'], 500, 0);
        $gd->resize($filePath, $Promoize['w'], $Promoize['h']);

        // to pass data through iframe you will need to encode all html tags
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        //echo json_encode($result);
        //return $result['filename'];
    } 
    
    function croping()
    {
        $gd = \CI::GdImage();

        foreach($_POST['imgcrop'] as $k => $v) 
        {
            /*
                1) delete the resized image from upload, we will only be working with the full size
                2) compute new coordinates of full size image
                3) crop full size image
                4) resize the cropped image to what ever size we need
            */

            // 1) delete resized, move to full size
            $filePath = UPLOAD_TEMP . $v['filename'];
            $fullSizeFilePath = UPLOAD_TEMP . $gd->createName($v['filename'], '_FULLSIZE');
            unlink($filePath);
            rename($fullSizeFilePath, $filePath);

            // 2) compute the new coordinates
            $scaledSize = $gd->getProperties($filePath);
            $percentChange = $scaledSize['w'] / 500; // we know we scaled by width of 500 in upload
            $newCoords = array(
                            'x' => $v['x'] * $percentChange,
                            'y' => $v['y'] * $percentChange,
                            'w' => $v['w'] * $percentChange,
                            'h' => $v['h'] * $percentChange
            );

            // 3) crop the full size image
            $gd->crop($filePath, $newCoords['x'], $newCoords['y'], $newCoords['w'], $newCoords['h']);

            // 4) resize the cropped image to whatever size we need (lets go with 533 wide)
            $ar = $gd->getAspectRatio($newCoords['w'], $newCoords['h'], 500, 0);
            $gd->resize($filePath, $ar['w'], $ar['h']);

        }

        echo "1";
    }
}