<?php namespace GoCart\Controller;

class Parameter extends Admin {
    
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('Variable','Search'));
		\CI::load()->helper(array('formatting'));
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'General Parameter';
        $data['code'] = $code;
        $term = false;
		
		$data['variable'] = \CI::input()->post('variable');
		
		$list = \CI::Variable()->get_list_parameter();
		$list_parameter[null] = "Choose Parameter";
		foreach($list as $lists)
		{
			$list_parameter[$lists->variable] = $lists->variable;
		}
		$data['list_parameter'] = $list_parameter;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }
		
        $data['term'] = $term;
        $data['parameter'] = \CI::variable()->get_parameter($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::variable()->get_parameter_count($term); 
        
        \CI::load()->library('pagination');

        $config['base_url'] = site_url('admin/parameter/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 7;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('parameter', $data);
    }
        
    public function form($id = false)
    {
    	\CI::load()->helper('form');
    	\CI::load()->library('form_validation');
    	 
    	$data['page_title'] = 'Entri Parameter';
    	
    	$list = \CI::Variable()->get_list_parameter();
    	$list_parameter[null] = "Choose Parameter";
    	foreach($list as $lists)
    	{
    		$list_parameter[$lists->variable] = $lists->variable;
    	}
    	$data['list_parameter'] = $list_parameter;
    
    	$data['id'] = '';
    	$data['variable'] = '';
    	$data['value'] = '';
    	$data['description'] = '';
    	    
    	if ($id)
    	{
    		$parameter = \CI::variable()->get_parameter_byid($id);
    
    		if (!$parameter)
    		{
    			\CI::session()->set_flashdata('error', lang('error_not_found'));
    			redirect('admin/parameter/index');
    		}
    
    		//set values to db values
    		$data['id'] = $parameter->id;
    		$data['variable'] = $parameter->variable;
			$data['value'] = $parameter->value;
			$data['description'] = $parameter->description;
    	}
    
    	\CI::form_validation()->set_rules('variable', 'TIPE PARAMETER', 'trim|required');
		\CI::form_validation()->set_rules('value', 'KODE PARAMETER', 'trim|required');
		\CI::form_validation()->set_rules('description', 'DESKRIPSI', 'trim|required');
    
    	if (\CI::form_validation()->run() == FALSE)
    	{
    		$this->view('parameter_form', $data);
    	}else
    	{    			
    		$save['id'] = \CI::input()->post('id');
    		$save['variable'] = \CI::input()->post('variable');
    		$save['value'] = \CI::input()->post('value');
    		$save['description'] = \CI::input()->post('description');
			
			//print_r($save); exit;
    			
    		$variable = \CI::variable()->save($save);
    		\CI::session()->set_flashdata('variable', $variable);
    		\CI::session()->set_flashdata('message', 'Parameter already saved.');
    		
    		redirect('admin/parameter/index');
    	}
    }
    
    public function delete()
    {
    	$id = \CI::input()->post('itemID');
    	if ($id)
    	{
    		$parameter = \CI::Variable()->get_parameter_byid($id);
    		if (!$parameter)
    		{
    			\CI::session()->set_flashdata('error', lang('error_not_found'));
    			redirect('admin/parameter/index');
    		}else
    		{
    			\CI::Variable()->delete($id);
    			\CI::session()->set_flashdata('message', 'Data parameter already deleted');
    			redirect('admin/parameter/index');
    		}
    	}else
    	{
    		\CI::session()->set_flashdata('error', lang('error_not_found'));
    		redirect('admin/parameter/index');
    	}
    }
    
    public function delete_selected()
    {
    	$item = array();
    	$item = \CI::input()->post('itemID');
    
    	if (!is_array($item))
    	{
    		// make an array
    		$item = array($item);
    	}
    
    	// loop array
    	foreach ($item as $id) {
    		$parameter = \CI::Variable()->get_parameter_byid($id);
    		 
    		if ($parameter)
    		{
    			\CI::Variable()->delete($id);
    			\CI::session()->set_flashdata('message', 'Data parameter already deleted');
    		}else
    		{
    			\CI::session()->set_flashdata('message', 'No data found');
    		}
    	}
    	redirect('admin/parameter/index');
    }
    
}