<?php namespace GoCart\Controller;

class Vacancy extends Admin {
    
    public function __construct()
    { 
        parent::__construct();
        
        \CI::load()->model(array('M_vacancy','Search'));
		\CI::load()->helper(array('formatting'));
    }
        	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=10)
    {
        \CI::load()->helper('form');
        \CI::load()->helper('date');
		
        $data['message'] = \CI::session()->flashdata('message');
        $data['page_title'] = 'vacancy';
        $data['code'] = $code;
        $term = false;
		
        $post = \CI::input()->post(null, false);
        if($post)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = json_encode($post);
            $code = \CI::Search()->recordTerm($term);
            $data['code'] = $code;
            //reset the term to an object for use
            $term   = (object)$post;
        }
        elseif ($code)
        {
            $term = \CI::Search()->getTerm($code);
            $term = json_decode($term);
        }

        $data['term'] = $term;
        $data['vacancy'] = \CI::M_vacancy()->get_vacancy($term, $sort_by, $sort_order, $rows, $page);
        $data['total'] = \CI::M_vacancy()->get_vacancy_count($term); 

        \CI::load()->library('pagination');

        $config['base_url'] = site_url('admin/vacancy/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $rows;
        $config['uri_segment'] = 7;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        \CI::pagination()->initialize($config);
        
        $data['sort_by'] = $sort_by;
        $data['sort_order'] = $sort_order;
        
        $this->view('vacancy', $data);
    }
    
    public function form($id = false)
    {
		\CI::load()->helper('form');
        \CI::load()->library('form_validation');
			        
        $data['page_title'] = 'Entry Vacancy';
        
        $data['id'] = '';
        $data['division'] = '';
        $data['position'] = '';
        $data['heading'] = '';
        $data['requirements'] = '';
        $data['responsibilities'] = '';
		$data['description'] = '';
		                
        if ($id)
        { 
            $vacancy = \CI::M_vacancy()->get_vacancy_byid($id);
            
            if (!$vacancy)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/vacancy/index');
            }
           			
            //set values to db values
			$data['id'] = $vacancy->id;
			$data['division'] = $vacancy->division;
			$data['position'] = $vacancy->position;
			$data['heading'] = $vacancy->heading;
			$data['requirements'] = $vacancy->requirements;
			$data['responsibilities'] = $vacancy->responsibilities;
			$data['description'] = $vacancy->description;
        }
        
        \CI::form_validation()->set_rules('division', 'Division', 'trim|required');
				                                        
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('vacancy_form', $data);
        }else
        {					
			$save['id'] = \CI::input()->post('id');
			$save['division'] = \CI::input()->post('division');
			$save['position'] = \CI::input()->post('position');
			$save['heading'] = \CI::input()->post('heading');
			$save['requirements'] = \CI::input()->post('requirements');
			$save['responsibilities'] = \CI::input()->post('responsibilities');
			$save['description'] = \CI::input()->post('description');
						
            $id = \CI::M_vacancy()->save($save);
            
            \CI::session()->set_flashdata('message', 'Vacancy already saved.');
            
            redirect('admin/vacancy/index');
        }
    }
    
    public function delete()
    {
        $id = \CI::input()->post('itemID'); 
        if ($id)
        { 
            $vacancy = \CI::M_vacancy()->get_vacancy_byid($id);
            if (!$vacancy)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/vacancy/index');
            }else
            {            	
                \CI::M_vacancy()->delete($id);
                \CI::session()->set_flashdata('message', 'Data already deleted');
                redirect('admin/vacancy/index');
            }
        }else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
            redirect('admin/vacancy/index');
        }
    }
    
    public function delete_selected()
    {
        $item = array();
        $item = \CI::input()->post('itemID');
        
        if (!is_array($item)) 
        {
            // make an array
            $item = array($item);
        }
        
        // loop array
        foreach ($item as $id) {
        	$vacancy = \CI::M_vacancy()->get_vacancy_byid($id);
        	
        	if ($vacancy)
        	{        		
        		\CI::M_vacancy()->delete($id); 
        		\CI::session()->set_flashdata('message', 'Data already deleted');        		
        	}else
        	{
        		\CI::session()->set_flashdata('message', 'No data found');
        	}       	
        }
        redirect('admin/vacancy/index');
    }
	
}