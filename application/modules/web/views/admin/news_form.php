<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>                 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">News</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Add News</strong>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-left">
                                    <a href="<?=site_url('admin/news/index/')?>" class="btn btn-primary"><i class="fa fa-list-alt"></i>&nbsp;<?php echo ('Back to List news'); ?></a>
                                    <a href="<?=site_url('admin/news/form/')?>" class="btn btn-primary hidden"><i class="fa fa-plus"></i>&nbsp;<?php echo ('Add news'); ?></a>
                                </div>
                            </div>                                                   
                        </div>
                        <hr>
                        <div class="row">
                        	<?php 
                                $attributes = array('class' => 'form-horizontal');
                                echo form_open_multipart('admin/news/form/'.$id, $attributes); 								
                            ?>
                            <div class="col-md-6">
                            	<div class="form-group">
	                                <label for="title_id" class="col-sm-2">News Title (Indonesia)*</label>
	                                <div class="col-sm-10">
	                                <?php 
	                                    echo form_input(['name'=>'title_id', 
	                                    		'id'=>'title_id',
	                                            'value'=>assign_value('title_id', $title_id), 
	                                            'class'=>'form-control',
												'placeholder'=>'Judul Bahasa Indonesia',
	                                            'style'=>'width: 100%;']); 
	                                ?>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label for="news_desc_id" class="col-sm-2">News Descriptions (Indonesia)*</label>
	                                <div class="col-sm-10">
										<?php
											echo form_textarea(['name'=>'news_desc_id', 
																'id'=>'news_desc_id',
																'value'=>assign_value('news_desc_id', $news_desc_id), 
																'class'=>'form-control',
																'rows'=>4,
																'style'=>'width: 100%;']);
										?>
									</div>
								</div>
                            </div>
                            <div class="col-md-6">                            
	                            <div class="form-group">
	                                <label for="title_en" class="col-sm-2">News Title (English)*</label>
	                                <div class="col-sm-10">
	                                <?php 
	                                    echo form_input(['name'=>'title_en', 
	                                    		'id'=>'title_en',
	                                            'value'=>assign_value('title_en', $title_en), 
	                                            'class'=>'form-control',
												'placeholder'=>'Judul Bahasa Inggris',
	                                            'style'=>'width: 100%;']); 
	                                ?>
	                                </div>
	                            </div>
								<div class="form-group">
	                                <label for="news_desc" class="col-sm-2">News Descriptions (English)*</label>
	                                <div class="col-sm-10">
										<?php
											echo form_textarea(['name'=>'news_desc_en', 
																'id'=>'news_desc_en',
																'value'=>assign_value('news_desc_en', $news_desc_en), 
																'class'=>'form-control',
																'rows'=>4,
																'style'=>'width: 100%;']);
										?>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <label for="photo_by" class="col-sm-2">Photo By </label>
	                                <div class="col-sm-10">
	                                <?php 
	                                    echo form_input(['name'=>'photo_by',
	                                    		'id'=>'photo_by',
	                                            'value'=>assign_value('photo_by', $photo_by), 
	                                            'class'=>'form-control',
												'placeholder'=>'Sumber Foto',
	                                            'style'=>'width: 60%;']); 
	                                ?>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label for="news_by" class="col-sm-2">News By </label>
	                                <div class="col-sm-10">
	                                <?php 
	                                    echo form_input(['name'=>'news_by',
	                                    		'id'=>'news_by',
	                                            'value'=>assign_value('news_by', $news_by), 
	                                            'class'=>'form-control',
												'placeholder'=>'Sumber Berita',
	                                            'style'=>'width: 60%;']); 
	                                ?>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label for="place" class="col-sm-2">Place </label>
	                                <div class="col-sm-10">
	                                <?php 
	                                    echo form_input(['name'=>'place',
	                                    		'id'=>'place',
	                                            'value'=>assign_value('place', $place), 
	                                            'class'=>'form-control',
												'placeholder'=>'Kota kejadian berita',
	                                            'style'=>'width: 60%;']); 
	                                ?>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label for="news_date" class="col-sm-2">News Date </label>
	                                <div class="col-sm-10">
			                            <div class="dateField">
			                                <input type="date" value="<?=assign_value('news_date', $news_date)?>" id="begin_period" name="news_date" class="dateInput">
			                                <a title="Open Calendar" onclick="javascript: dateType = 'date'; openCalendar('begin_period');" style="cursor: pointer;" class="calendarLink notAJAX"></a>
			                            </div>
			                        </div>
			                    </div>
								<div class="form-group">
									<label for="PhotoPrevs" class="col-sm-2">News Picture </label>
	                                <div class="col-sm-10">									
										<div id="UploadImages">
											<noscript>Please enable javascript to upload and crop images.</noscript>
										</div>
										<?php if($id && $image != '') { ?>
										<div id="PhotoPrevs">
											<div class="first"><img class="img-responsive" src="<?php echo theme_upload('news/'.$image);?>" /></div>
											<div class="second"><input type="hidden" name="file_name" value="" /></div>		
										</div>
										<?php } else { ?>
										<div id="PhotoPrevs">
											<div class="first"><img class="img-responsive" /></div>
											<div class="second"><input type="hidden" name="file_name" /></div>		
										</div>
										<?php } ?>	
										<h5><strong>Note</strong> :<br>
											- Upload image dengan format .JPG, square (4:3),<br>
	                                        - Image resolution 800pixel x 600pixel atau lebih besar.
										</h5>
									</div>                                
								</div>
								<div class="form-group">
	                                <div class="col-sm-10 col-sm-offset-2">
	                                <?php
	                                    if ($id) 
	                                    {
	                                ?>
	                                    <td>
	                                        <input class="button btn btn-success" type="submit" value="Update" name="saveData">
	                                        <input class="cancelButton button btn btn-warning" type="button" value="Batal" onclick="window.history.back()">
	                                        <input class="button btn btn-danger btn-delete confirmSubmit" type="button" onclick="confSubmit('deleteForm', 'Are you sure wants to delete data?\n')" value="Hapus data">
	                                    </td>
	                                <?php
	                                    }else {
	                                ?>
	                                    <td><input type="submit" value="Save" name="saveData" class="button btn btn-primary"></td>
	                                <?php
	                                    }
	                                ?>
	                                </div>
	                            </div>
								<input type="hidden" name="id" value="<?=$id?>" />
							</div>
                            </form>
                            <form action="<?=site_url('admin/news/delete')?>" id="deleteForm" method="post" style="display: inline;">
                                <input type="hidden" name="itemID" value="<?=$id?>" />
                            </form>                              
                    	</div>
                   	</div>
              	</div>
                <!-- End  Kitchen Sink -->
            </div>
        </div>
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
