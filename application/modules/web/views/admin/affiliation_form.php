<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>                 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">Affiliation</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Add Affiliation</strong>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-left">
                                    <a href="<?=site_url('admin/affiliation/index/')?>" class="btn btn-primary"><i class="fa fa-list-alt"></i>&nbsp;<?php echo ('Back to affiliation list'); ?></a>                                    
                                </div>
                            </div>                                                   
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                            <?php 
                                $attributes = array('class' => 'form-horizontal');
                                echo form_open_multipart('admin/affiliation/form/'.$id, $attributes); 								
                            ?>
                            <div class="form-group">
                                <label for="fullname" class="col-sm-2 control-label">Company Name *</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_input(['name'=>'company_name', 
                                    		'id'=>'company_name',
                                            'value'=>assign_value('company_name', $company_name), 
                                            'class'=>'form-control',
											'placeholder'=>'Company Name',
                                            'style'=>'width: 60%;']); 
                                ?>
                                </div>
                            </div>                            
							<div class="form-group">
								<label for="PhotoPrevs" class="col-sm-2 control-label">Image</label>
								<div class="col-sm-10">									
									<div id="UploadImages">
										<noscript>Please enable javascript to upload and crop images.</noscript>
									</div>
									<?php if($id && $image != '') { ?>
									<div id="PhotoPrevs">
										<div class="first"><img class="img-responsive" src="<?php echo theme_upload('affiliation/'.$image);?>" /></div>
										<div class="second"><input type="hidden" name="file_name" value="" /></div>		
									</div>
									<?php } else { ?>
									<div id="PhotoPrevs">
										<div class="first"><img class="img-responsive" /></div>
										<div class="second"><input type="hidden" name="file_name" /></div>		
									</div>
									<?php } ?>	
									<h5><strong>Note</strong> :<br>
										- Upload image dengan format .jpg, .png, square (16:9),<br>
										- Image resolution 500px x 281px atau lebih besar.
									</h5>
								</div>                                
							</div>								
							<div class="form-group">
								<label for="descriptions" class="col-sm-2">Descriptions</label>
								<div class="col-sm-10">
									<?php
										echo form_textarea(['name'=>'descriptions', 
															'id'=>'descriptions',
															'value'=>assign_value('descriptions', $descriptions), 
															'class'=>'form-control',
															'rows'=>4,
															'style'=>'width: 60%;']);
									?>
								</div>
							</div>
							<div class="form-group">
                                <label for="address1" class="col-sm-2 control-label">Address1</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_input(['name'=>'address1',
                                    		'id'=>'address1',
                                            'value'=>assign_value('address1', $address1), 
                                            'class'=>'form-control',
											'style'=>'width: 60%;']); 
                                ?>
                                </div>
                            </div>
							<div class="form-group">
                                <label for="address2" class="col-sm-2 control-label">Address2</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_input(['name'=>'address2',
                                    		'id'=>'address2',
                                            'value'=>assign_value('address2', $address2), 
                                            'class'=>'form-control',
											'style'=>'width: 60%;']); 
                                ?>
                                </div>
                            </div>
							<div class="form-group">
                                <label for="address3" class="col-sm-2 control-label">Address3</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_input(['name'=>'address3',
                                    		'id'=>'address3',
                                            'value'=>assign_value('address3', $address3), 
                                            'class'=>'form-control',
											'style'=>'width: 60%;']); 
                                ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_input(['name'=>'email',
                                    		'id'=>'email',
                                            'value'=>assign_value('email', $email), 
                                            'class'=>'form-control',
											'style'=>'width: 60%;']); 
                                ?>
                                </div>
                            </div>
							<div class="form-group">
                                <label for="phone" class="col-sm-2 control-label">Phone</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_input(['name'=>'phone',
                                    		'id'=>'phone',
                                            'value'=>assign_value('phone', $phone), 
                                            'class'=>'form-control',
											'placeholder'=>'+628xxxxxxxxx',
                                            'style'=>'width: 30%;']); 
                                ?>
                                </div>
                            </div>
							<div class="form-group">
                                <label for="website" class="col-sm-2 control-label">Website</label>
                                <div class="col-sm-10">
                                <?php 
                                    echo form_input(['name'=>'website',
                                    		'id'=>'website',
                                            'value'=>assign_value('website', $website), 
                                            'class'=>'form-control',
											'placeholder'=>'http://www.example.com',
                                            'style'=>'width: 60%;']); 
                                ?>
                                </div>
                            </div>   							
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                <?php
                                    if ($id) 
                                    {
                                ?>
                                    <td>
                                        <input class="button btn btn-primary" type="submit" value="Update" name="saveData">
                                        <input class="cancelButton button btn btn-primary" type="button" value="Batal" onclick="window.history.back()">
                                        <input class="button btn btn-danger btn-delete confirmSubmit" type="button" onclick="confSubmit('deleteForm', 'Apakah anda yakin akan menghapus data <?=$company_name?>?\n')" value="Hapus data">
                                    </td>
                                <?php
                                    }else {
                                ?>
                                    <td><input type="submit" value="Simpan" name="saveData" class="button btn btn-primary"></td>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
							<input type="hidden" name="id" value="<?=$id?>" />
                            </form>
                            <form action="<?=site_url('admin/affiliation/delete')?>" id="deleteForm" method="post" style="display: inline;">
                                <input type="hidden" name="itemID" value="<?=$id?>" />
                            </form>  
                            <hr>
                            </div>
                        </div>
                    </div>
                    <!-- End  Kitchen Sink -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
