<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; 

                    if ($term): 
                ?>
                    <div class="alert alert-info">
                        <?php echo sprintf(lang('search_returned'), intval($total));?>
                    </div>
                <?php endif;?>
                <?php
                    //set "code" for searches
                    if(!$code)
                    {
                        $code = '';
                    }
                    else
                    {
                        $code = '/'.$code;
                    }

                    function sort_url($lang, $by, $sort, $sorder, $code)
                    {
                        if ($sort == $by)
                        {
                            if ($sorder == 'asc')
                            {
                                $sort = 'desc';
                                $icon = ' <i class="fa fa-chevron-up"></i>';
                            }
                            else
                            {
                                $sort = 'asc';
                                $icon = ' <i class="fa fa-chevron-down"></i>';
                            }
                        }
                        else
                        {
                            $sort = 'asc';
                            $icon = '';
                        }

                        $return = site_url('admin/slider/index/'.$by.'/'.$sort.'/'.$code.'/');

                        echo '<a href="'.$return.'">'.$lang.$icon.'</a>';
                    }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">Slider</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--   Kitchen Sink -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Listing Slider</strong>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-left">
                                    <a href="<?=site_url('admin/slider/form/')?>" class="btn btn-primary"><i class="fa fa-list-alt"></i>&nbsp;<?php echo ('Tambah Image Slider'); ?></a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-right">
                                    <?php echo form_open('admin/slider/index/', 'class="form-inline"');?>
                                        <div class="form-group">
                                            <label for="search">Search </label>
                                            <input type="text" size="30" class="form-control" name="term" placeholder="kata kunci" />
                                        </div>
                                        <input type="submit" id="doSearch" value="<?php echo ('Search'); ?>" class="btn btn-info" />				
                                    </form>
                                </div>
                            </div>                        
                        </div>
                        <hr>
                        <div class="row">
                            <?php
                                $page_links = CI::pagination()->create_links();

                                if($page_links != ''):
                                    echo $page_links;
                                endif;
                            ?>
                            <form action="<?=site_url('admin/slider/delete_selected')?>" id="deleteForm" method="post">
                            <div class="table-responsive">
                                <table class="table table-striped datagrid" id="dataList">
                                    <thead>                                        
                                        <?php if(count($slider) < 1) { ?>
                                                <tr><th align="center" style="color: red; background-color: #CCCCCC;">No Data</th></tr>
                                        <?php }else { ?>
                                                <tr style="color: white; font-weight: bold; cursor: pointer; background-color: rgb(49, 53, 62);">
                                                    <th>Delete</th>
                                                    <th>Edit</th>
                                                    <th>
                                                        <?php echo sort_url('Slider Image', 'image', $sort_by, $sort_order, $code); ?>
                                                    </th>                                                                                                       
                                                </tr>                                      
                                    </thead>                                    
                                    <tbody>
                                        <?php 
                                            	$i = 1;
                                            	foreach($slider as $rows):
                                        ?>
                                                <tr>
                                                    <td valign="top" align="center" style="width: 5%;">
                                                        <input type="checkbox" class="gc_check" id="cbRow<?=$i?>" value="<?=$rows->id?>" name="itemID[]">
                                                    </td>
                                                    <td valign="top" align="center" style="width: 5%;">
                                                        <a title="Edit" href="<?=site_url('admin/slider/form/'.$rows->id)?>"><i class="fa fa-pencil-square-o"></i></a>
                                                    </td>
                                                    <td valign="top"><img class="img-responsive" src="<?php echo theme_upload('slider/'.$rows->image);?>" /></td>                                                    
                                                </tr>
                                        <?php   
                                            	$i++;
                                            	endforeach;
                                            }
                                        ?>
                                    </tbody>
                                </table>                                
                            </div>                            
                        </div> 
                        <input type="button" class="button btn btn-danger" onclick="chboxFormSubmit('deleteForm', 'Apakah anda yakin akan menghapus data yang dipilih?')" value="Hapus data yang dipilih"> 
                        <input type="button" class="check-all button btn btn-primary" value="Pilih semua"> 
                        <input type="button" class="uncheck-all button btn btn-info" value="Bersihkan centang"> 
                        <hr>
                    </div>
                </div>
                <!-- End  Kitchen Sink -->
                </form>
            </div>
        </div>
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
