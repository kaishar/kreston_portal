<div class="content-wrapper" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    //lets have the flashdata overright "$message" if it exists
                    if(CI::session()->flashdata('message'))
                    {
                        $message = CI::session()->flashdata('message');
                    }

                    if(CI::session()->flashdata('error'))
                    {
                        $error = CI::session()->flashdata('error');
                    }

                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error = validation_errors();
                    }
                ?>

                <div id="js_error_container" class="alert alert-error" style="display:none;">
                    <p id="js_error"></p>
                </div>

                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>

                <?php if (!empty($message)): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>                 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-head-line">Slider</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Add Slider</strong>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-left">
                                    <a href="<?=site_url('admin/slider/index/')?>" class="btn btn-primary"><i class="fa fa-list-alt"></i>&nbsp;<?php echo ('Back to List Slider'); ?></a>                                    
                                </div>
                            </div>                                                   
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                            <?php 
                                $attributes = array('class' => 'form-horizontal');
                                echo form_open_multipart('admin/slider/form/'.$id, $attributes); 								
                            ?>
                            
							<div class="form-group">
								<label for="PhotoPrevs" class="col-sm-2 control-label">Slider Picture *</label>
                                <div class="col-sm-10">									
									<div id="UploadImages">
										<noscript>Please enable javascript to upload and crop images.</noscript>
									</div>
									<?php if($id && $image != '') { ?>
									<div id="PhotoPrevs">
										<div class="first"><img class="img-responsive" src="<?php echo theme_upload('slider/'.$image);?>" /></div>
										<div class="second"><input type="hidden" name="file_name" value="" /></div>		
									</div>
									<?php } else { ?>
									<div id="PhotoPrevs">
										<div class="first"><img class="img-responsive" /></div>
										<div class="second"><input type="hidden" name="file_name" /></div>		
									</div>
									<?php } ?>	
									<h5><strong>Note</strong> :<br>
										- Upload image dengan format .JPG,<br>
                                        - Image resolution 1200pixel x 600pixel atau lebih besar.
									</h5>
								</div>                                
							</div>
														
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                <?php
                                    if ($id) 
                                    {
                                ?>
                                    <td>
                                        <input class="button btn btn-success" type="submit" value="Update" name="saveData">
                                        <input class="cancelButton button btn btn-warning" type="button" value="Batal" onclick="window.history.back()">
                                        <input class="button btn btn-danger btn-delete confirmSubmit" type="button" onclick="confSubmit('deleteForm', 'Apakah anda yakin akan menghapus data?\n')" value="Hapus data">
                                    </td>
                                <?php
                                    }else {
                                ?>
                                    <td><input type="submit" value="Simpan" name="saveData" class="button btn btn-success"></td>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
							<input type="hidden" name="id" value="<?=$id?>" />
                            </form>
                            <form action="<?=site_url('admin/slider/delete')?>" id="deleteForm" method="post" style="display: inline;">
                                <input type="hidden" name="itemID" value="<?=$id?>" />
                            </form>  
                            <hr>
                            </div>
                        </div>
                    </div>
                    <!-- End  Kitchen Sink -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
