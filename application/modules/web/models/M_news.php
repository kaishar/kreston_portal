<?php
Class M_news extends MY_Model
{
    public function get_news($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_news_searchlike($search->term);
            }
        }
        
        CI::db()->where('lang', \CI::lang()->lang());
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('news')->result();
    }

    public function get_news_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_news_searchlike($search->term);
            }
        }
        CI::db()->where('lang', \CI::lang()->lang());
        
        return CI::db()->count_all_results('news');
    }
	
    private function get_news_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( news.title ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )" ;
                        
            CI::db()->where($like);
        }
    }
	
    public function save($news)
    {
        $admin = $this->session->userdata('admin');
        $record = $this->get_news_byid($news['id'], $news['lang']);
		
		if(count($record) > 0)
        {
        	//echo $news['news_id']; exit;
            $news['changed_by'] = $admin['username'];
            $news['changed_at'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $news['id']);
            CI::db()->where('lang', $news['lang']);
            CI::db()->update('news', $news);
            //return $news['id'];
        }
        else
        {            
            $news['created_by'] = $admin['username'];
            $news['created_at'] = date('Y-m-d H:i:s');
                        
            CI::db()->insert('news', $news);
            return $news['id'];
        }
    }

    public function delete($id)
    {
        $admin = $this->session->userdata('admin');
               
        CI::db()->where('id', $id);
        CI::db()->delete('news');
    }
    	
    public function get_news_byid($id, $lang=false)
    {
        $result = CI::db()->get_where('news', array('id'=>$id, 'lang'=>$lang));
        return $result->result();
    }
    
    public function get_news_home($lang, $limit=0, $offset=0)
    {
    	$sql = "select
					id,
					ifnull(image,'noimage.jpg') as image,
					photo_by,
					news_by,
					place,
					news_date,
					title,
					news_desc
				from news
    			where lang = ?
				order by id DESC
				limit ".$offset.",".$limit;
    	return $this->db->query($sql, array($lang))->result();
    }
    
}
