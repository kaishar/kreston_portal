<?php
Class M_slider extends MY_Model
{
    public function get_slider($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_slider_searchlike($search->term);
            }
        }
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('slider')->result();
    }

    public function get_slider_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_slider_searchlike($search->term);
            }
        }
        
        return CI::db()->count_all_results('slider');
    }
	
    private function get_slider_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( slider.image ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )" ;
                        
            CI::db()->where($like);
        }
    }
	
    public function save($slider)
    {
        $admin = $this->session->userdata('admin');
        $record = $this->get_slider_byid($slider['id']);
		
		if(count($record) > 0)
        {
        	//echo $slider['slider_id']; exit;
            $slider['changedby'] = $admin['username'];
            $slider['changedat'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $slider['id']);
            CI::db()->update('slider', $slider);
            return $slider['id'];
        }
        else
        {            
            $slider['createdby'] = $admin['username'];
            $slider['createdat'] = date('Y-m-d H:i:s');
                        
            CI::db()->insert('slider', $slider);
            return CI::db()->insert_id();
        }
    }

    public function delete($id)
    {
        $admin = $this->session->userdata('admin');
               
        CI::db()->where('id', $id);
        CI::db()->delete('slider');
    }
    	
    public function get_slider_byid($id)
    {
        $result = CI::db()->get_where('slider', array('id'=>$id));
        return $result->row();
    }
    
    public function get_slider_lineup($limit)
    {
    	CI::db()->limit($limit);
    	return \CI::db()->get('slider')->result();
    }
        
}
