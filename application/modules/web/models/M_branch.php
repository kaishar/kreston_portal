<?php
Class M_branch extends MY_Model
{
    public function get_branch($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_branch_searchlike($search->term);
            }
        }
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('branch')->result();
    }

    public function get_branch_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_branch_searchlike($search->term);
            }
        }
        
        return CI::db()->count_all_results('branch');
    }
	
    private function get_branch_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( branch.branch_name ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
            $like .= $operator." branch.pic_name ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
            $like .= $operator." branch.city ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )" ;
            
            CI::db()->where($like);
        }
    }
	
    public function save($branch)
    {
        $admin = $this->session->userdata('admin');
        $record = $this->get_branch_byid($branch['id']);
		
		if(count($record) > 0)
        {
        	$branch['changedby'] = $admin['username'];
            $branch['changedat'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $branch['id']);
            CI::db()->update('branch', $branch);
            return $branch['id'];
        }
        else
        {            
            $branch['createdby'] = $admin['username'];
            $branch['createdat'] = date('Y-m-d H:i:s');
                        
            CI::db()->insert('branch', $branch);
            return $branch['id'];
        }
    }

    public function delete($id)
    {
        $admin = $this->session->userdata('admin');
               
        CI::db()->where('id', $id);
        CI::db()->delete('branch');
    }
    	
    public function get_branch_byid($id)
    {
        $result = CI::db()->get_where('branch', array('id'=>$id));
        return $result->row();
    }
    
    public function get_list_branch()
    {
    	$this->db->select('id,
                        branch_name');
    	return $this->db->get('branch')->result();
    }
	
	public function get_indonesia_branch()
    {
    	$this->db->select('id,
						pic_email,
                        branch_name');
		//$this->db->like('address2', 'Indonesia');
		$this->db->order_by('id', 'asc');
    	return $this->db->get('branch')->result();
    }
    
    public function get_locations()
    {
    	$sql = "select
                    branch.id,
                    branch.branch_name,
                    branch.address1,
                    branch.address2,
                    branch.city,
                    branch.phone,
					branch.fax,
					ifnull(branch.photo,'noimage.png') as photo,
                    branch.latitude,
                    branch.longitude,
                    branch.pic_name,
        			branch.pic_email
                from branch
                order by branch.id asc";
    	$query = CI::db()->query($sql);
    	return $query->result();
    }
	
	public function get_branchname_byemail($email)
    {
		$sql = "SELECT 
					branch_name					 
				FROM branch
				where 
					branch.pic_email = ?";
				
		return $this->db->query($sql, array($email))->row_array();
    }
    
}
