<?php
Class M_affiliation extends MY_Model
{
    public function get_affiliation($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_affiliation_searchlike($search->term);
            }
        }
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('affiliation')->result();
    }

    public function get_affiliation_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_affiliation_searchlike($search->term);
            }
        }
        
        return CI::db()->count_all_results('affiliation');
    }
	
    private function get_affiliation_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( company_name ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
			$like .= $operator." descriptions ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
            $like .= $operator." address1 ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
			$like .= $operator." email ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
			$like .= $operator." website ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )" ;
            
            CI::db()->where($like);
        }
    }
	
    public function save($affiliation)
    {
        $admin = $this->session->userdata('admin');
        $record = $this->get_affiliation_byid($affiliation['id']);
		
		if(count($record) > 0)
        {
        	$affiliation['updatedby'] = $admin['username'];
            $affiliation['updatedat'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $affiliation['id']);
            CI::db()->update('affiliation', $affiliation);
            return $affiliation['id'];
        }
        else
        {            
            $affiliation['createdby'] = $admin['username'];
            $affiliation['createdat'] = date('Y-m-d H:i:s');
                        
            CI::db()->insert('affiliation', $affiliation);
            return $affiliation['id'];
        }
    }

    public function delete($id)
    {
        $admin = $this->session->userdata('admin');
               
        CI::db()->where('id', $id);
        CI::db()->delete('affiliation');
    }
    	
    public function get_affiliation_byid($id)
    {
        $result = CI::db()->get_where('affiliation', array('id'=>$id));
        return $result->row();
    }
	
	public function get_affiliation_bycategory($category)
    {
		$sql = "select 
					a.id,
					a.fullname,
					a.category,
					a.photo,
					a.position,
					a.profile,
					a.email,
					a.phone
				from
					key_affiliation a
				left join variable b
					on b.id = a.category
				where b.value = ?";
		$query = CI::db()->query($sql, array($category));
		//echo $this->db->last_query(); exit;
    	return $query->result();
    }
        
}
