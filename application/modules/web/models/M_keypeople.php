<?php
Class M_keypeople extends MY_Model
{
    public function get_people($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_people_searchlike($search->term);
            }
        }
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('key_people')->result();
    }

    public function get_people_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_people_searchlike($search->term);
            }
        }
        
        return CI::db()->count_all_results('key_people');
    }
	
    private function get_people_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( key_people.fullname ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
            $like .= $operator." key_people.category ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
			$like .= $operator." key_people.position ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
			$like .= $operator." key_people.email ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
            $like .= $operator." key_people.phone ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )" ;
            
            CI::db()->where($like);
        }
    }
	
    public function save($people)
    {
        $admin = $this->session->userdata('admin');
        $record = $this->get_people_byid($people['id']);
		
		if(count($record) > 0)
        {
        	$people['changedby'] = $admin['username'];
            $people['changedat'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $people['id']);
            CI::db()->update('key_people', $people);
            return $people['id'];
        }
        else
        {            
            $people['createdby'] = $admin['username'];
            $people['createdat'] = date('Y-m-d H:i:s');
                        
            CI::db()->insert('key_people', $people);
            return $people['id'];
        }
    }

    public function delete($id)
    {
        $admin = $this->session->userdata('admin');
               
        CI::db()->where('id', $id);
        CI::db()->delete('key_people');
    }
    	
    public function get_people_byid($id)
    {
        $result = CI::db()->get_where('key_people', array('id'=>$id));
        return $result->row();
    }
	
	public function get_people_bycategory($category)
    {
		$sql = "select 
					a.id,
					a.fullname,
					a.category,
					a.photo,
					a.position,
					a.profile,
					a.email,
					a.phone
				from
					key_people a
				left join variable b
					on b.id = a.category
				where b.value = ?";
		$query = CI::db()->query($sql, array($category));
		//echo $this->db->last_query(); exit;
    	return $query->result();
    }
        
}
