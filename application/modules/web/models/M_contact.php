<?php
Class M_contact extends MY_Model
{
    public function save($contact)
    {
        $admin = $this->session->userdata('admin');
        		          
        $contact['createdby'] = $admin['username'];
        $contact['createdat'] = date('Y-m-d H:i:s');
        
        CI::db()->insert('contact', $contact);
        return CI::db()->insert_id();    
    }

    public function delete($id)
    {
        $admin = $this->session->userdata('admin');
               
        CI::db()->where('id', $id);
        CI::db()->delete('contact');
    }
    	
    public function get_contact_byid($id)
    {
        $result = CI::db()->get_where('contact', array('id'=>$id));
        return $result->row();
    }
    
    public function get_contact_home()
    {
    	$sql = "select
					id,
					ifnull(image,'noimage.jpg') as image,
					photo_by,
					contact_by,
					place,
					contact_date,
					title,
					contact_desc as description
				from contact
				order by id DESC
				limit 6";
    	return $this->db->query($sql)->result();
    }
	
	public function get_contactdetail_byid($id)
    {
		$sql = "SELECT 
					contact.id,
					contact.fullname,
					contact.email,
					contact.phone,
					contact.branch,
					branch.branch_name,
					vsender.description as sender,
					vtype.description as message_type,
					vcategory.description as message_category,
					contact.message 
				FROM contact
				left join branch on branch.id = contact.branch
				left join variable vsender on vsender.value = contact.sender and vsender.variable = 'MESSAGE_SENDER'
				left join variable vtype on vtype.value = contact.message_type and vtype.variable = 'MESSAGE_TYPE'
				left join variable vcategory on vcategory.value = contact.message_category and vcategory.variable = 'MESSAGE_CATEGORY'
				WHERE contact.id  = ?";
				
		return $this->db->query($sql, array($id))->row_array();
    }
    
}
