<?php
Class M_vacancy extends MY_Model
{
    public function get_vacancy($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_vacancy_searchlike($search->term);
            }
        }
        
        if($limit>0)
        {
            \CI::db()->limit($limit, $offset);
        }
        
        if(!empty($sort_by))
        {
            \CI::db()->order_by($sort_by, $sort_order);
        }

        return \CI::db()->get('vacancy')->result();
    }

    public function get_vacancy_count($search=false)
    {
        if ($search)
        {
            if(!empty($search->term))
            {
                $this->get_vacancy_searchlike($search->term);
            }
        }
        
        return CI::db()->count_all_results('vacancy');
    }
	
    private function get_vacancy_searchlike($str)
    {
        //support multiple words
        $term = explode(' ', $str);

        foreach($term as $t)
        {
            $not = '';
            $operator = 'OR';
            if(substr($t,0,1) == '-')
            {
                $not = 'NOT ';
                $operator = 'AND';
                //trim the - sign off
                $t = substr($t,1,strlen($t));
            }

            $like = '';
            $like .= "( division ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
			$like .= $operator." heading ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' " ;
            $like .= $operator." position ".$not."LIKE '%".CI::db()->escape_like_str($t)."%' )" ;
            
            CI::db()->where($like);
        }
    }
	
    public function save($vacancy)
    {
        $admin = $this->session->userdata('admin');
        $record = $this->get_vacancy_byid($vacancy['id']);
		
		if(count($record) > 0)
        {
        	$vacancy['updatedby'] = $admin['username'];
            $vacancy['updatedat'] = date('Y-m-d H:i:s');
            
            CI::db()->where('id', $vacancy['id']);
            CI::db()->update('vacancy', $vacancy);
            return $vacancy['id'];
        }
        else
        {            
            $vacancy['createdby'] = $admin['username'];
            $vacancy['createdat'] = date('Y-m-d H:i:s');
                        
            CI::db()->insert('vacancy', $vacancy);
            return $vacancy['id'];
        }
    }

    public function delete($id)
    {
        $admin = $this->session->userdata('admin');
               
        CI::db()->where('id', $id);
        CI::db()->delete('vacancy');
    }
    	
    public function get_vacancy_byid($id)
    {
        $result = CI::db()->get_where('vacancy', array('id'=>$id));
        return $result->row();
    }
	        
}
