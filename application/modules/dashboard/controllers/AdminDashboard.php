<?php namespace GoCart\Controller;

class AdminDashboard extends Admin {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->view('dashboard');
    }

}
