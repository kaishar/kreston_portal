<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Dashboard</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6">
            	<a href="<?php echo site_url('admin/news/index'); ?>">
                <div class="dashboard-div-wrapper blue">
                    <i  class="fa fa-newspaper-o dashboard-div-icon" ></i>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"></div>
                    </div>
                    <h4>NEWS </h4>
                </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
            	<a href="<?php echo site_url('admin/branch/index'); ?>">
                <div class="dashboard-div-wrapper purple">
                    <i  class="fa fa-building-o dashboard-div-icon" ></i>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"></div>
                    </div>
                    <h4>BRANCH </h4>
                </div>
                </a>
            </div>
			<div class="col-md-3 col-sm-3 col-xs-6">
            	<a href="<?php echo site_url('admin/people/index'); ?>">
                <div class="dashboard-div-wrapper red">
                    <i class="fa fa-bullseye dashboard-div-icon"></i>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"></div>
                    </div>
                    <h4>KEY PEOPLE </h4>
                </div>
                </a>
            </div>
			<div class="col-md-3 col-sm-3 col-xs-6">
            	<a href="<?php echo site_url('admin/affiliation/index'); ?>">
                <div class="dashboard-div-wrapper green">
                    <i class="fa fa-building-o dashboard-div-icon" ></i>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"></div>
                    </div>
                    <h4>AFFILIATION </h4>
                </div>
                </a>
            </div>
			<div class="col-md-3 col-sm-3 col-xs-6">
            	<a href="<?php echo site_url('admin/vacancy/index'); ?>">
                <div class="dashboard-div-wrapper purple">
                    <i class="fa fa-external-link dashboard-div-icon" ></i>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"></div>
                    </div>
                    <h4>CAREER </h4>
                </div>
                </a>
            </div>
        </div>      
	</div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->
