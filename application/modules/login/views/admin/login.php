<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="MobileOptimized" content="360">
    <meta name="description" content="Seino Indomobil Logistic">
    <meta name="designer" content="alphasquad inc.">
    <meta name="programmer" content="materawali">
    <meta name="author" content="Kreston Indonesia">
    <meta name="copyright" content="Kreston Indonesia">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Kreston Indonesia - Content Management System</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="<?=base_url('assets/css/font-awesome.css')?>" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" />
	<link href="<?=base_url('assets/css/background.css')?>" rel="stylesheet" />
	<link rel="shortcut icon" href="<?=theme_img('logoicon/favicon.ico')?>">
    <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>   
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                        //lets have the flashdata overright "$message" if it exists
                        if(CI::session()->flashdata('message'))
                        {
                            $message    = CI::session()->flashdata('message');
                        }

                        if(CI::session()->flashdata('error'))
                        {
                            $error  = CI::session()->flashdata('error');
                        }

                        if(function_exists('validation_errors') && validation_errors() != '')
                        {
                            $error  = validation_errors();
                        }
                    ?>                    
                </div>
            </div>
            <?php
                $attributes = array('class' => 'form-horizontal', 'style' => 'margin-top: 20px;');
                echo form_open('admin/login', $attributes);
            ?>
            <div id="loginForm">
                <div class="row">
                    <div class="col-md-3 hidden-xs hidden-smx">
                    	<img src="<?=base_url('assets/img/backend/kreston-hhes-logo.png')?>">
                    </div>
                    <div class="col-md-5 col-smx-6">
                        <h3 class="loginwelcome txt-blue1">Selamat Datang di CMS<br><span class="txt-grey3">Kreston Indonesia</span></h3>
                        <h2 class="hidden-xs hidden-smx">Silahkan Login Untuk Mengakses</h2>
                        <hr class="divider_boder-white visible-xs">
                    </div>
                    <div class="col-md-4 col-smx-6">
						<div id="js_error_container" class="alert alert-error" style="display:none;">
							<p id="js_error"></p>
						</div>

						<div id="js_note_container" class="alert alert-note" style="display:none;"></div>

						<?php if (!empty($message)): ?>
								<div class="alert alert-success" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<?php echo $message; ?>
								</div>
						<?php endif; ?>

						<?php if (!empty($error)): ?>
								<div class="alert alert-danger" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<?php echo $error; ?>
								</div>
						<?php endif; ?>
                        <div class="form-group">
                            <label for="inputUsername" class="col-sm-4 hidden-xs hidden-smx txt-grey2">Username</label>
                            <div class="col-sm-8">
                                <input type="text" name="username" class="form-control" id="inputUsername" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-4 hidden-xs hidden-smx txt-grey2">Password</label>
                            <div class="col-sm-8">
                                <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-user"></span> &nbsp;Login </a>&nbsp;</button>
                            </div>
                        </div>  
                        <input type="hidden" value="<?php echo $redirect; ?>" name="redirect" />
                        <input type="hidden" value="submitted" name="submitted" />
    
                        <?php echo  form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <!-- BACKGROUND-->
    <div class="s-background animated fadeIn bg-login">
    	<div class="tractordisplay wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms"></div>
    </div>
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
</body>
</html>