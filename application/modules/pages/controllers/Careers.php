<?php namespace GoCart\Controller;

class Careers extends Front{
	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=6)
    {
		\CI::load()->helper('date');
		\CI::load()->model(array('M_vacancy'));
		
		//key people
		$data['vacancy'] = \CI::M_vacancy()->get_vacancy();
		$this->view('career', $data);
    }
    
}
