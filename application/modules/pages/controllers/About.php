<?php namespace GoCart\Controller;

class About extends Front{
	
    public function index()
    {
		\CI::load()->helper('date');
		\CI::load()->model(array('M_keypeople', 'Variable', 'M_affiliation'));
		
		//key people
		$category = \CI::Variable()->get_variable_orderby('PEOPLE', 'orderby', 'asc');
		$data['category'] = $category;
		foreach($category as $rows):			
			$data[$rows->value] = \CI::M_keypeople()->get_people_bycategory($rows->value); 	
		endforeach;
		
		//affiliation
		$data['affiliation'] = \CI::M_affiliation()->get_affiliation();
		
		//load the view
		$this->view('about', $data);
    }
    
}

/* End of file About.php */
/* Location: ./GoCart/controllers/About.php */