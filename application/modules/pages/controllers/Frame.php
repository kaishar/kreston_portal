<?php namespace GoCart\Controller;

class Frame extends Front {

	public function __construct()
    {		
        parent::__construct();

        \CI::load()->model(array('M_news', 'M_keypeople', 'M_affiliation', 'M_vacancy'));
        \CI::load()->helper(array('date', 'url'));
    }
	
    public function news()
    {
    	$id = \CI::uri()->segment(4);
    	    
    	$data['news'] = \CI::M_news()->get_news_byid($id, \CI::lang()->lang());
    	    	
    	$this->partial('modal_news', $data);
    }
	
	public function people()
    {
		$id = \CI::uri()->segment(4);
    	    
    	$data['people'] = \CI::M_keypeople()->get_people_byid($id);
    	    	
    	$this->partial('modal_keypeople', $data);
    }
	
	public function affiliation()
    {
		$id = \CI::uri()->segment(4);
    	    
    	$data['affiliation'] = \CI::M_affiliation()->get_affiliation_byid($id);
    	    	
    	$this->partial('modal_affiliation', $data);
    }
	
	public function vacancy()
    {
		$id = \CI::uri()->segment(4);
    	    
    	$data['vacancy'] = \CI::M_vacancy()->get_vacancy_byid($id);
    	    	
    	$this->partial('modal_vacancy', $data);
    }
	
}