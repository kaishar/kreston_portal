<?php namespace GoCart\Controller;

class Newsroom extends Front{
	
    public function index($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=6)
    {
		\CI::load()->helper('date','language','string');
		\CI::load()->model(array('M_news'));
		\CI::lang()->load(array('common', 'menu', 'news'));
						
		$data['page_title'] = 'News';
		
		$data['news'] = \CI::M_news()->get_news(false, $sort_by, $sort_order, $rows, $page);
		$data['total'] = \CI::M_news()->get_news_count(false);
		
		\CI::load()->library('pagination');
		
		$config['base_url'] = site_url('newsroom/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $rows;
		$config['uri_segment'] = 6;
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['full_tag_open'] = '<div class="text-center"><ul class="pager">';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		\CI::pagination()->initialize($config);
		
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		
		$this->view('newsroom', $data);
    }
    
}

/* End of file About.php */
/* Location: ./GoCart/controllers/About.php */