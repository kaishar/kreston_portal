<?php namespace GoCart\Controller;

class Page extends Front{
	private $siteKey = '6LcHTyQUAAAAAJN7NnzKmlNgI195y5u4vwi5rpId';
	private $secret = '6LcHTyQUAAAAAKmla1bQ1PQBvUHup23tR-WWAhI_';
	
    public function homepage()
    {
        \CI::load()->library('form_validation');
        \CI::load()->helper('date','language');
		\CI::load()->model(array('M_branch', 'Variable'));
		\CI::lang()->load(array('common', 'menu', 'banner'));
        
        //do we have a homepage view?
        if(file_exists(FCPATH.'themes/'.config_item('theme').'/views/homepage.php'))
        {                       
			$data['siteKey'] = $this->siteKey;
			$data['secret'] = $this->secret;
            
            $data['lang'] = \CI::lang()->lang();
			
			//populate dropdown branch
			$list = \CI::M_branch()->get_indonesia_branch();
            $list_branch[null] = "choose branch";
            foreach($list as $rows)
            {
                $list_branch[$rows->pic_email] = $rows->branch_name;
            }
            $data['list_branch'] = $list_branch;
            
			//populate dropdown sender
			$list = \CI::Variable()->get_variable('MESSAGE_SENDER');			
            $list_sender[null] = "Choose";
            foreach($list as $rows)
            {
                $list_sender[$rows->id] = $rows->description;
            }
            $data['list_sender'] = $list_sender;
			
			//populate dropdown sender
			$list = \CI::Variable()->get_variable('MESSAGE_TYPE');			
            $list_type[null] = "Choose";
            foreach($list as $rows)
            {
                $list_type[$rows->id] = $rows->description;
            }
            $data['list_type'] = $list_type;
			
			//populate dropdown sender
			$list = \CI::Variable()->get_variable('MESSAGE_CATEGORY');			
            $list_category[null] = "Choose";
            foreach($list as $rows)
            {
                $list_category[$rows->id] = $rows->description;
            }
            $data['list_category'] = $list_category;
			
            $data['locations'] = $this->locations();
			
            //bagian news
			/*
            $news = \CI::M_news()->get_news_home(\CI::lang()->lang(), 5, 0);
            
            $data['news_headline'] = array_slice($news, 0, 1);
            $data['newsscroll'] = array_slice($news, 1);
			*/
			
			
            $this->view('homepage',$data);
            return;
        }else
        {
            //if we don't have a homepage view, check for a registered homepage
            if(config_item('homepage'))
            {
                if(isset($this->pages['all'][config_item('homepage')]))
                {
                    //we have a registered homepage and it's active
                    $this->index($this->pages['all'][config_item('homepage')]->slug, false);
                    return;
                }
            }
        }

        // wow, we do not have a registered homepage and we do not have a homepage.php
        // let's give them something default to look at.
        $this->view('homepage_fallback');
    }

    public function show404()
    {
        $this->view('404');
    }
    
    public function locations()
    {
        $str = '[';
        $zoom = 17;
        $icon = theme_img('logoicon/map-branch.png');
        $animation = 'google.maps.Animation.DROP';

        $branch = \CI::M_branch()->get_locations();
        foreach($branch as $rows):
            $html = '';
			$html .= "<h5 class='text-uppercase txt-white bg-blue1'>".$rows->branch_name."</h5>";
			$html .= "<div class='row'><div class='col-xs-3'>";
			$html .= "<img class='img-responsive' src='".theme_upload('branch/'.$rows->photo)."' style='width: 90px; height: 90px;'>";
			$html .= "</div><div class='col-xs-7'>";
            $html .= "<h4>Address: <strong>".$rows->address1.", ".$rows->city."</strong></h4>";
            $html .= "<h4>Phone: ".$rows->phone."</h4>";
			$html .= "<h4>Fax: ".$rows->fax."</h4>";
            $html .= "<h4>Email : ";
            $html .= "<a href='mailto: ".$rows->pic_email."'>&nbsp;".$rows->pic_email."</a></h4>";
			$html .= "</div></div>";
                                    
            $str = $str . '{"lat":'.(float)$rows->latitude.',"lon":'.(float)$rows->longitude.',"title":"'.$rows->branch_name.'","html":"'.$html.'","zoom":'.$zoom.',"icon":"'.$icon.'","animation":"'.$animation.'"},';
        endforeach;
        $str = $str . ']';
        //echo $str; exit;
        return $str;
    }
                
    public function contactus()
    {
		$recaptcha = new \ReCaptcha\ReCaptcha($this->secret);
				
    	\CI::load()->helper(array('email','date'));
    	\CI::load()->model(array('M_contact'));
    	
    	$data = array();
    	$data['error_string'] = array();
    	$data['inputerror'] = array();
    	$data['status'] = TRUE;
    
    	$fullname = \CI::input()->post('fullname');
    	$email = \CI::input()->post('email');
    	$phone = \CI::input()->post('phone');
    	$branch = \CI::input()->post('branch');
    	$sender = \CI::input()->post('sender');
    	$message_type = \CI::input()->post('messagetype');
    	$message_category = \CI::input()->post('messagecategory');
    	$message = \CI::input()->post('message');
		
		$recaptcha_respon = \CI::input()->post('g-recaptcha-response');
		//var_dump($recaptcha_respon); exit;
        $response = $recaptcha->verify($recaptcha_respon);
    	
    	//$msg = '';
    
    	if ($fullname == '')
    	{
			if(\CI::lang()->lang() == 'en') {
				$data['inputerror'][] = 'c_fullname';
				$data['error_string'][] = 'Full Name required';
				$data['status'] = FALSE;
				//$msg .= '<p>Please input your Full Name</p>';
			}else {
				//$msg .= '<p>Masukan Nama Lengkap anda</p>';
				$data['inputerror'][] = 'c_fullname';
				$data['error_string'][] = 'Masukan nama lengkap anda';
				$data['status'] = FALSE;
			}
		}
		    
    	if (!valid_email($email))
    	{
			if(\CI::lang()->lang() == 'en') {
				//$msg .= '<p>Email is invalid</p>';
				$data['inputerror'][] = 'c_email';
				$data['error_string'][] = 'Email is invalid';
				$data['status'] = FALSE;
			}else {
				//$msg .= '<p>Alamat Email tidak valid</p>';
				$data['inputerror'][] = 'c_email';
				$data['error_string'][] = 'Alamat email tidak valid';
				$data['status'] = FALSE;
			}
    	}
    	
    	if ($phone == '')
    	{
    		if(\CI::lang()->lang() == 'en') {
    			//$msg .= '<p>Please input your Phone Number</p>';
    			$data['inputerror'][] = 'c_phone';
    			$data['error_string'][] = 'Phone number required';
    			$data['status'] = FALSE;
    		}else {
    			//$msg .= '<p>Masukan Nomor Telepon anda</p>';
    			$data['inputerror'][] = 'c_phone';
    			$data['error_string'][] = 'Masukan nomor telepon anda';
    			$data['status'] = FALSE;
    		}
    	}
    	
    	if ($message == '')
    	{
    		if(\CI::lang()->lang() == 'en') {
    			//$msg .= '<p>Please input your message</p>';
    			$data['inputerror'][] = 'c_message';
    			$data['error_string'][] = 'Message is required';
    			$data['status'] = FALSE;
    		}else {
    			//$msg .= '<p>Masukan pesan atau saran anda</p>';
    			$data['inputerror'][] = 'c_message';
    			$data['error_string'][] = 'Masukan pesan atau saran anda';
    			$data['status'] = FALSE;
    		}
    	}
		
		if(!$response->isSuccess())
		{
			$data['inputerror'][] = 'g-recaptcha';
			$data['error_string'][] = $response->getErrorCodes()[0];
			$data['status'] = FALSE;
			
			//$msg .= 'Captcha ';
			/*
			if(\CI::lang()->lang() == 'en') {
				//$msg .= $response->getErrorCodes()[0];
				//$msg .= '<p>Captcha code invalid</p>';
				$data['inputerror'][] = 'g-recaptcha';
				$data['error_string'][] = 'Captcha code invalid';
				$data['status'] = FALSE;
			}else {
				//$msg .= $response->getErrorCodes()[0];
				//$msg .= '<p>Kode Captcha tidak valid</p>';
				$data['inputerror'][] = 'g-recaptcha';
				$data['error_string'][] = 'Kode captcha tidak valid';
				$data['status'] = FALSE;
			}
			*/
		}
    
		/*
    	if($msg != '')
    	{
    		echo $msg;
    		exit;
    	}
    	*/
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
		    
    	$save=array(
    			'fullname'      => $fullname,
    			'email'      	=> $email,
    			'phone'         => $phone,
    			'branch'        => $branch,
    			'sender'        => $sender,
    			'message_type'  => $message_type,
    			'message_category' => $message_category,
    			'message'       => $message
    	);
    
    	$id = \CI::M_contact()->save($save);
    
    	if($id)
    	{
    		\GoCart\Emails::Contactus($id);
    		//echo 'Thanks for your question, we will contact you as soon as posible.';
    		if(\CI::lang()->lang() == 'en') {
    			$data['message'][] = 'Thank You for your question, we will response soon as posible';
    		}else {
    			$data['message'][] = 'Terimakasih atas pertanyaan anda, kami akan meresponnya sesegera mungkin';
    		}
    	}else
    	{
    		//echo 'Email already registered, please use another email.';
    		$data['inputerror'][] = 'message_contactus';
			$data['error_string'][] = 'Failed...';
			$data['status'] = FALSE;
    	}
    	echo json_encode($data);
    }  
    
    public function search($sort_by='id',$sort_order='desc', $code=0, $page=0, $rows=5)
    {
    	\CI::load()->model(array('M_news', 'Search'));
		\CI::load()->helper(array('string'));
    
    	$data['page_title'] = 'Pencarian ';
    	$data['code'] = $code;
    	$term = false;
    
    	$post = \CI::input()->post(null, false);
    	if($post)
    	{
    		//if the term is in post, save it to the db and give me a reference
    		$term = json_encode($post);
    		$code = \CI::Search()->recordTerm($term);
    		$data['code'] = $code;
    		//reset the term to an object for use
    		$term   = (object)$post;
    	}
    	elseif ($code)
    	{
    		$term = \CI::Search()->getTerm($code);
    		$term = json_decode($term);
    	}
    
    	$data['term'] = $term;
    	$data['news'] = \CI::M_news()->get_news($term, $sort_by, $sort_order, $rows, $page);
    	$data['total'] = \CI::M_news()->get_news_count($term);
    
    	\CI::load()->library('pagination');
    
    	$config['base_url'] = site_url('search/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
    	$config['total_rows'] = $data['total'];
    	$config['per_page'] = $rows;
    	$config['uri_segment'] = 6;
    	$config['first_link'] = 'First';
    	$config['first_tag_open'] = '<li>';
    	$config['first_tag_close'] = '</li>';
    	$config['last_link'] = 'Last';
    	$config['last_tag_open'] = '<li>';
    	$config['last_tag_close'] = '</li>';
    
    	$config['full_tag_open'] = '<div class="text-center"><ul class="pager">';
    	$config['full_tag_close'] = '</ul></div>';
    	$config['cur_tag_open'] = '<li class="active"><a href="#">';
    	$config['cur_tag_close'] = '</a></li>';
    
    	$config['num_tag_open'] = '<li>';
    	$config['num_tag_close'] = '</li>';
    
    	$config['prev_link'] = '&larr; Previous';
    	$config['prev_tag_open'] = '<li>';
    	$config['prev_tag_close'] = '</li>';
    
    	$config['next_link'] = 'Next &rarr;';
    	$config['next_tag_open'] = '<li>';
    	$config['next_tag_close'] = '</li>';
    
    	\CI::pagination()->initialize($config);
    
    	$data['sort_by'] = $sort_by;
    	$data['sort_order'] = $sort_order;
    
    	$this->partial('detail_search', $data);
    }
    
}

/* End of file Page.php */
/* Location: ./GoCart/controllers/Page.php */