<?php namespace GoCart\Controller;

class Download extends Front 
{
	public function __construct()
	{
		parent::__construct();
		\CI::load()->helper(array('url','download'));
	}
	
	public function product()
	{
		$filename = \CI::uri()->segment(3);
		force_download('uploads/brochure/',$filename);
	}	
}