<?php

$routes[] = ['GET|POST', '/[en|id]/frame/news/[i:id]?', 'GoCart\Controller\Frame#news'];
$routes[] = ['GET|POST', '/[en|id]/page/contactus', 'GoCart\Controller\Page#contactus'];
$routes[] = ['GET|POST', '/[en|id]/search', 'GoCart\Controller\Page#search'];
$routes[] = ['GET|POST', '/[en|id]/search/[:orderBy]/[:orderDir]/[:code]/[i:page]?', 'GoCart\Controller\Page#search'];
$routes[] = ['GET|POST', '/[en|id]/about-us', 'GoCart\Controller\About#index'];
$routes[] = ['GET|POST', '/[en|id]/newsroom', 'GoCart\Controller\Newsroom#index'];
$routes[] = ['GET|POST', '/[en|id]/newsroom/[:orderBy]/[:orderDir]/[:code]/[i:page]?', 'GoCart\Controller\Newsroom#index'];
$routes[] = ['GET|POST', '/[en|id]/contact-us', 'GoCart\Controller\Contact#index'];
$routes[] = ['GET|POST', '/[en|id]/career', 'GoCart\Controller\Careers#index'];
$routes[] = ['GET|POST', '/[en|id]/frame/people/[i:id]?', 'GoCart\Controller\Frame#people'];
$routes[] = ['GET|POST', '/[en|id]/frame/affiliation/[i:id]?', 'GoCart\Controller\Frame#affiliation'];
$routes[] = ['GET|POST', '/[en|id]/frame/career/[i:id]?', 'GoCart\Controller\Frame#vacancy'];
