<!-- ===== modal vacancy ===== -->
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title"><?=$vacancy->position?></h4>
</div>
<div class="modal-body">
	<div class="descriptionmodal">
		<p><?=$vacancy->heading?></p>
	</div>
	<hr class="divider_boder-white">
	<h5 class="modal-title">Requirements</h5>
	<p><?=$vacancy->requirements?></p>
	<h5 class="modal-title">Responsibilities</h5>
	<p><?=$vacancy->responsibilities?></p>
	<?php if(isset($vacancy->description)){ ?>
	<h5 class="modal-title">Description</h5>
	<p><?=$vacancy->description?></p>
	<?php } ?>
</div>
<div class="modal-footer">
	<div class="btn-group">
		<button type="button" class="btn btn-alpha_whiteline" data-dismiss="modal">Close</button>
	</div>
</div>
<!-- ===== end modal vacancy ===== -->