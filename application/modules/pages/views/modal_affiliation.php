<!-- ===== modal affiliation ===== -->
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title"><?=$affiliation->company_name?></h4>
</div>
<div class="modal-body">
	<div class="img-display">
		<img class="img-responsive" src="<?=theme_upload('affiliation/'.$affiliation->image)?>" alt="<?=$affiliation->company_name?>">
	</div>
	<hr class="divider_boder-white">
	<div class="descriptionmodal">
		<p><?=$affiliation->descriptions?></p>
	</div>
	<hr class="divider_boder-white">
	<address>
		<strong><?=$affiliation->address1?></strong><br>
		<?=$affiliation->address2?><br>
		<?=$affiliation->address3?><br>
		Email :
		<a href="mailto:<?=$affiliation->email?>" class="btn btn-link"><?=$affiliation->email?></a>
	</address>
	<address>
		Phone :
		<a href="tel:<?=$affiliation->phone?>" class="btn btn-link"><?=$affiliation->phone?></a><br>
	</address>
</div>
<div class="modal-footer">
	<div class="btn-group">
		<a href="<?=$affiliation->website?>" target="_blank" class="btn btn-alpha_blue">Go to Website</a>
		<button type="button" class="btn btn-alpha_whiteline" data-dismiss="modal">Close</button>
	</div>
</div>
<!-- ===== end modal affiliation ===== -->