<!-- ===== modal keypeople ===== -->	
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title"><?=$people->fullname?></h4>
	<h5 class="txt-grey2"><?=$people->position?></h5>
</div>
<div class="modal-body">
	<div class="descriptionmodal">
		<p><?=$people->profile?></p>
	</div>
	<hr class="divider_boder-white">
	<address>
		Email : 
		<a href="mailto:<?=$people->email?>" class="btn btn-link"><?=$people->email?></a>
	</address>
	<address>
		Phone :
		<a href="tel:<?=$people->phone?>" class="btn btn-link"><?=$people->phone?></a><br>
	</address>
</div>
<div class="modal-footer">
	<div class="btn-group">
	<button type="button" class="btn btn-alpha_whiteline" data-dismiss="modal">Close</button>
	</div>
</div>			
<!-- ===== end modal keypeople ===== -->            