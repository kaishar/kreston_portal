

<section class="generalsection">
    <div class="container">
    	<div class="row">
            <div class="page-header text-center">
                <h1 class="" style="margin:70px auto;">404 The page you are looking for could not be found!</h1>
                <div class="btn-group btn-group-sm" role="group">
                    <a href="<?=base_url()?>" class="btn btn-primary">BACK TO MAIN PAGE</a>
                </div>
            </div>
        </div>
    </div>
</section>