<!-- ===== Modal events ===== -->
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title modal-sharing text-center">Events Detail</h4>
</div>
<?php foreach ($news as $rows): ?>
<div class="modal-body">
	<h6><?php echo $rows->place.', '.$rows->news_date; ?></h6>
	<h1><?php echo $rows->title; ?></h1>
	<div class="img-display">
		<img class="img-responsive" src="<?=theme_upload('news/'.$rows->image)?>" alt="img">
	</div>
	<div class="articlecredits">
		Photos: <mark><?=$rows->photo_by?></mark> | Teks: <mark><?=$rows->news_by?></mark>
	</div>
	<div class="dropcap txt-news"><?=$rows->news_desc?></div>
</div>
<?php endforeach; ?>
<div class="modal-footer">
	<div class="btn-group">
		<button type="button" class="btn btn-alpha_whiteline" data-dismiss="modal">Close</button>
	</div>
</div>
<!-- ===== End Modal events ===== -->              